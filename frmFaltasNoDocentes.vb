﻿Public Class frmFaltasNoDocentes
    Dim elUsuario As New clsUser
    Dim elFuncionario As New clsFuncionarioNoDocente
    Dim frmFaltasNoDocentes As frmFaltasNoDocentes

    Dim seleccionEsVacia As Boolean = False

    Public Function seleccionVacia() As Boolean
        Dim devolver As Boolean = False
        frmFaltasNoDocentes = Me

        If lsvFaltas.FocusedItem Is Nothing Then
            MsgBox("No se ha seleccionado ninguna falta.")
            devolver = True
        End If

        Return devolver
    End Function

    Public Sub New(elU As clsUser, elF As clsFuncionarioNoDocente)
        'TODO: Complete member initialization 
        elUsuario = elU
        elFuncionario = elF
        frmFaltasNoDocentes = Me
        InitializeComponent()
    End Sub

    Public Sub listar()
        Dim unaC As New clsControladora
        Dim colRegistro As New List(Of clsFaltaNoDocente)
        colRegistro = unaC.listarFaltasNoDocentes(elFuncionario.ci, txtInicial.Text, txtFinal.Text)

        Dim lsi As New ListViewItem
        lsvFaltas.Items.Clear()

        For Each unaF In colRegistro
            lsi = lsvFaltas.Items.Add(unaF.fecha)
            lsi.SubItems.Add(unaF.descripcion)
            lsi.SubItems.Add(unaF.horaInicio)
            lsi.SubItems.Add(unaF.horaFinal)
            lsi.SubItems.Add(unaF.excepcion.idExcepcion)
        Next
    End Sub

    Private Sub frmFaltasNoDocentes_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        lblUser.Text = elFuncionario.nombre & " " & elFuncionario.apellido

        ' Le da el primer y ultimo dia del mes a los txt de fecha
        Dim FechaActual As Date = Now
        Dim DiasEnMes As Integer = Date.DaysInMonth(FechaActual.Year, FechaActual.Month)
        Dim UltimoDiaEnMes As Date = New Date(FechaActual.Year, FechaActual.Month, DiasEnMes)
        Dim PrimerDiaEnMes As Date = New Date(FechaActual.Year, FechaActual.Month, 1)

        txtInicial.Text = PrimerDiaEnMes
        txtFinal.Text = UltimoDiaEnMes

        listar()
    End Sub

    Private Sub btnAgregar_Click(sender As Object, e As EventArgs) Handles btnAgregar.Click
        Dim unFrm As New frmAltaFaltaNoDocente(elFuncionario)
        unFrm.Show()
    End Sub

    Private Sub btnSalir_Click(sender As Object, e As EventArgs) Handles btnSalir.Click
        Me.Close()
    End Sub

    ' Cuando las fecha cambian automaticamente se vuelve a listar todo
    Private Sub txtInicial_TextChanged(sender As Object, e As EventArgs) Handles txtInicial.TextChanged
        listar()
    End Sub

    Private Sub txtFinal_TextChanged(sender As Object, e As EventArgs) Handles txtFinal.TextChanged
        listar()
    End Sub

    Private Sub btnModificar_Click(sender As Object, e As EventArgs) Handles btnModificar.Click
        seleccionEsVacia = seleccionVacia()

        If seleccionEsVacia = False Then
            Dim unaExepcion As New clsExcepcion
            If lsvFaltas.FocusedItem.SubItems(4).Text = "" Then

                unaExepcion.idExcepcion = lsvFaltas.FocusedItem.SubItems(4).Text
            End If
            Dim unaFal As New clsFaltaNoDocente(vbEmpty, lsvFaltas.FocusedItem.SubItems(0).Text, lsvFaltas.FocusedItem.SubItems(2).Text, lsvFaltas.FocusedItem.SubItems(3).Text, lsvFaltas.FocusedItem.SubItems(1).Text, elFuncionario, unaExepcion)
            Dim unFrm As New frmModificarFaltaNoDocente(unaFal)
            unFrm.Show()
        End If
    End Sub

    Private Sub btnEliminar_Click(sender As Object, e As EventArgs) Handles btnEliminar.Click
        seleccionEsVacia = seleccionVacia()

        If seleccionEsVacia = False Then
            Dim unaExepcion As New clsExcepcion
            If lsvFaltas.FocusedItem.SubItems(4).Text = "" Then

                unaExepcion.idExcepcion = lsvFaltas.FocusedItem.SubItems(4).Text
            End If
            Dim unaFal As New clsFaltaNoDocente(vbEmpty, lsvFaltas.FocusedItem.SubItems(0).Text, lsvFaltas.FocusedItem.SubItems(2).Text, lsvFaltas.FocusedItem.SubItems(3).Text, lsvFaltas.FocusedItem.SubItems(1).Text, elFuncionario, unaExepcion)
            Dim unFrm As New frmBajaFaltaNoDocente(unaFal, frmFaltasNoDocentes)
            unFrm.Show()
        End If
    End Sub
End Class