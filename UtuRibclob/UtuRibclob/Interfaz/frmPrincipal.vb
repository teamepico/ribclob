﻿Public Class frmPrincipal

    Private elUsuario As clsUser

    Public Sub New(elU As clsUser)
        ' TODO: Complete member initialization 
        elUsuario = elU
        InitializeComponent()
    End Sub

    ' Funcion para que el programa entero se cierre cuando se cierra frmPrincipal
    Private Sub frmPrincipal_FormClosing(sender As Object, e As FormClosingEventArgs) Handles Me.FormClosing
        Application.Exit()
    End Sub

    Private Sub frmPrincipal_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        lblUsuario.Text = "Bienvenido, " & elUsuario.nombre
        RegistroToolStripMenuItem.Visible = False
        Me.MinimumSize = New Size(908, 592)

        If elUsuario.tipo = "adscripto" Then
            UsuariosToolStripMenuItem.Enabled = False
        ElseIf elUsuario.tipo = "desarrollador" Then
            RegistroToolStripMenuItem.Visible = True
        ElseIf elUsuario.tipo = "director" Then

        ElseIf elUsuario.tipo = "administrador" Then

        End If
    End Sub

    Private Sub DocentesToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles DocentesToolStripMenuItem.Click
        Dim formularioDocentes As New frmFuncionariosDocentes(elUsuario)
        formularioDocentes.Show()
    End Sub

    Private Sub lblUsuario_DoubleClick(sender As Object, e As EventArgs) Handles lblUsuario.Click
        Dim easterEgg As New frmEasterEgg
        easterEgg.Show()
    End Sub

    Private Sub UsuariosToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles UsuariosToolStripMenuItem.Click
        Dim formularioUsuarios As New frmUsuarios(elUsuario)
        formularioUsuarios.Show()
    End Sub

    Private Sub InternosToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles InternosToolStripMenuItem.Click
        Dim formularioInternos As New frmInternos(elUsuario)
        formularioInternos.Show()
    End Sub

    Private Sub ListadoToolStripMenuItem_Click(sender As Object, e As EventArgs)
        Dim formularioSalon As New frmSalones(elUsuario)
        formularioSalon.Show()
    End Sub

    Private Sub NoDocentesToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles NoDocentesToolStripMenuItem.Click
        Dim frmNoDocente As New frmFuncionariosNoDocentes(elUsuario)
        frmNoDocente.Show()
    End Sub

    Private Sub SalonesToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles SalonesToolStripMenuItem.Click
        Dim frmSalones As New frmSalones(elUsuario)
        frmSalones.Show()
    End Sub

    Private Sub GruposToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles GruposToolStripMenuItem.Click
        Dim unFrmGrupo As New frmGrupos(elUsuario)
        unFrmGrupo.Show()
    End Sub

    Private Sub MateriasToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles MateriasToolStripMenuItem.Click
        Dim unFrmMateria As New frmMaterias(elUsuario)
        unFrmMateria.Show()
    End Sub

    Private Sub ExcepcionesToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles ExcepcionesToolStripMenuItem.Click
        Dim unFrmExc As New frmExcepciones(elUsuario)
        unFrmExc.Show()
    End Sub

    ' Funcion para que el contenido de la ventana se adapte cuando se maximiza o se cambia el tamaño
    Private Sub frmPrincipal_Resize(sender As Object, e As EventArgs) Handles Me.Resize
        Dim y = Me.Size.Height
        y = y - 140
        Dim xForm = Me.Size.Width
        Dim xLabel = lblUsuario.Size.Width
        xForm = xForm / 2
        xLabel = xLabel / 2
        Dim xPosta = xForm - xLabel
        'MsgBox(xPosta & ", " & y)

        Me.lblUsuario.Location = New System.Drawing.Point(xPosta, y)
    End Sub

    Private Sub RegistroToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles RegistroToolStripMenuItem.Click
        Dim unFrm As New frmRegistro
        unFrm.Show()
    End Sub

    Private Sub HorariosToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles HorariosToolStripMenuItem.Click
        Dim unForm As New frmHorarioDocente(elUsuario)
        unForm.Show()
    End Sub

    Private Sub DiarioToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles DiarioToolStripMenuItem.Click
        Dim unForm As New frmReporte
        unForm.Show()
    End Sub

    Private Sub AdelantosToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles AdelantosToolStripMenuItem.Click
        Dim unFrm As New frmAdelantoRecuperacion(elUsuario)
        unFrm.Show()
    End Sub

    Private Sub VerReporteMensualToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles VerReporteMensualToolStripMenuItem.Click
        Dim unFrm As New frmReporteListadoDocente(elUsuario)
        unFrm.Show()
    End Sub

    Private Sub VerReporteDiarioToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles VerReporteDiarioToolStripMenuItem.Click
        Dim unFrm As New frmReporteListadoNoDocente(elUsuario)
        unFrm.Show()
    End Sub

    Private Sub PictureBox1_Click(sender As Object, e As EventArgs)

    End Sub
End Class