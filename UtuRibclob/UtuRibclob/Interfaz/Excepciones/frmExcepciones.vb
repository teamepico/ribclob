﻿Public Class frmExcepciones
    Dim elUsuario As New clsUser
    Private frmExcepciones As frmExcepciones

    Public Sub New(elU As clsUser)
        ' TODO: Complete member initialization 
        elUsuario = elU
        frmExcepciones = Me
        InitializeComponent()
    End Sub

    Private Sub Listar()
        Dim unaC As New clsControladora
        Dim colExc As List(Of clsExcepcion)

        colExc = unaC.listarExcepciones()

        Dim lsi As New ListViewItem
        lsvExcepciones.Items.Clear()

        For Each unaEx In colExc
            lsi = lsvExcepciones.Items.Add(unaEx.tipoExcepcion)
            lsi.SubItems.Add(unaEx.comienzoExcepcion)
            lsi.SubItems.Add(unaEx.finalExcepcion)
        Next
    End Sub

    Public Function seleccionVacia() As Boolean
        Dim devolver As Boolean = False

        If lsvExcepciones.FocusedItem Is Nothing Then
            MsgBox("No se ha seleccionado ninguna excepción.")
            devolver = True
        End If

        Return devolver
    End Function

    Private Sub frmExcepciones_Activated(sender As Object, e As EventArgs) Handles Me.Activated
        Listar()
    End Sub

    Private Sub frmExpeciones_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Me.MinimumSize = New Size(785, 559)
        Listar()
    End Sub

    Private Sub btnAgregar_Click(sender As Object, e As EventArgs) Handles btnAgregar.Click
        Dim unFrm As New frmAltaExcepciones(elUsuario, frmExcepciones)
        unFrm.Show()
    End Sub

    Private Sub lsvFuncionarios_SelectedIndexChanged(sender As Object, e As EventArgs) Handles lsvExcepciones.SelectedIndexChanged
        Dim datoCi As String = lsvExcepciones.FocusedItem.SubItems(2).Text
    End Sub

    Private Sub btnSalir_Click(sender As Object, e As EventArgs) Handles btnSalir.Click
        Me.Close()
    End Sub

    Private Sub btnBorrar_Click(sender As Object, e As EventArgs) Handles btnBorrar.Click
        Dim seleccionEsVacia As Boolean
        seleccionEsVacia = seleccionVacia()

        If seleccionEsVacia = False Then
            Dim unaExc As New clsExcepcion(vbEmpty, lsvExcepciones.FocusedItem.SubItems(0).Text, lsvExcepciones.FocusedItem.SubItems(1).Text, lsvExcepciones.FocusedItem.SubItems(2).Text)
            Dim unFormEliminar As New frmBajaExcepciones(unaExc, elUsuario, frmExcepciones)
            unFormEliminar.Show()
        End If
    End Sub

    Private Sub btnListar_Click(sender As Object, e As EventArgs)
        Listar()
    End Sub

    Private Sub btnModificar_Click(sender As Object, e As EventArgs) Handles btnModificar.Click
        Dim seleccionEsVacia As Boolean
        seleccionEsVacia = seleccionVacia()

        If seleccionEsVacia = False Then
            Dim unaExc As New clsExcepcion(vbEmpty, lsvExcepciones.FocusedItem.SubItems(0).Text, lsvExcepciones.FocusedItem.SubItems(1).Text, lsvExcepciones.FocusedItem.SubItems(2).Text)
            Dim unForm As New frmModificarExcepciones(unaExc, elUsuario, frmExcepciones)
            unForm.Show()
        End If
    End Sub

    Private Sub txtBuscar_TextChanged(sender As Object, e As EventArgs) Handles txtBuscar.TextChanged
        Dim unaC As New clsControladora
        Dim colExc As List(Of clsExcepcion)

        colExc = unaC.buscarExcepciones(txtBuscar.Text)

        Dim lsi As New ListViewItem
        lsvExcepciones.Items.Clear()

        For Each unaEx In colExc
            lsi = lsvExcepciones.Items.Add(unaEx.tipoExcepcion)
            lsi.SubItems.Add(unaEx.comienzoExcepcion)
            lsi.SubItems.Add(unaEx.finalExcepcion)
        Next
    End Sub

    Private Sub frmExcepciones_Resize(sender As Object, e As EventArgs) Handles Me.Resize
        Dim lsv_x = Me.Size.Width
        Dim lsv_y = Me.Size.Height
        lsv_x = lsv_x - 40
        lsv_y = lsv_y - 129
        Me.lsvExcepciones.Size = New System.Drawing.Size(lsv_x, lsv_y)

        Dim btnAgregar_x = btnAgregar.Location.X
        Dim btnModificar_x = btnModificar.Location.X
        Dim btnEliminar_x = btnBorrar.Location.X
        Dim btnSalir_x = Me.Size.Width
        Dim SalirSize = btnSalir.Size.Width / 2

        Dim btn_y = Me.Size.Height
        btn_y = btn_y - 81

        btnSalir_x = btnSalir_x - 81 - SalirSize

        Me.btnAgregar.Location = New System.Drawing.Point(btnAgregar_x, btn_y)
        Me.btnModificar.Location = New System.Drawing.Point(btnModificar_x, btn_y)
        Me.btnBorrar.Location = New System.Drawing.Point(btnEliminar_x, btn_y)
        Me.btnSalir.Location = New System.Drawing.Point(btnSalir_x, btn_y)
    End Sub
End Class