﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmAltaExcepciones
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.txtTipo = New System.Windows.Forms.TextBox()
        Me.lblNombre = New System.Windows.Forms.Label()
        Me.btnAgregar = New System.Windows.Forms.Button()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.txtInicio = New System.Windows.Forms.DateTimePicker()
        Me.txtFinal = New System.Windows.Forms.DateTimePicker()
        Me.imgTick = New System.Windows.Forms.PictureBox()
        Me.Timer1 = New System.Windows.Forms.Timer(Me.components)
        CType(Me.imgTick, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'txtTipo
        '
        Me.txtTipo.Location = New System.Drawing.Point(171, 33)
        Me.txtTipo.Name = "txtTipo"
        Me.txtTipo.Size = New System.Drawing.Size(137, 20)
        Me.txtTipo.TabIndex = 0
        '
        'lblNombre
        '
        Me.lblNombre.AutoSize = True
        Me.lblNombre.Location = New System.Drawing.Point(30, 40)
        Me.lblNombre.Name = "lblNombre"
        Me.lblNombre.Size = New System.Drawing.Size(28, 13)
        Me.lblNombre.TabIndex = 1
        Me.lblNombre.Text = "Tipo"
        '
        'btnAgregar
        '
        Me.btnAgregar.Location = New System.Drawing.Point(184, 181)
        Me.btnAgregar.Name = "btnAgregar"
        Me.btnAgregar.Size = New System.Drawing.Size(124, 33)
        Me.btnAgregar.TabIndex = 10
        Me.btnAgregar.Text = "Confirmar"
        Me.btnAgregar.UseVisualStyleBackColor = True
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(30, 82)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(32, 13)
        Me.Label1.TabIndex = 12
        Me.Label1.Text = "Inicio"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(30, 127)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(29, 13)
        Me.Label2.TabIndex = 14
        Me.Label2.Text = "Final"
        '
        'txtInicio
        '
        Me.txtInicio.CustomFormat = "yyyy-MM-dd HH:mm:ss"
        Me.txtInicio.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.txtInicio.Location = New System.Drawing.Point(171, 76)
        Me.txtInicio.Name = "txtInicio"
        Me.txtInicio.Size = New System.Drawing.Size(137, 20)
        Me.txtInicio.TabIndex = 15
        '
        'txtFinal
        '
        Me.txtFinal.CustomFormat = "yyyy-MM-dd HH:mm:ss"
        Me.txtFinal.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.txtFinal.Location = New System.Drawing.Point(171, 120)
        Me.txtFinal.Name = "txtFinal"
        Me.txtFinal.Size = New System.Drawing.Size(137, 20)
        Me.txtFinal.TabIndex = 16
        '
        'imgTick
        '
        Me.imgTick.Image = Global.UtuRibclob.My.Resources.Resources.tick
        Me.imgTick.Location = New System.Drawing.Point(143, 181)
        Me.imgTick.Name = "imgTick"
        Me.imgTick.Size = New System.Drawing.Size(35, 33)
        Me.imgTick.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.imgTick.TabIndex = 17
        Me.imgTick.TabStop = False
        '
        'Timer1
        '
        Me.Timer1.Interval = 1000
        '
        'frmAltaExcepciones
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(333, 237)
        Me.Controls.Add(Me.imgTick)
        Me.Controls.Add(Me.txtFinal)
        Me.Controls.Add(Me.txtInicio)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.btnAgregar)
        Me.Controls.Add(Me.lblNombre)
        Me.Controls.Add(Me.txtTipo)
        Me.MaximizeBox = False
        Me.MaximumSize = New System.Drawing.Size(349, 276)
        Me.MinimizeBox = False
        Me.MinimumSize = New System.Drawing.Size(349, 276)
        Me.Name = "frmAltaExcepciones"
        Me.ShowIcon = False
        Me.ShowInTaskbar = False
        Me.Text = "Alta excepciones"
        Me.TopMost = True
        CType(Me.imgTick, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents txtTipo As System.Windows.Forms.TextBox
    Friend WithEvents lblNombre As System.Windows.Forms.Label
    Friend WithEvents btnAgregar As System.Windows.Forms.Button
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents txtInicio As System.Windows.Forms.DateTimePicker
    Friend WithEvents txtFinal As System.Windows.Forms.DateTimePicker
    Friend WithEvents imgTick As System.Windows.Forms.PictureBox
    Friend WithEvents Timer1 As System.Windows.Forms.Timer
End Class
