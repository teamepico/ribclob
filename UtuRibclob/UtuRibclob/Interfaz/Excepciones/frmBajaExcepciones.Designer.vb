﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmBajaExcepciones
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.lblEstasSeguro = New System.Windows.Forms.Label()
        Me.lblFinal = New System.Windows.Forms.Label()
        Me.lblTipo = New System.Windows.Forms.Label()
        Me.lblAdvertencia = New System.Windows.Forms.Label()
        Me.btnCancelar = New System.Windows.Forms.Button()
        Me.btnEliminar = New System.Windows.Forms.Button()
        Me.lblInicio = New System.Windows.Forms.Label()
        Me.SuspendLayout()
        '
        'lblEstasSeguro
        '
        Me.lblEstasSeguro.AutoSize = True
        Me.lblEstasSeguro.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEstasSeguro.Location = New System.Drawing.Point(51, 220)
        Me.lblEstasSeguro.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lblEstasSeguro.Name = "lblEstasSeguro"
        Me.lblEstasSeguro.Size = New System.Drawing.Size(130, 24)
        Me.lblEstasSeguro.TabIndex = 27
        Me.lblEstasSeguro.Text = "¿Esta seguro?"
        '
        'lblFinal
        '
        Me.lblFinal.AutoSize = True
        Me.lblFinal.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblFinal.Location = New System.Drawing.Point(49, 158)
        Me.lblFinal.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lblFinal.Name = "lblFinal"
        Me.lblFinal.Size = New System.Drawing.Size(54, 25)
        Me.lblFinal.TabIndex = 26
        Me.lblFinal.Text = "Final"
        '
        'lblTipo
        '
        Me.lblTipo.AutoSize = True
        Me.lblTipo.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTipo.Location = New System.Drawing.Point(49, 75)
        Me.lblTipo.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lblTipo.Name = "lblTipo"
        Me.lblTipo.Size = New System.Drawing.Size(51, 25)
        Me.lblTipo.TabIndex = 25
        Me.lblTipo.Text = "Tipo"
        '
        'lblAdvertencia
        '
        Me.lblAdvertencia.AutoSize = True
        Me.lblAdvertencia.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAdvertencia.Location = New System.Drawing.Point(13, 28)
        Me.lblAdvertencia.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lblAdvertencia.Name = "lblAdvertencia"
        Me.lblAdvertencia.Size = New System.Drawing.Size(432, 24)
        Me.lblAdvertencia.TabIndex = 24
        Me.lblAdvertencia.Text = "Atencion, esta por eliminar la siguiente excepción:"
        '
        'btnCancelar
        '
        Me.btnCancelar.Location = New System.Drawing.Point(171, 266)
        Me.btnCancelar.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.btnCancelar.Name = "btnCancelar"
        Me.btnCancelar.Size = New System.Drawing.Size(165, 41)
        Me.btnCancelar.TabIndex = 23
        Me.btnCancelar.Text = "Cancelar"
        Me.btnCancelar.UseVisualStyleBackColor = True
        '
        'btnEliminar
        '
        Me.btnEliminar.Location = New System.Drawing.Point(344, 266)
        Me.btnEliminar.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.btnEliminar.Name = "btnEliminar"
        Me.btnEliminar.Size = New System.Drawing.Size(165, 41)
        Me.btnEliminar.TabIndex = 22
        Me.btnEliminar.Text = "Confirmar"
        Me.btnEliminar.UseVisualStyleBackColor = True
        '
        'lblInicio
        '
        Me.lblInicio.AutoSize = True
        Me.lblInicio.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblInicio.Location = New System.Drawing.Point(49, 117)
        Me.lblInicio.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lblInicio.Name = "lblInicio"
        Me.lblInicio.Size = New System.Drawing.Size(57, 25)
        Me.lblInicio.TabIndex = 28
        Me.lblInicio.Text = "Inicio"
        '
        'frmBajaExcepciones
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(521, 309)
        Me.Controls.Add(Me.lblInicio)
        Me.Controls.Add(Me.lblEstasSeguro)
        Me.Controls.Add(Me.lblFinal)
        Me.Controls.Add(Me.lblTipo)
        Me.Controls.Add(Me.lblAdvertencia)
        Me.Controls.Add(Me.btnCancelar)
        Me.Controls.Add(Me.btnEliminar)
        Me.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.MaximizeBox = False
        Me.MaximumSize = New System.Drawing.Size(539, 356)
        Me.MinimizeBox = False
        Me.MinimumSize = New System.Drawing.Size(539, 356)
        Me.Name = "frmBajaExcepciones"
        Me.ShowIcon = False
        Me.ShowInTaskbar = False
        Me.Text = "Eliminar excepciones"
        Me.TopMost = True
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents lblEstasSeguro As System.Windows.Forms.Label
    Friend WithEvents lblFinal As System.Windows.Forms.Label
    Friend WithEvents lblTipo As System.Windows.Forms.Label
    Friend WithEvents lblAdvertencia As System.Windows.Forms.Label
    Friend WithEvents btnCancelar As System.Windows.Forms.Button
    Friend WithEvents btnEliminar As System.Windows.Forms.Button
    Friend WithEvents lblInicio As System.Windows.Forms.Label
End Class
