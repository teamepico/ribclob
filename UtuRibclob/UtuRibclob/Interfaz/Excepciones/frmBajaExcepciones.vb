﻿Public Class frmBajaExcepciones
    Private bajaExcepcion As New clsExcepcion
    Private datosUsuario As New clsUser
    Private frmExcepciones As frmExcepciones

    Private Sub frmBajaExcepciones_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        lblTipo.Text = bajaExcepcion.tipoExcepcion
        lblInicio.Text = bajaExcepcion.comienzoExcepcion
        lblFinal.Text = bajaExcepcion.finalExcepcion
    End Sub

    Public Sub New(bajaE As clsExcepcion, unU As clsUser, unFrm As frmExcepciones)
        bajaExcepcion = bajaE
        datosUsuario = unU
        frmExcepciones = unFrm
        InitializeComponent()
    End Sub


    Private Sub btnEliminar_Click(sender As Object, e As EventArgs) Handles btnEliminar.Click
        Dim resultadoSQL As String
        Dim unaC As New clsControladora


        resultadoSQL = unaC.bajaExcepcion(bajaExcepcion)

        If Not resultadoSQL = Nothing Then
            MsgBox(resultadoSQL)
        ElseIf resultadoSQL = Nothing Then
            frmExcepciones.Activate()
            Me.Close()
        End If
    End Sub

    Private Sub btnCancelar_Click(sender As Object, e As EventArgs) Handles btnCancelar.Click
        Me.Close()

    End Sub
End Class