﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmExcepciones
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmExcepciones))
        Me.ContextMenuStrip1 = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.ModificarToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.EliminarToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ListarFaltasToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ListarStrikesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ListarHorariosToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.btnSalir = New System.Windows.Forms.Button()
        Me.lsvExcepciones = New System.Windows.Forms.ListView()
        Me.Tipo = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.Inicio = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.Final = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.btnAgregar = New System.Windows.Forms.Button()
        Me.btnBorrar = New System.Windows.Forms.Button()
        Me.btnModificar = New System.Windows.Forms.Button()
        Me.txtBuscar = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.ContextMenuStrip1.SuspendLayout()
        Me.SuspendLayout()
        '
        'ContextMenuStrip1
        '
        Me.ContextMenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ModificarToolStripMenuItem, Me.EliminarToolStripMenuItem, Me.ListarFaltasToolStripMenuItem, Me.ListarStrikesToolStripMenuItem, Me.ListarHorariosToolStripMenuItem})
        Me.ContextMenuStrip1.Name = "ContextMenuStrip1"
        Me.ContextMenuStrip1.Size = New System.Drawing.Size(149, 114)
        '
        'ModificarToolStripMenuItem
        '
        Me.ModificarToolStripMenuItem.Name = "ModificarToolStripMenuItem"
        Me.ModificarToolStripMenuItem.Size = New System.Drawing.Size(148, 22)
        Me.ModificarToolStripMenuItem.Text = "Modificar"
        '
        'EliminarToolStripMenuItem
        '
        Me.EliminarToolStripMenuItem.Name = "EliminarToolStripMenuItem"
        Me.EliminarToolStripMenuItem.Size = New System.Drawing.Size(148, 22)
        Me.EliminarToolStripMenuItem.Text = "Eliminar"
        '
        'ListarFaltasToolStripMenuItem
        '
        Me.ListarFaltasToolStripMenuItem.Name = "ListarFaltasToolStripMenuItem"
        Me.ListarFaltasToolStripMenuItem.Size = New System.Drawing.Size(148, 22)
        Me.ListarFaltasToolStripMenuItem.Text = "Listar faltas"
        '
        'ListarStrikesToolStripMenuItem
        '
        Me.ListarStrikesToolStripMenuItem.Name = "ListarStrikesToolStripMenuItem"
        Me.ListarStrikesToolStripMenuItem.Size = New System.Drawing.Size(148, 22)
        Me.ListarStrikesToolStripMenuItem.Text = "Listar strikes"
        '
        'ListarHorariosToolStripMenuItem
        '
        Me.ListarHorariosToolStripMenuItem.Name = "ListarHorariosToolStripMenuItem"
        Me.ListarHorariosToolStripMenuItem.Size = New System.Drawing.Size(148, 22)
        Me.ListarHorariosToolStripMenuItem.Text = "Listar horarios"
        '
        'btnSalir
        '
        Me.btnSalir.Location = New System.Drawing.Point(640, 479)
        Me.btnSalir.Name = "btnSalir"
        Me.btnSalir.Size = New System.Drawing.Size(117, 30)
        Me.btnSalir.TabIndex = 1
        Me.btnSalir.Text = "Salir"
        Me.btnSalir.UseVisualStyleBackColor = True
        '
        'lsvExcepciones
        '
        Me.lsvExcepciones.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.Tipo, Me.Inicio, Me.Final})
        Me.lsvExcepciones.GridLines = True
        Me.lsvExcepciones.Location = New System.Drawing.Point(12, 38)
        Me.lsvExcepciones.Name = "lsvExcepciones"
        Me.lsvExcepciones.Size = New System.Drawing.Size(745, 435)
        Me.lsvExcepciones.TabIndex = 6
        Me.lsvExcepciones.UseCompatibleStateImageBehavior = False
        Me.lsvExcepciones.View = System.Windows.Forms.View.Details
        '
        'Tipo
        '
        Me.Tipo.Text = "Tipo"
        Me.Tipo.Width = 165
        '
        'Inicio
        '
        Me.Inicio.Text = "Inicio"
        Me.Inicio.Width = 191
        '
        'Final
        '
        Me.Final.Text = "Final"
        Me.Final.Width = 205
        '
        'btnAgregar
        '
        Me.btnAgregar.Location = New System.Drawing.Point(12, 479)
        Me.btnAgregar.Name = "btnAgregar"
        Me.btnAgregar.Size = New System.Drawing.Size(117, 30)
        Me.btnAgregar.TabIndex = 7
        Me.btnAgregar.Text = "Agregar"
        Me.btnAgregar.UseVisualStyleBackColor = True
        '
        'btnBorrar
        '
        Me.btnBorrar.Location = New System.Drawing.Point(135, 479)
        Me.btnBorrar.Name = "btnBorrar"
        Me.btnBorrar.Size = New System.Drawing.Size(117, 30)
        Me.btnBorrar.TabIndex = 9
        Me.btnBorrar.Text = "Eliminar"
        Me.btnBorrar.UseVisualStyleBackColor = True
        '
        'btnModificar
        '
        Me.btnModificar.ContextMenuStrip = Me.ContextMenuStrip1
        Me.btnModificar.Location = New System.Drawing.Point(258, 479)
        Me.btnModificar.Name = "btnModificar"
        Me.btnModificar.Size = New System.Drawing.Size(117, 30)
        Me.btnModificar.TabIndex = 11
        Me.btnModificar.Tag = "C:\Users\Ceibal\Documents\SourceTree\ribclob\UtuRibclob\UtuRibclob\Interfaz\Funci" & _
    "onariosDocentes\frmAltaFuncionarioDocente.vb"
        Me.btnModificar.Text = "Modificar"
        Me.btnModificar.UseVisualStyleBackColor = True
        '
        'txtBuscar
        '
        Me.txtBuscar.Location = New System.Drawing.Point(160, 12)
        Me.txtBuscar.Name = "txtBuscar"
        Me.txtBuscar.Size = New System.Drawing.Size(167, 20)
        Me.txtBuscar.TabIndex = 15
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(34, 19)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(120, 13)
        Me.Label1.TabIndex = 14
        Me.Label1.Text = "Buscar por apellido:"
        '
        'frmExcepciones
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(769, 520)
        Me.Controls.Add(Me.txtBuscar)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.btnModificar)
        Me.Controls.Add(Me.btnBorrar)
        Me.Controls.Add(Me.btnAgregar)
        Me.Controls.Add(Me.lsvExcepciones)
        Me.Controls.Add(Me.btnSalir)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MinimumSize = New System.Drawing.Size(785, 559)
        Me.Name = "frmExcepciones"
        Me.Text = "Excepciones"
        Me.ContextMenuStrip1.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents btnSalir As System.Windows.Forms.Button
    Friend WithEvents ContextMenuStrip1 As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents ModificarToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents EliminarToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ListarFaltasToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ListarStrikesToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ListarHorariosToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents lsvExcepciones As System.Windows.Forms.ListView
    Friend WithEvents btnAgregar As System.Windows.Forms.Button
    Friend WithEvents btnBorrar As System.Windows.Forms.Button
    Friend WithEvents btnModificar As System.Windows.Forms.Button
    Friend WithEvents Tipo As System.Windows.Forms.ColumnHeader
    Friend WithEvents Inicio As System.Windows.Forms.ColumnHeader
    Friend WithEvents Final As System.Windows.Forms.ColumnHeader
    Friend WithEvents txtBuscar As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
End Class
