﻿Public Class frmAltaExcepciones
    Dim datosUsuario As New clsUser
    Private frmExcepciones As frmExcepciones
    Dim horarioEsCorrecto As Boolean = False
    Public Sub New(elUsuario As clsUser, unFrm As frmExcepciones)

        ' Llamada necesaria para el diseñador.
        InitializeComponent()

        ' Agregue cualquier inicialización después de la llamada a InitializeComponent().
        datosUsuario = elUsuario
        frmExcepciones = unFrm
    End Sub

    Private Sub btnAgregar_Click(sender As Object, e As EventArgs) Handles btnAgregar.Click
        Dim unaEx As New clsExcepcion(vbEmpty, txtTipo.Text, txtInicio.Text, txtFinal.Text)

        Console.WriteLine(txtInicio.Text)


        Dim horarioEntrada As Date = Date.ParseExact(txtInicio.Text, "yyyy-MM-dd HH:mm:ss", System.Globalization.DateTimeFormatInfo.InvariantInfo)
        Dim horarioSalida As Date = Date.ParseExact(txtFinal.Text, "yyyy-MM-dd HH:mm:ss", System.Globalization.DateTimeFormatInfo.InvariantInfo)

        If horarioSalida > horarioEntrada Then
            horarioEsCorrecto = True
        End If

        If horarioEsCorrecto = True Then
            Dim unC As New clsControladora
            Dim errorSQL As String
            errorSQL = unC.altaExcepciones(unaEx)

            If errorSQL = "" Then
                clear()
                imgTick.Visible = True
                Timer1.Enabled = True
                frmExcepciones.Activate()
            Else
                MsgBox("Error al insertar el horario")
            End If
        Else
            MsgBox("El horario de salida no puede ser menor a el de entrada.")
        End If

    End Sub

    Public Sub clear()
        txtTipo.Clear()
    End Sub

    Private Sub frmAltaExcepciones_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        imgTick.Visible = False
        Timer1.Enabled = False
    End Sub

    Private Sub Timer1_Tick(sender As Object, e As EventArgs) Handles Timer1.Tick
        imgTick.Visible = False
        Timer1.Enabled = False
    End Sub
End Class