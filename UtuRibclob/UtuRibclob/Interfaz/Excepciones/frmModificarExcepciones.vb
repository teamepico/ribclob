﻿Public Class frmModificarExcepciones
    Dim datosExcepciones As New clsExcepcion
    Dim datosUsuario As New clsUser
    Private frmExcepciones As frmExcepciones
    Dim horarioEsCorrecto As Boolean = False

    Public Sub New(unaEx As clsExcepcion, elUsuario As clsUser, unFrm As frmExcepciones)

        ' Llamada necesaria para el diseñador.
        InitializeComponent()

        ' Agregue cualquier inicialización después de la llamada a InitializeComponent().
        datosExcepciones = unaEx
        datosUsuario = elUsuario
        frmExcepciones = unFrm
    End Sub

    Private Sub frmModificarExcepciones_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        txtTipo.Text = datosExcepciones.tipoExcepcion

        Dim horarioInicial As Date = Date.ParseExact(datosExcepciones.comienzoExcepcion, "dd/MM/yyyy HH:mm:ss", System.Globalization.DateTimeFormatInfo.InvariantInfo)
        Dim horarioFinal As Date = Date.ParseExact(datosExcepciones.finalExcepcion, "dd/MM/yyyy HH:mm:ss", System.Globalization.DateTimeFormatInfo.InvariantInfo)

        txtFinal.Value = horarioFinal
        txtInicio.Value = horarioInicial
    End Sub

    Private Sub btnAgregar_Click(sender As Object, e As EventArgs) Handles btnAgregar.Click

        Dim unaExc As New clsExcepcion(vbEmpty, txtTipo.Text, txtInicio.Text, txtFinal.Text)

        Dim horarioEntrada As Date = Date.ParseExact(txtInicio.Text, "yyyy-MM-dd HH:mm:ss", System.Globalization.DateTimeFormatInfo.InvariantInfo)
        Dim horarioSalida As Date = Date.ParseExact(txtFinal.Text, "yyyy-MM-dd HH:mm:ss", System.Globalization.DateTimeFormatInfo.InvariantInfo)

        If horarioSalida > horarioEntrada Then
            horarioEsCorrecto = True
        End If

        If horarioEsCorrecto = True Then
            Dim unC As New clsControladora
            Dim errorSQL As String
            errorSQL = unC.modificarExcepciones(unaExc, datosExcepciones)

            If errorSQL = "" Then
                frmExcepciones.Activate()
                Me.Close()
            Else
                MsgBox("Error en el sistema al cargar la excepcion.")
            End If
        Else
            MsgBox("El horario de salida no puede ser menor a el de entrada.")
        End If
    End Sub
End Class