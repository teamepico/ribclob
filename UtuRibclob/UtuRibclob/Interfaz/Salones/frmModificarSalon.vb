﻿Public Class frmModificarSalon
    Dim elUsuario As New clsUser
    Dim elSalon As New clsSalon
    Private frmSalon As frmSalones

    Private Sub btnAgregar_Click(sender As Object, e As EventArgs) Handles btnAgregar.Click
        Dim unaC As New clsControladora
        Dim unSal As New clsSalon(txtNombreSalon.Text, txtUbicacionSalon.Text)
        Dim resultadoSQL As String
        resultadoSQL = unaC.modificarSalon(unSal)
        If Not resultadoSQL = Nothing Then
            MsgBox(resultadoSQL)
        ElseIf resultadoSQL = Nothing Then
            frmSalon.Activate()
            Me.Close()
        End If
    End Sub

    Public Sub New(unU As clsUser, unS As clsSalon, unFrm As frmSalones)
        elUsuario = unU
        elSalon = unS
        frmSalon = unFrm
        InitializeComponent()
    End Sub

    Private Sub limpiar()
        txtNombreSalon.Clear()
        txtUbicacionSalon.Clear()
    End Sub

    Private Sub frmAltaSalon_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        txtNombreSalon.Text = elSalon.nombreSalon
        txtUbicacionSalon.Text = elSalon.ubicacionSalon
    End Sub
End Class