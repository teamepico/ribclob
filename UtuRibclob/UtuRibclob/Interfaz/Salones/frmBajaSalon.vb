﻿Public Class frmBajaSalon
    Dim bajaSalon As New clsSalon
    Dim datosUsuario As New clsUser
    Private frmSalon As frmSalones

    Private Sub frmBajaSalon_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        lblNombre.Text = bajaSalon.nombreSalon
        lblUbicacion.Text = bajaSalon.ubicacionSalon
    End Sub

    Public Sub New(Salon As clsSalon, unU As clsUser, unFrm As frmSalones)
        bajaSalon = Salon
        datosUsuario = unU
        InitializeComponent()
        frmSalon = unFrm
    End Sub

    Private Sub btnEliminar_Click_1(sender As Object, e As EventArgs) Handles btnEliminar.Click
        Dim resultadoSQL
        Dim unaC As New clsControladora
        resultadoSQL = unaC.bajaSalones(bajaSalon)

        If Not resultadoSQL = Nothing Then
            MsgBox("Hubo un error en el sistema y la acción probablemente no se pudo llevar a cabo. Probablemente, el salón está relacionado con algún docente u horario y por lo tanto no se puede eliminar. Para más informacion, ingrese a la consola.")
            Console.WriteLine(resultadoSQL)
        ElseIf resultadoSQL = Nothing Then
            frmSalon.Activate()
            Me.Close()
        End If
    End Sub

    Private Sub btnCancelar_Click(sender As Object, e As EventArgs) Handles btnCancelar.Click
        Me.Close()
    End Sub
End Class