﻿Public Class frmAltaSalon
    Private frmSalon As frmSalones

    Public Sub New(unFrm As frmSalones)

        ' Llamada necesaria para el diseñador.
        InitializeComponent()

        ' Agregue cualquier inicialización después de la llamada a InitializeComponent().
        frmSalon = unFrm
    End Sub

    Private Sub btnAgregar_Click(sender As Object, e As EventArgs) Handles btnAgregar.Click
        Dim unaC As New clsControladora
        Dim unSal As New clsSalon(txtNombreSalon.Text, txtUbicacionSalon.Text)
        Dim errorSql As String
        errorSql = unaC.altaSalon(unSal)
        If errorSql = "" Then

            imgTick.Visible = True
            Timer1.Enabled = True

            frmSalon.Activate()
            Me.Activate()
            limpiar()

        Else
            MsgBox("No se pudo agregar el salon")
        End If

        
    End Sub

    Private Sub limpiar()
        txtNombreSalon.Clear()
        txtUbicacionSalon.Clear()
    End Sub

    Private Sub frmAltaSalon_Load(sender As Object, e As EventArgs) Handles MyBase.Load

    End Sub

    Private Sub Timer1_Tick(sender As Object, e As EventArgs) Handles Timer1.Tick
        imgTick.Visible = False
        Timer1.Enabled = False
    End Sub

    Private Sub imgTick_Click(sender As Object, e As EventArgs) Handles imgTick.Click

    End Sub
End Class