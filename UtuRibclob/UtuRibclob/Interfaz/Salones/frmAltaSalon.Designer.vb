﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmAltaSalon
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.btnAgregar = New System.Windows.Forms.Button()
        Me.lblNombre = New System.Windows.Forms.Label()
        Me.txtNombreSalon = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.txtUbicacionSalon = New System.Windows.Forms.TextBox()
        Me.Timer1 = New System.Windows.Forms.Timer(Me.components)
        Me.imgTick = New System.Windows.Forms.PictureBox()
        CType(Me.imgTick, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'btnAgregar
        '
        Me.btnAgregar.Location = New System.Drawing.Point(241, 206)
        Me.btnAgregar.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.btnAgregar.Name = "btnAgregar"
        Me.btnAgregar.Size = New System.Drawing.Size(165, 41)
        Me.btnAgregar.TabIndex = 11
        Me.btnAgregar.Text = "Confirmar"
        Me.btnAgregar.UseVisualStyleBackColor = True
        '
        'lblNombre
        '
        Me.lblNombre.AutoSize = True
        Me.lblNombre.Location = New System.Drawing.Point(41, 53)
        Me.lblNombre.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lblNombre.Name = "lblNombre"
        Me.lblNombre.Size = New System.Drawing.Size(58, 17)
        Me.lblNombre.TabIndex = 13
        Me.lblNombre.Text = "Nombre"
        '
        'txtNombreSalon
        '
        Me.txtNombreSalon.Location = New System.Drawing.Point(224, 53)
        Me.txtNombreSalon.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.txtNombreSalon.Name = "txtNombreSalon"
        Me.txtNombreSalon.Size = New System.Drawing.Size(181, 22)
        Me.txtNombreSalon.TabIndex = 12
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(41, 100)
        Me.Label1.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(70, 17)
        Me.Label1.TabIndex = 15
        Me.Label1.Text = "Ubicacion"
        '
        'txtUbicacionSalon
        '
        Me.txtUbicacionSalon.Location = New System.Drawing.Point(224, 100)
        Me.txtUbicacionSalon.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.txtUbicacionSalon.Name = "txtUbicacionSalon"
        Me.txtUbicacionSalon.Size = New System.Drawing.Size(181, 22)
        Me.txtUbicacionSalon.TabIndex = 14
        '
        'Timer1
        '
        Me.Timer1.Interval = 1000
        '
        'imgTick
        '
        Me.imgTick.Image = Global.UtuRibclob.My.Resources.Resources.tick
        Me.imgTick.Location = New System.Drawing.Point(151, 206)
        Me.imgTick.Margin = New System.Windows.Forms.Padding(4)
        Me.imgTick.Name = "imgTick"
        Me.imgTick.Size = New System.Drawing.Size(47, 41)
        Me.imgTick.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.imgTick.TabIndex = 19
        Me.imgTick.TabStop = False
        Me.imgTick.Visible = False
        '
        'frmAltaSalon
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(439, 263)
        Me.Controls.Add(Me.imgTick)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.txtUbicacionSalon)
        Me.Controls.Add(Me.lblNombre)
        Me.Controls.Add(Me.txtNombreSalon)
        Me.Controls.Add(Me.btnAgregar)
        Me.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.MaximizeBox = False
        Me.MaximumSize = New System.Drawing.Size(457, 310)
        Me.MinimizeBox = False
        Me.MinimumSize = New System.Drawing.Size(457, 310)
        Me.Name = "frmAltaSalon"
        Me.ShowIcon = False
        Me.ShowInTaskbar = False
        Me.Text = "Agregar salon"
        Me.TopMost = True
        CType(Me.imgTick, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents btnAgregar As System.Windows.Forms.Button
    Friend WithEvents lblNombre As System.Windows.Forms.Label
    Friend WithEvents txtNombreSalon As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents txtUbicacionSalon As System.Windows.Forms.TextBox
    Friend WithEvents Timer1 As System.Windows.Forms.Timer
    Friend WithEvents imgTick As System.Windows.Forms.PictureBox
End Class
