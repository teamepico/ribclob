﻿Public Class frmSalones
    Dim elUsuario As New clsUser
    Dim frmSalones As frmSalones
    Dim seleccionEsVacia As Boolean = False

    Public Function seleccionVacia() As Boolean
        Dim devolver As Boolean = False

        If lsvSalones.FocusedItem Is Nothing Then
            MsgBox("No se ha seleccionado ningun salón.")
            devolver = True
        End If

        Return devolver
    End Function

    Public Sub New(elU As clsUser)
        'TODO: Complete member initialization 
        elUsuario = elU
        InitializeComponent()
        frmSalones = Me
    End Sub

    Public Sub listar()
        Dim unaC As New clsControladora
        Dim colSalones As New List(Of clsSalon)
        colSalones = unaC.listarSalones()

        Dim lsi As New ListViewItem
        lsvSalones.Items.Clear()

        For Each unSalon In colSalones
            lsi = lsvSalones.Items.Add(unSalon.nombreSalon)
            lsi.SubItems.Add(unSalon.ubicacionSalon)
        Next
    End Sub

    Private Sub frmSalones_Activated(sender As Object, e As EventArgs) Handles Me.Activated
        listar()
    End Sub

    Private Sub frmFuncionariosDocentes_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Me.MinimumSize = New Size(728, 560)
        listar()
    End Sub

    Private Sub btnAgregar_Click(sender As Object, e As EventArgs) Handles btnAgregar.Click
        Dim frmAsalon As New frmAltaSalon(frmSalones)
        frmAsalon.Show()
    End Sub

    Private Sub btnSalir_Click(sender As Object, e As EventArgs) Handles btnSalir.Click
        Me.Close()
    End Sub

    Private Sub btnModificar_Click(sender As Object, e As EventArgs) Handles btnModificar.Click
        seleccionEsVacia = seleccionVacia()

        If seleccionEsVacia = False Then
            Dim unSalon As New clsSalon(lsvSalones.FocusedItem.SubItems(0).Text, lsvSalones.FocusedItem.SubItems(1).Text)
            Dim unFrmMod As New frmModificarSalon(elUsuario, unSalon, frmSalones)
            unFrmMod.Show()
        End If
    End Sub

    Private Sub btnEliminar_Click(sender As Object, e As EventArgs) Handles btnEliminar.Click
        seleccionEsVacia = seleccionVacia()

        If seleccionEsVacia = False Then
            Dim unSalon As New clsSalon(lsvSalones.FocusedItem.SubItems(0).Text, lsvSalones.FocusedItem.SubItems(1).Text)
            Dim unFrmEl As New frmBajaSalon(unSalon, elUsuario, frmSalones)
            unFrmEl.Show()
        End If

    End Sub

    Private Sub txtBuscar_TextChanged(sender As Object, e As EventArgs) Handles txtBuscar.TextChanged
        Dim unaC As New clsControladora
        Dim colSalones As New List(Of clsSalon)

        colSalones = unaC.buscarSalones(txtBuscar.Text)

        Dim lsi As New ListViewItem
        lsvSalones.Items.Clear()

        For Each unSalon In colSalones
            lsi = lsvSalones.Items.Add(unSalon.nombreSalon)
            lsi.SubItems.Add(unSalon.ubicacionSalon)
        Next
    End Sub

    Private Sub frmSalones_Resize(sender As Object, e As EventArgs) Handles Me.Resize
        Dim lsv_x = Me.Size.Width
        Dim lsv_y = Me.Size.Height
        lsv_x = lsv_x - 40
        lsv_y = lsv_y - 129
        Me.lsvSalones.Size = New System.Drawing.Size(lsv_x, lsv_y)
        Dim btnAgregar_x = btnAgregar.Location.X
        Dim btnModificar_x = btnModificar.Location.X
        Dim btnEliminar_x = btnEliminar.Location.X
        Dim btnSalir_x = Me.Size.Width
        Dim SalirSize = btnSalir.Size.Width / 2

        Dim btn_y = Me.Size.Height
        btn_y = btn_y - 81

        btnSalir_x = btnSalir_x - 81 - SalirSize

        Me.btnAgregar.Location = New System.Drawing.Point(btnAgregar_x, btn_y)
        Me.btnModificar.Location = New System.Drawing.Point(btnModificar_x, btn_y)
        Me.btnEliminar.Location = New System.Drawing.Point(btnEliminar_x, btn_y)
        Me.btnSalir.Location = New System.Drawing.Point(btnSalir_x, btn_y)
    End Sub
End Class