﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmModificarSalon
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.btnAgregar = New System.Windows.Forms.Button()
        Me.lblNombre = New System.Windows.Forms.Label()
        Me.txtNombreSalon = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.txtUbicacionSalon = New System.Windows.Forms.TextBox()
        Me.SuspendLayout()
        '
        'btnAgregar
        '
        Me.btnAgregar.Location = New System.Drawing.Point(188, 149)
        Me.btnAgregar.Name = "btnAgregar"
        Me.btnAgregar.Size = New System.Drawing.Size(124, 33)
        Me.btnAgregar.TabIndex = 11
        Me.btnAgregar.Text = "Confirmar"
        Me.btnAgregar.UseVisualStyleBackColor = True
        '
        'lblNombre
        '
        Me.lblNombre.AutoSize = True
        Me.lblNombre.Location = New System.Drawing.Point(38, 46)
        Me.lblNombre.Name = "lblNombre"
        Me.lblNombre.Size = New System.Drawing.Size(44, 13)
        Me.lblNombre.TabIndex = 13
        Me.lblNombre.Text = "Nombre"
        '
        'txtNombreSalon
        '
        Me.txtNombreSalon.Location = New System.Drawing.Point(175, 39)
        Me.txtNombreSalon.Name = "txtNombreSalon"
        Me.txtNombreSalon.ReadOnly = True
        Me.txtNombreSalon.Size = New System.Drawing.Size(137, 20)
        Me.txtNombreSalon.TabIndex = 12
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(38, 84)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(55, 13)
        Me.Label1.TabIndex = 15
        Me.Label1.Text = "Ubicacion"
        '
        'txtUbicacionSalon
        '
        Me.txtUbicacionSalon.Location = New System.Drawing.Point(175, 77)
        Me.txtUbicacionSalon.Name = "txtUbicacionSalon"
        Me.txtUbicacionSalon.Size = New System.Drawing.Size(137, 20)
        Me.txtUbicacionSalon.TabIndex = 14
        '
        'frmModificarSalon
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(370, 222)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.txtUbicacionSalon)
        Me.Controls.Add(Me.lblNombre)
        Me.Controls.Add(Me.txtNombreSalon)
        Me.Controls.Add(Me.btnAgregar)
        Me.Margin = New System.Windows.Forms.Padding(2)
        Me.MaximizeBox = False
        Me.MaximumSize = New System.Drawing.Size(386, 261)
        Me.MinimizeBox = False
        Me.MinimumSize = New System.Drawing.Size(386, 261)
        Me.Name = "frmModificarSalon"
        Me.ShowIcon = False
        Me.ShowInTaskbar = False
        Me.Text = "Modificar salon"
        Me.TopMost = True
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents btnAgregar As System.Windows.Forms.Button
    Friend WithEvents lblNombre As System.Windows.Forms.Label
    Friend WithEvents txtNombreSalon As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents txtUbicacionSalon As System.Windows.Forms.TextBox
End Class
