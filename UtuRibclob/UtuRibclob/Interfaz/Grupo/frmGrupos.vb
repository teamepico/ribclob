﻿Public Class frmGrupos
    Dim elUsuario As New clsUser
    Private frmGrupos As frmGrupos
    Dim seleccionEsVacia As Boolean = False

    Public Function seleccionVacia() As Boolean
        Dim devolver As Boolean = False

        If lsvGrupos.FocusedItem Is Nothing Then
            MsgBox("No se ha seleccionado ningun grupo.")
            devolver = True
        End If

        Return devolver
    End Function

    Public Sub New(elU As clsUser)
        'TODO: Complete member initialization 
        elUsuario = elU
        frmGrupos = Me
        InitializeComponent()
    End Sub

    Public Sub listar()
        Dim unaC As New clsControladora
        Dim colGrupo As New List(Of clsGrupo)
        colGrupo = unaC.listarGrupos()

        Dim lsi As New ListViewItem
        lsvGrupos.Items.Clear()

        For Each unGrupo In colGrupo
            lsi = lsvGrupos.Items.Add(unGrupo.NombreGrupo)
            lsi.SubItems.Add(unGrupo.DescripcionGrupo)
        Next
    End Sub

    Private Sub btnAgregar_Click(sender As Object, e As EventArgs) Handles btnAgregar.Click
        Dim frmAGrupo As New frmAltaGrupo(frmGrupos)
        frmAGrupo.Show()
    End Sub

    Private Sub btnSalir_Click(sender As Object, e As EventArgs) Handles btnSalir.Click
        Me.Close()
    End Sub

    Private Sub frmGrupos_Activated(sender As Object, e As EventArgs) Handles Me.Activated
        listar()
    End Sub

    Private Sub frmGrupos_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Me.MinimumSize = New Size(730, 561)
        listar()
    End Sub

    Private Sub btnModificar_Click(sender As Object, e As EventArgs) Handles btnModificar.Click
        seleccionEsVacia = seleccionVacia()

        If seleccionEsVacia = False Then
            Dim unGrupo As New clsGrupo(lsvGrupos.FocusedItem.SubItems(0).Text, lsvGrupos.FocusedItem.SubItems(1).Text)
            Dim unFrmMod As New frmModificarGrupo(elUsuario, unGrupo, frmGrupos)
            unFrmMod.Show()
        End If
    End Sub

    Private Sub btnListar_Click(sender As Object, e As EventArgs)
        listar()
    End Sub

    Private Sub btnEliminar_Click(sender As Object, e As EventArgs) Handles btnEliminar.Click
        seleccionEsVacia = seleccionVacia()

        If seleccionEsVacia = False Then
            Dim unGrupo As New clsGrupo(lsvGrupos.FocusedItem.SubItems(0).Text, lsvGrupos.FocusedItem.SubItems(1).Text)
            Dim unFrmBaja As New frmBajaGrupo(elUsuario, unGrupo, frmGrupos)
            unFrmBaja.Show()
        End If
    End Sub

    Private Sub txtBuscar_TextChanged(sender As Object, e As EventArgs) Handles txtBuscar.TextChanged
        Dim unaC As New clsControladora
        Dim colGrupo As New List(Of clsGrupo)

        colGrupo = unaC.buscarGrupos(txtBuscar.Text)

        Dim lsi As New ListViewItem
        lsvGrupos.Items.Clear()

        For Each unGrupo In colGrupo
            lsi = lsvGrupos.Items.Add(unGrupo.NombreGrupo)
            lsi.SubItems.Add(unGrupo.DescripcionGrupo)
        Next
    End Sub

    Private Sub frmGrupos_Resize(sender As Object, e As EventArgs) Handles Me.Resize
        Dim lsv_x = Me.Size.Width
        Dim lsv_y = Me.Size.Height
        lsv_x = lsv_x - 40
        lsv_y = lsv_y - 129
        Me.lsvGrupos.Size = New System.Drawing.Size(lsv_x, lsv_y)

        Dim btnAgregar_x = btnAgregar.Location.X
        Dim btnModificar_x = btnModificar.Location.X
        Dim btnEliminar_x = btnEliminar.Location.X
        Dim btnSalir_x = Me.Size.Width
        Dim SalirSize = btnSalir.Size.Width / 2

        Dim btn_y = Me.Size.Height
        btn_y = btn_y - 81

        btnSalir_x = btnSalir_x - 81 - SalirSize

        Me.btnAgregar.Location = New System.Drawing.Point(btnAgregar_x, btn_y)
        Me.btnModificar.Location = New System.Drawing.Point(btnModificar_x, btn_y)
        Me.btnEliminar.Location = New System.Drawing.Point(btnEliminar_x, btn_y)
        Me.btnSalir.Location = New System.Drawing.Point(btnSalir_x, btn_y)
    End Sub
End Class