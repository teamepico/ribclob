﻿Public Class frmModificarGrupo
    Dim elUsuario As New clsUser
    Dim elGrupo As New clsGrupo
    Private frmGrupos As frmGrupos

    Public Sub New(unU As clsUser, unG As clsGrupo, unFrm As frmGrupos)
        elUsuario = unU
        elGrupo = unG
        frmGrupos = unFrm
        InitializeComponent()
    End Sub

    Private Sub limpiar()
        txtNombreGrupo.Clear()
        txtDescripcionGrupo.Clear()
    End Sub

    Private Sub btnAgregar_Click(sender As Object, e As EventArgs) Handles btnAgregar.Click
        Dim unaC As New clsControladora
        Dim unGru As New clsGrupo(txtNombreGrupo.Text, txtDescripcionGrupo.Text)
        Dim resultadoSQL As String
        resultadoSQL = unaC.modificarGrupo(unGru)
        If Not resultadoSQL = "" Then
            MsgBox(resultadoSQL)
        ElseIf resultadoSQL = "" Then
            frmGrupos.Activate()
            Me.Close()
        Else
            MsgBox("Algo salio horriblemente mal.")
        End If
    End Sub


    Private Sub frmModificarGrupo_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        txtNombreGrupo.Text = elGrupo.NombreGrupo
        txtDescripcionGrupo.Text = elGrupo.DescripcionGrupo
    End Sub
End Class