﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmModificarGrupo
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.txtDescripcionGrupo = New System.Windows.Forms.TextBox()
        Me.lblNombre = New System.Windows.Forms.Label()
        Me.txtNombreGrupo = New System.Windows.Forms.TextBox()
        Me.btnAgregar = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(21, 84)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(63, 13)
        Me.Label1.TabIndex = 25
        Me.Label1.Text = "Descripcion"
        '
        'txtDescripcionGrupo
        '
        Me.txtDescripcionGrupo.Location = New System.Drawing.Point(158, 84)
        Me.txtDescripcionGrupo.Multiline = True
        Me.txtDescripcionGrupo.Name = "txtDescripcionGrupo"
        Me.txtDescripcionGrupo.Size = New System.Drawing.Size(137, 55)
        Me.txtDescripcionGrupo.TabIndex = 24
        '
        'lblNombre
        '
        Me.lblNombre.AutoSize = True
        Me.lblNombre.Location = New System.Drawing.Point(21, 46)
        Me.lblNombre.Name = "lblNombre"
        Me.lblNombre.Size = New System.Drawing.Size(44, 13)
        Me.lblNombre.TabIndex = 23
        Me.lblNombre.Text = "Nombre"
        '
        'txtNombreGrupo
        '
        Me.txtNombreGrupo.AccessibleName = ""
        Me.txtNombreGrupo.Location = New System.Drawing.Point(158, 38)
        Me.txtNombreGrupo.Name = "txtNombreGrupo"
        Me.txtNombreGrupo.ReadOnly = True
        Me.txtNombreGrupo.Size = New System.Drawing.Size(137, 20)
        Me.txtNombreGrupo.TabIndex = 22
        '
        'btnAgregar
        '
        Me.btnAgregar.Location = New System.Drawing.Point(95, 190)
        Me.btnAgregar.Name = "btnAgregar"
        Me.btnAgregar.Size = New System.Drawing.Size(124, 33)
        Me.btnAgregar.TabIndex = 21
        Me.btnAgregar.Text = "Confirmar"
        Me.btnAgregar.UseVisualStyleBackColor = True
        '
        'frmModificarGrupo
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(317, 249)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.txtDescripcionGrupo)
        Me.Controls.Add(Me.lblNombre)
        Me.Controls.Add(Me.txtNombreGrupo)
        Me.Controls.Add(Me.btnAgregar)
        Me.Margin = New System.Windows.Forms.Padding(2)
        Me.MaximizeBox = False
        Me.MaximumSize = New System.Drawing.Size(333, 288)
        Me.MinimizeBox = False
        Me.MinimumSize = New System.Drawing.Size(333, 288)
        Me.Name = "frmModificarGrupo"
        Me.ShowIcon = False
        Me.ShowInTaskbar = False
        Me.Text = "Modificar grupo"
        Me.TopMost = True
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents txtDescripcionGrupo As System.Windows.Forms.TextBox
    Friend WithEvents lblNombre As System.Windows.Forms.Label
    Friend WithEvents txtNombreGrupo As System.Windows.Forms.TextBox
    Friend WithEvents btnAgregar As System.Windows.Forms.Button
End Class
