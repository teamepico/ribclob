﻿Public Class frmBajaGrupo
    Dim bajaGrupo As New clsGrupo
    Dim datosUsuario As New clsUser
    Private frmGrupos As frmGrupos
    Public Sub New(unU As clsUser, unG As clsGrupo, unFrm As frmGrupos)
        frmGrupos = unFrm
        datosUsuario = unU
        bajaGrupo = unG
        InitializeComponent()
    End Sub
    Private Sub frmBajaGrupo_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        lblNombre.Text = bajaGrupo.NombreGrupo
        lblDesc.Text = bajaGrupo.DescripcionGrupo
    End Sub
    Private Sub btnEliminar_Click_1(sender As Object, e As EventArgs) Handles btnEliminar.Click
        Dim resultadoSQL
        Dim unaC As New clsControladora
        resultadoSQL = unaC.bajaGrupo(bajaGrupo)

        If Not resultadoSQL = "" Then
            MsgBox(resultadoSQL)
        ElseIf resultadoSQL = "" Then
            frmGrupos.Activate()
            Me.Close()
        End If
    End Sub
    Private Sub btnCancelar_Click(sender As Object, e As EventArgs) Handles btnCancelar.Click
        Me.Close()
    End Sub
End Class