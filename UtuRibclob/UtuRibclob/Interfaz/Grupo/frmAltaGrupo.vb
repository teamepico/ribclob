﻿Public Class frmAltaGrupo
    Private frmGrupos As frmGrupos
    Public Sub New(unFrm As frmGrupos)

        ' Llamada necesaria para el diseñador.
        InitializeComponent()

        ' Agregue cualquier inicialización después de la llamada a InitializeComponent().
        frmGrupos = unFrm
    End Sub
    Private Sub btnAgregar_Click(sender As Object, e As EventArgs) Handles btnAgregar.Click
        Dim unaC As New clsControladora
        Dim unGru As New clsGrupo(txtNombreGrupo.Text, txtDescripcionGrupo.Text)
        unaC.altaGrupo(unGru)
        limpiar()
        frmGrupos.Activate()
    End Sub
    Private Sub limpiar()
        txtNombreGrupo.Clear()
        txtDescripcionGrupo.Clear()
    End Sub

    Private Sub frmAltaGrupo_Load(sender As Object, e As EventArgs) Handles MyBase.Load

    End Sub
End Class