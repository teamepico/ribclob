﻿Public Class frmRegistro

    Private Sub frmRegistro_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        lblNombre.Text = ""
        lblRol.Text = ""
    End Sub

    Private Sub checkDatos()
        Dim unaC As New clsControladora
        Dim unI As New clsInterno

        unI = unaC.checkInternos(txtCi.Text.ToString)

        If Not unI.nombre = "" Then
            lblNombre.Text = unI.nombre & " " & unI.apellido
            lblRol.Text = "Interno"
        Else
            Dim unD As New clsFuncionarioDocente
            unD = unaC.checkDocentes(txtCi.Text)
            If Not unD.nombre = "" Then
                lblNombre.Text = unD.nombre & " " & unD.apellido
                lblRol.Text = "Docente"
            Else
                Dim unF As New clsFuncionarioNoDocente
                unF = unaC.checkFuncionarios(txtCi.Text)
                If Not unF.nombre = "" Then
                    lblNombre.Text = unF.nombre & " " & unF.apellido
                    lblRol.Text = "Funcionario"
                Else
                    lblNombre.Text = ""
                    lblRol.Text = ""
                End If
            End If
        End If
    End Sub

    Private Sub registro()
        Dim fecha As String = DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss")
        Dim unaC As New clsControladora
        Dim errorsql As String
        MsgBox(fecha)

        If lblRol.Text = "Interno" Then
            errorsql = unaC.altaRegistroInternos(txtCi.Text, fecha)

            If Not errorsql = "" Then
                MsgBox(errorsql)
            End If
        ElseIf lblRol.Text = "Docente" Or lblRol.Text = "Funcionario" Then
            errorsql = unaC.altaRegistroFuncionarios(txtCi.Text, fecha)

            If Not errorsql = "" Then
                MsgBox(errorsql)
            End If
        End If
    End Sub



    Private Sub btnIngresar_Click(sender As Object, e As EventArgs) Handles btnIngresar.Click
        registro()
    End Sub

    Private Sub txtCi_TextChanged(sender As Object, e As EventArgs) Handles txtCi.TextChanged
        checkDatos()
    End Sub
End Class