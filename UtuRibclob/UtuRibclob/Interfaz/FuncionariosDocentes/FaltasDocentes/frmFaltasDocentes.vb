﻿Public Class frmFaltasDocentes
    Dim elUsuario As New clsUser
    Dim elFuncionario As New clsFuncionarioDocente
    Dim frmFaltasDocentes As frmFaltasDocentes
    Public Sub New(elU As clsUser, elF As clsFuncionarioDocente)
        'TODO: Complete member initialization 
        elUsuario = elU
        elFuncionario = elF
        frmFaltasDocentes = Me
        InitializeComponent()
    End Sub

    Public Sub listar()
        Dim unaC As New clsControladora
        Dim colFaltas As New List(Of clsFaltaDocente)
        colFaltas = unaC.listarFaltasDocentes(elFuncionario.ci, txtInicial.Text, txtFinal.Text)

        Dim lsi As New ListViewItem
        lsvFaltas.Items.Clear()

        For Each unaF In colFaltas
            lsi = lsvFaltas.Items.Add(unaF.fecha)
            lsi.SubItems.Add(unaF.descripcion)
            lsi.SubItems.Add(unaF.horarinicio)
            lsi.SubItems.Add(unaF.horafinal)
        Next
    End Sub

    Private Sub frmFaltasDocentes_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        lblUser.Text = elFuncionario.nombre & " " & elFuncionario.apellido

        ' Le da el primer y ultimo dia del mes a los txt de fecha
        Dim FechaActual As Date = Now
        Dim DiasEnMes As Integer = Date.DaysInMonth(FechaActual.Year, FechaActual.Month)
        Dim UltimoDiaEnMes As Date = New Date(FechaActual.Year, FechaActual.Month, DiasEnMes)
        Dim PrimerDiaEnMes As Date = New Date(FechaActual.Year, FechaActual.Month, 1)

        txtInicial.Text = PrimerDiaEnMes
        txtFinal.Text = UltimoDiaEnMes

        listar()
    End Sub

    ' Cuando las fecha cambian automaticamente se vuelve a listar todo
    Private Sub txtInicial_TextChanged(sender As Object, e As EventArgs) Handles txtInicial.TextChanged
        listar()
    End Sub

    Private Sub txtFinal_TextChanged(sender As Object, e As EventArgs) Handles txtFinal.TextChanged
        listar()
    End Sub

    Private Sub btnListar_Click(sender As Object, e As EventArgs)
        listar()
    End Sub

    Private Sub btnSalir_Click(sender As Object, e As EventArgs) Handles btnSalir.Click
        Me.Close()
    End Sub

    Private Sub btnListar_Click_1(sender As Object, e As EventArgs)
        listar()
    End Sub

    Private Sub btnAgregar_Click(sender As Object, e As EventArgs) Handles btnAgregar.Click
        Dim unFrm As New frmAltaFaltaDocente(elFuncionario, Me)
        unFrm.Show()
    End Sub

    Private Sub btnEliminar_Click(sender As Object, e As EventArgs) Handles btnEliminar.Click
        Dim unaE As New clsExcepcion()
        Dim unaFal As New clsFaltaDocente(vbEmpty, lsvFaltas.FocusedItem.SubItems(0).Text, lsvFaltas.FocusedItem.SubItems(1).Text, lsvFaltas.FocusedItem.SubItems(2).Text, lsvFaltas.FocusedItem.SubItems(3).Text, elFuncionario, unaE)

        Dim unFrm As New frmBajaFaltaDocente(unaFal, frmFaltasDocentes)
        unFrm.Show()
    End Sub

    Private Sub btnModificar_Click(sender As Object, e As EventArgs) Handles btnModificar.Click
        Dim unaE As New clsExcepcion()
        Dim unaFal As New clsFaltaDocente(vbEmpty, lsvFaltas.FocusedItem.SubItems(0).Text, lsvFaltas.FocusedItem.SubItems(1).Text, lsvFaltas.FocusedItem.SubItems(2).Text, lsvFaltas.FocusedItem.SubItems(3).Text, elFuncionario, unaE)

        Dim unFrm As New frmModificarFaltaDocente(unaFal, frmFaltasDocentes)
        unFrm.Show()
    End Sub

    Private Sub btnI_Click(sender As Object, e As EventArgs) Handles btnI.Click
        'ACA indicamos las configuraciones del print se igualen a las puestas por el user en la pantalla de PrintDialog

        PrintDialog1.Document = PrintDocument1
        PrintDocument1.PrinterSettings = PrintDialog1.PrinterSettings
        With PrintDocument1
            .PrinterSettings.DefaultPageSettings.Landscape = False
            .Print()

        End With
    End Sub

    Private Sub PrintDocument1_PrintPage(sender As Object, e As Printing.PrintPageEventArgs) Handles PrintDocument1.PrintPage
        ' ACA indicamos los espacios , letras , todo en la impr.
        Dim h As Integer = 0
        h = 50
        e.Graphics.DrawString("Faltas De" & elFuncionario.nombre & elFuncionario.apellido, New Drawing.Font("Times New Roman", 10), Brushes.Black, 50, h)
        h += 50

        For Each itm As ListViewItem In lsvFaltas.Items

            e.Graphics.DrawString(itm.Text, New Drawing.Font("Times New Roman", 10), Brushes.Black, 50, h)
            e.Graphics.DrawString(itm.SubItems(1).Text, New Drawing.Font("Times New Roman", 10), Brushes.Black, 150, h)
            e.Graphics.DrawString(itm.SubItems(2).Text, New Drawing.Font("Times New Roman", 10), Brushes.Black, 250, h)
            e.Graphics.DrawString(itm.SubItems(3).Text, New Drawing.Font("Times New Roman", 10), Brushes.Black, 350, h)
            e.Graphics.DrawString(itm.SubItems(4).Text, New Drawing.Font("Times New Roman", 10), Brushes.Black, 450, h)
          


            h += 20
            ' ponemos  50 150 250 , eso es X en la IMPR. Y es H que cada vez que pasa una fila , hace un +20 y asi con cada una , no hacemos X variable porque no es necesario eso sera algo fijo.

        Next

    End Sub
End Class