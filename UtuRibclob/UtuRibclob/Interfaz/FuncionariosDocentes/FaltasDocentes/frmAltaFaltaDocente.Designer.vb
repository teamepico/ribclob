﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmAltaFaltaDocente
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.btnAgregar = New System.Windows.Forms.Button()
        Me.txtCausal = New System.Windows.Forms.TextBox()
        Me.lblNombreDoc = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.txtFecha = New System.Windows.Forms.DateTimePicker()
        Me.SuspendLayout()
        '
        'btnAgregar
        '
        Me.btnAgregar.Location = New System.Drawing.Point(62, 134)
        Me.btnAgregar.Margin = New System.Windows.Forms.Padding(2)
        Me.btnAgregar.Name = "btnAgregar"
        Me.btnAgregar.Size = New System.Drawing.Size(140, 34)
        Me.btnAgregar.TabIndex = 72
        Me.btnAgregar.Text = "Agregar"
        Me.btnAgregar.UseVisualStyleBackColor = True
        '
        'txtCausal
        '
        Me.txtCausal.Location = New System.Drawing.Point(62, 51)
        Me.txtCausal.Margin = New System.Windows.Forms.Padding(2)
        Me.txtCausal.Multiline = True
        Me.txtCausal.Name = "txtCausal"
        Me.txtCausal.Size = New System.Drawing.Size(177, 53)
        Me.txtCausal.TabIndex = 70
        '
        'lblNombreDoc
        '
        Me.lblNombreDoc.AutoSize = True
        Me.lblNombreDoc.Location = New System.Drawing.Point(195, 20)
        Me.lblNombreDoc.Name = "lblNombreDoc"
        Me.lblNombreDoc.Size = New System.Drawing.Size(39, 13)
        Me.lblNombreDoc.TabIndex = 68
        Me.lblNombreDoc.Text = "Docen"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.Location = New System.Drawing.Point(18, 20)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(119, 13)
        Me.Label6.TabIndex = 67
        Me.Label6.Text = "Agregando Falta a: "
        '
        'txtFecha
        '
        Me.txtFecha.Location = New System.Drawing.Point(62, 109)
        Me.txtFecha.Name = "txtFecha"
        Me.txtFecha.Size = New System.Drawing.Size(200, 20)
        Me.txtFecha.TabIndex = 73
        '
        'frmAltaFaltaDocente
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(585, 455)
        Me.Controls.Add(Me.txtFecha)
        Me.Controls.Add(Me.btnAgregar)
        Me.Controls.Add(Me.txtCausal)
        Me.Controls.Add(Me.lblNombreDoc)
        Me.Controls.Add(Me.Label6)
        Me.Name = "frmAltaFaltaDocente"
        Me.ShowIcon = False
        Me.ShowInTaskbar = False
        Me.Text = "Agregar aviso de falta"
        Me.TopMost = True
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents btnAgregar As System.Windows.Forms.Button
    Friend WithEvents txtCausal As System.Windows.Forms.TextBox
    Friend WithEvents lblNombreDoc As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents txtFecha As System.Windows.Forms.DateTimePicker
End Class
