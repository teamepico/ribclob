﻿Public Class frmModificarFaltaDocente

    Dim falta As New clsFaltaDocente
    Dim excepcion As New clsExcepcion
    Dim frmFalta As frmFaltasDocentes

    Private Sub frmModificarFaltaNoDocente_Load(sender As Object, e As EventArgs) Handles MyBase.Load

        lvlDocente.Text = falta.docente.nombre & "  " & falta.docente.apellido
        lvlFecha.Text = falta.fecha
        lvlHorainicio.Text = falta.horarinicio
        lvlHorafinal.Text = falta.horafinal

    End Sub


    Public Sub New(faltaNoDocente As clsFaltaDocente, frmFaltasDocentes As frmFaltasDocentes)
        falta = faltaNoDocente
        frmFalta = frmFaltasDocentes
        ' Llamada necesaria para el diseñador.
        InitializeComponent()

        ' Agregue cualquier inicialización después de la llamada a InitializeComponent().

    End Sub

    Private Sub btnAceptar_Click(sender As Object, e As EventArgs) Handles btnAceptar.Click
        Dim unaC As New clsControladora
        Dim unaFalta As New clsFaltaDocente(vbEmpty, lvlFecha.Text, txtCausal.Text, lvlHorainicio.Text, lvlHorafinal.Text, falta.docente, excepcion)
        Dim errorsql As String
        errorsql = unaC.modificarFaltasDocentes(unaFalta)

        If errorsql = "" Then
            imgTick.Visible = True
            Timer1.Enabled = True
            frmFalta.Activate()
        End If
        

            

    End Sub

    Private Sub Timer1_Tick(sender As Object, e As EventArgs) Handles Timer1.Tick
        imgTick.Visible = False
        Timer1.Enabled = False
    End Sub

End Class
