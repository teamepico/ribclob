﻿Public Class frmAltaFaltaDocente
    Dim botones As New List(Of clsBoton)
    Dim colHorarios As New List(Of clsDocentes_horariosdocente)
    Dim docente As New clsFuncionarioDocente
    Private frmFalta As frmFaltasDocentes
    Dim idHorarios As New List(Of String)

    Private Function listarHorarios() As List(Of clsDocentes_horariosdocente)
        Dim unaC As New clsControladora
        Return unaC.listarHorarioHasDocente(docente.ci)
    End Function

    Public Sub botonDinamicoHorario(colHorarios As List(Of clsDocentes_horariosdocente))

        Dim x = 400, y = 20
        Dim n As Integer = 0

        For Each unHorario In colHorarios

            Dim boton As New clsBoton
            botones.Add(boton)
            botones(n).Visible = True
            botones(n).Width = 100
            botones(n).Height = 50
            botones(n).BackColor = Color.White
            botones(n).Location = New Point(x, y) 'x,y
            botones(n).ForeColor = Color.Black
            botones(n).Name = "btn" & (n + 1)
            botones(n).entrada = unHorario.horario.entrada
            botones(n).Text = (botones(n).entrada)
            botones(n).salida = unHorario.horario.salida
            botones(n).Text = botones(n).Text & vbCr & botones(n).salida
            botones(n).diaSemanal = unHorario.diaHorario
            botones(n).Text = botones(n).Text & vbCr & botones(n).diaSemanal
            botones(n).Font = New Font(FontFamily.GenericSansSerif, 10.0F, FontStyle.Bold)

            boton.idHorario = unHorario.horario.IdHorarioDocente

            If ((n + 1) Mod 2 = 0) Then
                y += 110
                x = 270
            End If

            AddHandler botones(n).Click, AddressOf eventoClick
            Me.Controls.Add(botones(n))

            x += 130
            n += 1
        Next

    End Sub

    Private Sub eventoClick(sender As Object, e As EventArgs)
        If CType(sender, clsBoton).presionado = False Then
            CType(sender, clsBoton).BackColor = Color.Blue
            CType(sender, clsBoton).presionado = True
        Else
            CType(sender, clsBoton).BackColor = Color.White
            CType(sender, clsBoton).presionado = False
        End If
    End Sub
    Public Sub recorrerFalta()
        Dim fecha As Date
        Dim fechaAsString As String
        fecha = txtFecha.Value
        fechaAsString = fecha.ToString("yyyy-MM-dd hh:mm:ss")

        If fechaAsString >= Today Then

            For Each boton In botones
                Dim unaC As New clsControladora
                If boton.presionado = True Then
                    Dim unaE As New clsExcepcion()
                    Dim unaFalta As New clsFaltaDocente(vbEmpty, fechaAsString, txtCausal.Text, boton.entrada, boton.salida, docente, unaE)
                    Dim errorsql As String
                    errorsql = unaC.altaFaltasDocentes(unaFalta)
                    If errorsql = "" Then
                        frmFalta.Activate()
                        Me.Close()
                    Else
                        MsgBox("No se pudo agregar una falta")
                        Console.Write(errorsql)
                    End If

                End If
            Next

        Else

            MsgBox("Debe ingresar una falta futura, para cambiar causal modifique la falta ya existente")

        End If
    End Sub

    Private Sub frmAltaFaltaDocente_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        colHorarios = listarHorarios()
        botonDinamicoHorario(colHorarios)
        recorrerFalta()
        lblNombreDoc.Text = docente.nombre & "  " & docente.apellido

    End Sub

    Private Sub btnAgregar_Click(sender As Object, e As EventArgs) Handles btnAgregar.Click

        recorrerFalta()
    End Sub
    Public Sub New(funcionario As clsFuncionarioDocente, frm As frmFaltasDocentes)
        docente = funcionario
        frmFalta = frm

        ' Llamada necesaria para el diseñador.
        InitializeComponent()

        ' Agregue cualquier inicialización después de la llamada a InitializeComponent().

    End Sub




End Class