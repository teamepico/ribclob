﻿Public Class frmBajaFaltaDocente
    Dim unaFalta As clsFaltaDocente
    Private fechaActual As Date = DateTime.Now.ToString("yyyy/MM/dd")
    Private fechaFalta As Date
    Dim frmFalta As frmFaltasDocentes
    Public Sub New(unaFal As clsFaltaDocente, unFrm As frmFaltasDocentes)
        'TODO: Complete member initialization 
        unaFalta = unaFal
        InitializeComponent()
        frmFalta = unFrm
    End Sub


    Private Sub btnConfirmar_Click(sender As Object, e As EventArgs) Handles btnConfirmar.Click
        Dim resultadoSQL As String
        Dim unaC As New clsControladora
        resultadoSQL = unaC.bajaFaltaDocente(unaFalta)
        If Not resultadoSQL = "" Then
            MsgBox(resultadoSQL)
        ElseIf resultadoSQL = "" Then
            frmFalta.Activate()
            Me.Close()
        End If
    End Sub

    Private Sub btnSalir_Click(sender As Object, e As EventArgs) Handles btnSalir.Click
        Me.Close()
    End Sub

    Private Sub frmBajaFaltaDocente_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Dim fechaFalta As Date = Date.ParseExact(unaFalta.fecha, "dd/MM/yyyy", System.Globalization.DateTimeFormatInfo.InvariantInfo)

        If fechaFalta = fechaActual Or fechaActual > fechaFalta Then
            MsgBox("Solo se pueden modificar faltas que sean avisos.")
            Me.Close()
        End If

    End Sub
End Class