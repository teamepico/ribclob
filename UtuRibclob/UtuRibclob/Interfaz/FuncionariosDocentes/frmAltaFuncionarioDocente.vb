﻿Public Class frmAltaFuncionarioDocente
    Dim frmDocentes As frmFuncionariosDocentes

    Private Sub frmAltaFuncionarioDocente_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        imgTick.Visible = False
    End Sub

    Public Sub New(frmDocente As frmFuncionariosDocentes)
        frmDocentes = frmDocente
        ' Llamada necesaria para el diseñador.
        InitializeComponent()

        ' Agregue cualquier inicialización después de la llamada a InitializeComponent().

    End Sub

    Private Sub btnAgregar_Click(sender As Object, e As EventArgs) Handles btnAgregar.Click

        Dim unFun As New clsFuncionarioDocente(txtCI.Text.ToString, txtNombre.Text, txtApellido.Text, txtCorreo.Text, txtTelefono.Text, txtDireccion.Text)
            Dim unaC As New clsControladora
            Dim errorsql As String
            errorsql = unaC.altaFuncionarioDocente(unFun)
            If errorsql = "" Then
            imgTick.Visible = True
            Timer1.Enabled = True

                frmDocentes.Activate()
                Me.Activate()
            ElseIf errorsql = "Ya existe un Funcionario Docente con ese CI" Then
                MsgBox("Ya existe un Funcionario Docente con ese CI")
            Else
                MsgBox("Ha ocurrido un error, intentelo mas tarde")
            MsgBox(errorsql)
            End If
            
        limpiar()
    End Sub

    Public Sub limpiar()
        txtCI.Text = ""
        txtNombre.Text = ""
        txtApellido.Text = ""
        txtCorreo.Text = ""
        txtDireccion.Text = ""
        txtTelefono.Text = ""
    End Sub

    Public Function verificarPersona(ByVal cCi As String) As Boolean
        Dim unaC As New clsControladora
        Return unaC.verificarPersona(cCi)
    End Function



    Private Sub Timer1_Tick(sender As Object, e As EventArgs) Handles Timer1.Tick
        imgTick.Visible = False
        Timer1.Enabled = False
    End Sub

    Private Sub imgTick_Click(sender As Object, e As EventArgs) Handles imgTick.Click

    End Sub
End Class