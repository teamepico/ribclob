﻿Public Class frmFuncionariosDocentes
    Dim elUsuario As New clsUser
    Dim frmDocente As frmFuncionariosDocentes
    Dim seleccionEsVacia As Boolean = False

    Public Function seleccionVacia() As Boolean
        Dim devolver As Boolean = False

        If lsvFuncionarios.FocusedItem Is Nothing Then
            MsgBox("No se ha seleccionado ningun funcionario.")
            devolver = True
        End If

        Return devolver
    End Function

    Public Sub New(elU As clsUser)
        ' TODO: Complete member initialization 
        elUsuario = elU
        frmDocente = Me
        InitializeComponent()
    End Sub

    Private Sub Listar()
        Dim unaC As New clsControladora
        Dim colUsuarios = unaC.listarFuncionarioDocentes
        Dim lsi As New ListViewItem
        lsvFuncionarios.Items.Clear()

        For Each unFun In colUsuarios
            lsi = lsvFuncionarios.Items.Add(unFun.nombre)
            lsi.SubItems.Add(unFun.apellido)
            lsi.SubItems.Add(unFun.ci)
            lsi.SubItems.Add(unFun.numero)
            lsi.SubItems.Add(unFun.correo)
            lsi.SubItems.Add(unFun.direccion)
        Next
    End Sub

    Public Sub enviar()
        Dim unFun As New clsFuncionarioDocente(lsvFuncionarios.SelectedItems.Item(2).Text, lsvFuncionarios.SelectedItems.Item(0).Text, lsvFuncionarios.SelectedItems.Item(1).Text, lsvFuncionarios.SelectedItems.Item(4).Text, lsvFuncionarios.SelectedItems.Item(3).Text, lsvFuncionarios.SelectedItems.Item(5).Text)
        Dim unForm As New frmHorarioDocente(elUsuario)
        unForm.Show()
    End Sub


    Private Sub frmFuncionariosDocentes_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Listar()
    End Sub

    Private Sub btnAgregar_Click(sender As Object, e As EventArgs) Handles btnAgregar.Click
        Dim unFrm As New frmAltaFuncionarioDocente(frmDocente)
        unFrm.Show()
    End Sub

    Private Sub lsvFuncionarios_SelectedIndexChanged(sender As Object, e As EventArgs) Handles lsvFuncionarios.SelectedIndexChanged
        Dim datoCi As String = lsvFuncionarios.FocusedItem.SubItems(2).Text
    End Sub


    Private Sub btnHorarios_Click(sender As Object, e As EventArgs) Handles btnHorarios.Click
        seleccionEsVacia = seleccionVacia()

        If seleccionEsVacia = False Then
            Dim datoPersona As New clsFuncionarioDocente(lsvFuncionarios.FocusedItem.SubItems(2).Text, lsvFuncionarios.FocusedItem.SubItems(0).Text, lsvFuncionarios.FocusedItem.SubItems(1).Text, lsvFuncionarios.FocusedItem.SubItems(4).Text, lsvFuncionarios.FocusedItem.SubItems(3).Text, vbEmpty)
            Dim frmHorarios As New frmDocente_has_horario(elUsuario, datoPersona)

            frmHorarios.Show()
        End If
    End Sub

    Private Sub btnSalir_Click(sender As Object, e As EventArgs) Handles btnSalir.Click
        Me.Close()
    End Sub

    Private Sub btnBorrar_Click(sender As Object, e As EventArgs) Handles btnBorrar.Click
        seleccionEsVacia = seleccionVacia()

        If seleccionEsVacia = False Then
            Dim unDocente As New clsFuncionarioDocente(lsvFuncionarios.FocusedItem.SubItems(2).Text, lsvFuncionarios.FocusedItem.SubItems(0).Text, lsvFuncionarios.FocusedItem.SubItems(1).Text, vbEmpty, vbEmpty, vbEmpty)
            Dim unFormEliminar As New frmBajaDocente(unDocente, elUsuario, Me)
            unFormEliminar.Show()
        End If
    End Sub

    Private Sub btnListar_Click(sender As Object, e As EventArgs)
        Listar()
    End Sub

    Private Sub btnModificar_Click(sender As Object, e As EventArgs) Handles btnModificar.Click
        seleccionEsVacia = seleccionVacia()

        If seleccionEsVacia = False Then
            Dim unFun As New clsFuncionarioDocente(lsvFuncionarios.FocusedItem.SubItems(2).Text, lsvFuncionarios.FocusedItem.SubItems(0).Text, lsvFuncionarios.FocusedItem.SubItems(1).Text, lsvFuncionarios.FocusedItem.SubItems(4).Text, lsvFuncionarios.FocusedItem.SubItems(3).Text, lsvFuncionarios.FocusedItem.SubItems(5).Text)
            Dim unForm As New frmModificarFuncionarioDocente(unFun, elUsuario, frmDocente)
            unForm.Show()
        End If
    End Sub


    Private Sub frmFuncionariosDocentes_Activated(sender As Object, e As EventArgs) Handles Me.Activated
        Listar()
    End Sub

    Private Sub txtBuscar_TextChanged(sender As Object, e As EventArgs) Handles txtBuscar.TextChanged
        Dim unaC As New clsControladora
        Dim colFun As New List(Of clsFuncionarioDocente)

        colFun = unaC.buscarFuncionariosDocentes(txtBuscar.Text)

        Dim lsi As New ListViewItem
        lsvFuncionarios.Items.Clear()

        For Each unFun In colFun
            lsi = lsvFuncionarios.Items.Add(unFun.nombre)
            lsi.SubItems.Add(unFun.apellido)
            lsi.SubItems.Add(unFun.ci)
            lsi.SubItems.Add(unFun.numero)
            lsi.SubItems.Add(unFun.correo)
            lsi.SubItems.Add(unFun.direccion)
        Next
    End Sub

    Private Sub frmFuncionariosDocentes_Resize(sender As Object, e As EventArgs) Handles Me.Resize
        Dim lsv_x = Me.Size.Width
        Dim lsv_y = Me.Size.Height
        lsv_x = lsv_x - 40
        lsv_y = lsv_y - 156
        Me.lsvFuncionarios.Size = New System.Drawing.Size(lsv_x, lsv_y)

        Dim btnAgregar_x = btnAgregar.Location.X
        Dim btnModificar_x = btnModificar.Location.X
        Dim btnEliminar_x = btnBorrar.Location.X
        Dim btnHorario_x = btnHorarios.Location.X
        Dim btnStrikes_x = btnStrikes.Location.X
        Dim btnSalir_x = Me.Size.Width
        Dim btnRegistro_x = btnRegistros.Location.X
        Dim btnFaltas_x = btnFaltas.Location.X
        Dim SalirSize = btnSalir.Size.Width / 2

        Dim btnFunciones_y = Me.Size.Height
        Dim btnFuncionario_y = Me.Size.Height
        btnFuncionario_y = btnFuncionario_y - 81
        btnFunciones_y = btnFunciones_y - 118

        btnSalir_x = btnSalir_x - 81 - SalirSize

        Me.btnHorarios.Location = New System.Drawing.Point(btnHorario_x, btnFuncionario_y)
        Me.btnAgregar.Location = New System.Drawing.Point(btnAgregar_x, btnFunciones_y)
        Me.btnModificar.Location = New System.Drawing.Point(btnModificar_x, btnFunciones_y)
        Me.btnBorrar.Location = New System.Drawing.Point(btnEliminar_x, btnFunciones_y)
        Me.btnSalir.Location = New System.Drawing.Point(btnSalir_x, btnFuncionario_y)
        Me.btnFaltas.Location = New System.Drawing.Point(btnFaltas_x, btnFuncionario_y)
        Me.btnRegistros.Location = New System.Drawing.Point(btnRegistro_x, btnFuncionario_y)
        Me.btnStrikes.Location = New System.Drawing.Point(btnStrikes_x, btnFuncionario_y)
    End Sub

    Private Sub btnRegistros_Click(sender As Object, e As EventArgs) Handles btnRegistros.Click
        seleccionEsVacia = seleccionVacia()

        If seleccionEsVacia = False Then
            Dim datoPersona As New clsFuncionario(lsvFuncionarios.FocusedItem.SubItems(2).Text, lsvFuncionarios.FocusedItem.SubItems(0).Text, lsvFuncionarios.FocusedItem.SubItems(1).Text, lsvFuncionarios.FocusedItem.SubItems(4).Text, lsvFuncionarios.FocusedItem.SubItems(3).Text, vbEmpty)
            Dim unFrm As New frmRegistroFuncionario(elUsuario, datoPersona)
            unFrm.Show()
        End If
    End Sub

    Private Sub btnStrikes_Click(sender As Object, e As EventArgs) Handles btnStrikes.Click
        seleccionEsVacia = seleccionVacia()

        If seleccionEsVacia = False Then
            Dim datoPersona As New clsFuncionarioDocente(lsvFuncionarios.FocusedItem.SubItems(2).Text, lsvFuncionarios.FocusedItem.SubItems(0).Text, lsvFuncionarios.FocusedItem.SubItems(1).Text, lsvFuncionarios.FocusedItem.SubItems(4).Text, lsvFuncionarios.FocusedItem.SubItems(3).Text, vbEmpty)
            Dim unFrm As New frmStrikes(elUsuario, datoPersona)
            unFrm.Show()
        End If
    End Sub

    Private Sub btnFaltas_Click(sender As Object, e As EventArgs) Handles btnFaltas.Click
        seleccionEsVacia = seleccionVacia()

        If seleccionEsVacia = False Then
            Dim datoPersona As New clsFuncionarioDocente(lsvFuncionarios.FocusedItem.SubItems(2).Text, lsvFuncionarios.FocusedItem.SubItems(0).Text, lsvFuncionarios.FocusedItem.SubItems(1).Text, lsvFuncionarios.FocusedItem.SubItems(4).Text, lsvFuncionarios.FocusedItem.SubItems(3).Text, vbEmpty)
            Dim unFrm As New frmFaltasDocentes(elUsuario, datoPersona)
            unFrm.Show()
        End If
    End Sub
End Class