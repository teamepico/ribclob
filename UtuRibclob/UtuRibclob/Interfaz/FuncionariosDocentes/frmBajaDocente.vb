﻿Public Class frmBajaDocente
    Private bajaDocente As New clsFuncionarioDocente
    Private datosUsuario As New clsUser
    Private frmDocente As frmFuncionariosDocentes

    Private frmInternos As frmReporteListadoNoDocente
    Private Sub frmBajaDocente_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        lblUsuario.Text = bajaDocente.nombre & " " & bajaDocente.apellido
        lblCI.Text = bajaDocente.ci
    End Sub

    Public Sub New(bajaD As clsFuncionarioDocente, unU As clsUser, unFrm As frmFuncionariosDocentes)
        bajaDocente = bajaD
        datosUsuario = unU
        frmDocente = unFrm
        InitializeComponent()
    End Sub

    Private Sub btnEliminar_Click(sender As Object, e As EventArgs) Handles btnEliminar.Click
        Dim resultadoSQL As String
        Dim unaC As New clsControladora
        resultadoSQL = unaC.bajaDocente(bajaDocente.ci)

        If Not resultadoSQL = "" Then
            MsgBox(resultadoSQL)
        ElseIf resultadoSQL = "Cannot delete or update a parent row: a foreign key constraint fails (`ribclob`.`faltasdocentes`, CONSTRAINT `fk_FaltasDocentes_Docentes1` FOREIGN KEY (`Docentes_Funcionarios_Persona_ciPersona`) REFERENCES `docentes` (`Funcionarios_Persona_ciPersona`) ON DELETE )" Then
            MsgBox("Existen horarios asociados a este docente.")
        Else
            frmDocente.Activate()
            Me.Close()
        End If
    End Sub

    'comento esto

    Private Sub btnCancelar_Click(sender As Object, e As EventArgs) Handles btnCancelar.Click
        Me.Close()
    End Sub
End Class