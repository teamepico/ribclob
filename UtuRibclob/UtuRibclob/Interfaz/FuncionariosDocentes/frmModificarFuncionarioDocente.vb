﻿Public Class frmModificarFuncionarioDocente
    Dim datosFuncionario As New clsFuncionarioDocente
    Dim datosUsuario As New clsUser
    Private frmFuncionariosDocentes As frmFuncionariosDocentes

    Public Sub New(unFuncionario As clsFuncionario, elUsuario As clsUser, frm As frmFuncionariosDocentes)

        ' Llamada necesaria para el diseñador.
        InitializeComponent()

        ' Agregue cualquier inicialización después de la llamada a InitializeComponent().
        datosFuncionario = unFuncionario
        datosUsuario = elUsuario
        frmFuncionariosDocentes = frm
    End Sub

    Private Sub frmModificarFuncionarioDocente_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        txtCI.Text = datosFuncionario.ci
        txtNombre.Text = datosFuncionario.nombre
        txtApellido.Text = datosFuncionario.apellido
        txtTelefono.Text = datosFuncionario.numero
        txtDireccion.Text = datosFuncionario.direccion
        txtCorreo.Text = datosFuncionario.correo
    End Sub

    Private Sub btnAgregar_Click(sender As Object, e As EventArgs) Handles btnAgregar.Click
        Dim unFun As New clsFuncionarioDocente(txtCI.Text.ToString, txtNombre.Text, txtApellido.Text, txtCorreo.Text, txtTelefono.Text, txtDireccion.Text.ToString)
        Dim unC As New clsControladora
        Dim errorSQL As String
        errorSQL = unC.modificarFuncionarioDocente(unFun)

        If Not errorSQL = Nothing Then
            MsgBox(errorSQL)
        ElseIf errorSQL = Nothing Then
            frmFuncionariosDocentes.Activate()
            Me.Close()
        End If

    End Sub



End Class