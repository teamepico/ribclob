﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmStrikes
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmStrikes))
        Me.btnSalir = New System.Windows.Forms.Button()
        Me.lsvSanciones = New System.Windows.Forms.ListView()
        Me.Descripcion = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.lblUser = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.PrintDocument1 = New System.Drawing.Printing.PrintDocument()
        Me.PrintDialog1 = New System.Windows.Forms.PrintDialog()
        Me.btnI = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'btnSalir
        '
        Me.btnSalir.Location = New System.Drawing.Point(783, 620)
        Me.btnSalir.Margin = New System.Windows.Forms.Padding(4)
        Me.btnSalir.Name = "btnSalir"
        Me.btnSalir.Size = New System.Drawing.Size(156, 37)
        Me.btnSalir.TabIndex = 1
        Me.btnSalir.Text = "Salir"
        Me.btnSalir.UseVisualStyleBackColor = True
        '
        'lsvSanciones
        '
        Me.lsvSanciones.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.Descripcion})
        Me.lsvSanciones.GridLines = True
        Me.lsvSanciones.Location = New System.Drawing.Point(16, 78)
        Me.lsvSanciones.Margin = New System.Windows.Forms.Padding(4)
        Me.lsvSanciones.Name = "lsvSanciones"
        Me.lsvSanciones.Size = New System.Drawing.Size(921, 534)
        Me.lsvSanciones.TabIndex = 6
        Me.lsvSanciones.UseCompatibleStateImageBehavior = False
        Me.lsvSanciones.View = System.Windows.Forms.View.Details
        '
        'Descripcion
        '
        Me.Descripcion.Text = "Descripcion"
        Me.Descripcion.Width = 917
        '
        'lblUser
        '
        Me.lblUser.AutoSize = True
        Me.lblUser.Font = New System.Drawing.Font("Segoe UI", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblUser.Location = New System.Drawing.Point(253, 22)
        Me.lblUser.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lblUser.Name = "lblUser"
        Me.lblUser.Size = New System.Drawing.Size(198, 32)
        Me.lblUser.TabIndex = 11
        Me.lblUser.Text = "Nombre Apellido"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Segoe UI", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(68, 22)
        Me.Label1.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(171, 32)
        Me.Label1.TabIndex = 10
        Me.Label1.Text = "Sanciones de:"
        '
        'PrintDocument1
        '
        '
        'PrintDialog1
        '
        Me.PrintDialog1.UseEXDialog = True
        '
        'btnI
        '
        Me.btnI.BackgroundImage = Global.UtuRibclob.My.Resources.Resources.print
        Me.btnI.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnI.Location = New System.Drawing.Point(16, 618)
        Me.btnI.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.btnI.Name = "btnI"
        Me.btnI.Size = New System.Drawing.Size(45, 38)
        Me.btnI.TabIndex = 26
        Me.btnI.UseVisualStyleBackColor = True
        '
        'frmStrikes
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(952, 656)
        Me.Controls.Add(Me.btnI)
        Me.Controls.Add(Me.lblUser)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.lsvSanciones)
        Me.Controls.Add(Me.btnSalir)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Margin = New System.Windows.Forms.Padding(4)
        Me.MaximizeBox = False
        Me.MaximumSize = New System.Drawing.Size(970, 703)
        Me.MinimumSize = New System.Drawing.Size(970, 703)
        Me.Name = "frmStrikes"
        Me.Text = "Sanciones de docente"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents btnSalir As System.Windows.Forms.Button
    Friend WithEvents lsvSanciones As System.Windows.Forms.ListView
    Friend WithEvents Descripcion As System.Windows.Forms.ColumnHeader
    Friend WithEvents lblUser As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents PrintDocument1 As System.Drawing.Printing.PrintDocument
    Friend WithEvents PrintDialog1 As System.Windows.Forms.PrintDialog
    Friend WithEvents btnI As System.Windows.Forms.Button
End Class
