﻿Public Class frmStrikes
    Dim elUsuario As New clsUser
    Dim elFuncionario As New clsFuncionarioDocente

    Public Sub New(elU As clsUser, elF As clsFuncionarioDocente)
        'TODO: Complete member initialization 
        elUsuario = elU
        elFuncionario = elF
        InitializeComponent()
    End Sub

    Public Sub listar()
        Dim unaC As New clsControladora
        Dim colSancion As New List(Of clsSancion)
        colSancion = unaC.listarSanciones(elFuncionario.ci)

        Dim lsi As New ListViewItem
        lsvSanciones.Items.Clear()

        For Each unaS In colSancion
            lsi = lsvSanciones.Items.Add(unaS.descripcion)
        Next
    End Sub

    Private Sub frmRegistroInternos_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        lblUser.Text = elFuncionario.nombre & " " & elFuncionario.apellido

        listar()
    End Sub

    Private Sub btnListar_Click(sender As Object, e As EventArgs)
        listar()
    End Sub

    Private Sub btnSalir_Click(sender As Object, e As EventArgs) Handles btnSalir.Click
        Me.Close()
    End Sub

    Private Sub btnListar_Click_1(sender As Object, e As EventArgs)
        listar()
    End Sub

    Private Sub btnI_Click(sender As Object, e As EventArgs) Handles btnI.Click
        'ACA indicamos las configuraciones del print se igualen a las puestas por el user en la pantalla de PrintDialog

        PrintDialog1.Document = PrintDocument1
        PrintDocument1.PrinterSettings = PrintDialog1.PrinterSettings
        With PrintDocument1
            .PrinterSettings.DefaultPageSettings.Landscape = False
            .Print()

        End With
    End Sub

    Private Sub PrintDocument1_PrintPage(sender As Object, e As Printing.PrintPageEventArgs) Handles PrintDocument1.PrintPage
        ' ACA indicamos los espacios , letras , todo en la impr.
        Dim h As Integer = 0
        h = 50
        e.Graphics.DrawString("Sanciones de " & elFuncionario.nombre & elFuncionario.apellido, New Drawing.Font("Times New Roman", 10), Brushes.Black, 50, h)
        h += 50

        For Each itm As ListViewItem In lsvSanciones.Items

            e.Graphics.DrawString(itm.Text, New Drawing.Font("Times New Roman", 10), Brushes.Black, 50, h)


            h += 20
            ' ponemos  50 150 250 , eso es X en la IMPR. Y es H que cada vez que pasa una fila , hace un +20 y asi con cada una , no hacemos X variable porque no es necesario eso sera algo fijo.

        Next
    End Sub
End Class