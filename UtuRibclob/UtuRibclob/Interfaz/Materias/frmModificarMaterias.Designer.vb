﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmModificarMaterias
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.btnAgregarMateria = New System.Windows.Forms.Button()
        Me.txtDescripcionMateria = New System.Windows.Forms.TextBox()
        Me.txtNombreMateria = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.SuspendLayout()
        '
        'btnAgregarMateria
        '
        Me.btnAgregarMateria.Location = New System.Drawing.Point(135, 182)
        Me.btnAgregarMateria.Name = "btnAgregarMateria"
        Me.btnAgregarMateria.Size = New System.Drawing.Size(124, 33)
        Me.btnAgregarMateria.TabIndex = 26
        Me.btnAgregarMateria.Text = "Confirmar"
        Me.btnAgregarMateria.UseVisualStyleBackColor = True
        '
        'txtDescripcionMateria
        '
        Me.txtDescripcionMateria.Location = New System.Drawing.Point(135, 95)
        Me.txtDescripcionMateria.Multiline = True
        Me.txtDescripcionMateria.Name = "txtDescripcionMateria"
        Me.txtDescripcionMateria.Size = New System.Drawing.Size(137, 52)
        Me.txtDescripcionMateria.TabIndex = 25
        '
        'txtNombreMateria
        '
        Me.txtNombreMateria.Location = New System.Drawing.Point(135, 45)
        Me.txtNombreMateria.Name = "txtNombreMateria"
        Me.txtNombreMateria.ReadOnly = True
        Me.txtNombreMateria.Size = New System.Drawing.Size(137, 20)
        Me.txtNombreMateria.TabIndex = 24
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(12, 95)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(63, 13)
        Me.Label2.TabIndex = 23
        Me.Label2.Text = "Descripcion"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(12, 45)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(44, 13)
        Me.Label1.TabIndex = 22
        Me.Label1.Text = "Nombre"
        '
        'frmModificarMaterias
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(284, 231)
        Me.Controls.Add(Me.btnAgregarMateria)
        Me.Controls.Add(Me.txtDescripcionMateria)
        Me.Controls.Add(Me.txtNombreMateria)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.MaximizeBox = False
        Me.MaximumSize = New System.Drawing.Size(300, 270)
        Me.MinimizeBox = False
        Me.MinimumSize = New System.Drawing.Size(300, 270)
        Me.Name = "frmModificarMaterias"
        Me.ShowIcon = False
        Me.ShowInTaskbar = False
        Me.Text = "Modificar materia"
        Me.TopMost = True
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents btnAgregarMateria As System.Windows.Forms.Button
    Friend WithEvents txtDescripcionMateria As System.Windows.Forms.TextBox
    Friend WithEvents txtNombreMateria As System.Windows.Forms.TextBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
End Class
