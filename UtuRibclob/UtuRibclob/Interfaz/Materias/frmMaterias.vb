﻿Public Class frmMaterias
    Dim elUsuario As New clsUser
    Private frmMaterias As frmMaterias
    Dim seleccionEsVacia As Boolean = False

    Public Sub New(usuario As clsUser)

        ' Llamada necesaria para el diseñador.
        InitializeComponent()

        ' Agregue cualquier inicialización después de la llamada a InitializeComponent().
        elUsuario = usuario
        frmMaterias = Me
    End Sub

    Public Function seleccionVacia() As Boolean
        Dim devolver As Boolean = False

        If lsvMaterias.FocusedItem Is Nothing Then
            MsgBox("No se ha seleccionado ninguna materia.")
            devolver = True
        End If

        Return devolver
    End Function

    Public Sub listar()
        Dim unaC As New clsControladora
        Dim colMaterias = unaC.listarMaterias()

        Dim lsi As New ListViewItem
        lsvMaterias.Items.Clear()

        For Each unI In colMaterias
            lsi = lsvMaterias.Items.Add(unI.NombreMateria)
            lsi.SubItems.Add(unI.DescripcionMateria)

        Next
    End Sub

    Private Sub btnListar_Click(sender As Object, e As EventArgs)
        listar()
    End Sub

    Private Sub btnModificar_Click(sender As Object, e As EventArgs) Handles btnModificar.Click
        seleccionEsVacia = seleccionVacia()

        If seleccionEsVacia = False Then
            Dim unMat As New clsMateria(lsvMaterias.FocusedItem.SubItems(0).Text, lsvMaterias.FocusedItem.SubItems(1).Text)
            Dim unForm As New frmModificarMaterias(unMat, frmMaterias)
            unForm.Show()
        End If
    End Sub

    Private Sub btnSalir_Click(sender As Object, e As EventArgs) Handles btnSalir.Click
        Me.Close()
    End Sub

    Private Sub btnAgregar_Click(sender As Object, e As EventArgs) Handles btnAgregar.Click
        Dim unFormulario As New frmAltaMaterias(frmMaterias)
        unFormulario.Show()
    End Sub

    Private Sub btnEliminar_Click_1(sender As Object, e As EventArgs) Handles btnEliminar.Click
        seleccionEsVacia = seleccionVacia()

        If seleccionEsVacia = False Then
            Dim unaMateria As New clsMateria(lsvMaterias.FocusedItem.SubItems(0).Text, lsvMaterias.FocusedItem.SubItems(1).Text)
            Dim unForm As New frmBajaMaterias(elUsuario, unaMateria, frmMaterias)
            unForm.Show()
        End If
    End Sub

    Private Sub frmMaterias_Activated(sender As Object, e As EventArgs) Handles Me.Activated
        listar()
    End Sub

    Private Sub frmMaterias_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Me.MinimumSize = New Size(872, 467)
        listar()
    End Sub

    Private Sub txtBuscar_TextChanged(sender As Object, e As EventArgs) Handles txtBuscar.TextChanged
        Dim unaC As New clsControladora
        Dim colMaterias As New List(Of clsMateria)

        colMaterias = unaC.buscarMaterias(txtBuscar.Text)
        Dim lsi As New ListViewItem
        lsvMaterias.Items.Clear()

        For Each unaMateria In colMaterias
            lsi = lsvMaterias.Items.Add(unaMateria.NombreMateria)
            lsi.SubItems.Add(unaMateria.DescripcionMateria)
        Next
    End Sub

    Private Sub frmMaterias_Resize(sender As Object, e As EventArgs) Handles Me.Resize
        Dim lsv_x = Me.Size.Width
        Dim lsv_y = Me.Size.Height
        lsv_x = lsv_x - 40
        lsv_y = lsv_y - 129
        Me.lsvMaterias.Size = New System.Drawing.Size(lsv_x, lsv_y)

        Dim btnAgregar_x = btnAgregar.Location.X
        Dim btnModificar_x = btnModificar.Location.X
        Dim btnEliminar_x = btnEliminar.Location.X
        Dim btnSalir_x = Me.Size.Width
        Dim SalirSize = btnSalir.Size.Width / 2

        Dim btn_y = Me.Size.Height
        btn_y = btn_y - 81

        btnSalir_x = btnSalir_x - 81 - SalirSize

        Me.btnAgregar.Location = New System.Drawing.Point(btnAgregar_x, btn_y)
        Me.btnModificar.Location = New System.Drawing.Point(btnModificar_x, btn_y)
        Me.btnEliminar.Location = New System.Drawing.Point(btnEliminar_x, btn_y)
        Me.btnSalir.Location = New System.Drawing.Point(btnSalir_x, btn_y)
    End Sub
End Class