﻿Public Class frmBajaMaterias
    Dim unaMateria As New clsMateria
    Dim unUsuario As New clsUser
    Private frmMaterias As frmMaterias

    Public Sub New(usuario As clsUser, materia As clsMateria, unFrm As frmMaterias)

        ' Llamada necesaria para el diseñador.
        InitializeComponent()

        ' Agregue cualquier inicialización después de la llamada a InitializeComponent().
        unaMateria = materia
        unUsuario = usuario
        frmMaterias = unFrm
    End Sub

    Private Sub frmBajaMaterias_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        lblMateria.Text = unaMateria.NombreMateria
    End Sub

    Private Sub btnEliminar_Click(sender As Object, e As EventArgs) Handles btnEliminar.Click
        Dim unaC As New clsControladora

        Dim resultadoSQL As String

        resultadoSQL = unaC.bajaMateria(unaMateria.NombreMateria)

        If Not resultadoSQL = Nothing Then
            MsgBox(resultadoSQL)
        ElseIf resultadoSQL = Nothing Then
            frmMaterias.Activate()
            Me.Close()
        End If

    End Sub

    Private Sub btnCancelar_Click(sender As Object, e As EventArgs) Handles btnCancelar.Click
        Me.Close()
    End Sub
End Class