﻿Public Class frmAltaMaterias
    Private frmMaterias As frmMaterias

    Public Sub New(unFrm As frmMaterias)

        ' Llamada necesaria para el diseñador.
        InitializeComponent()

        ' Agregue cualquier inicialización después de la llamada a InitializeComponent().
        frmMaterias = unFrm
    End Sub

    Private Sub frmAltaMaterias_Load(sender As Object, e As EventArgs) Handles MyBase.Load

    End Sub

    Private Sub btnAgregarMateria_Click(sender As Object, e As EventArgs) Handles btnAgregarMateria.Click
        Dim unaC As New clsControladora
        Dim unMat As New clsMateria(txtNombreMateria.Text, txtDescripcionMateria.Text)
        unMat.altaMateria(unMat)
        limpiar()
        frmMaterias.Activate()
    End Sub

    Private Sub limpiar()
        txtNombreMateria.Clear()
        txtDescripcionMateria.Clear()
    End Sub

End Class