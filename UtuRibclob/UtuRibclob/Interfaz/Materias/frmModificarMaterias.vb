﻿Public Class frmModificarMaterias

    Dim datosMateria As New clsMateria
    Private frmMaterias As frmMaterias


    Public Sub New(unMat As clsMateria, unFrm As frmMaterias)

        ' Llamada necesaria para el diseñador.
        InitializeComponent()

        ' Agregue cualquier inicialización después de la llamada a InitializeComponent().
        datosMateria = unMat
        frmMaterias = unFrm
    End Sub

    Private Sub frmModificarFuncionarioDocente_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        txtNombreMateria.Text = datosMateria.NombreMateria
        txtDescripcionMateria.Text = datosMateria.DescripcionMateria
    End Sub

    Private Sub btnAgregar_Click(sender As Object, e As EventArgs) Handles btnAgregarMateria.Click
        Dim unMateria As New clsMateria(txtNombreMateria.Text, txtDescripcionMateria.Text)
        Dim unC As New clsControladora
        Dim errorSQL As String
        errorSQL = unC.modificarMateria(unMateria)


    End Sub

End Class