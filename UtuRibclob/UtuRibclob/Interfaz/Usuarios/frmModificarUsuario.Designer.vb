﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmModificarUsuario
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.lblApellido = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.btnAgregar = New System.Windows.Forms.Button()
        Me.cmbTipo = New System.Windows.Forms.ComboBox()
        Me.lblContrasena = New System.Windows.Forms.Label()
        Me.txtContrasena = New System.Windows.Forms.TextBox()
        Me.txtCI = New System.Windows.Forms.MaskedTextBox()
        Me.SuspendLayout()
        '
        'lblApellido
        '
        Me.lblApellido.AutoSize = True
        Me.lblApellido.Location = New System.Drawing.Point(30, 146)
        Me.lblApellido.Name = "lblApellido"
        Me.lblApellido.Size = New System.Drawing.Size(28, 13)
        Me.lblApellido.TabIndex = 3
        Me.lblApellido.Text = "Tipo"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(30, 59)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(17, 13)
        Me.Label3.TabIndex = 5
        Me.Label3.Text = "CI"
        '
        'btnAgregar
        '
        Me.btnAgregar.Location = New System.Drawing.Point(184, 208)
        Me.btnAgregar.Name = "btnAgregar"
        Me.btnAgregar.Size = New System.Drawing.Size(124, 33)
        Me.btnAgregar.TabIndex = 10
        Me.btnAgregar.Text = "Confirmar"
        Me.btnAgregar.UseVisualStyleBackColor = True
        '
        'cmbTipo
        '
        Me.cmbTipo.FormattingEnabled = True
        Me.cmbTipo.Items.AddRange(New Object() {"Administador", "Adscripto"})
        Me.cmbTipo.Location = New System.Drawing.Point(171, 138)
        Me.cmbTipo.Name = "cmbTipo"
        Me.cmbTipo.Size = New System.Drawing.Size(137, 21)
        Me.cmbTipo.TabIndex = 12
        '
        'lblContrasena
        '
        Me.lblContrasena.AutoSize = True
        Me.lblContrasena.Location = New System.Drawing.Point(30, 103)
        Me.lblContrasena.Name = "lblContrasena"
        Me.lblContrasena.Size = New System.Drawing.Size(61, 13)
        Me.lblContrasena.TabIndex = 14
        Me.lblContrasena.Text = "Contraseña"
        '
        'txtContrasena
        '
        Me.txtContrasena.Location = New System.Drawing.Point(171, 96)
        Me.txtContrasena.Name = "txtContrasena"
        Me.txtContrasena.Size = New System.Drawing.Size(137, 20)
        Me.txtContrasena.TabIndex = 13
        '
        'txtCI
        '
        Me.txtCI.Location = New System.Drawing.Point(171, 52)
        Me.txtCI.Mask = "0000000-0"
        Me.txtCI.Name = "txtCI"
        Me.txtCI.ReadOnly = True
        Me.txtCI.Size = New System.Drawing.Size(137, 20)
        Me.txtCI.TabIndex = 15
        '
        'frmModificarUsuario
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(333, 264)
        Me.Controls.Add(Me.txtCI)
        Me.Controls.Add(Me.lblContrasena)
        Me.Controls.Add(Me.txtContrasena)
        Me.Controls.Add(Me.cmbTipo)
        Me.Controls.Add(Me.btnAgregar)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.lblApellido)
        Me.MaximizeBox = False
        Me.MaximumSize = New System.Drawing.Size(349, 303)
        Me.MinimizeBox = False
        Me.MinimumSize = New System.Drawing.Size(349, 303)
        Me.Name = "frmModificarUsuario"
        Me.ShowIcon = False
        Me.ShowInTaskbar = False
        Me.Text = "Modificar usuario"
        Me.TopMost = True
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents lblApellido As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents btnAgregar As System.Windows.Forms.Button
    Friend WithEvents cmbTipo As System.Windows.Forms.ComboBox
    Friend WithEvents lblContrasena As System.Windows.Forms.Label
    Friend WithEvents txtContrasena As System.Windows.Forms.TextBox
    Friend WithEvents txtCI As System.Windows.Forms.MaskedTextBox
End Class
