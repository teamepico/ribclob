﻿Public Class frmModificarUsuario
    Dim datosUsuario As New clsUser
    Dim modificarUsuario As New clsUser
    Private frmUsuario As frmUsuarios

    Public Sub New(modUser As clsUser, user As clsUser, unFrm As frmUsuarios)
        frmUsuario = unFrm
        datosUsuario = user
        modificarUsuario = modUser
        InitializeComponent()
    End Sub

    Private Sub frmModificarUsuario_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        txtContrasena.Text = modificarUsuario.contrasena
        cmbTipo.Text = modificarUsuario.tipo
        txtCI.Text = modificarUsuario.ci
    End Sub

    Private Sub btnAgregar_Click(sender As Object, e As EventArgs) Handles btnAgregar.Click
        Dim unU As New clsUser(txtCI.Text.ToString, vbEmpty, vbEmpty, vbEmpty, vbEmpty, vbEmpty, cmbTipo.Text, txtContrasena.Text)
        Dim unaC As New clsControladora
        unaC.modificarUsuario(unU)
        frmUsuario.Activate()
        Me.Close()
    End Sub

    Public Function esVacio() As Boolean
        Dim vacio As Boolean
        vacio = False
        If txtCI.Text.ToString Or txtContrasena.Text Or cmbTipo.Text.ToString Is Nothing Then
            vacio = True
        End If
        Return vacio
    End Function

    Private Sub cmbTipo_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cmbTipo.SelectedIndexChanged

    End Sub
End Class