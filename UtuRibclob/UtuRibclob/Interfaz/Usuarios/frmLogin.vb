﻿Public Class frmLogin

    Private Sub btnLogin_Click(sender As Object, e As EventArgs) Handles btnLogin.Click
        Dim unaControladora As New clsControladora()

        Dim elUsuario = unaControladora.login(txtCI.Text.ToString, txtContrasena.Text)

        If Not elUsuario.ci = "" Then
            Dim unFrmPrincipal As New frmPrincipal(elUsuario)
            unFrmPrincipal.Show()
            Me.Hide()
        ElseIf elUsuario.ci = "" Then
            MsgBox("El usuario no existe o algo está escrito incorrectamente")
        Else
            MsgBox("Algo salió horriblemente mal y el programa no sabe que hacer")
        End If


    End Sub

    Private Sub frmLogin_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        txtCI.Select()
        Me.AcceptButton = btnLogin
    End Sub

End Class