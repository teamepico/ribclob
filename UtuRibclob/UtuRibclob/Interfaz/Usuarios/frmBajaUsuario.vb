﻿Public Class frmBajaUsuario
    Dim bajaUsuario As New clsUser
    Dim datosUsuario As New clsUser
    Private frmUsuario As frmUsuarios

    Private Sub frmAltaInterno_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        lblUsuario.Text = bajaUsuario.nombre & " " & bajaUsuario.apellido
        lblCI.Text = datosUsuario.ci
    End Sub

    Public Sub New(bajaUser As clsUser, unU As clsUser, unFrm As frmUsuarios)
        bajaUsuario = bajaUser
        datosUsuario = unU
        frmUsuario = unFrm
        InitializeComponent()
    End Sub

    Private Sub btnEliminar_Click(sender As Object, e As EventArgs) Handles btnCancelar.Click
        Me.Close()
    End Sub

    Private Sub btnEliminar_Click_1(sender As Object, e As EventArgs) Handles btnEliminar.Click
        Dim resultadoSQL
        Dim unaC As New clsControladora
        resultadoSQL = unaC.bajaUsuario(bajaUsuario.ci)
        If Not resultadoSQL = Nothing Then
            MsgBox(resultadoSQL)
        ElseIf resultadoSQL = Nothing Then
            frmUsuario.Activate()
            Me.Close()
        End If
        
    End Sub
End Class