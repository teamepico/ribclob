﻿Public Class frmAltaUsuario
    Private frmUsuario As frmUsuarios

    Public Sub New(unFrm As frmUsuarios)

        ' Llamada necesaria para el diseñador.
        InitializeComponent()

        ' Agregue cualquier inicialización después de la llamada a InitializeComponent().
        frmUsuario = unFrm
    End Sub

    Private Sub frmAltaInterno_Load(sender As Object, e As EventArgs) Handles MyBase.Load

    End Sub

    Private Sub btnAgregar_Click(sender As Object, e As EventArgs) Handles btnAgregar.Click
        Dim vacio As Boolean = esVacio()
        If vacio = False Then
            Dim unU As New clsUser(txtCI.Text.ToString, vbEmpty, vbEmpty, vbEmpty, vbEmpty, txtCI.Text, cmbTipo.Text, txtContrasena.Text)
            Dim unaC As New clsControladora
            Dim resultadoSQL As String
            resultadoSQL = unaC.altaUsuario(unU)

            If resultadoSQL = "No existe CI" Then
                MsgBox("No existe un funcionario con la Cedula de Identidad ingresada. Por favor, primero ingrese el funcionario al sistema y luego ingrese su usuario.")
                Me.Close()
            Else
                frmUsuario.Activate()
                Me.Close()
            End If
            ' MsgBox para debuggear
            'MsgBox("k here it goes, ci: " & txtCI.Text.ToString & " usuario: " & txtUsuario.Text & " tipo:" & cmbTipo.Text.ToString & " xD ")
        Else
            MsgBox("No pueden existir campos vacíos.")
        End If
    End Sub

    Public Function esVacio() As Boolean
        Dim vacio As Boolean
        vacio = False
        If txtCI.Text.ToString = "" Or txtContrasena.Text = "" Or cmbTipo.Text.ToString = "" Then
            vacio = True
        End If
        Return vacio
    End Function
End Class