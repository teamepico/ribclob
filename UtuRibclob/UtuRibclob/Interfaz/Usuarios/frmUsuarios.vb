﻿Public Class frmUsuarios
    Dim elUsuario As New clsUser
    Private frmUsuarios As frmUsuarios
    Dim seleccionEsVacia As Boolean = False

    Public Function seleccionVacia() As Boolean
        Dim devolver As Boolean = False

        If lsvUsuarios.FocusedItem Is Nothing Then
            MsgBox("No se ha seleccionado ningun usuario.")
            devolver = True
        End If

        Return devolver
    End Function

    Public Sub New(elU As clsUser)
        ' TODO: Complete member initialization 
        elUsuario = elU
        frmUsuarios = Me
        InitializeComponent()
    End Sub

    Public Sub listar()
        Dim unaC As New clsControladora
        Dim colUsuario As New List(Of clsUser)
        colUsuario = unaC.listarUsuarios()

        Dim lsi As New ListViewItem
        lsvUsuarios.Items.Clear()

        For Each unU In colUsuario
            lsi = lsvUsuarios.Items.Add(unU.ci)
            lsi.SubItems.Add(unU.nombre)
            lsi.SubItems.Add(unU.apellido)
            lsi.SubItems.Add(unU.contrasena)
            lsi.SubItems.Add(unU.tipo)
        Next
    End Sub

    Private Sub frmUsuarios_Activated(sender As Object, e As EventArgs) Handles Me.Activated
        listar()
    End Sub

    Private Sub frmFuncionariosDocentes_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Me.MinimumSize = New Size(785, 566)
        listar()
    End Sub

    Private Sub btnAgregar_Click(sender As Object, e As EventArgs) Handles btnAgregar.Click
        Dim frmAlta As New frmAltaUsuario(frmUsuarios)
        frmAlta.Show()
    End Sub

    Private Sub btnSalir_Click(sender As Object, e As EventArgs) Handles btnSalir.Click
        Me.Close()
    End Sub

    Private Sub btnModificar_Click(sender As Object, e As EventArgs) Handles btnModificar.Click
        seleccionEsVacia = seleccionVacia()

        If seleccionEsVacia = False Then
            Dim modUsuario As New clsUser(lsvUsuarios.FocusedItem.SubItems(0).Text, lsvUsuarios.FocusedItem.SubItems(1).Text, lsvUsuarios.FocusedItem.SubItems(2).Text, vbEmpty, vbEmpty, vbEmpty, lsvUsuarios.FocusedItem.SubItems(4).Text, lsvUsuarios.FocusedItem.SubItems(3).Text)
            Dim unForm As New frmModificarUsuario(modUsuario, elUsuario, frmUsuarios)
            unForm.Show()
        End If
    End Sub

    Private Sub btnEliminar_Click(sender As Object, e As EventArgs) Handles btnEliminar.Click
        seleccionEsVacia = seleccionVacia()

        If seleccionEsVacia = False Then
            Dim bajaUsuario As New clsUser(lsvUsuarios.FocusedItem.SubItems(0).Text, lsvUsuarios.FocusedItem.SubItems(1).Text, lsvUsuarios.FocusedItem.SubItems(2).Text, vbEmpty, vbEmpty, vbEmpty, lsvUsuarios.FocusedItem.SubItems(4).Text, lsvUsuarios.FocusedItem.SubItems(3).Text)
            Dim unForm As New frmBajaUsuario(bajaUsuario, elUsuario, frmUsuarios)
            unForm.Show()
        End If
    End Sub

    Private Sub txtBuscar_TextChanged(sender As Object, e As EventArgs) Handles txtBuscar.TextChanged
        Dim unaC As New clsControladora
        Dim colUsuario As New List(Of clsUser)
        colUsuario = unaC.buscarUsuarios(txtBuscar.Text)

        Dim lsi As New ListViewItem
        lsvUsuarios.Items.Clear()

        For Each unU In colUsuario
            lsi = lsvUsuarios.Items.Add(unU.ci)
            lsi.SubItems.Add(unU.nombre)
            lsi.SubItems.Add(unU.apellido)
            lsi.SubItems.Add(unU.contrasena)
            lsi.SubItems.Add(unU.tipo)
        Next
    End Sub

    Private Sub frmUsuarios_Resize(sender As Object, e As EventArgs) Handles Me.Resize
        Dim lsv_x = Me.Size.Width
        Dim lsv_y = Me.Size.Height
        lsv_x = lsv_x - 40
        lsv_y = lsv_y - 129
        Me.lsvUsuarios.Size = New System.Drawing.Size(lsv_x, lsv_y)

        Dim btnAgregar_x = btnAgregar.Location.X
        Dim btnModificar_x = btnModificar.Location.X
        Dim btnEliminar_x = btnEliminar.Location.X
        Dim btnSalir_x = Me.Size.Width
        Dim SalirSize = btnSalir.Size.Width / 2

        Dim btn_y = Me.Size.Height
        btn_y = btn_y - 81

        btnSalir_x = btnSalir_x - 81 - SalirSize

        Me.btnAgregar.Location = New System.Drawing.Point(btnAgregar_x, btn_y)
        Me.btnModificar.Location = New System.Drawing.Point(btnModificar_x, btn_y)
        Me.btnEliminar.Location = New System.Drawing.Point(btnEliminar_x, btn_y)
        Me.btnSalir.Location = New System.Drawing.Point(btnSalir_x, btn_y)
    End Sub
End Class