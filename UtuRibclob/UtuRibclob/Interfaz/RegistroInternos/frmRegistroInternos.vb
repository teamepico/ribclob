﻿Public Class frmRegistroInternos
    Dim elUsuario As New clsUser
    Dim elInternado As New clsInterno

    Public Sub New(elU As clsUser, elI As clsInterno)
        'TODO: Complete member initialization 
        elUsuario = elU
        elInternado = elI
        InitializeComponent()
    End Sub

    Public Sub listar()
        Dim unaC As New clsControladora
        Dim colRegistro As New List(Of clsRegistroInternos)
        colRegistro = unaC.listarRegistroInternos(elInternado.ci, txtInicial.Text, txtFinal.Text)

        Dim lsi As New ListViewItem
        lsvSalones.Items.Clear()

        For Each unI In colRegistro
            If unI.residencia = 1 Then
                lsi = lsvSalones.Items.Add("Salida")
            ElseIf unI.residencia = 0 Then
                lsi = lsvSalones.Items.Add("Entrada")
            End If
            lsi.SubItems.Add(unI.fecha)
        Next
    End Sub

    Private Sub frmRegistroInternos_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        lblUser.Text = elInternado.nombre & " " & elInternado.apellido

        ' Le da el primer y ultimo dia del mes a los txt de fecha
        Dim FechaActual As Date = Now
        Dim DiasEnMes As Integer = Date.DaysInMonth(FechaActual.Year, FechaActual.Month)
        Dim UltimoDiaEnMes As Date = New Date(FechaActual.Year, FechaActual.Month, DiasEnMes)
        Dim PrimerDiaEnMes As Date = New Date(FechaActual.Year, FechaActual.Month, 1)

        txtInicial.Text = PrimerDiaEnMes
        txtFinal.Text = UltimoDiaEnMes

        listar()
    End Sub

    ' Cuando las fecha cambian automaticamente se vuelve a listar todo
    Private Sub txtInicial_TextChanged(sender As Object, e As EventArgs) Handles txtInicial.TextChanged
        listar()
    End Sub

    Private Sub txtFinal_TextChanged(sender As Object, e As EventArgs) Handles txtFinal.TextChanged
        listar()
    End Sub

    Private Sub btnListar_Click(sender As Object, e As EventArgs)
        listar()
    End Sub

    Private Sub btnSalir_Click(sender As Object, e As EventArgs) Handles btnSalir.Click
        Me.Close()
    End Sub

    Private Sub btnListar_Click_1(sender As Object, e As EventArgs)
        listar()
    End Sub

    Private Sub frmRegistroInternos_Resize(sender As Object, e As EventArgs) Handles Me.Resize
        Dim lsv_x = Me.Size.Width
        Dim lsv_y = Me.Size.Height
        lsv_x = lsv_x - 31
        lsv_y = lsv_y - 197
        Me.lsvSalones.Size = New System.Drawing.Size(lsv_x, lsv_y)

        Dim btnSalir_x = Me.Size.Width
        Dim SalirSize = btnSalir.Size.Width / 2

        Dim btn_y = Me.Size.Height
        btn_y = btn_y - 81

        btnSalir_x = btnSalir_x - 81 - SalirSize

        Me.btnSalir.Location = New System.Drawing.Point(btnSalir_x, btn_y)
    End Sub
End Class