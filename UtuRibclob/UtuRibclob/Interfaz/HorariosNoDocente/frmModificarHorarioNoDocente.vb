﻿Public Class frmModificarHorarioNoDocente
    Private frmHorarioNoDocente As frmHorarioNoDocente
    Dim unHorario As New clsHorarioNoDocente

    Dim horaEntrada As Integer
    Dim horaSalida As Integer
    Dim minutoEntrada As Integer
    Dim minutoSalida As Integer

    Dim horaEsCorrecta As Boolean = False

    Public Sub New(unH As clsHorarioNoDocente, unFrm As frmHorarioNoDocente)

        ' Llamada necesaria para el diseñador.
        InitializeComponent()

        ' Agregue cualquier inicialización después de la llamada a InitializeComponent().
        unHorario = unH
        frmHorarioNoDocente = unFrm
    End Sub

    'sub proceso de limpiar
    Private Sub limpiar()
        txtCI.Clear()
        txtEntrada.Clear()
        txtSalida.Clear()
        txtDia.Items.Clear()
    End Sub

    Private Sub btnAgregar_Click(sender As Object, e As EventArgs) Handles btnAgregar.Click
        Dim unaC As New clsControladora

        If horaSalida > horaEntrada Then
            horaEsCorrecta = True
        ElseIf horaEntrada = horaSalida And minutoSalida > minutoEntrada Then
            horaEsCorrecta = True
        End If

        If horaEsCorrecta = True Then
            Dim unFun As New clsFuncionarioNoDocente(txtCI.Text, "", "", "", vbEmpty, "")
            Dim horarioNuevo As New clsHorarioNoDocente(vbEmpty, txtEntrada.Text, txtSalida.Text, txtDia.Text, unFun)
            Dim errorSql As String
            errorSql = unaC.modificarHorarioNoDocente(horarioNuevo, unHorario)

            If errorSql = "" Then
                frmHorarioNoDocente.Activate()
                Me.Close()
            Else
                MsgBox("Error al actualizar el horario.")
            End If
        Else
            MsgBox("El horario de salida no puede ser menor a el de entrada.")
        End If


    End Sub

    Private Sub txtEntrada_Validating(sender As Object, e As System.ComponentModel.CancelEventArgs) Handles txtEntrada.Validating
        Dim value As String = txtEntrada.Text.Replace(":", "").Trim
        If value = String.Empty Then Return

        Dim hora As Int32
        If Int32.TryParse(value.Substring(0, 2), hora) = False Then
            e.Cancel = True
            Return
        Else
            If hora < 0 OrElse hora > 23 Then
                MsgBox("Ese horario es incorrecto, ingrese el horario en formato de 24hs.")
                e.Cancel = True
                Return
            End If
        End If

        Dim minuto As Int32
        If Int32.TryParse(value.Substring(2, 2), minuto) = False Then
            e.Cancel = True
            Return
        Else
            If minuto < 0 OrElse minuto > 59 Then
                e.Cancel = True
                MsgBox("Ese horario es incorrecto, ingrese el horario en formato de 24hs.")
                Return
            End If
        End If

        horaEntrada = hora
        minutoEntrada = minuto
    End Sub

    Private Sub txtSalida_Validating(sender As Object, e As System.ComponentModel.CancelEventArgs) Handles txtSalida.Validating
        Dim value As String = txtSalida.Text.Replace(":", "").Trim
        Dim horarioSalida As String = ""

        If value = String.Empty Then Return

        Dim hora As Int32
        If Int32.TryParse(value.Substring(0, 2), hora) = False Then
            e.Cancel = True
            Return
        Else
            If hora < 0 OrElse hora > 23 Then
                e.Cancel = True
                MsgBox("Ese horario es incorrecto, ingrese el horario en formato de 24hs.")
                Return
            End If
        End If

        Dim minuto As Int32
        If Int32.TryParse(value.Substring(2, 2), minuto) = False Then
            e.Cancel = True
            Return
        Else
            If minuto < 0 OrElse minuto > 59 Then
                e.Cancel = True
                MsgBox("Ese horario es incorrecto, ingrese el horario en formato de 24hs.")
                Return
            End If
        End If

        horaSalida = hora
        minutoSalida = minuto
    End Sub

    Private Sub frmAltaHorarioNoDocente_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        txtEntrada.Text = unHorario.entradaHorarioNoDocente
        txtSalida.Text = unHorario.salidaHorarioNoDocente
        txtDia.Text = unHorario.diaSemanalHorarioNoDocente
        txtCI.Text = unHorario.funcionario.ci
    End Sub
End Class