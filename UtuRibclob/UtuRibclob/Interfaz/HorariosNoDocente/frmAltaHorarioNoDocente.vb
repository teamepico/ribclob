﻿Public Class frmAltaHorarioNoDocente
    Private frmHorarioNoDocente As frmHorarioNoDocente
    Dim horaEntrada As Integer
    Dim horaSalida As Integer
    Dim minutoEntrada As Integer
    Dim minutoSalida As Integer

    Dim horaEsCorrecta As Boolean = False

    ''Pre set de ci cuando viene de el form funcionarios no docente
    Public Sub preSeted(ci As String, unFrm As frmHorarioNoDocente)
        txtCI.Text = ci
        frmHorarioNoDocente = unFrm
    End Sub

    'sub proceso de limpiar
    Private Sub limpiar()
        txtEntrada.Clear()

    End Sub

    Private Sub btnAgregar_Click(sender As Object, e As EventArgs) Handles btnAgregar.Click
        Dim unaC As New clsControladora



        
        If horaSalida > horaEntrada Then
            horaEsCorrecta = True
        ElseIf horaEntrada = horaSalida And minutoSalida > minutoEntrada Then
            horaEsCorrecta = True
        End If

        If horaEsCorrecta = True Then
            Dim unFun As New clsFuncionarioNoDocente(txtCI.Text, "", "", "", vbEmpty, "")
            Dim unHorario As New clsHorarioNoDocente(vbEmpty, txtEntrada.Text, txtSalida.Text, txtDia.Text, unFun)
            Dim errorSql As String
            errorSql = unaC.altaHorarioNoDocente(unHorario)
            limpiar()

            If errorSql = "" Then

                imgTick.Visible = True
                Timer1.Enabled = True
                frmHorarioNoDocente.Activate()
                Me.Activate()
            Else
                MsgBox("Error al insertar el horario")
            End If
        Else
            MsgBox("El horario de salida no puede ser menor a el de entrada.")

        End If
        'Else
        'MsgBox("El horario de salida no puede ser menor a el de entrada.")
        'End If


    End Sub

    Private Sub txtEntrada_Validating(sender As Object, e As System.ComponentModel.CancelEventArgs) Handles txtEntrada.Validating
        Dim value As String = txtEntrada.Text.Replace(":", "").Trim
        If value = String.Empty Then Return

        Dim hora As Int32
        If Int32.TryParse(value.Substring(0, 2), hora) = False Then
            e.Cancel = True
            Return
        Else
            If hora < 0 OrElse hora > 23 Then
                MsgBox("Ese horario es incorrecto, ingrese el horario en formato de 24hs.")
                e.Cancel = True
                Return
            End If
        End If

        Dim minuto As Int32
        If Int32.TryParse(value.Substring(2, 2), minuto) = False Then
            e.Cancel = True
            Return
        Else
            If minuto < 0 OrElse minuto > 59 Then
                e.Cancel = True
                MsgBox("Ese horario es incorrecto, ingrese el horario en formato de 24hs.")
                Return
            End If
        End If

        horaEntrada = hora
        minutoEntrada = minuto
    End Sub

    Private Sub txtSalida_Validating(sender As Object, e As System.ComponentModel.CancelEventArgs) Handles txtSalida.Validating
        Dim value As String = txtSalida.Text.Replace(":", "").Trim
        Dim horarioSalida As String = ""

        If value = String.Empty Then Return

        Dim hora As Int32
        If Int32.TryParse(value.Substring(0, 2), hora) = False Then
            e.Cancel = True
            Return
        Else
            If hora < 0 OrElse hora > 23 Then
                e.Cancel = True
                MsgBox("Ese horario es incorrecto, ingrese el horario en formato de 24hs.")
                Return
            End If
        End If

        Dim minuto As Int32
        If Int32.TryParse(value.Substring(2, 2), minuto) = False Then
            e.Cancel = True
            Return
        Else
            If minuto < 0 OrElse minuto > 59 Then
                e.Cancel = True
                MsgBox("Ese horario es incorrecto, ingrese el horario en formato de 24hs.")
                Return
            End If
        End If

        horaSalida = hora
        minutoSalida = minuto
    End Sub

    Private Sub frmAltaHorarioNoDocente_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        txtDia.SelectedItem = "Lunes"
    End Sub

    Private Sub imgTick_Click(sender As Object, e As EventArgs) Handles imgTick.Click

    End Sub

    Private Sub Timer1_Tick(sender As Object, e As EventArgs) Handles Timer1.Tick
        imgTick.Visible = False
        Timer1.Enabled = False
    End Sub
End Class