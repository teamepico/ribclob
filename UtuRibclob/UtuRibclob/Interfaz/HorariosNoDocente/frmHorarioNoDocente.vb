﻿Public Class frmHorarioNoDocente
    Dim elUsuario As New clsUser
    Dim datosFuncionario As clsFuncionarioNoDocente
    Private frmHorarioNoDocente As frmHorarioNoDocente
    Dim seleccionEsVacia As Boolean = False

    Public Sub New(elU As clsUser, persona As clsFuncionarioNoDocente)
        ' TODO: Complete member initialization 
        elUsuario = elU
        datosFuncionario = persona
        frmHorarioNoDocente = Me

        InitializeComponent()
    End Sub

    Public Function seleccionVacia() As Boolean
        Dim devolver As Boolean = False

        If lsvHorarios.FocusedItem Is Nothing Then
            MsgBox("No se ha seleccionado ningun horario.")
            devolver = True
        End If

        Return devolver
    End Function

    Private Sub frmHorarioNoDocente_Activated(sender As Object, e As EventArgs) Handles Me.Activated
        Listar()
    End Sub

    Private Sub frmHorarioNoDocente_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Listar()
        frmHorarioNoDocente = Me

        lblUser.Text = " " & datosFuncionario.nombre & " " & datosFuncionario.apellido
        Me.MinimumSize = New Size(788, 583)
    End Sub

    Private Sub Listar()
        Dim unaC As New clsControladora
        Dim colHorarios = unaC.listarHorarioNoDocente(datosFuncionario.ci)
        Dim lsi As New ListViewItem
        lsvHorarios.Items.Clear()

        For Each unHorario In colHorarios
            lsi = lsvHorarios.Items.Add(unHorario.entradaHorarioNoDocente)
            lsi.SubItems.Add(unHorario.salidaHorarioNoDocente)
            lsi.SubItems.Add(unHorario.diaSemanalHorarioNoDocente)
            lsi.SubItems.Add(unHorario.idHorarioNoDocente)
        Next
    End Sub

    Private Sub btnAgregar_Click(sender As Object, e As EventArgs) Handles btnAgregar.Click
        Dim agregarForm As New frmAltaHorarioNoDocente
        agregarForm.preSeted(datosFuncionario.ci, frmHorarioNoDocente)
        agregarForm.Show()
    End Sub

    Private Sub frmHorarioNoDocente_Resize(sender As Object, e As EventArgs) Handles Me.Resize
        Dim lsv_x = Me.Size.Width
        Dim lsv_y = Me.Size.Height
        lsv_x = lsv_x - 30
        lsv_y = lsv_y - 150
        Me.lsvHorarios.Size = New System.Drawing.Size(lsv_x, lsv_y)

        Dim btnAgregar_x = btnAgregar.Location.X
        Dim btnI_x = btnI.Location.X
        Dim btnModificar_x = btnModificar.Location.X
        Dim btnEliminar_x = btnEliminar.Location.X
        Dim btnSalir_x = Me.Size.Width
        Dim SalirSize = btnSalir.Size.Width / 2

        Dim btn_y = Me.Size.Height
        btn_y = btn_y - 81

        btnSalir_x = btnSalir_x - 81 - SalirSize

        Me.btnAgregar.Location = New System.Drawing.Point(btnAgregar_x, btn_y)
        Me.btnI.Location = New System.Drawing.Point(btnI_x, btn_y)
        Me.btnModificar.Location = New System.Drawing.Point(btnModificar_x, btn_y)
        Me.btnEliminar.Location = New System.Drawing.Point(btnEliminar_x, btn_y)
        Me.btnSalir.Location = New System.Drawing.Point(btnSalir_x, btn_y)
    End Sub

    Private Sub btnModificar_Click(sender As Object, e As EventArgs) Handles btnModificar.Click
        seleccionEsVacia = seleccionVacia()

        If seleccionEsVacia = False Then
            Dim unHorario As New clsHorarioNoDocente(vbEmpty, lsvHorarios.FocusedItem.SubItems(0).Text, lsvHorarios.FocusedItem.SubItems(1).Text, lsvHorarios.FocusedItem.SubItems(2).Text, datosFuncionario)
            Dim unFrm As New frmModificarHorarioNoDocente(unHorario, frmHorarioNoDocente)
            unFrm.Show()
        End If
    End Sub

    Private Sub btnEliminar_Click(sender As Object, e As EventArgs) Handles btnEliminar.Click
        seleccionEsVacia = seleccionVacia()

        If seleccionEsVacia = False Then
            Dim unHorario As New clsHorarioNoDocente(vbEmpty, lsvHorarios.FocusedItem.SubItems(0).Text, lsvHorarios.FocusedItem.SubItems(1).Text, lsvHorarios.FocusedItem.SubItems(2).Text, datosFuncionario)
            Dim unFrm As New frmBajaHorarioNoDocente(unHorario, datosFuncionario, frmHorarioNoDocente)
            unFrm.Show()
        End If
    End Sub

    Private Sub btnSalir_Click(sender As Object, e As EventArgs) Handles btnSalir.Click
        Me.Close()
    End Sub

    Private Sub btnI_Click(sender As Object, e As EventArgs) Handles btnI.Click
        PrintDialog1.Document = PrintDocument1
        PrintDocument1.PrinterSettings = PrintDialog1.PrinterSettings
        With PrintDocument1
            .PrinterSettings.DefaultPageSettings.Landscape = False
            .Print()

        End With
    End Sub

    Private Sub PrintDocument1_PrintPage(sender As Object, e As Printing.PrintPageEventArgs) Handles PrintDocument1.PrintPage
        ' ACA indicamos los espacios , letras , todo en la impr.
        Dim h As Integer = 0
        h = 50
        e.Graphics.DrawString("Horarios de funcionarios no docentes: " & datosFuncionario.nombre & "  " & datosFuncionario.ci, New Drawing.Font("Times New Roman", 10), Brushes.Black, 50, h)
        h += 50

        For Each itm As ListViewItem In lsvHorarios.Items

            e.Graphics.DrawString(itm.Text, New Drawing.Font("Times New Roman", 10), Brushes.Black, 50, h)
            e.Graphics.DrawString(itm.SubItems(1).Text, New Drawing.Font("Times New Roman", 10), Brushes.Black, 150, h)
            e.Graphics.DrawString(itm.SubItems(2).Text, New Drawing.Font("Times New Roman", 10), Brushes.Black, 250, h)

            h += 20
            ' ponemos  50 150 250 , eso es X en la IMPR. Y es H que cada vez que pasa una fila , hace un +20 y asi con cada una , no hacemos X variable porque no es necesario eso sera algo fijo.
        Next
    End Sub
End Class