﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmAltaHorarioNoDocente
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.txtCI = New System.Windows.Forms.MaskedTextBox()
        Me.btnAgregar = New System.Windows.Forms.Button()
        Me.lblTelefono = New System.Windows.Forms.Label()
        Me.lblCI = New System.Windows.Forms.Label()
        Me.lblApellido = New System.Windows.Forms.Label()
        Me.lblCorreo = New System.Windows.Forms.Label()
        Me.txtDia = New System.Windows.Forms.ComboBox()
        Me.txtEntrada = New System.Windows.Forms.MaskedTextBox()
        Me.txtSalida = New System.Windows.Forms.MaskedTextBox()
        Me.Timer1 = New System.Windows.Forms.Timer(Me.components)
        Me.imgTick = New System.Windows.Forms.PictureBox()
        CType(Me.imgTick, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'txtCI
        '
        Me.txtCI.Location = New System.Drawing.Point(181, 186)
        Me.txtCI.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.txtCI.Mask = "0000000-0"
        Me.txtCI.Name = "txtCI"
        Me.txtCI.ReadOnly = True
        Me.txtCI.Size = New System.Drawing.Size(145, 22)
        Me.txtCI.TabIndex = 38
        '
        'btnAgregar
        '
        Me.btnAgregar.Location = New System.Drawing.Point(163, 249)
        Me.btnAgregar.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.btnAgregar.Name = "btnAgregar"
        Me.btnAgregar.Size = New System.Drawing.Size(165, 41)
        Me.btnAgregar.TabIndex = 37
        Me.btnAgregar.Text = "Confirmar"
        Me.btnAgregar.UseVisualStyleBackColor = True
        '
        'lblTelefono
        '
        Me.lblTelefono.AutoSize = True
        Me.lblTelefono.Location = New System.Drawing.Point(23, 97)
        Me.lblTelefono.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lblTelefono.Name = "lblTelefono"
        Me.lblTelefono.Size = New System.Drawing.Size(47, 17)
        Me.lblTelefono.TabIndex = 34
        Me.lblTelefono.Text = "Salida"
        '
        'lblCI
        '
        Me.lblCI.AutoSize = True
        Me.lblCI.Location = New System.Drawing.Point(23, 186)
        Me.lblCI.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lblCI.Name = "lblCI"
        Me.lblCI.Size = New System.Drawing.Size(97, 17)
        Me.lblCI.TabIndex = 33
        Me.lblCI.Text = "CI No docente"
        '
        'lblApellido
        '
        Me.lblApellido.AutoSize = True
        Me.lblApellido.Location = New System.Drawing.Point(23, 52)
        Me.lblApellido.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lblApellido.Name = "lblApellido"
        Me.lblApellido.Size = New System.Drawing.Size(58, 17)
        Me.lblApellido.TabIndex = 32
        Me.lblApellido.Text = "Entrada"
        '
        'lblCorreo
        '
        Me.lblCorreo.AutoSize = True
        Me.lblCorreo.Location = New System.Drawing.Point(23, 140)
        Me.lblCorreo.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lblCorreo.Name = "lblCorreo"
        Me.lblCorreo.Size = New System.Drawing.Size(84, 17)
        Me.lblCorreo.TabIndex = 36
        Me.lblCorreo.Text = "DiaSemanal"
        '
        'txtDia
        '
        Me.txtDia.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.txtDia.FormattingEnabled = True
        Me.txtDia.Items.AddRange(New Object() {"Lunes", "Martes", "Miercoles", "Jueves", "Viernes", "Sabado"})
        Me.txtDia.Location = New System.Drawing.Point(181, 137)
        Me.txtDia.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.txtDia.Name = "txtDia"
        Me.txtDia.Size = New System.Drawing.Size(145, 24)
        Me.txtDia.TabIndex = 41
        '
        'txtEntrada
        '
        Me.txtEntrada.Location = New System.Drawing.Point(181, 49)
        Me.txtEntrada.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.txtEntrada.Mask = "00:00"
        Me.txtEntrada.Name = "txtEntrada"
        Me.txtEntrada.Size = New System.Drawing.Size(145, 22)
        Me.txtEntrada.TabIndex = 39
        '
        'txtSalida
        '
        Me.txtSalida.Location = New System.Drawing.Point(181, 89)
        Me.txtSalida.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.txtSalida.Mask = "00:00"
        Me.txtSalida.Name = "txtSalida"
        Me.txtSalida.Size = New System.Drawing.Size(145, 22)
        Me.txtSalida.TabIndex = 42
        '
        'Timer1
        '
        Me.Timer1.Interval = 1000
        '
        'imgTick
        '
        Me.imgTick.Image = Global.UtuRibclob.My.Resources.Resources.tick
        Me.imgTick.Location = New System.Drawing.Point(73, 249)
        Me.imgTick.Margin = New System.Windows.Forms.Padding(4)
        Me.imgTick.Name = "imgTick"
        Me.imgTick.Size = New System.Drawing.Size(47, 41)
        Me.imgTick.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.imgTick.TabIndex = 43
        Me.imgTick.TabStop = False
        Me.imgTick.Visible = False
        '
        'frmAltaHorarioNoDocente
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(356, 318)
        Me.Controls.Add(Me.imgTick)
        Me.Controls.Add(Me.txtSalida)
        Me.Controls.Add(Me.txtDia)
        Me.Controls.Add(Me.txtEntrada)
        Me.Controls.Add(Me.txtCI)
        Me.Controls.Add(Me.btnAgregar)
        Me.Controls.Add(Me.lblCorreo)
        Me.Controls.Add(Me.lblTelefono)
        Me.Controls.Add(Me.lblCI)
        Me.Controls.Add(Me.lblApellido)
        Me.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.MaximizeBox = False
        Me.MaximumSize = New System.Drawing.Size(374, 365)
        Me.MinimizeBox = False
        Me.MinimumSize = New System.Drawing.Size(374, 365)
        Me.Name = "frmAltaHorarioNoDocente"
        Me.ShowIcon = False
        Me.ShowInTaskbar = False
        Me.Text = "Crear horario no docente"
        Me.TopMost = True
        CType(Me.imgTick, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents txtCI As System.Windows.Forms.MaskedTextBox
    Friend WithEvents btnAgregar As System.Windows.Forms.Button
    Friend WithEvents lblTelefono As System.Windows.Forms.Label
    Friend WithEvents lblCI As System.Windows.Forms.Label
    Friend WithEvents lblApellido As System.Windows.Forms.Label
    Friend WithEvents lblCorreo As System.Windows.Forms.Label
    Friend WithEvents txtDia As System.Windows.Forms.ComboBox
    Friend WithEvents txtEntrada As System.Windows.Forms.MaskedTextBox
    Friend WithEvents txtSalida As System.Windows.Forms.MaskedTextBox
    Friend WithEvents Timer1 As System.Windows.Forms.Timer
    Friend WithEvents imgTick As System.Windows.Forms.PictureBox
End Class
