﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmModificarHorarioNoDocente
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.txtCI = New System.Windows.Forms.MaskedTextBox()
        Me.btnAgregar = New System.Windows.Forms.Button()
        Me.lblCI = New System.Windows.Forms.Label()
        Me.lblCorreo = New System.Windows.Forms.Label()
        Me.txtDia = New System.Windows.Forms.ComboBox()
        Me.txtSalida = New System.Windows.Forms.MaskedTextBox()
        Me.txtEntrada = New System.Windows.Forms.MaskedTextBox()
        Me.lblTelefono = New System.Windows.Forms.Label()
        Me.lblApellido = New System.Windows.Forms.Label()
        Me.SuspendLayout()
        '
        'txtCI
        '
        Me.txtCI.Location = New System.Drawing.Point(136, 148)
        Me.txtCI.Mask = "0000000-0"
        Me.txtCI.Name = "txtCI"
        Me.txtCI.ReadOnly = True
        Me.txtCI.Size = New System.Drawing.Size(110, 20)
        Me.txtCI.TabIndex = 38
        '
        'btnAgregar
        '
        Me.btnAgregar.Location = New System.Drawing.Point(122, 202)
        Me.btnAgregar.Name = "btnAgregar"
        Me.btnAgregar.Size = New System.Drawing.Size(124, 33)
        Me.btnAgregar.TabIndex = 37
        Me.btnAgregar.Text = "Confirmar"
        Me.btnAgregar.UseVisualStyleBackColor = True
        '
        'lblCI
        '
        Me.lblCI.AutoSize = True
        Me.lblCI.Location = New System.Drawing.Point(17, 151)
        Me.lblCI.Name = "lblCI"
        Me.lblCI.Size = New System.Drawing.Size(76, 13)
        Me.lblCI.TabIndex = 33
        Me.lblCI.Text = "CI No docente"
        '
        'lblCorreo
        '
        Me.lblCorreo.AutoSize = True
        Me.lblCorreo.Location = New System.Drawing.Point(17, 114)
        Me.lblCorreo.Name = "lblCorreo"
        Me.lblCorreo.Size = New System.Drawing.Size(64, 13)
        Me.lblCorreo.TabIndex = 36
        Me.lblCorreo.Text = "DiaSemanal"
        '
        'txtDia
        '
        Me.txtDia.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.txtDia.FormattingEnabled = True
        Me.txtDia.Items.AddRange(New Object() {"Lunes", "Martes", "Miercoles", "Jueves", "Viernes", "Sabado"})
        Me.txtDia.Location = New System.Drawing.Point(136, 111)
        Me.txtDia.Name = "txtDia"
        Me.txtDia.Size = New System.Drawing.Size(110, 21)
        Me.txtDia.TabIndex = 41
        '
        'txtSalida
        '
        Me.txtSalida.Location = New System.Drawing.Point(136, 72)
        Me.txtSalida.Margin = New System.Windows.Forms.Padding(2)
        Me.txtSalida.Mask = "00:00"
        Me.txtSalida.Name = "txtSalida"
        Me.txtSalida.Size = New System.Drawing.Size(110, 20)
        Me.txtSalida.TabIndex = 46
        '
        'txtEntrada
        '
        Me.txtEntrada.Location = New System.Drawing.Point(136, 40)
        Me.txtEntrada.Margin = New System.Windows.Forms.Padding(2)
        Me.txtEntrada.Mask = "00:00"
        Me.txtEntrada.Name = "txtEntrada"
        Me.txtEntrada.Size = New System.Drawing.Size(110, 20)
        Me.txtEntrada.TabIndex = 45
        '
        'lblTelefono
        '
        Me.lblTelefono.AutoSize = True
        Me.lblTelefono.Location = New System.Drawing.Point(17, 79)
        Me.lblTelefono.Name = "lblTelefono"
        Me.lblTelefono.Size = New System.Drawing.Size(36, 13)
        Me.lblTelefono.TabIndex = 44
        Me.lblTelefono.Text = "Salida"
        '
        'lblApellido
        '
        Me.lblApellido.AutoSize = True
        Me.lblApellido.Location = New System.Drawing.Point(17, 42)
        Me.lblApellido.Name = "lblApellido"
        Me.lblApellido.Size = New System.Drawing.Size(44, 13)
        Me.lblApellido.TabIndex = 43
        Me.lblApellido.Text = "Entrada"
        '
        'frmModificarHorarioNoDocente
        '
        Me.AcceptButton = Me.btnAgregar
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(269, 266)
        Me.Controls.Add(Me.txtSalida)
        Me.Controls.Add(Me.txtEntrada)
        Me.Controls.Add(Me.lblTelefono)
        Me.Controls.Add(Me.lblApellido)
        Me.Controls.Add(Me.txtDia)
        Me.Controls.Add(Me.txtCI)
        Me.Controls.Add(Me.btnAgregar)
        Me.Controls.Add(Me.lblCorreo)
        Me.Controls.Add(Me.lblCI)
        Me.Margin = New System.Windows.Forms.Padding(2)
        Me.MaximizeBox = False
        Me.MaximumSize = New System.Drawing.Size(285, 305)
        Me.MinimizeBox = False
        Me.MinimumSize = New System.Drawing.Size(285, 305)
        Me.Name = "frmModificarHorarioNoDocente"
        Me.ShowIcon = False
        Me.ShowInTaskbar = False
        Me.Text = "Modificar horario no docente"
        Me.TopMost = True
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents txtCI As System.Windows.Forms.MaskedTextBox
    Friend WithEvents btnAgregar As System.Windows.Forms.Button
    Friend WithEvents lblCI As System.Windows.Forms.Label
    Friend WithEvents lblCorreo As System.Windows.Forms.Label
    Friend WithEvents txtDia As System.Windows.Forms.ComboBox
    Friend WithEvents txtSalida As System.Windows.Forms.MaskedTextBox
    Friend WithEvents txtEntrada As System.Windows.Forms.MaskedTextBox
    Friend WithEvents lblTelefono As System.Windows.Forms.Label
    Friend WithEvents lblApellido As System.Windows.Forms.Label
End Class
