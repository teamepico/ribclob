﻿Public Class frmBajaHorarioNoDocente
    Dim bajaHorario As New clsHorarioNoDocente
    Dim datosFuncionario As New clsFuncionarioNoDocente
    Private frmHorarioNoDocente As frmHorarioNoDocente

    Public Sub New(unHor As clsHorarioNoDocente, datosFun As clsFuncionarioNoDocente, unFrm As frmHorarioNoDocente)

        ' Llamada necesaria para el diseñador.
        InitializeComponent()

        ' Agregue cualquier inicialización después de la llamada a InitializeComponent().
        bajaHorario = unHor
        datosFuncionario = datosFun
        frmHorarioNoDocente = unFrm
    End Sub

    Private Function obtenerDatos(ByVal unFuncionario As String) As clsFuncionarioDocente
        Dim unaC As New clsControladora
        Return unaC.obtenerDatosFuncionarioDocente(unFuncionario)
    End Function

    Private Sub frmAltaInterno_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        'datosFuncionario = obtenerDatos(bajaHorario.ci)
        lblNombre.Text = datosFuncionario.nombre & " " & datosFuncionario.apellido
        lblCI.Text = datosFuncionario.ci
        lblDia.Text = bajaHorario.diaSemanalHorarioNoDocente
        lblEntrada.Text = bajaHorario.entradaHorarioNoDocente
        lblSalida.Text = bajaHorario.salidaHorarioNoDocente
    End Sub

    Private Sub btnEliminar_Click(sender As Object, e As EventArgs) Handles btnEliminar.Click
        Dim unaC As New clsControladora
        Dim errorSQL As String
        errorSQL = unaC.bajaHorarioNoDocente(bajaHorario)
        frmHorarioNoDocente.Activate()

        If errorSQL = "" Then
            frmHorarioNoDocente.Activate()
            Me.Close()
        Else
            MsgBox("Error al eliminar el horario.")
        End If
    End Sub
End Class