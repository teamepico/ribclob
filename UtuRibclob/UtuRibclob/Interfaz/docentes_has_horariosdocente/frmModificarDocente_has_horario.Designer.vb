﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmModificarDocente_has_horario
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.txtSalon = New System.Windows.Forms.ComboBox()
        Me.txtHorarioEntrada = New System.Windows.Forms.ComboBox()
        Me.txtGrupo = New System.Windows.Forms.ComboBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.txtMaterias = New System.Windows.Forms.ComboBox()
        Me.btnConfirmar = New System.Windows.Forms.Button()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.txtHorarioSalida = New System.Windows.Forms.MaskedTextBox()
        Me.lblDia = New System.Windows.Forms.Label()
        Me.txtDia = New System.Windows.Forms.ComboBox()
        Me.SuspendLayout()
        '
        'txtSalon
        '
        Me.txtSalon.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.txtSalon.FormattingEnabled = True
        Me.txtSalon.Location = New System.Drawing.Point(180, 34)
        Me.txtSalon.Name = "txtSalon"
        Me.txtSalon.Size = New System.Drawing.Size(121, 21)
        Me.txtSalon.TabIndex = 0
        '
        'txtHorarioEntrada
        '
        Me.txtHorarioEntrada.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.txtHorarioEntrada.FormattingEnabled = True
        Me.txtHorarioEntrada.Location = New System.Drawing.Point(180, 77)
        Me.txtHorarioEntrada.Name = "txtHorarioEntrada"
        Me.txtHorarioEntrada.Size = New System.Drawing.Size(121, 21)
        Me.txtHorarioEntrada.TabIndex = 1
        '
        'txtGrupo
        '
        Me.txtGrupo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.txtGrupo.FormattingEnabled = True
        Me.txtGrupo.Location = New System.Drawing.Point(180, 162)
        Me.txtGrupo.Name = "txtGrupo"
        Me.txtGrupo.Size = New System.Drawing.Size(121, 21)
        Me.txtGrupo.TabIndex = 3
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(32, 42)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(34, 13)
        Me.Label1.TabIndex = 4
        Me.Label1.Text = "Salon"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(32, 85)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(44, 13)
        Me.Label2.TabIndex = 5
        Me.Label2.Text = "Entrada"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(32, 170)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(36, 13)
        Me.Label4.TabIndex = 7
        Me.Label4.Text = "Grupo"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(32, 217)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(47, 13)
        Me.Label6.TabIndex = 10
        Me.Label6.Text = "Materias"
        '
        'txtMaterias
        '
        Me.txtMaterias.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.txtMaterias.FormattingEnabled = True
        Me.txtMaterias.Location = New System.Drawing.Point(180, 209)
        Me.txtMaterias.Name = "txtMaterias"
        Me.txtMaterias.Size = New System.Drawing.Size(121, 21)
        Me.txtMaterias.TabIndex = 8
        '
        'btnConfirmar
        '
        Me.btnConfirmar.Location = New System.Drawing.Point(205, 311)
        Me.btnConfirmar.Name = "btnConfirmar"
        Me.btnConfirmar.Size = New System.Drawing.Size(96, 32)
        Me.btnConfirmar.TabIndex = 13
        Me.btnConfirmar.Text = "Confirmar"
        Me.btnConfirmar.UseVisualStyleBackColor = True
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(32, 128)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(36, 13)
        Me.Label3.TabIndex = 15
        Me.Label3.Text = "Salida"
        '
        'txtHorarioSalida
        '
        Me.txtHorarioSalida.Location = New System.Drawing.Point(181, 120)
        Me.txtHorarioSalida.Name = "txtHorarioSalida"
        Me.txtHorarioSalida.ReadOnly = True
        Me.txtHorarioSalida.Size = New System.Drawing.Size(120, 20)
        Me.txtHorarioSalida.TabIndex = 16
        '
        'lblDia
        '
        Me.lblDia.AutoSize = True
        Me.lblDia.Location = New System.Drawing.Point(32, 263)
        Me.lblDia.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.lblDia.Name = "lblDia"
        Me.lblDia.Size = New System.Drawing.Size(23, 13)
        Me.lblDia.TabIndex = 60
        Me.lblDia.Text = "Dia"
        '
        'txtDia
        '
        Me.txtDia.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.txtDia.FormattingEnabled = True
        Me.txtDia.Location = New System.Drawing.Point(181, 255)
        Me.txtDia.Margin = New System.Windows.Forms.Padding(2)
        Me.txtDia.Name = "txtDia"
        Me.txtDia.Size = New System.Drawing.Size(120, 21)
        Me.txtDia.TabIndex = 59
        '
        'frmModificarDocente_has_horario
        '
        Me.AcceptButton = Me.btnConfirmar
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(333, 365)
        Me.Controls.Add(Me.lblDia)
        Me.Controls.Add(Me.txtDia)
        Me.Controls.Add(Me.txtHorarioSalida)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.btnConfirmar)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.txtMaterias)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.txtGrupo)
        Me.Controls.Add(Me.txtHorarioEntrada)
        Me.Controls.Add(Me.txtSalon)
        Me.Margin = New System.Windows.Forms.Padding(2)
        Me.MaximizeBox = False
        Me.MaximumSize = New System.Drawing.Size(349, 404)
        Me.MinimizeBox = False
        Me.MinimumSize = New System.Drawing.Size(349, 404)
        Me.Name = "frmModificarDocente_has_horario"
        Me.ShowIcon = False
        Me.ShowInTaskbar = False
        Me.Text = "Modificar horario a docente"
        Me.TopMost = True
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents txtSalon As System.Windows.Forms.ComboBox
    Friend WithEvents txtHorarioEntrada As System.Windows.Forms.ComboBox
    Friend WithEvents txtGrupo As System.Windows.Forms.ComboBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents txtMaterias As System.Windows.Forms.ComboBox
    Friend WithEvents btnConfirmar As System.Windows.Forms.Button
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents txtHorarioSalida As System.Windows.Forms.MaskedTextBox
    Friend WithEvents lblDia As System.Windows.Forms.Label
    Friend WithEvents txtDia As System.Windows.Forms.ComboBox
End Class
