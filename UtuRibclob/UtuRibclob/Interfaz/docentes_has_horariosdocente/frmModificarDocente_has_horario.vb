﻿Public Class frmModificarDocente_has_horario
    Dim elUsuario As New clsUser
    Dim unHorario As New clsDocentes_horariosdocente
    Dim colHorario As New List(Of clsHorarioDocente)
    Dim ci As String
    Private frmDocente As frmDocente_has_horario
    Private docente As New clsFuncionarioDocente

    Public Sub New(usuario As clsUser, horario As clsDocentes_horariosdocente, funcionario As clsFuncionarioDocente, unFrm As frmDocente_has_horario)
        InitializeComponent()
        frmDocente = unFrm
        elUsuario = usuario
        unHorario = horario
    End Sub

    Public Sub llenarDatos()
        txtDia.SelectedItem = unHorario.diaHorario
        txtGrupo.SelectedItem = unHorario.grupo.NombreGrupo
        txtHorarioEntrada.SelectedItem = unHorario.horario.entrada
        txtHorarioSalida.Text = unHorario.horario.salida
        txtMaterias.SelectedItem = unHorario.materia.NombreMateria
        txtSalon.SelectedItem = unHorario.salon.nombreSalon
    End Sub

    Public Sub listarSalones()
        Dim unaC As New clsControladora
        Dim colSalon As New List(Of clsSalon)

        colSalon = unaC.listarSalones

        For Each unSalon In colSalon
            txtSalon.Items.Add(unSalon.nombreSalon)
        Next
    End Sub

    Public Sub listarHorario()
        Dim unaC As New clsControladora

        colHorario = unaC.listarHorarioDocente

        For Each unH In colHorario
            txtHorarioEntrada.Items.Add(unH.entrada)
        Next
    End Sub

    Public Sub listarGrupo()
        Dim unaC As New clsControladora
        Dim colGrupo As New List(Of clsGrupo)

        colGrupo = unaC.listarGrupos

        For Each unGrupo In colGrupo
            txtGrupo.Items.Add(unGrupo.NombreGrupo)
        Next

    End Sub

    Public Sub listarMaterias()
        Dim unaC As New clsControladora
        Dim colMateria As New List(Of clsMateria)

        colMateria = unaC.listarMaterias()

        For Each unaMateria In colMateria
            txtMaterias.Items.Add(unaMateria.NombreMateria)
        Next
    End Sub

    Private Sub frmModificarDocente_has_horario_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        listarSalones()
        listarGrupo()
        listarHorario()
        listarMaterias()
        rellenarDias()

        ci = unHorario.docente.ci

        llenarDatos()
    End Sub

    Public Sub rellenarDias()
        txtDia.Items.Insert(0, "Lunes")
        txtDia.Items.Insert(1, "Martes")
        txtDia.Items.Insert(2, "Miercoles")
        txtDia.Items.Insert(3, "Jueves")
        txtDia.Items.Insert(4, "Viernes")
        txtDia.Items.Insert(5, "Sabado")
        txtDia.Items.Insert(6, "Domingo")
    End Sub

    Private Sub btnConfirmar_Click(sender As Object, e As EventArgs) Handles btnConfirmar.Click
        Dim unaC As New clsControladora
        Dim errorSql As String = ""

        Dim salon As New clsSalon(txtSalon.Text, "")
        Dim grupo As New clsGrupo(txtGrupo.Text, "")
        Dim horario As New clsHorarioDocente(vbEmpty, txtHorarioEntrada.Text, txtHorarioSalida.Text)
        Dim materia As New clsMateria(txtMaterias.Text, "")

        Dim unNuevoHorario As New clsDocentes_horariosdocente(salon, materia, horario, docente, grupo, txtDia.Text)

        errorSql = unaC.modificarDocenteHorarioDocente(unNuevoHorario, unHorario)

        If errorSql = "" Then
            frmDocente.Activate()
            Me.Close()
        Else
            MsgBox("Hubo un error cargando el horario.")
        End If
    End Sub

    Private Sub txtHorarioEntrada_TextChanged(sender As Object, e As EventArgs) Handles txtHorarioEntrada.TextChanged
        For Each unH In colHorario
            If unH.entrada = txtHorarioEntrada.Text Then
                txtHorarioSalida.Text = unH.salida
            End If
        Next
    End Sub
End Class