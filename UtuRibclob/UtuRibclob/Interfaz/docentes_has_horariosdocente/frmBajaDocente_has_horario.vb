﻿Public Class frmBajaDocente_has_horario
    Dim bajaHorario As New clsDocentes_horariosdocente
    Dim datosUsuario As New clsUser
    Private frmDocente As frmDocente_has_horario

    Public Sub New(unU As clsUser, unH As clsDocentes_horariosdocente, unFrm As frmDocente_has_horario)
        datosUsuario = unU
        bajaHorario = unH
        frmDocente = unFrm
        InitializeComponent()
    End Sub

    Private Sub btnEliminar_Click_1(sender As Object, e As EventArgs) Handles btnEliminar.Click
        Dim resultadoSQL As String
        Dim unaC As New clsControladora
        resultadoSQL = unaC.bajaDocenteHorarioDocente(bajaHorario)

        If Not resultadoSQL = "" Then
            Dim mostrarError As String = "Hubo un error en el servidor SQL. Ingrese como desarrollador para arreglarlo."
            If datosUsuario.tipo = "desarrollador" Then mostrarError = mostrarError & resultadoSQL
            MsgBox(mostrarError)
        ElseIf resultadoSQL = "" Then
            frmDocente.Activate()
            Me.Close()
        Else
            MsgBox("Algo salio horriblemente mal.")
        End If

    End Sub

    Private Sub btnCancelar_Click(sender As Object, e As EventArgs) Handles btnCancelar.Click
        Me.Close()
    End Sub

    Private Sub frmBajaDocente_has_horario_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        lblInicio.Text = bajaHorario.horario.entrada
        lblFinal.Text = bajaHorario.horario.salida
        lblDia.Text = bajaHorario.diaHorario
    End Sub
End Class