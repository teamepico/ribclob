﻿Public Class frmAltaDocente_has_horario
    Dim colSalones As New List(Of clsSalon)
    Dim botones As New List(Of clsBoton)
    Dim colHorarios As New List(Of clsHorarioDocente)
    Dim docente As New clsFuncionarioDocente
    Dim colGrupos As New List(Of clsGrupo)
    Dim colMaterias As New List(Of clsMateria)
    Private frmDocente As frmDocente_has_horario
    Dim idHorarios As New List(Of String)
    Dim frmY As String
    Dim frmX As String
    Dim esVacio As Boolean = False

    Private Function listarGrupos() As List(Of clsGrupo)
        Dim unaC As New clsControladora
        Return unaC.listarGrupos()
    End Function

    Private Function listarSalones() As List(Of clsSalon)
        Dim unaC As New clsControladora
        Return unaC.listarSalones()
    End Function


    Private Function listarMaterias() As List(Of clsMateria)
        Dim unaC As New clsControladora
        Return unaC.listarMaterias()
    End Function

    Public Sub rellenarGrupos(colGrupos As List(Of clsGrupo))
        Dim i As Integer = 0
        For Each grupo In colGrupos
            txtGrupo.Items.Insert(i, grupo.NombreGrupo)
            i += 1
        Next
    End Sub

    Public Sub rellenarSalones(colSalones As List(Of clsSalon))
        Dim i As Integer = 0
        For Each salon In colSalones
            txtSalon.Items.Insert(i, salon.nombreSalon)
            i += 1
        Next
    End Sub

    Public Sub rellenarMaterias(colMateria As List(Of clsMateria))
        Dim i As Integer = 0
        For Each materia In colMateria
            txtMateria.Items.Insert(i, materia.NombreMateria)
            i += 1
        Next
    End Sub

    Public Sub rellenarDias()
        txtDia.Items.Insert(0, "Lunes")
        txtDia.Items.Insert(1, "Martes")
        txtDia.Items.Insert(2, "Miercoles")
        txtDia.Items.Insert(3, "Jueves")
        txtDia.Items.Insert(4, "Viernes")
        txtDia.Items.Insert(5, "Sabado")
    End Sub

    Private Function listarHorarios() As List(Of clsHorarioDocente)
        Dim unaC As New clsControladora
        Return unaC.listarHorarioDocente()
    End Function

    Public Sub botonDinamicoHorario(colHorarios As List(Of clsHorarioDocente))

        Dim x = 400, y = 20
        Dim n As Integer = 0

        For Each unHorario In colHorarios

            Dim boton As New clsBoton
            botones.Add(boton)
            botones(n).Visible = True
            botones(n).Width = 100
            botones(n).Height = 50
            botones(n).BackColor = Color.White
            botones(n).Location = New Point(x, y) 'x,y
            botones(n).ForeColor = Color.Black
            botones(n).Name = "btn" & (n + 1)
            botones(n).entrada = unHorario.entrada
            botones(n).Text = (botones(n).entrada)
            botones(n).salida = unHorario.salida
            botones(n).Text = botones(n).Text & vbCr & botones(n).salida
            botones(n).Font = New Font(FontFamily.GenericSansSerif, 10.0F, FontStyle.Bold)

            boton.idHorario = unHorario.IdHorarioDocente

            If ((n + 1) Mod 2 = 0) Then
                y += 110
                x = 270
            End If

            AddHandler botones(n).Click, AddressOf eventoClick
            Me.Controls.Add(botones(n))

            x += 130
            n += 1
        Next

    End Sub


    Private Sub eventoClick(sender As Object, e As EventArgs)
        If CType(sender, clsBoton).presionado = False Then
            CType(sender, clsBoton).BackColor = Color.Blue
            CType(sender, clsBoton).presionado = True
        Else
            CType(sender, clsBoton).BackColor = Color.White
            CType(sender, clsBoton).presionado = False
        End If
    End Sub


    Public Sub recorrerHorarios()
        For Each boton In botones

            If boton.presionado = True Then
                Dim unaC As New clsControladora

                Dim salon As New clsSalon(txtSalon.Text, "")
                Dim materia As New clsMateria(txtMateria.Text, "")
                Dim horario As New clsHorarioDocente(boton.idHorario, boton.entrada, boton.salida)
                Dim grupo As New clsGrupo(txtGrupo.Text, "")
                Dim errorSQL As String

                Dim unHorario As New clsDocentes_horariosdocente(salon, materia, horario, docente, grupo, txtDia.Text)

                errorSQL = unaC.altaDocenteHasHorario(unHorario)

                If errorSQL = "" Then
                    imgTick.Visible = True
                    Timer1.Enabled = True
                Else
                    MsgBox("Hubo un error cargando los datos. Intente de nuevo.")
                End If
            End If
        Next
        limpiar()
    End Sub

    Public Sub limpiar()
        colGrupos = listarGrupos()
        colSalones = listarSalones()
        colHorarios = listarHorarios()
        colMaterias = listarMaterias()

        rellenarGrupos(colGrupos)
        rellenarSalones(colSalones)
        rellenarDias()
        rellenarMaterias(colMaterias)

        txtCI.Text = docente.ci
        lblNombreDoc.Text = docente.nombre & "  " & docente.apellido
        botonDinamicoHorario(colHorarios)
    End Sub

    Private Sub frmAltaDocente_has_horario_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        txtCI.Enabled = False
        limpiar()
    End Sub

    Public Sub New(funcionario As clsFuncionarioDocente, unFrm As frmDocente_has_horario)
        docente = funcionario
        ' Llamada necesaria para el diseñador.
        InitializeComponent()

        ' Agregue cualquier inicialización después de la llamada a InitializeComponent().
        frmDocente = unFrm
    End Sub

    Public Sub verificarVacio()
        If txtCI.Text = "" Or txtDia.Text = "" Or txtGrupo.Text = "" Or txtMateria.Text = "" Or txtSalon.Text = "" Then
            esVacio = True
        End If
    End Sub

    Private Sub btnAgregar_Click(sender As Object, e As EventArgs) Handles btnAgregar.Click
        verificarVacio()
        recorrerHorarios()
    End Sub

    Private Sub Timer1_Tick(sender As Object, e As EventArgs) Handles Timer1.Tick
        imgTick.Visible = False
        Timer1.Enabled = False
    End Sub

    Private Sub imgTick_Click(sender As Object, e As EventArgs) Handles imgTick.Click

    End Sub
End Class