﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmDocente_has_horario
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmDocente_has_horario))
        Me.btnEliminar = New System.Windows.Forms.Button()
        Me.btnModificar = New System.Windows.Forms.Button()
        Me.btnAgregar = New System.Windows.Forms.Button()
        Me.lsvHorarios = New System.Windows.Forms.ListView()
        Me.Entrada = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.Salida = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.Grupo = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.Dia = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.Materia = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.Salon = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.btnSalir = New System.Windows.Forms.Button()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.lblPersona = New System.Windows.Forms.Label()
        Me.btnRecuperar = New System.Windows.Forms.Button()
        Me.PrintDocument1 = New System.Drawing.Printing.PrintDocument()
        Me.PrintDialog1 = New System.Windows.Forms.PrintDialog()
        Me.btnI = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'btnEliminar
        '
        Me.btnEliminar.Location = New System.Drawing.Point(258, 558)
        Me.btnEliminar.Name = "btnEliminar"
        Me.btnEliminar.Size = New System.Drawing.Size(117, 31)
        Me.btnEliminar.TabIndex = 16
        Me.btnEliminar.Text = "Eliminar"
        Me.btnEliminar.UseVisualStyleBackColor = True
        '
        'btnModificar
        '
        Me.btnModificar.Location = New System.Drawing.Point(135, 558)
        Me.btnModificar.Name = "btnModificar"
        Me.btnModificar.Size = New System.Drawing.Size(117, 31)
        Me.btnModificar.TabIndex = 15
        Me.btnModificar.Text = "Modificar"
        Me.btnModificar.UseVisualStyleBackColor = True
        '
        'btnAgregar
        '
        Me.btnAgregar.Location = New System.Drawing.Point(12, 558)
        Me.btnAgregar.Name = "btnAgregar"
        Me.btnAgregar.Size = New System.Drawing.Size(117, 31)
        Me.btnAgregar.TabIndex = 14
        Me.btnAgregar.Text = "Agregar"
        Me.btnAgregar.UseVisualStyleBackColor = True
        '
        'lsvHorarios
        '
        Me.lsvHorarios.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.Entrada, Me.Salida, Me.Grupo, Me.Dia, Me.Materia, Me.Salon})
        Me.lsvHorarios.GridLines = True
        Me.lsvHorarios.Location = New System.Drawing.Point(12, 101)
        Me.lsvHorarios.Name = "lsvHorarios"
        Me.lsvHorarios.Size = New System.Drawing.Size(887, 451)
        Me.lsvHorarios.TabIndex = 13
        Me.lsvHorarios.UseCompatibleStateImageBehavior = False
        Me.lsvHorarios.View = System.Windows.Forms.View.Details
        '
        'Entrada
        '
        Me.Entrada.Text = "Entrada"
        Me.Entrada.Width = 115
        '
        'Salida
        '
        Me.Salida.Text = "Salida"
        Me.Salida.Width = 145
        '
        'Grupo
        '
        Me.Grupo.Text = "Grupo"
        Me.Grupo.Width = 150
        '
        'Dia
        '
        Me.Dia.Text = "Dia"
        Me.Dia.Width = 151
        '
        'Materia
        '
        Me.Materia.Text = "Materia"
        Me.Materia.Width = 184
        '
        'Salon
        '
        Me.Salon.Text = "Salon"
        Me.Salon.Width = 138
        '
        'btnSalir
        '
        Me.btnSalir.Location = New System.Drawing.Point(782, 558)
        Me.btnSalir.Name = "btnSalir"
        Me.btnSalir.Size = New System.Drawing.Size(117, 30)
        Me.btnSalir.TabIndex = 12
        Me.btnSalir.Text = "Salir"
        Me.btnSalir.UseVisualStyleBackColor = True
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 13.8!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(57, 45)
        Me.Label1.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(125, 24)
        Me.Label1.TabIndex = 17
        Me.Label1.Text = "Horarios de:"
        '
        'lblPersona
        '
        Me.lblPersona.AutoSize = True
        Me.lblPersona.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPersona.Location = New System.Drawing.Point(179, 49)
        Me.lblPersona.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.lblPersona.Name = "lblPersona"
        Me.lblPersona.Size = New System.Drawing.Size(117, 18)
        Me.lblPersona.TabIndex = 18
        Me.lblPersona.Text = "Nombre Apellido"
        '
        'btnRecuperar
        '
        Me.btnRecuperar.Location = New System.Drawing.Point(381, 558)
        Me.btnRecuperar.Name = "btnRecuperar"
        Me.btnRecuperar.Size = New System.Drawing.Size(117, 31)
        Me.btnRecuperar.TabIndex = 19
        Me.btnRecuperar.Text = "Recuperar horario"
        Me.btnRecuperar.UseVisualStyleBackColor = True
        '
        'PrintDocument1
        '
        '
        'PrintDialog1
        '
        Me.PrintDialog1.UseEXDialog = True
        '
        'btnI
        '
        Me.btnI.BackgroundImage = Global.UtuRibclob.My.Resources.Resources.print
        Me.btnI.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnI.Location = New System.Drawing.Point(503, 558)
        Me.btnI.Margin = New System.Windows.Forms.Padding(2, 2, 2, 2)
        Me.btnI.Name = "btnI"
        Me.btnI.Size = New System.Drawing.Size(34, 31)
        Me.btnI.TabIndex = 25
        Me.btnI.UseVisualStyleBackColor = True
        '
        'frmDocente_has_horario
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(911, 600)
        Me.Controls.Add(Me.btnI)
        Me.Controls.Add(Me.btnRecuperar)
        Me.Controls.Add(Me.lblPersona)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.btnEliminar)
        Me.Controls.Add(Me.btnModificar)
        Me.Controls.Add(Me.btnAgregar)
        Me.Controls.Add(Me.lsvHorarios)
        Me.Controls.Add(Me.btnSalir)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Margin = New System.Windows.Forms.Padding(2, 2, 2, 2)
        Me.MinimumSize = New System.Drawing.Size(927, 639)
        Me.Name = "frmDocente_has_horario"
        Me.Text = "Horarios de docente"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents btnEliminar As System.Windows.Forms.Button
    Friend WithEvents btnModificar As System.Windows.Forms.Button
    Friend WithEvents btnAgregar As System.Windows.Forms.Button
    Friend WithEvents lsvHorarios As System.Windows.Forms.ListView
    Friend WithEvents Entrada As System.Windows.Forms.ColumnHeader
    Friend WithEvents Salida As System.Windows.Forms.ColumnHeader
    Friend WithEvents Grupo As System.Windows.Forms.ColumnHeader
    Friend WithEvents Dia As System.Windows.Forms.ColumnHeader
    Friend WithEvents Materia As System.Windows.Forms.ColumnHeader
    Friend WithEvents Salon As System.Windows.Forms.ColumnHeader
    Friend WithEvents btnSalir As System.Windows.Forms.Button
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents lblPersona As System.Windows.Forms.Label
    Friend WithEvents btnRecuperar As System.Windows.Forms.Button
    Friend WithEvents PrintDocument1 As System.Drawing.Printing.PrintDocument
    Friend WithEvents PrintDialog1 As System.Windows.Forms.PrintDialog
    Friend WithEvents btnI As System.Windows.Forms.Button
End Class
