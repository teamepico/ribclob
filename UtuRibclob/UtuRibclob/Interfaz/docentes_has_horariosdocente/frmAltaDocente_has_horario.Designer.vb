﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmAltaDocente_has_horario
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.btnAgregar = New System.Windows.Forms.Button()
        Me.lblNombreDoc = New System.Windows.Forms.Label()
        Me.lblDia = New System.Windows.Forms.Label()
        Me.txtDia = New System.Windows.Forms.ComboBox()
        Me.txtMateria = New System.Windows.Forms.ComboBox()
        Me.txtGrupo = New System.Windows.Forms.ComboBox()
        Me.txtSalon = New System.Windows.Forms.ComboBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.lblEntrada = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.txtCI = New System.Windows.Forms.MaskedTextBox()
        Me.imgTick = New System.Windows.Forms.PictureBox()
        Me.Timer1 = New System.Windows.Forms.Timer(Me.components)
        CType(Me.imgTick, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.Location = New System.Drawing.Point(29, 52)
        Me.Label6.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(170, 17)
        Me.Label6.TabIndex = 35
        Me.Label6.Text = "Agregando Horario a: "
        '
        'btnAgregar
        '
        Me.btnAgregar.Location = New System.Drawing.Point(324, 512)
        Me.btnAgregar.Margin = New System.Windows.Forms.Padding(5)
        Me.btnAgregar.Name = "btnAgregar"
        Me.btnAgregar.Size = New System.Drawing.Size(220, 50)
        Me.btnAgregar.TabIndex = 24
        Me.btnAgregar.Text = "Confirmar"
        Me.btnAgregar.UseVisualStyleBackColor = True
        '
        'lblNombreDoc
        '
        Me.lblNombreDoc.AutoSize = True
        Me.lblNombreDoc.Location = New System.Drawing.Point(264, 52)
        Me.lblNombreDoc.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lblNombreDoc.Name = "lblNombreDoc"
        Me.lblNombreDoc.Size = New System.Drawing.Size(49, 17)
        Me.lblNombreDoc.TabIndex = 36
        Me.lblNombreDoc.Text = "Docen"
        '
        'lblDia
        '
        Me.lblDia.AutoSize = True
        Me.lblDia.Location = New System.Drawing.Point(40, 268)
        Me.lblDia.Name = "lblDia"
        Me.lblDia.Size = New System.Drawing.Size(29, 17)
        Me.lblDia.TabIndex = 58
        Me.lblDia.Text = "Dia"
        '
        'txtDia
        '
        Me.txtDia.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.txtDia.FormattingEnabled = True
        Me.txtDia.Location = New System.Drawing.Point(131, 266)
        Me.txtDia.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.txtDia.Name = "txtDia"
        Me.txtDia.Size = New System.Drawing.Size(121, 24)
        Me.txtDia.TabIndex = 57
        '
        'txtMateria
        '
        Me.txtMateria.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.txtMateria.FormattingEnabled = True
        Me.txtMateria.Location = New System.Drawing.Point(131, 361)
        Me.txtMateria.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.txtMateria.Name = "txtMateria"
        Me.txtMateria.Size = New System.Drawing.Size(121, 24)
        Me.txtMateria.TabIndex = 56
        '
        'txtGrupo
        '
        Me.txtGrupo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.txtGrupo.FormattingEnabled = True
        Me.txtGrupo.Location = New System.Drawing.Point(131, 309)
        Me.txtGrupo.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.txtGrupo.Name = "txtGrupo"
        Me.txtGrupo.Size = New System.Drawing.Size(121, 24)
        Me.txtGrupo.TabIndex = 55
        '
        'txtSalon
        '
        Me.txtSalon.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.txtSalon.FormattingEnabled = True
        Me.txtSalon.Location = New System.Drawing.Point(131, 405)
        Me.txtSalon.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.txtSalon.Name = "txtSalon"
        Me.txtSalon.Size = New System.Drawing.Size(121, 24)
        Me.txtSalon.TabIndex = 54
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(37, 361)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(59, 17)
        Me.Label2.TabIndex = 53
        Me.Label2.Text = "Materia:"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(37, 309)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(52, 17)
        Me.Label5.TabIndex = 50
        Me.Label5.Text = "Grupo:"
        Me.Label5.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(40, 409)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(48, 17)
        Me.Label4.TabIndex = 49
        Me.Label4.Text = "Salon:"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(40, 223)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(51, 17)
        Me.Label7.TabIndex = 48
        Me.Label7.Text = "Salida:"
        '
        'lblEntrada
        '
        Me.lblEntrada.AutoSize = True
        Me.lblEntrada.Location = New System.Drawing.Point(37, 183)
        Me.lblEntrada.Name = "lblEntrada"
        Me.lblEntrada.Size = New System.Drawing.Size(62, 17)
        Me.lblEntrada.TabIndex = 47
        Me.lblEntrada.Text = "Entrada:"
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(37, 132)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(73, 17)
        Me.Label8.TabIndex = 46
        Me.Label8.Text = "CiDocente"
        '
        'txtCI
        '
        Me.txtCI.Location = New System.Drawing.Point(131, 132)
        Me.txtCI.Margin = New System.Windows.Forms.Padding(4)
        Me.txtCI.Mask = "0000000-0"
        Me.txtCI.Name = "txtCI"
        Me.txtCI.Size = New System.Drawing.Size(100, 22)
        Me.txtCI.TabIndex = 45
        '
        'imgTick
        '
        Me.imgTick.Image = Global.UtuRibclob.My.Resources.Resources.tick
        Me.imgTick.Location = New System.Drawing.Point(205, 512)
        Me.imgTick.Margin = New System.Windows.Forms.Padding(4)
        Me.imgTick.Name = "imgTick"
        Me.imgTick.Size = New System.Drawing.Size(47, 41)
        Me.imgTick.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.imgTick.TabIndex = 59
        Me.imgTick.TabStop = False
        Me.imgTick.Visible = False
        '
        'Timer1
        '
        Me.Timer1.Interval = 1000
        '
        'frmAltaDocente_has_horario
        '
        Me.AcceptButton = Me.btnAgregar
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(948, 578)
        Me.Controls.Add(Me.imgTick)
        Me.Controls.Add(Me.lblDia)
        Me.Controls.Add(Me.txtDia)
        Me.Controls.Add(Me.txtMateria)
        Me.Controls.Add(Me.txtGrupo)
        Me.Controls.Add(Me.txtSalon)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.Label7)
        Me.Controls.Add(Me.lblEntrada)
        Me.Controls.Add(Me.Label8)
        Me.Controls.Add(Me.txtCI)
        Me.Controls.Add(Me.lblNombreDoc)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.btnAgregar)
        Me.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.MinimumSize = New System.Drawing.Size(963, 616)
        Me.Name = "frmAltaDocente_has_horario"
        Me.ShowIcon = False
        Me.ShowInTaskbar = False
        Me.Text = "Agregar horario a docente"
        Me.TopMost = True
        CType(Me.imgTick, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents btnAgregar As System.Windows.Forms.Button
    Friend WithEvents lblNombreDoc As System.Windows.Forms.Label
    Friend WithEvents lblDia As System.Windows.Forms.Label
    Friend WithEvents txtDia As System.Windows.Forms.ComboBox
    Friend WithEvents txtMateria As System.Windows.Forms.ComboBox
    Friend WithEvents txtGrupo As System.Windows.Forms.ComboBox
    Friend WithEvents txtSalon As System.Windows.Forms.ComboBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents lblEntrada As System.Windows.Forms.Label
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents txtCI As System.Windows.Forms.MaskedTextBox
    Friend WithEvents imgTick As System.Windows.Forms.PictureBox
    Friend WithEvents Timer1 As System.Windows.Forms.Timer
End Class
