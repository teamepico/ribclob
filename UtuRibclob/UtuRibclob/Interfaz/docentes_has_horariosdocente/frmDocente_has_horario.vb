﻿Public Class frmDocente_has_horario
    Private frmDocente As frmDocente_has_horario
    Dim elUsuario As New clsUser
    Dim funcionario As New clsFuncionarioDocente
    Dim seleccionEsVacia As Boolean = False

    Public Sub New(elU As clsUser, datoPersona As clsFuncionarioDocente)
        ' TODO: Complete member initialization 
        elUsuario = elU
        funcionario = datoPersona
        frmDocente = Me
        InitializeComponent()
    End Sub

    Public Function seleccionVacia() As Boolean
        Dim devolver As Boolean = False

        If lsvHorarios.FocusedItem Is Nothing Then
            MsgBox("No se ha seleccionado ningún horario.")
            devolver = True
        End If

        Return devolver
    End Function

    Private Sub Listar()
        Dim unaC As New clsControladora
        Dim colHorarios = unaC.listarHorarioHasDocente(funcionario.ci)
        Dim lsi As New ListViewItem
        lsvHorarios.Items.Clear()

        For Each unHorario In colHorarios
            lsi = lsvHorarios.Items.Add(unHorario.horario.entrada)
            lsi.SubItems.Add(unHorario.horario.salida)
            lsi.SubItems.Add(unHorario.grupo.NombreGrupo)
            lsi.SubItems.Add(unHorario.diaHorario)
            lsi.SubItems.Add(unHorario.materia.NombreMateria)
            lsi.SubItems.Add(unHorario.salon.nombreSalon)
        Next
    End Sub

    Private Sub lsvHorarios_SelectedIndexChanged(sender As Object, e As EventArgs) Handles lsvHorarios.SelectedIndexChanged
        Dim datoID As String = lsvHorarios.FocusedItem.SubItems(4).Text
    End Sub

    Private Sub btnAgregar_Click(sender As Object, e As EventArgs) Handles btnAgregar.Click
        Dim unFrm As New frmAltaDocente_has_horario(funcionario, frmDocente)
        unFrm.Show()
    End Sub

    Private Sub frmDocente_has_horario_Activated(sender As Object, e As EventArgs) Handles Me.Activated
        Listar()
    End Sub

    Private Sub btnModificar_Click(sender As Object, e As EventArgs) Handles btnModificar.Click
        seleccionEsVacia = seleccionVacia()

        If seleccionEsVacia = False Then
            Dim salon As New clsSalon(lsvHorarios.FocusedItem.SubItems(5).Text, "")
            Dim materia As New clsMateria(lsvHorarios.FocusedItem.SubItems(4).Text, "")
            Dim horario As New clsHorarioDocente(vbEmpty, lsvHorarios.FocusedItem.SubItems(0).Text, lsvHorarios.FocusedItem.SubItems(1).Text)
            Dim grupo As New clsGrupo(lsvHorarios.FocusedItem.SubItems(2).Text, "")

            Dim unHorarioSeleccionado As New clsDocentes_horariosdocente(salon, materia, horario, funcionario, grupo, lsvHorarios.FocusedItem.SubItems(3).Text)

            Dim frmModificarHorario As New frmModificarDocente_has_horario(elUsuario, unHorarioSeleccionado, funcionario, frmDocente)
            frmModificarHorario.Show()
        End If
    End Sub

    Private Sub btnEliminar_Click(sender As Object, e As EventArgs) Handles btnEliminar.Click
        seleccionEsVacia = seleccionVacia()

        If seleccionEsVacia = False Then
            Dim salon As New clsSalon(lsvHorarios.FocusedItem.SubItems(5).Text, "")
            Dim materia As New clsMateria(lsvHorarios.FocusedItem.SubItems(4).Text, "")
            Dim horario As New clsHorarioDocente(vbEmpty, lsvHorarios.FocusedItem.SubItems(0).Text, lsvHorarios.FocusedItem.SubItems(1).Text)
            Dim grupo As New clsGrupo(lsvHorarios.FocusedItem.SubItems(2).Text, "")

            Dim unHorarioSeleccionado As New clsDocentes_horariosdocente(salon, materia, horario, funcionario, grupo, lsvHorarios.FocusedItem.SubItems(3).Text)
            Dim frmBajaHorario As New frmBajaDocente_has_horario(elUsuario, unHorarioSeleccionado, frmDocente)
            frmBajaHorario.Show()
        End If
    End Sub

    Private Sub frmDocente_has_horario_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        lblPersona.Text = funcionario.nombre & "  " & funcionario.apellido
        Listar()
    End Sub

    Private Sub frmDocente_has_horario_Resize(sender As Object, e As EventArgs) Handles Me.Resize
        Dim lsvx = Me.Size.Width
        Dim lsvy = Me.Size.Height
        Dim lsv_x = lsvx - 30
        Dim lsv_y = lsvy - 195
        Me.lsvHorarios.Size = New System.Drawing.Size(lsv_x, lsv_y)

        Dim btnAgregar_x = btnAgregar.Location.X
        Dim btnModificar_x = btnModificar.Location.X
        Dim btnEliminar_x = btnEliminar.Location.X
        Dim btnRecuperar_x = btnRecuperar.Location.X
        Dim btnI_x = btnI.Location.X
        Dim btnSalir_x = Me.Size.Width
        Dim SalirSize = btnEliminar.Size.Width / 2

        Dim btn_y = Me.Size.Height
        btn_y = btn_y - 81

        btnSalir_x = btnSalir_x - 81 - SalirSize

        Me.btnI.Location = New System.Drawing.Point(btnI_x, btn_y)
        Me.btnAgregar.Location = New System.Drawing.Point(btnAgregar_x, btn_y)
        Me.btnModificar.Location = New System.Drawing.Point(btnModificar_x, btn_y)
        Me.btnEliminar.Location = New System.Drawing.Point(btnEliminar_x, btn_y)
        Me.btnRecuperar.Location = New System.Drawing.Point(btnRecuperar_x, btn_y)
        Me.btnSalir.Location = New System.Drawing.Point(btnSalir_x, btn_y)
    End Sub

    Private Sub btnSalir_Click(sender As Object, e As EventArgs) Handles btnSalir.Click
        Me.Close()
    End Sub

    Private Sub btnRecuperar_Click(sender As Object, e As EventArgs) Handles btnRecuperar.Click
        seleccionEsVacia = seleccionVacia()

        If seleccionEsVacia = False Then
            Dim salon As New clsSalon(lsvHorarios.FocusedItem.SubItems(5).Text, "")
            Dim materia As New clsMateria(lsvHorarios.FocusedItem.SubItems(4).Text, "")
            Dim horario As New clsHorarioDocente(vbEmpty, lsvHorarios.FocusedItem.SubItems(0).Text, lsvHorarios.FocusedItem.SubItems(1).Text)
            Dim grupo As New clsGrupo(lsvHorarios.FocusedItem.SubItems(2).Text, "")

            Dim unHorarioSeleccionado As New clsDocentes_horariosdocente(salon, materia, horario, funcionario, grupo, lsvHorarios.FocusedItem.SubItems(3).Text)
            Dim unFrm As New frmAltaRecuperacionAdelanto(elUsuario, unHorarioSeleccionado, frmDocente)
            unFrm.Show()
        End If
    End Sub

    Private Sub PrintDocument1_PrintPage(sender As Object, e As Printing.PrintPageEventArgs) Handles PrintDocument1.PrintPage
        ' ACA indicamos los espacios , letras , todo en la impr.
        Dim h As Integer = 0
        h = 50
        e.Graphics.DrawString("Horarios de " & funcionario.nombre & "  " & funcionario.ci, New Drawing.Font("Times New Roman", 10), Brushes.Black, 50, h)
        h += 50

        For Each itm As ListViewItem In lsvHorarios.Items

            e.Graphics.DrawString(itm.Text, New Drawing.Font("Times New Roman", 10), Brushes.Black, 50, h)
            e.Graphics.DrawString(itm.SubItems(1).Text, New Drawing.Font("Times New Roman", 10), Brushes.Black, 150, h)
            e.Graphics.DrawString(itm.SubItems(2).Text, New Drawing.Font("Times New Roman", 10), Brushes.Black, 250, h)
            e.Graphics.DrawString(itm.SubItems(3).Text, New Drawing.Font("Times New Roman", 10), Brushes.Black, 350, h)
            e.Graphics.DrawString(itm.SubItems(4).Text, New Drawing.Font("Times New Roman", 10), Brushes.Black, 450, h)
            e.Graphics.DrawString(itm.SubItems(5).Text, New Drawing.Font("Times New Roman", 10), Brushes.Black, 550, h)

            h += 20
            ' ponemos  50 150 250 , eso es X en la IMPR. Y es H que cada vez que pasa una fila , hace un +20 y asi con cada una , no hacemos X variable porque no es necesario eso sera algo fijo.
        Next
    End Sub

    Private Sub btnI_Click(sender As Object, e As EventArgs) Handles btnI.Click
        PrintDialog1.Document = PrintDocument1
        PrintDocument1.PrinterSettings = PrintDialog1.PrinterSettings
        With PrintDocument1
            .PrinterSettings.DefaultPageSettings.Landscape = False
            .Print()

        End With
    End Sub
End Class