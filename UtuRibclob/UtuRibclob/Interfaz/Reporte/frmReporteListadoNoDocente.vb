﻿Public Class frmReporteListadoNoDocente
    Dim elUsuario As New clsUser

    Public Sub New(elU As clsUser)
        'TODO: Complete member initialization 
        elUsuario = elU
        InitializeComponent()
    End Sub

    Public Sub listar()
        Dim unaC As New clsControladora
        Dim colFaltas As New List(Of clsFaltaNoDocente)
        colFaltas = unaC.listarFaltasNoDocentes(txtInicial.Text, txtFinal.Text)

        Dim lsi As New ListViewItem
        lsvFaltas.Items.Clear()

        For Each unaF In colFaltas
            lsi = lsvFaltas.Items.Add(unaF.fecha)
            lsi.SubItems.Add(unaF.descripcion)
            lsi.SubItems.Add(unaF.funcionario.ci)
            lsi.SubItems.Add(unaF.funcionario.nombre & " " & unaF.funcionario.apellido)
            lsi.SubItems.Add(unaF.excepcion.tipoExcepcion)
            lsi.SubItems.Add(unaF.horaInicio)
            lsi.SubItems.Add(unaF.horaFinal)
        Next
    End Sub

    Private Sub frmRegistroInternos_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        listar()

        ' Le da el primer y ultimo dia del mes a los txt de fecha
        Dim FechaActual As Date = Now
        Dim DiasEnMes As Integer = Date.DaysInMonth(FechaActual.Year, FechaActual.Month)
        Dim UltimoDiaEnMes As Date = New Date(FechaActual.Year, FechaActual.Month, DiasEnMes)
        Dim PrimerDiaEnMes As Date = New Date(FechaActual.Year, FechaActual.Month, 1)

        txtInicial.Text = PrimerDiaEnMes
        txtFinal.Text = UltimoDiaEnMes
    End Sub

    Private Sub btnSalir_Click(sender As Object, e As EventArgs) Handles btnSalir.Click
        Me.Close()
    End Sub

    Private Sub btnI_Click(sender As Object, e As EventArgs)

    End Sub

    Private Sub PrintDocument1_PrintPage(sender As Object, e As Printing.PrintPageEventArgs) Handles PrintDocument1.PrintPage
        ' ACA indicamos los espacios , letras , todo en la impr.
        Dim h As Integer = 0
        h = 50
        e.Graphics.DrawString("Faltas de Funcionarios no Docentes", New Drawing.Font("Times New Roman", 10), Brushes.Black, 50, h)
        h += 50

        For Each itm As ListViewItem In lsvFaltas.Items

            e.Graphics.DrawString(itm.Text, New Drawing.Font("Times New Roman", 10), Brushes.Black, 50, h)
            e.Graphics.DrawString(itm.SubItems(1).Text, New Drawing.Font("Times New Roman", 10), Brushes.Black, 150, h)
            e.Graphics.DrawString(itm.SubItems(2).Text, New Drawing.Font("Times New Roman", 10), Brushes.Black, 250, h)
            e.Graphics.DrawString(itm.SubItems(3).Text, New Drawing.Font("Times New Roman", 10), Brushes.Black, 350, h)
            e.Graphics.DrawString(itm.SubItems(4).Text, New Drawing.Font("Times New Roman", 10), Brushes.Black, 450, h)
            e.Graphics.DrawString(itm.SubItems(5).Text, New Drawing.Font("Times New Roman", 10), Brushes.Black, 550, h)
            e.Graphics.DrawString(itm.SubItems(6).Text, New Drawing.Font("Times New Roman", 10), Brushes.Black, 650, h)

            h += 20
            ' ponemos  50 150 250 , eso es X en la IMPR. Y es H que cada vez que pasa una fila , hace un +20 y asi con cada una , no hacemos X variable porque no es necesario eso sera algo fijo.

        Next

    End Sub

    Private Sub btnListar_Click_2(sender As Object, e As EventArgs)
        listar()
    End Sub

    ' Cuando las fecha cambian automaticamente se vuelve a listar todo
    Private Sub txtInicial_TextChanged(sender As Object, e As EventArgs) Handles txtInicial.TextChanged
        listar()
    End Sub

    Private Sub txtFinal_TextChanged(sender As Object, e As EventArgs) Handles txtFinal.TextChanged
        listar()
    End Sub

    Private Sub bntI_Click(sender As Object, e As EventArgs) Handles bntI.Click
        'ACA indicamos las configuraciones del print se igualen a las puestas por el user en la pantalla de PrintDialog

        PrintDialog1.Document = PrintDocument1
        PrintDocument1.PrinterSettings = PrintDialog1.PrinterSettings
        With PrintDocument1
            .PrinterSettings.DefaultPageSettings.Landscape = False
            .Print()

        End With
    End Sub

    Private Sub frmReporteListadoNoDocente_Resize(sender As Object, e As EventArgs) Handles Me.Resize

    End Sub
End Class