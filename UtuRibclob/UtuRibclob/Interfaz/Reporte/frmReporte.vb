﻿Public Class frmReporte
    ' frm donde el usuario 
    Private Sub frmReporte_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        chxDocentes.Checked = True
        chxNoDocentes.Checked = True
    End Sub

    Private Sub btnReporte_Click(sender As Object, e As EventArgs) Handles btnReporte.Click

        Console.WriteLine(txtFecha.ToString)

        Dim fecha As Date
        Dim fechaAsString As String
        Dim futuro As Boolean = False
        Dim fechaToday As Date = Date.Today.AddDays(1)
        fecha = txtFecha.Value
        fechaAsString = fecha.ToString("yyyy-MM-dd hh:mm:ss")

        If fecha > fechaToday Then
            futuro = True
        End If

        If futuro = False Then

            If chxDocentes.Checked = True Then
                Dim unaC As New clsControladora
                Dim errorSQL As String = ""

                errorSQL = unaC.registroDiarioDocente(fechaAsString)
            End If

            If chxNoDocentes.Checked = True Then
                Dim unaC As New clsControladora
                Dim errorSQL As String = ""

                errorSQL = unaC.registroDiarioNoDocente(fechaAsString)
            End If

            If chxDocentes.Checked = False And chxNoDocentes.Checked = False Then
                MsgBox("Por favor, seleccione un objetivos para registrar faltas.")
            End If
        Else
            MsgBox("Por favor, seleccione una fecha que no sea a futuro.")
        End If
    End Sub

End Class