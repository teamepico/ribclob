﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmReporteListadoNoDocente
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmReporteListadoNoDocente))
        Me.btnSalir = New System.Windows.Forms.Button()
        Me.lsvFaltas = New System.Windows.Forms.ListView()
        Me.Fecha = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.Descripcion = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.CI = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.Nombre = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.Excepciones = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.PrintDialog1 = New System.Windows.Forms.PrintDialog()
        Me.PrintDocument1 = New System.Drawing.Printing.PrintDocument()
        Me.txtInicial = New System.Windows.Forms.DateTimePicker()
        Me.txtFinal = New System.Windows.Forms.DateTimePicker()
        Me.bntI = New System.Windows.Forms.Button()
        Me.HoraInicio = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.HoraFinal = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.SuspendLayout()
        '
        'btnSalir
        '
        Me.btnSalir.Location = New System.Drawing.Point(783, 633)
        Me.btnSalir.Margin = New System.Windows.Forms.Padding(4)
        Me.btnSalir.Name = "btnSalir"
        Me.btnSalir.Size = New System.Drawing.Size(156, 37)
        Me.btnSalir.TabIndex = 1
        Me.btnSalir.Text = "Salir"
        Me.btnSalir.UseVisualStyleBackColor = True
        '
        'lsvFaltas
        '
        Me.lsvFaltas.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.Fecha, Me.Descripcion, Me.CI, Me.Nombre, Me.Excepciones, Me.HoraInicio, Me.HoraFinal})
        Me.lsvFaltas.GridLines = True
        Me.lsvFaltas.Location = New System.Drawing.Point(16, 90)
        Me.lsvFaltas.Margin = New System.Windows.Forms.Padding(4)
        Me.lsvFaltas.MinimumSize = New System.Drawing.Size(921, 534)
        Me.lsvFaltas.Name = "lsvFaltas"
        Me.lsvFaltas.Size = New System.Drawing.Size(921, 534)
        Me.lsvFaltas.TabIndex = 6
        Me.lsvFaltas.UseCompatibleStateImageBehavior = False
        Me.lsvFaltas.View = System.Windows.Forms.View.Details
        '
        'Fecha
        '
        Me.Fecha.Text = "Fecha"
        Me.Fecha.Width = 90
        '
        'Descripcion
        '
        Me.Descripcion.Text = "Descripcion"
        Me.Descripcion.Width = 91
        '
        'CI
        '
        Me.CI.Text = "CI"
        Me.CI.Width = 102
        '
        'Nombre
        '
        Me.Nombre.Text = "Nombre"
        Me.Nombre.Width = 126
        '
        'Excepciones
        '
        Me.Excepciones.Text = "Excepciones"
        Me.Excepciones.Width = 105
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(520, 39)
        Me.Label3.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(43, 17)
        Me.Label3.TabIndex = 21
        Me.Label3.Text = "hasta"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(305, 39)
        Me.Label2.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(49, 17)
        Me.Label2.TabIndex = 20
        Me.Label2.Text = "Desde"
        '
        'PrintDialog1
        '
        Me.PrintDialog1.UseEXDialog = True
        '
        'PrintDocument1
        '
        '
        'txtInicial
        '
        Me.txtInicial.CustomFormat = "dd/MM/yyyy"
        Me.txtInicial.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.txtInicial.Location = New System.Drawing.Point(364, 34)
        Me.txtInicial.Margin = New System.Windows.Forms.Padding(4)
        Me.txtInicial.Name = "txtInicial"
        Me.txtInicial.Size = New System.Drawing.Size(147, 22)
        Me.txtInicial.TabIndex = 25
        '
        'txtFinal
        '
        Me.txtFinal.CustomFormat = "dd/MM/yyyy"
        Me.txtFinal.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.txtFinal.Location = New System.Drawing.Point(572, 34)
        Me.txtFinal.Margin = New System.Windows.Forms.Padding(4)
        Me.txtFinal.Name = "txtFinal"
        Me.txtFinal.Size = New System.Drawing.Size(147, 22)
        Me.txtFinal.TabIndex = 26
        '
        'bntI
        '
        Me.bntI.BackgroundImage = Global.UtuRibclob.My.Resources.Resources.print
        Me.bntI.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.bntI.Location = New System.Drawing.Point(16, 633)
        Me.bntI.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.bntI.Name = "bntI"
        Me.bntI.Size = New System.Drawing.Size(45, 38)
        Me.bntI.TabIndex = 27
        Me.bntI.UseVisualStyleBackColor = True
        '
        'HoraInicio
        '
        Me.HoraInicio.Text = "Hora Inicio"
        Me.HoraInicio.Width = 101
        '
        'HoraFinal
        '
        Me.HoraFinal.Text = "Hora Final"
        Me.HoraFinal.Width = 103
        '
        'frmReporteListadoNoDocente
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(952, 667)
        Me.Controls.Add(Me.bntI)
        Me.Controls.Add(Me.txtFinal)
        Me.Controls.Add(Me.txtInicial)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.lsvFaltas)
        Me.Controls.Add(Me.btnSalir)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Margin = New System.Windows.Forms.Padding(4)
        Me.MaximizeBox = False
        Me.MaximumSize = New System.Drawing.Size(970, 714)
        Me.MinimizeBox = False
        Me.MinimumSize = New System.Drawing.Size(970, 714)
        Me.Name = "frmReporteListadoNoDocente"
        Me.Text = "Faltas de funcionarios"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents btnSalir As System.Windows.Forms.Button
    Friend WithEvents lsvFaltas As System.Windows.Forms.ListView
    Friend WithEvents Fecha As System.Windows.Forms.ColumnHeader
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents PrintDialog1 As System.Windows.Forms.PrintDialog
    Friend WithEvents PrintDocument1 As System.Drawing.Printing.PrintDocument
    Friend WithEvents txtInicial As System.Windows.Forms.DateTimePicker
    Friend WithEvents txtFinal As System.Windows.Forms.DateTimePicker
    Friend WithEvents Descripcion As System.Windows.Forms.ColumnHeader
    Friend WithEvents CI As System.Windows.Forms.ColumnHeader
    Friend WithEvents Nombre As System.Windows.Forms.ColumnHeader
    Friend WithEvents Excepciones As System.Windows.Forms.ColumnHeader
    Friend WithEvents bntI As System.Windows.Forms.Button
    Friend WithEvents HoraInicio As System.Windows.Forms.ColumnHeader
    Friend WithEvents HoraFinal As System.Windows.Forms.ColumnHeader
End Class
