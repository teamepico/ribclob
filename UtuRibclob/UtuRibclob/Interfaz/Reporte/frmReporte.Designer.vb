﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmReporte
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmReporte))
        Me.btnReporte = New System.Windows.Forms.Button()
        Me.txtFecha = New System.Windows.Forms.DateTimePicker()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.chxNoDocentes = New System.Windows.Forms.CheckBox()
        Me.chxDocentes = New System.Windows.Forms.CheckBox()
        Me.GroupBox1.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.SuspendLayout()
        '
        'btnReporte
        '
        Me.btnReporte.Location = New System.Drawing.Point(64, 313)
        Me.btnReporte.Name = "btnReporte"
        Me.btnReporte.Size = New System.Drawing.Size(193, 32)
        Me.btnReporte.TabIndex = 0
        Me.btnReporte.Text = "Hacer reporte de faltas"
        Me.btnReporte.UseVisualStyleBackColor = True
        '
        'txtFecha
        '
        Me.txtFecha.Location = New System.Drawing.Point(18, 48)
        Me.txtFecha.Name = "txtFecha"
        Me.txtFecha.Size = New System.Drawing.Size(200, 20)
        Me.txtFecha.TabIndex = 1
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.txtFecha)
        Me.GroupBox1.Location = New System.Drawing.Point(39, 30)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(240, 100)
        Me.GroupBox1.TabIndex = 3
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Seleccione una fecha"
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.chxNoDocentes)
        Me.GroupBox2.Controls.Add(Me.chxDocentes)
        Me.GroupBox2.Location = New System.Drawing.Point(39, 156)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(240, 127)
        Me.GroupBox2.TabIndex = 4
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Seleccione objetivos"
        '
        'chxNoDocentes
        '
        Me.chxNoDocentes.AutoSize = True
        Me.chxNoDocentes.Location = New System.Drawing.Point(25, 79)
        Me.chxNoDocentes.Name = "chxNoDocentes"
        Me.chxNoDocentes.Size = New System.Drawing.Size(152, 17)
        Me.chxNoDocentes.TabIndex = 1
        Me.chxNoDocentes.Text = "Funcionarios No Docentes"
        Me.chxNoDocentes.UseVisualStyleBackColor = True
        '
        'chxDocentes
        '
        Me.chxDocentes.AutoSize = True
        Me.chxDocentes.Location = New System.Drawing.Point(25, 40)
        Me.chxDocentes.Name = "chxDocentes"
        Me.chxDocentes.Size = New System.Drawing.Size(135, 17)
        Me.chxDocentes.TabIndex = 0
        Me.chxDocentes.Text = "Funcionarios Docentes"
        Me.chxDocentes.UseVisualStyleBackColor = True
        '
        'frmReporte
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(320, 379)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.btnReporte)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.MaximumSize = New System.Drawing.Size(336, 418)
        Me.MinimumSize = New System.Drawing.Size(336, 418)
        Me.Name = "frmReporte"
        Me.Text = "Reporte diario de faltas"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents btnReporte As System.Windows.Forms.Button
    Friend WithEvents txtFecha As System.Windows.Forms.DateTimePicker
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents chxNoDocentes As System.Windows.Forms.CheckBox
    Friend WithEvents chxDocentes As System.Windows.Forms.CheckBox
End Class
