﻿Public Class frmAdelantoRecuperacion
    Dim elUsuario As New clsUser
    Dim idAdelantos As New List(Of String)
    Dim frmAdelanto As frmAdelantoRecuperacion
    Dim seleccionEsVacia As Boolean = False

    Public Function seleccionVacia() As Boolean
        Dim devolver As Boolean = False

        If lsvAdelantos.FocusedItem Is Nothing Then
            MsgBox("No se ha seleccionado ningun adelanto o recuperación.")
            devolver = True
        End If

        Return devolver
    End Function

    Public Sub New(elU As clsUser)
        'TODO: Complete member initialization 
        elUsuario = elU
        InitializeComponent()
    End Sub

    Public Sub listar()
        Dim unaC As New clsControladora
        Dim colAdelantoRecuperacion As New List(Of clsAdelantoRecuperacion)

        colAdelantoRecuperacion = unaC.listarAdelantoRecuperacion(txtInicial.Text, txtFinal.Text)

        Dim lsi As New ListViewItem
        lsvAdelantos.Items.Clear()

        For Each unAd In colAdelantoRecuperacion
            lsi = lsvAdelantos.Items.Add(unAd.docente.nombre & " " & unAd.docente.apellido)
            lsi.SubItems.Add(unAd.docente.ci)
            lsi.SubItems.Add(unAd.horario.diaHorario & " : " & unAd.horario.horario.entrada & " / " & unAd.horario.horario.salida)
            lsi.SubItems.Add(unAd.fecha)
            lsi.SubItems.Add(unAd.horaInicio)
            lsi.SubItems.Add(unAd.horaFinal)

            idAdelantos.Add(unAd.id)
        Next
    End Sub

    Private Sub frmAdelantoRecuperacion_Activated(sender As Object, e As EventArgs) Handles Me.Activated
        listar()
    End Sub

    Private Sub frmAdelantoRecuperacion_Load(sender As Object, e As EventArgs) Handles MyBase.Load

        ' Le da el primer y ultimo dia del mes a los txt de fecha
        Dim FechaActual As Date = Now
        Dim DiasEnMes As Integer = Date.DaysInMonth(FechaActual.Year, FechaActual.Month)
        Dim UltimoDiaEnMes As Date = New Date(FechaActual.Year, FechaActual.Month, DiasEnMes)
        Dim PrimerDiaEnMes As Date = New Date(FechaActual.Year, FechaActual.Month, 1)

        txtInicial.Text = PrimerDiaEnMes
        txtFinal.Text = UltimoDiaEnMes

        frmAdelanto = Me

        listar()
    End Sub

    ' Cuando las fecha cambian automaticamente se vuelve a listar todo
    Private Sub txtInicial_TextChanged(sender As Object, e As EventArgs) Handles txtInicial.TextChanged
        listar()
    End Sub

    Private Sub txtFinal_TextChanged(sender As Object, e As EventArgs) Handles txtFinal.TextChanged
        listar()
    End Sub


    Private Sub btnSalir_Click(sender As Object, e As EventArgs) Handles btnSalir.Click
        Me.Close()
    End Sub

    Private Sub btnEliminar_Click(sender As Object, e As EventArgs) Handles btnEliminar.Click
        seleccionEsVacia = seleccionVacia()

        If seleccionEsVacia = False Then
            Dim unDoc As New clsFuncionarioDocente()
            unDoc.ci = lsvAdelantos.FocusedItem.SubItems(1).Text
            Dim unHor As New clsDocentes_horariosdocente()
            Dim unAd As New clsAdelantoRecuperacion(vbEmpty, lsvAdelantos.FocusedItem.SubItems(3).Text, lsvAdelantos.FocusedItem.SubItems(4).Text, lsvAdelantos.FocusedItem.SubItems(5).Text, "", unDoc, unHor)

            Dim unaC As New clsControladora
            Dim unAdCompleto As clsAdelantoRecuperacion = unaC.obtenerDatosAdelanto(unAd)

            Dim unFrm As New frmBajaAdelantoRecuperacion(elUsuario, unAdCompleto, frmAdelanto)
            unFrm.Show()
        End If
    End Sub

    Private Sub btnModificar_Click(sender As Object, e As EventArgs) Handles btnModificar.Click
        seleccionEsVacia = seleccionVacia()

        If seleccionEsVacia = False Then
            Dim unDoc As New clsFuncionarioDocente()
            unDoc.ci = lsvAdelantos.FocusedItem.SubItems(1).Text
            Dim unHor As New clsDocentes_horariosdocente()
            Dim unAd As New clsAdelantoRecuperacion(vbEmpty, lsvAdelantos.FocusedItem.SubItems(3).Text, lsvAdelantos.FocusedItem.SubItems(4).Text, lsvAdelantos.FocusedItem.SubItems(5).Text, "", unDoc, unHor)

            Dim unaC As New clsControladora
            Dim unAdCompleto As clsAdelantoRecuperacion = unaC.obtenerDatosAdelanto(unAd)

            Dim unFrm As New frmModificarAdelantoRecuperacion(elUsuario, unAdCompleto, frmAdelanto)
            unFrm.Show()
        End If
    End Sub
End Class