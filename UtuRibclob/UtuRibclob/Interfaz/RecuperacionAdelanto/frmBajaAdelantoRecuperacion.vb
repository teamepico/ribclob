﻿Public Class frmBajaAdelantoRecuperacion
    Dim unUsuario As New clsUser
    Dim unAdelanto As New clsAdelantoRecuperacion
    Dim unFrm As frmAdelantoRecuperacion
    Dim adelantoFecha As Date

    Public Sub New(unU As clsUser, unA As clsAdelantoRecuperacion, frm As frmAdelantoRecuperacion)
        'TODO: Complete member initialization 
        InitializeComponent()

        unUsuario = unU
        unAdelanto = unA
        unFrm = frm
    End Sub

    Private Sub btnConfirmar_Click(sender As Object, e As EventArgs)

    End Sub

    Private Sub btnSalir_Click(sender As Object, e As EventArgs)
        Me.Close()
    End Sub

    Private Sub frmBajaAdelantoRecuperacion_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        ' Pasamos la fecha de Adelanto a algo que VBNET pueda entender
        adelantoFecha = Date.ParseExact(unAdelanto.fecha, "dd/MM/yyyy", System.Globalization.DateTimeFormatInfo.InvariantInfo)

        lblFecha.Text = adelantoFecha
        lblHorario.Text = unAdelanto.horaInicio & " " & unAdelanto.horaFinal
        lblNombre.Text = unAdelanto.docente.nombre & " " & unAdelanto.docente.apellido
        lblCI.Text = unAdelanto.docente.ci
    End Sub

    Private Sub btnCancelar_Click(sender As Object, e As EventArgs) Handles btnCancelar.Click
        Me.Close()
    End Sub

    Private Sub btnEliminar_Click(sender As Object, e As EventArgs) Handles btnEliminar.Click
        Dim unaC As New clsControladora
        Dim funcionario As New clsFuncionarioDocente
        Dim horario As New clsDocentes_horariosdocente
        Dim unHorario As New clsAdelantoRecuperacion(unAdelanto.id, "", "", "", "", funcionario, horario)
        Dim errorSql As String

        errorSql = unaC.bajaAdelantos(unHorario)

        If errorSql = "" Then
            unFrm.Activate()
            Me.Close()
        Else
            MsgBox("Error al modificar la recuperacion. Contacte al administrador.")
        End If
    End Sub
End Class