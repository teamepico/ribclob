﻿Public Class frmModificarAdelantoRecuperacion
    Dim adelanto As New clsAdelantoRecuperacion
    Dim usuario As New clsUser
    Dim unFrm As frmAdelantoRecuperacion

    Dim horaEntrada As Integer
    Dim horaSalida As Integer
    Dim minutoEntrada As Integer
    Dim minutoSalida As Integer

    Dim horaEsCorrecta As Boolean = False
    Dim adelantoFecha As Date

    Private Sub frmAltaRecuperacionAdelanto_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        ' Pasamos la fecha de Adelanto a algo que VBNET pueda entender
        adelantoFecha = Date.ParseExact(adelanto.fecha, "dd/MM/yyyy", System.Globalization.DateTimeFormatInfo.InvariantInfo)

        cargarDatos()
    End Sub

    Public Sub New(unU As clsUser, unA As clsAdelantoRecuperacion, frm As frmAdelantoRecuperacion)

        ' Llamada necesaria para el diseñador.
        InitializeComponent()

        ' Agregue cualquier inicialización después de la llamada a InitializeComponent().
        usuario = unU
        adelanto = unA
        unFrm = frm
    End Sub

    Private Sub cargarDatos()
        lblEntrada.Text = adelanto.horario.horario.entrada
        lblSalida.Text = adelanto.horario.horario.salida
        lblDia.Text = adelanto.horario.diaHorario
        lblDocente.Text = adelanto.docente.nombre & " " & adelanto.docente.apellido
        lblCI.Text = adelanto.docente.ci

        txtEntrada.Text = adelanto.horaInicio
        txtSalida.Text = adelanto.horaFinal
        txtFecha.Text = adelantoFecha

        txtEntrada.ValidateText()
        txtSalida.ValidateText()
    End Sub

    Private Sub txtEntrada_Validating(sender As Object, e As System.ComponentModel.CancelEventArgs) Handles txtEntrada.Validating
        Dim value As String = txtEntrada.Text.Replace(":", "").Trim
        If value = String.Empty Then Return

        Dim hora As Int32
        If Int32.TryParse(value.Substring(0, 2), hora) = False Then
            e.Cancel = True
            Return
        Else
            If hora < 0 OrElse hora > 23 Then
                MsgBox("Ese horario es incorrecto, ingrese el horario en formato de 24hs.")
                e.Cancel = True
                Return
            End If
        End If

        Dim minuto As Int32
        If Int32.TryParse(value.Substring(2, 2), minuto) = False Then
            e.Cancel = True
            Return
        Else
            If minuto < 0 OrElse minuto > 59 Then
                e.Cancel = True
                MsgBox("Ese horario es incorrecto, ingrese el horario en formato de 24hs.")
                Return
            End If
        End If

        horaEntrada = hora
        minutoEntrada = minuto
    End Sub

    Private Sub txtSalida_Validating(sender As Object, e As System.ComponentModel.CancelEventArgs) Handles txtSalida.Validating
        Dim value As String = txtSalida.Text.Replace(":", "").Trim
        Dim horarioSalida As String = ""

        If value = String.Empty Then Return

        Dim hora As Int32
        If Int32.TryParse(value.Substring(0, 2), hora) = False Then
            e.Cancel = True
            Return
        Else
            If hora < 0 OrElse hora > 23 Then
                e.Cancel = True
                MsgBox("Ese horario es incorrecto, ingrese el horario en formato de 24hs.")
                Return
            End If
        End If

        Dim minuto As Int32
        If Int32.TryParse(value.Substring(2, 2), minuto) = False Then
            e.Cancel = True
            Return
        Else
            If minuto < 0 OrElse minuto > 59 Then
                e.Cancel = True
                MsgBox("Ese horario es incorrecto, ingrese el horario en formato de 24hs.")
                Return
            End If
        End If

        horaSalida = hora
        minutoSalida = minuto
    End Sub

    Private Sub btnAgregar_Click(sender As Object, e As EventArgs) Handles btnAgregar.Click
        Dim unaC As New clsControladora

        If horaSalida > horaEntrada Then
            horaEsCorrecta = True
        ElseIf horaEntrada = horaSalida And minutoSalida > minutoEntrada Then
            horaEsCorrecta = True
        End If

        Dim horarioEntrada As String = txtEntrada.Text & ":00"
        Dim horarioSalida As String = txtSalida.Text & ":00"

        If horaEsCorrecta = True Then
            Dim unHorario As New clsAdelantoRecuperacion(adelanto.id, txtFecha.Text, txtEntrada.Text, txtSalida.Text, lblDia.Text, adelanto.docente, adelanto.horario)
            Dim errorSql As String

            errorSql = unaC.modificarAdelanto(unHorario)

            If errorSql = "" Then
                unFrm.Activate()
                Me.Close()
            Else
                MsgBox("Error al modificar la recuperacion. Contacte al administrador.")
            End If
        Else
            MsgBox("El horario de salida no puede ser menor a el de entrada.")
        End If
    End Sub
End Class