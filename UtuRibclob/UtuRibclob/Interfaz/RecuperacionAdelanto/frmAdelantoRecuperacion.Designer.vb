﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmAdelantoRecuperacion
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmAdelantoRecuperacion))
        Me.btnSalir = New System.Windows.Forms.Button()
        Me.lsvAdelantos = New System.Windows.Forms.ListView()
        Me.Nombre = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.CI = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.Horario = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.Fecha = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.HoraEntrada = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.HoraSalida = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.txtFinal = New System.Windows.Forms.DateTimePicker()
        Me.txtInicial = New System.Windows.Forms.DateTimePicker()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.btnEliminar = New System.Windows.Forms.Button()
        Me.btnModificar = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'btnSalir
        '
        Me.btnSalir.Location = New System.Drawing.Point(1152, 641)
        Me.btnSalir.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.btnSalir.Name = "btnSalir"
        Me.btnSalir.Size = New System.Drawing.Size(156, 37)
        Me.btnSalir.TabIndex = 1
        Me.btnSalir.Text = "Salir"
        Me.btnSalir.UseVisualStyleBackColor = True
        '
        'lsvAdelantos
        '
        Me.lsvAdelantos.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.Nombre, Me.CI, Me.Horario, Me.Fecha, Me.HoraEntrada, Me.HoraSalida})
        Me.lsvAdelantos.GridLines = True
        Me.lsvAdelantos.Location = New System.Drawing.Point(16, 98)
        Me.lsvAdelantos.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.lsvAdelantos.Name = "lsvAdelantos"
        Me.lsvAdelantos.Size = New System.Drawing.Size(1291, 534)
        Me.lsvAdelantos.TabIndex = 6
        Me.lsvAdelantos.UseCompatibleStateImageBehavior = False
        Me.lsvAdelantos.View = System.Windows.Forms.View.Details
        '
        'Nombre
        '
        Me.Nombre.Text = "Nombre"
        Me.Nombre.Width = 138
        '
        'CI
        '
        Me.CI.Text = "CI"
        Me.CI.Width = 123
        '
        'Horario
        '
        Me.Horario.Text = "Horario a reemplazar"
        Me.Horario.Width = 210
        '
        'Fecha
        '
        Me.Fecha.Text = "Fecha"
        Me.Fecha.Width = 137
        '
        'HoraEntrada
        '
        Me.HoraEntrada.Text = "Hora Entrada"
        Me.HoraEntrada.Width = 114
        '
        'HoraSalida
        '
        Me.HoraSalida.Text = "Hora Salida"
        Me.HoraSalida.Width = 124
        '
        'txtFinal
        '
        Me.txtFinal.CustomFormat = "dd/MM/yyyy"
        Me.txtFinal.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.txtFinal.Location = New System.Drawing.Point(365, 36)
        Me.txtFinal.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.txtFinal.Name = "txtFinal"
        Me.txtFinal.Size = New System.Drawing.Size(147, 22)
        Me.txtFinal.TabIndex = 34
        '
        'txtInicial
        '
        Me.txtInicial.CustomFormat = "dd/MM/yyyy"
        Me.txtInicial.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.txtInicial.Location = New System.Drawing.Point(157, 36)
        Me.txtInicial.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.txtInicial.Name = "txtInicial"
        Me.txtInicial.Size = New System.Drawing.Size(147, 22)
        Me.txtInicial.TabIndex = 33
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(313, 41)
        Me.Label3.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(43, 17)
        Me.Label3.TabIndex = 32
        Me.Label3.Text = "hasta"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(99, 41)
        Me.Label2.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(49, 17)
        Me.Label2.TabIndex = 31
        Me.Label2.Text = "Desde"
        '
        'btnEliminar
        '
        Me.btnEliminar.Location = New System.Drawing.Point(16, 641)
        Me.btnEliminar.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.btnEliminar.Name = "btnEliminar"
        Me.btnEliminar.Size = New System.Drawing.Size(156, 37)
        Me.btnEliminar.TabIndex = 18
        Me.btnEliminar.Text = "Eliminar"
        Me.btnEliminar.UseVisualStyleBackColor = True
        '
        'btnModificar
        '
        Me.btnModificar.Location = New System.Drawing.Point(180, 641)
        Me.btnModificar.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.btnModificar.Name = "btnModificar"
        Me.btnModificar.Size = New System.Drawing.Size(156, 37)
        Me.btnModificar.TabIndex = 35
        Me.btnModificar.Text = "Modificar"
        Me.btnModificar.UseVisualStyleBackColor = True
        '
        'frmAdelantoRecuperacion
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1321, 678)
        Me.Controls.Add(Me.btnModificar)
        Me.Controls.Add(Me.txtFinal)
        Me.Controls.Add(Me.txtInicial)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.btnEliminar)
        Me.Controls.Add(Me.lsvAdelantos)
        Me.Controls.Add(Me.btnSalir)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.MaximizeBox = False
        Me.MaximumSize = New System.Drawing.Size(1339, 725)
        Me.MinimumSize = New System.Drawing.Size(1339, 725)
        Me.Name = "frmAdelantoRecuperacion"
        Me.Text = "Adelantos y recuperaciones"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents btnSalir As System.Windows.Forms.Button
    Friend WithEvents lsvAdelantos As System.Windows.Forms.ListView
    Friend WithEvents HoraEntrada As System.Windows.Forms.ColumnHeader
    Friend WithEvents HoraSalida As System.Windows.Forms.ColumnHeader
    Friend WithEvents Fecha As System.Windows.Forms.ColumnHeader
    Friend WithEvents Horario As System.Windows.Forms.ColumnHeader
    Friend WithEvents txtFinal As System.Windows.Forms.DateTimePicker
    Friend WithEvents txtInicial As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents btnEliminar As System.Windows.Forms.Button
    Friend WithEvents CI As System.Windows.Forms.ColumnHeader
    Friend WithEvents Nombre As System.Windows.Forms.ColumnHeader
    Friend WithEvents btnModificar As System.Windows.Forms.Button
End Class
