﻿Public Class frmAltaRecuperacionAdelanto
    Dim horario As New clsDocentes_horariosdocente
    Dim usuario As New clsUser
    Dim unFrm As frmDocente_has_horario

    Dim horaEntrada As Integer
    Dim horaSalida As Integer
    Dim minutoEntrada As Integer
    Dim minutoSalida As Integer

    Dim horaEsCorrecta As Boolean = False

    Private Sub frmAltaRecuperacionAdelanto_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        cargarDatos()
    End Sub

    Public Sub New(unU As clsUser, unH As clsDocentes_horariosdocente, frm As frmDocente_has_horario)

        ' Llamada necesaria para el diseñador.
        InitializeComponent()

        ' Agregue cualquier inicialización después de la llamada a InitializeComponent().
        usuario = unU
        unFrm = frm
        horario = unH
    End Sub

    Private Sub cargarDatos()
        lblCI.Text = horario.docente.ci
        lblEntrada.Text = horario.horario.entrada
        lblSalida.Text = horario.horario.salida
        lblDocente.Text = horario.docente.nombre & " " & horario.docente.apellido
        lblDia.Text = horario.diaHorario
    End Sub

    Private Sub txtEntrada_Validating(sender As Object, e As System.ComponentModel.CancelEventArgs) Handles txtEntrada.Validating
        Dim value As String = txtEntrada.Text.Replace(":", "").Trim
        If value = String.Empty Then Return

        Dim hora As Int32
        If Int32.TryParse(value.Substring(0, 2), hora) = False Then
            e.Cancel = True
            Return
        Else
            If hora < 0 OrElse hora > 23 Then
                MsgBox("Ese horario es incorrecto, ingrese el horario en formato de 24hs.")
                e.Cancel = True
                Return
            End If
        End If

        Dim minuto As Int32
        If Int32.TryParse(value.Substring(2, 2), minuto) = False Then
            e.Cancel = True
            Return
        Else
            If minuto < 0 OrElse minuto > 59 Then
                e.Cancel = True
                MsgBox("Ese horario es incorrecto, ingrese el horario en formato de 24hs.")
                Return
            End If
        End If

        horaEntrada = hora
        minutoEntrada = minuto
    End Sub

    Private Sub txtSalida_Validating(sender As Object, e As System.ComponentModel.CancelEventArgs) Handles txtSalida.Validating
        Dim value As String = txtSalida.Text.Replace(":", "").Trim
        Dim horarioSalida As String = ""

        If value = String.Empty Then Return

        Dim hora As Int32
        If Int32.TryParse(value.Substring(0, 2), hora) = False Then
            e.Cancel = True
            Return
        Else
            If hora < 0 OrElse hora > 23 Then
                e.Cancel = True
                MsgBox("Ese horario es incorrecto, ingrese el horario en formato de 24hs.")
                Return
            End If
        End If

        Dim minuto As Int32
        If Int32.TryParse(value.Substring(2, 2), minuto) = False Then
            e.Cancel = True
            Return
        Else
            If minuto < 0 OrElse minuto > 59 Then
                e.Cancel = True
                MsgBox("Ese horario es incorrecto, ingrese el horario en formato de 24hs.")
                Return
            End If
        End If

        horaSalida = hora
        minutoSalida = minuto
    End Sub

    Private Sub btnAgregar_Click(sender As Object, e As EventArgs) Handles btnAgregar.Click
        Dim unaC As New clsControladora

        If horaSalida > horaEntrada Then
            horaEsCorrecta = True
        ElseIf horaEntrada = horaSalida And minutoSalida > minutoEntrada Then
            horaEsCorrecta = True
        End If

        Dim horarioEntrada As String = txtEntrada.Text & ":00"
        Dim horarioSalida As String = txtSalida.Text & ":00"

        If horaEsCorrecta = True Then
            Dim unHorario As New clsAdelantoRecuperacion(vbEmpty, txtFecha.Text, txtEntrada.Text, txtSalida.Text, lblDia.Text, horario.docente, horario)
            Dim errorSql As String

            errorSql = unaC.altaAdelanto(unHorario)

            If errorSql = "" Then
                unFrm.Activate()
                Me.Close()
            Else
                MsgBox("Error al insertar la recuperacion. Contacte al administrador.")
            End If
        Else
            MsgBox("El horario de salida no puede ser menor a el de entrada.")
        End If
    End Sub
End Class