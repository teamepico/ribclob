﻿Public Class frmAltaInterno
    Dim frmInternos As frmInternos

    Private Sub limpiar()
        txtCI.Clear()
        txtApellido.Clear()
        txtNombre.Clear()
        txtCorreo.Clear()
        txtDireccion.Clear()
        txtNumero.Clear()
        txtCurso.Clear()
        txtNumeroTutor.Clear()
        txtTutor.Clear()
    End Sub

    Private Sub btnAgregar_Click(sender As Object, e As EventArgs) Handles btnAgregar.Click
        Dim errorSQL As String
        'Primero tiramos el ci solo a la funcion verificar que pasa por la cls persona , y verifica que no exista otra cedula igual en la bsd , si esto da true es que hay otra persona.
        If verificarPersona(txtCI.Text) = True Then

            MsgBox("Existe otra Cedula igual")

        Else
            Dim unaC As New clsControladora
            Dim unI As New clsInterno(txtCI.Text.ToString, txtNombre.Text, txtApellido.Text, txtCorreo.Text, txtNumero.Text, txtDireccion.Text, txtTutor.Text, txtNumeroTutor.Text, txtCurso.Text)
            errorSQL = unaC.altaInterno(unI)

            If errorSQL = "" Then
                imgTick.Visible = True
                Timer1.Enabled = True
            Else
                MsgBox(errorSQL)
            End If

            limpiar()

            frmInternos.Activate()
            Me.Activate()
        End If
    End Sub

    Public Sub New(frm As frmInternos)
        frmInternos = frm
        InitializeComponent()
    End Sub

    Public Function verificarPersona(ByVal cCi As String) As Boolean
        Dim unaC As New clsControladora
        Return unaC.verificarPersona(cCi)
    End Function

    Private Sub frmAltaInterno_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        imgTick.Visible = False
        Timer1.Enabled = False
    End Sub

    Private Sub Timer1_Tick(sender As Object, e As EventArgs) Handles Timer1.Tick
        imgTick.Visible = False
        Timer1.Enabled = False
    End Sub
End Class