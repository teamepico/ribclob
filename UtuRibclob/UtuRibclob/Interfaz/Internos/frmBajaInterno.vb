﻿Public Class frmBajaInterno
    Dim bajaInterno As New clsInterno
    Dim datosUsuario As New clsUser

    Private Sub frmAltaInterno_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        lblUsuario.Text = bajaInterno.nombre & " " & bajaInterno.apellido
        lblCI.Text = bajaInterno.ci

    End Sub

    Public Sub New(bajaI As clsInterno, unU As clsUser)
        bajaInterno = bajaI
        datosUsuario = unU
        InitializeComponent()
    End Sub

    Private Sub btnEliminar_Click_1(sender As Object, e As EventArgs) Handles btnEliminar.Click
        Dim resultadoSQL As String
        Dim unaC As New clsControladora
        resultadoSQL = unaC.bajaInterno(bajaInterno.ci)

        If Not resultadoSQL = Nothing Then
            MsgBox(resultadoSQL)
        ElseIf resultadoSQL = Nothing Then
            Me.Close()
        End If
    End Sub

    Private Sub btnCancelar_Click(sender As Object, e As EventArgs) Handles btnCancelar.Click
        Me.Close()
    End Sub
End Class