﻿Public Class frmInternos
    Dim elUsuario As New clsUser
    Dim unFrm As frmInternos

    Public Sub New(elU As clsUser)
        ' TODO: Complete member initialization 
        elUsuario = elU
        unFrm = Me
        InitializeComponent()
    End Sub

    Public Function seleccionVacia() As Boolean
        Dim devolver As Boolean = False

        If lsvInternos.FocusedItem Is Nothing Then
            MsgBox("No se ha seleccionado ningún interno.")
            devolver = True
        End If

        Return devolver
    End Function

    Public Sub listar()
        Dim unaC As New clsControladora
        Dim colInternos = unaC.listarInternos()

        Dim lsi As New ListViewItem
        lsvInternos.Items.Clear()

        For Each unI In colInternos
            lsi = lsvInternos.Items.Add(unI.nombre)
            lsi.SubItems.Add(unI.apellido)
            lsi.SubItems.Add(unI.ci)
            lsi.SubItems.Add(unI.correo)
            lsi.SubItems.Add(unI.numero)
            lsi.SubItems.Add(unI.tutor)
            lsi.SubItems.Add(unI.numeroTutor)
        Next
    End Sub

    Private Sub frmInternos_Activated(sender As Object, e As EventArgs) Handles Me.Activated
        listar()
    End Sub

    Private Sub frmInternos_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Me.MinimumSize = New Size(982, 564)
        listar()
    End Sub

    Private Sub btnAgregar_Click(sender As Object, e As EventArgs) Handles btnAgregar.Click
        Dim unFormulario As New frmAltaInterno(unFrm)
        unFormulario.Show()

        listar()
    End Sub

    Private Sub btnSalir_Click(sender As Object, e As EventArgs) Handles btnSalir.Click
        Me.Close()
    End Sub

    Private Sub btnModificar_Click(sender As Object, e As EventArgs) Handles btnModificar.Click
        Dim seleccionEsVacia As Boolean
        seleccionEsVacia = seleccionVacia()

        If seleccionEsVacia = False Then
            Dim unForm As New frmModificarInterno(lsvInternos.FocusedItem.SubItems(2).Text, elUsuario)
            unForm.Show()
        End If
    End Sub

    Private Sub btnEliminar_Click(sender As Object, e As EventArgs) Handles btnEliminar.Click
        Dim seleccionEsVacia As Boolean
        seleccionEsVacia = seleccionVacia()

        If seleccionEsVacia = False Then
            Dim unInterno As New clsInterno(lsvInternos.FocusedItem.SubItems(2).Text, lsvInternos.FocusedItem.SubItems(0).Text, lsvInternos.FocusedItem.SubItems(1).Text, vbEmpty, vbEmpty, vbEmpty, vbEmpty, vbEmpty, vbEmpty)
            Dim unFormEliminar As New frmBajaInterno(unInterno, elUsuario)
            unFormEliminar.Show()
        End If
    End Sub

    Private Sub btnListar_Click(sender As Object, e As EventArgs)
        listar()
    End Sub

    Private Sub btnRegistro_Click(sender As Object, e As EventArgs) Handles btnRegistro.Click
        Dim seleccionEsVacia As Boolean
        seleccionEsVacia = seleccionVacia()

        If seleccionEsVacia = False Then
            Dim unInterno As New clsInterno(lsvInternos.FocusedItem.SubItems(2).Text, lsvInternos.FocusedItem.SubItems(0).Text, lsvInternos.FocusedItem.SubItems(1).Text, vbEmpty, vbEmpty, vbEmpty, vbEmpty, vbEmpty, vbEmpty)
            Dim unFormRegistro As New frmRegistroInternos(elUsuario, unInterno)
            unFormRegistro.Show()
        End If
    End Sub

    Private Sub txtBuscar_TextChanged(sender As Object, e As EventArgs) Handles txtBuscar.TextChanged
        Dim unaC As New clsControladora
        Dim colInternos = unaC.buscarInternos(txtBuscar.Text)

        Dim lsi As New ListViewItem
        lsvInternos.Items.Clear()

        For Each unI In colInternos
            lsi = lsvInternos.Items.Add(unI.nombre)
            lsi.SubItems.Add(unI.apellido)
            lsi.SubItems.Add(unI.ci)
            lsi.SubItems.Add(unI.numero)
        Next
    End Sub

    Private Sub frmInternos_Resize(sender As Object, e As EventArgs) Handles Me.Resize
        Dim lsv_x = Me.Size.Width
        Dim lsv_y = Me.Size.Height
        lsv_x = lsv_x - 40
        lsv_y = lsv_y - 129
        Me.lsvInternos.Size = New System.Drawing.Size(lsv_x, lsv_y)

        Dim btnAgregar_x = btnAgregar.Location.X
        Dim btnModificar_x = btnModificar.Location.X
        Dim btnEliminar_x = btnEliminar.Location.X
        Dim btnRegistro_x = btnRegistro.Location.X

        Dim btnSalir_x = Me.Size.Width
        Dim SalirSize = btnSalir.Size.Width / 2

        Dim btn_y = Me.Size.Height
        btn_y = btn_y - 81

        btnSalir_x = btnSalir_x - 81 - SalirSize

        Me.btnAgregar.Location = New System.Drawing.Point(btnAgregar_x, btn_y)
        Me.btnModificar.Location = New System.Drawing.Point(btnModificar_x, btn_y)
        Me.btnEliminar.Location = New System.Drawing.Point(btnEliminar_x, btn_y)
        Me.btnRegistro.Location = New System.Drawing.Point(btnRegistro_x, btn_y)
        Me.btnSalir.Location = New System.Drawing.Point(btnSalir_x, btn_y)

    End Sub
End Class