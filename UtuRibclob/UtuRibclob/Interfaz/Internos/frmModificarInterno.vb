﻿Public Class frmModificarInterno
    Dim ciInterno As String
    Dim datosUser As New clsUser
    Dim datosInterno As New clsInterno

    Public Sub New(ci As String, unU As clsUser)

        ciInterno = ci
        datosUser = unU

        ' Llamada necesaria para el diseñador.
        InitializeComponent()

        ' Agregue cualquier inicialización después de la llamada a InitializeComponent().

    End Sub

    Private Sub frmAltaInterno_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        datosInterno = obtenerDatos(ciInterno)

        txtCI.Text = datosInterno.ci
        txtApellido.Text = datosInterno.apellido
        txtNombre.Text = datosInterno.nombre
        txtCorreo.Text = datosInterno.correo
        txtDireccion.Text = datosInterno.direccion
        txtNumero.Text = datosInterno.numero
        txtNumeroTutor.Text = datosInterno.numeroTutor
        txtCurso.Text = datosInterno.curso
        txtTutor.Text = datosInterno.tutor
    End Sub

    Private Function obtenerDatos(ci As String) As clsInterno
        Dim unaControladora As New clsControladora
        Return unaControladora.obtenerDatosInterno(ci)
    End Function

    Private Sub limpiar()
        txtCI.Clear()
        txtApellido.Clear()
        txtNombre.Clear()
        txtCorreo.Clear()
        txtDireccion.Clear()
        txtCurso.Clear()
        txtNumeroTutor.Clear()
        txtTutor.Clear()
    End Sub

    Private Sub btnAgregar_Click(sender As Object, e As EventArgs) Handles btnAgregar.Click
        Dim unaC As New clsControladora
        Dim unI As New clsInterno(txtCI.Text.ToString, txtNombre.Text, txtApellido.Text, txtCorreo.Text, txtNumero.Text, txtDireccion.Text, txtTutor.Text, txtNumeroTutor.Text, txtCurso.Text)
        unaC.modificarInterno(unI)

        Me.Close()
    End Sub


    Public Function verificarPersona(ByVal cCi As String) As Boolean
        Dim unaC As New clsControladora
        Return unaC.verificarPersona(cCi)
    End Function
End Class