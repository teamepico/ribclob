﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmPrincipal
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmPrincipal))
        Me.MenuStrip1 = New System.Windows.Forms.MenuStrip()
        Me.FuncionariosToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.DocentesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.NoDocentesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.InternosToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.SalonesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.HorariosToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.MateriasToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.UsuariosToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.GruposToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ExcepcionesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.AdelantosToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.RegistroToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ReporteToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.DiarioToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.VerReporteMensualToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.VerReporteDiarioToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.lblUsuario = New System.Windows.Forms.Label()
        Me.MenuStrip1.SuspendLayout()
        Me.SuspendLayout()
        '
        'MenuStrip1
        '
        Me.MenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.FuncionariosToolStripMenuItem, Me.InternosToolStripMenuItem, Me.SalonesToolStripMenuItem, Me.HorariosToolStripMenuItem, Me.MateriasToolStripMenuItem, Me.UsuariosToolStripMenuItem, Me.GruposToolStripMenuItem, Me.ExcepcionesToolStripMenuItem, Me.AdelantosToolStripMenuItem, Me.RegistroToolStripMenuItem, Me.ReporteToolStripMenuItem})
        Me.MenuStrip1.Location = New System.Drawing.Point(0, 0)
        Me.MenuStrip1.Name = "MenuStrip1"
        Me.MenuStrip1.Size = New System.Drawing.Size(892, 24)
        Me.MenuStrip1.TabIndex = 0
        Me.MenuStrip1.Text = "MenuStrip1"
        '
        'FuncionariosToolStripMenuItem
        '
        Me.FuncionariosToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.DocentesToolStripMenuItem, Me.NoDocentesToolStripMenuItem})
        Me.FuncionariosToolStripMenuItem.Name = "FuncionariosToolStripMenuItem"
        Me.FuncionariosToolStripMenuItem.Size = New System.Drawing.Size(87, 20)
        Me.FuncionariosToolStripMenuItem.Text = "Funcionarios"
        '
        'DocentesToolStripMenuItem
        '
        Me.DocentesToolStripMenuItem.Name = "DocentesToolStripMenuItem"
        Me.DocentesToolStripMenuItem.Size = New System.Drawing.Size(142, 22)
        Me.DocentesToolStripMenuItem.Text = "Docentes"
        '
        'NoDocentesToolStripMenuItem
        '
        Me.NoDocentesToolStripMenuItem.Name = "NoDocentesToolStripMenuItem"
        Me.NoDocentesToolStripMenuItem.Size = New System.Drawing.Size(142, 22)
        Me.NoDocentesToolStripMenuItem.Text = "No Docentes"
        '
        'InternosToolStripMenuItem
        '
        Me.InternosToolStripMenuItem.Name = "InternosToolStripMenuItem"
        Me.InternosToolStripMenuItem.Size = New System.Drawing.Size(62, 20)
        Me.InternosToolStripMenuItem.Text = "Internos"
        '
        'SalonesToolStripMenuItem
        '
        Me.SalonesToolStripMenuItem.Name = "SalonesToolStripMenuItem"
        Me.SalonesToolStripMenuItem.Size = New System.Drawing.Size(59, 20)
        Me.SalonesToolStripMenuItem.Text = "Salones"
        '
        'HorariosToolStripMenuItem
        '
        Me.HorariosToolStripMenuItem.Name = "HorariosToolStripMenuItem"
        Me.HorariosToolStripMenuItem.Size = New System.Drawing.Size(99, 20)
        Me.HorariosToolStripMenuItem.Text = "Horario Escolar"
        '
        'MateriasToolStripMenuItem
        '
        Me.MateriasToolStripMenuItem.Name = "MateriasToolStripMenuItem"
        Me.MateriasToolStripMenuItem.Size = New System.Drawing.Size(64, 20)
        Me.MateriasToolStripMenuItem.Text = "Materias"
        '
        'UsuariosToolStripMenuItem
        '
        Me.UsuariosToolStripMenuItem.Name = "UsuariosToolStripMenuItem"
        Me.UsuariosToolStripMenuItem.Size = New System.Drawing.Size(64, 20)
        Me.UsuariosToolStripMenuItem.Text = "Usuarios"
        '
        'GruposToolStripMenuItem
        '
        Me.GruposToolStripMenuItem.Name = "GruposToolStripMenuItem"
        Me.GruposToolStripMenuItem.Size = New System.Drawing.Size(57, 20)
        Me.GruposToolStripMenuItem.Text = "Grupos"
        '
        'ExcepcionesToolStripMenuItem
        '
        Me.ExcepcionesToolStripMenuItem.Name = "ExcepcionesToolStripMenuItem"
        Me.ExcepcionesToolStripMenuItem.Size = New System.Drawing.Size(83, 20)
        Me.ExcepcionesToolStripMenuItem.Text = "Excepciones"
        '
        'AdelantosToolStripMenuItem
        '
        Me.AdelantosToolStripMenuItem.Name = "AdelantosToolStripMenuItem"
        Me.AdelantosToolStripMenuItem.Size = New System.Drawing.Size(164, 20)
        Me.AdelantosToolStripMenuItem.Text = "Adelantos y recuperaciones"
        '
        'RegistroToolStripMenuItem
        '
        Me.RegistroToolStripMenuItem.Name = "RegistroToolStripMenuItem"
        Me.RegistroToolStripMenuItem.Size = New System.Drawing.Size(62, 20)
        Me.RegistroToolStripMenuItem.Text = "Registro"
        '
        'ReporteToolStripMenuItem
        '
        Me.ReporteToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.DiarioToolStripMenuItem, Me.VerReporteMensualToolStripMenuItem, Me.VerReporteDiarioToolStripMenuItem})
        Me.ReporteToolStripMenuItem.Name = "ReporteToolStripMenuItem"
        Me.ReporteToolStripMenuItem.Size = New System.Drawing.Size(60, 20)
        Me.ReporteToolStripMenuItem.Text = "Reporte"
        '
        'DiarioToolStripMenuItem
        '
        Me.DiarioToolStripMenuItem.Name = "DiarioToolStripMenuItem"
        Me.DiarioToolStripMenuItem.ShowShortcutKeys = False
        Me.DiarioToolStripMenuItem.Size = New System.Drawing.Size(230, 22)
        Me.DiarioToolStripMenuItem.Text = "Realizar reporte"
        '
        'VerReporteMensualToolStripMenuItem
        '
        Me.VerReporteMensualToolStripMenuItem.Name = "VerReporteMensualToolStripMenuItem"
        Me.VerReporteMensualToolStripMenuItem.Size = New System.Drawing.Size(230, 22)
        Me.VerReporteMensualToolStripMenuItem.Text = "Reporte de faltas docentes"
        '
        'VerReporteDiarioToolStripMenuItem
        '
        Me.VerReporteDiarioToolStripMenuItem.Name = "VerReporteDiarioToolStripMenuItem"
        Me.VerReporteDiarioToolStripMenuItem.Size = New System.Drawing.Size(230, 22)
        Me.VerReporteDiarioToolStripMenuItem.Text = "Reporte de faltas no docentes"
        '
        'lblUsuario
        '
        Me.lblUsuario.AutoSize = True
        Me.lblUsuario.Font = New System.Drawing.Font("Segoe UI", 26.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblUsuario.Location = New System.Drawing.Point(274, 460)
        Me.lblUsuario.Name = "lblUsuario"
        Me.lblUsuario.Size = New System.Drawing.Size(337, 47)
        Me.lblUsuario.TabIndex = 1
        Me.lblUsuario.Text = "Bienvenido, Usuario!"
        '
        'frmPrincipal
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackgroundImage = Global.UtuRibclob.My.Resources.Resources.background2
        Me.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.ClientSize = New System.Drawing.Size(892, 553)
        Me.Controls.Add(Me.lblUsuario)
        Me.Controls.Add(Me.MenuStrip1)
        Me.DoubleBuffered = True
        Me.HelpButton = True
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MainMenuStrip = Me.MenuStrip1
        Me.Name = "frmPrincipal"
        Me.Text = "Ribclob"
        Me.MenuStrip1.ResumeLayout(False)
        Me.MenuStrip1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents MenuStrip1 As System.Windows.Forms.MenuStrip
    Friend WithEvents FuncionariosToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents DocentesToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents NoDocentesToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents InternosToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents SalonesToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents UsuariosToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents lblUsuario As System.Windows.Forms.Label
    Friend WithEvents HorariosToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents GruposToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents MateriasToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ExcepcionesToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents RegistroToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ReporteToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents DiarioToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents VerReporteMensualToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents VerReporteDiarioToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents AdelantosToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
End Class
