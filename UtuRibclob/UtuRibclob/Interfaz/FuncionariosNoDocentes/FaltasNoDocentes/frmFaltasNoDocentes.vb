﻿Public Class frmFaltasNoDocentes
    Dim elUsuario As New clsUser
    Dim elFuncionario As New clsFuncionarioNoDocente
    Dim frmFaltasNoDocentes As frmFaltasNoDocentes

    Dim seleccionEsVacia As Boolean = False

    Public Function seleccionVacia() As Boolean
        Dim devolver As Boolean = False
        frmFaltasNoDocentes = Me

        If lsvFaltas.FocusedItem Is Nothing Then
            MsgBox("No se ha seleccionado ninguna falta.")
            devolver = True
        End If

        Return devolver
    End Function

    Public Sub New(elU As clsUser, elF As clsFuncionarioNoDocente)
        'TODO: Complete member initialization 
        elUsuario = elU
        elFuncionario = elF
        frmFaltasNoDocentes = Me
        InitializeComponent()
    End Sub

    Public Sub listar()
        Dim unaC As New clsControladora
        Dim colRegistro As New List(Of clsFaltaNoDocente)
        colRegistro = unaC.listarFaltasNoDocentes(elFuncionario.ci, txtInicial.Text, txtFinal.Text)

        Dim lsi As New ListViewItem
        lsvFaltas.Items.Clear()

        For Each unaF In colRegistro
            lsi = lsvFaltas.Items.Add(unaF.fecha)
            lsi.SubItems.Add(unaF.descripcion)
            lsi.SubItems.Add(unaF.horaInicio)
            lsi.SubItems.Add(unaF.horaFinal)
            lsi.SubItems.Add(unaF.excepcion.idExcepcion)
        Next
    End Sub

    Private Sub frmFaltasNoDocentes_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        lblUser.Text = elFuncionario.nombre & " " & elFuncionario.apellido

        ' Le da el primer y ultimo dia del mes a los txt de fecha
        Dim FechaActual As Date = Now
        Dim DiasEnMes As Integer = Date.DaysInMonth(FechaActual.Year, FechaActual.Month)
        Dim UltimoDiaEnMes As Date = New Date(FechaActual.Year, FechaActual.Month, DiasEnMes)
        Dim PrimerDiaEnMes As Date = New Date(FechaActual.Year, FechaActual.Month, 1)

        txtInicial.Text = PrimerDiaEnMes
        txtFinal.Text = UltimoDiaEnMes

        listar()
    End Sub

    Private Sub btnAgregar_Click(sender As Object, e As EventArgs) Handles btnAgregar.Click
        Dim unFrm As New frmAltaFaltaNoDocente(elFuncionario)
        unFrm.Show()
    End Sub

    Private Sub btnSalir_Click(sender As Object, e As EventArgs) Handles btnSalir.Click
        Me.Close()
    End Sub

    ' Cuando las fecha cambian automaticamente se vuelve a listar todo
    Private Sub txtInicial_TextChanged(sender As Object, e As EventArgs) Handles txtInicial.TextChanged
        listar()
    End Sub

    Private Sub txtFinal_TextChanged(sender As Object, e As EventArgs) Handles txtFinal.TextChanged
        listar()
    End Sub

    Private Sub btnModificar_Click(sender As Object, e As EventArgs) Handles btnModificar.Click
        seleccionEsVacia = seleccionVacia()

        If seleccionEsVacia = False Then
            Dim unaExepcion As New clsExcepcion
            If lsvFaltas.FocusedItem.SubItems(4).Text = "" Then

                unaExepcion.idExcepcion = lsvFaltas.FocusedItem.SubItems(4).Text
            End If
            Dim unaFal As New clsFaltaNoDocente(vbEmpty, lsvFaltas.FocusedItem.SubItems(0).Text, lsvFaltas.FocusedItem.SubItems(2).Text, lsvFaltas.FocusedItem.SubItems(3).Text, lsvFaltas.FocusedItem.SubItems(1).Text, elFuncionario, unaExepcion)
            Dim unFrm As New frmModificarFaltaNoDocente(unaFal)
            unFrm.Show()
        End If
    End Sub

    Private Sub btnEliminar_Click(sender As Object, e As EventArgs) Handles btnEliminar.Click
        seleccionEsVacia = seleccionVacia()

        If seleccionEsVacia = False Then
            Dim unaExepcion As New clsExcepcion
            If lsvFaltas.FocusedItem.SubItems(4).Text = "" Then

                unaExepcion.idExcepcion = lsvFaltas.FocusedItem.SubItems(4).Text
            End If
            Dim unaFal As New clsFaltaNoDocente(vbEmpty, lsvFaltas.FocusedItem.SubItems(0).Text, lsvFaltas.FocusedItem.SubItems(2).Text, lsvFaltas.FocusedItem.SubItems(3).Text, lsvFaltas.FocusedItem.SubItems(1).Text, elFuncionario, unaExepcion)
            Dim unFrm As New frmBajaFaltaNoDocente(unaFal, frmFaltasNoDocentes)
            unFrm.Show()
        End If
    End Sub

    Private Sub btnI_Click(sender As Object, e As EventArgs) Handles btnI.Click
        'ACA indicamos las configuraciones del print se igualen a las puestas por el user en la pantalla de PrintDialog

        PrintDialog1.Document = PrintDocument1
        PrintDocument1.PrinterSettings = PrintDialog1.PrinterSettings
        With PrintDocument1
            .PrinterSettings.DefaultPageSettings.Landscape = False
            .Print()

        End With

    End Sub

    Private Sub PrintDocument1_PrintPage(sender As Object, e As Printing.PrintPageEventArgs) Handles PrintDocument1.PrintPage
        ' ACA indicamos los espacios , letras , todo en la impr.
        Dim h As Integer = 0
        h = 50
        e.Graphics.DrawString("Faltas Docentes", New Drawing.Font("Times New Roman", 10), Brushes.Black, 50, h)
        h += 50

        For Each itm As ListViewItem In lsvFaltas.Items

            e.Graphics.DrawString(itm.Text, New Drawing.Font("Times New Roman", 10), Brushes.Black, 50, h)
            e.Graphics.DrawString(itm.SubItems(1).Text, New Drawing.Font("Times New Roman", 10), Brushes.Black, 150, h)
            e.Graphics.DrawString(itm.SubItems(2).Text, New Drawing.Font("Times New Roman", 10), Brushes.Black, 250, h)
            e.Graphics.DrawString(itm.SubItems(3).Text, New Drawing.Font("Times New Roman", 10), Brushes.Black, 350, h)
            e.Graphics.DrawString(itm.SubItems(4).Text, New Drawing.Font("Times New Roman", 10), Brushes.Black, 450, h)
           

            h += 20
            ' ponemos  50 150 250 , eso es X en la IMPR. Y es H que cada vez que pasa una fila , hace un +20 y asi con cada una , no hacemos X variable porque no es necesario eso sera algo fijo.

        Next
    End Sub
End Class