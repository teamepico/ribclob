﻿Public Class frmAltaFaltaNoDocente
        Dim botones As New List(Of clsBoton)
        Dim colHorarios As New List(Of clsHorarioNoDocente)
        Dim docente As New clsFuncionarioNoDocente
        Private frmFalta As frmAdelantoRecuperacion
        Dim idHorarios As New List(Of String)
    Dim excepcion As New clsExcepcion

    Private Sub frmAltaFuncionarioDocente_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        imgTick.Visible = False
    End Sub

        Private Function listarHorarios() As List(Of clsHorarioNoDocente)
            Dim unaC As New clsControladora
            Return unaC.listarHorarioNoDocente(docente.ci)
        End Function

        Public Sub botonDinamicoHorario(colHorarios As List(Of clsHorarioNoDocente))

            Dim x = 400, y = 20
            Dim n As Integer = 0

            For Each unHorario In colHorarios

                Dim boton As New clsBoton
                botones.Add(boton)
                botones(n).Visible = True
                botones(n).Width = 100
                botones(n).Height = 50
                botones(n).BackColor = Color.White
                botones(n).Location = New Point(x, y) 'x,y
                botones(n).ForeColor = Color.Black
                botones(n).Name = "btn" & (n + 1)
                botones(n).entrada = unHorario.entradaHorarioNoDocente
                botones(n).Text = (botones(n).entrada)
                botones(n).salida = unHorario.salidaHorarioNoDocente
                botones(n).Text = botones(n).Text & vbCr & botones(n).salida
                botones(n).diaSemanal = unHorario.diaSemanalHorarioNoDocente
                botones(n).Text = botones(n).Text & vbCr & botones(n).diaSemanal
                botones(n).Font = New Font(FontFamily.GenericSansSerif, 10.0F, FontStyle.Bold)

                boton.idHorario = unHorario.idHorarioNoDocente

                If ((n + 1) Mod 2 = 0) Then
                    y += 110
                    x = 270
                End If

                AddHandler botones(n).Click, AddressOf eventoClick
                Me.Controls.Add(botones(n))

                x += 130
                n += 1
            Next

        End Sub

        Private Sub eventoClick(sender As Object, e As EventArgs)
            If CType(sender, clsBoton).presionado = False Then
                CType(sender, clsBoton).BackColor = Color.Blue
                CType(sender, clsBoton).presionado = True
            Else
                CType(sender, clsBoton).BackColor = Color.White
                CType(sender, clsBoton).presionado = False
            End If
        End Sub
        Public Sub recorrerFalta()
            Dim fecha As Date
            Dim fechaAsString As String
            fecha = txtFecha.Value
            fechaAsString = fecha.ToString("yyyy-MM-dd hh:mm:ss")

            If fechaAsString >= Today Then

                For Each boton In botones
                    Dim unaC As New clsControladora
                    If boton.presionado = True Then
                    Dim unaFalta As New clsFaltaNoDocente(vbEmpty, fechaAsString, boton.entrada, boton.salida, txtCausal.Text, docente, excepcion)
                    Dim errorsql As String
                    errorsql = unaC.altaFaltasNoDocentes(unaFalta)

                    If errorsql = "" Then
                        imgTick.Visible = True
                        Timer1.Enabled = True
                    End If
                End If
            Next

            Else

                MsgBox("Debe ingresar una falta futura, para cambiar causal modifique la falta ya existente")

            End If
        End Sub

        Private Sub frmAltaFaltaNoDocente_Load(sender As Object, e As EventArgs) Handles MyBase.Load
            colHorarios = listarHorarios()
            botonDinamicoHorario(colHorarios)
            recorrerFalta()
            lblNombreDoc.Text = docente.nombre & "  " & docente.apellido

        End Sub

    Private Sub btnAgregar_Click(sender As Object, e As EventArgs) Handles btnAgregar.Click

        recorrerFalta()
    End Sub
        Public Sub New(funcionario As clsFuncionarioNoDocente)
            docente = funcionario
            ' Llamada necesaria para el diseñador.
            InitializeComponent()

            ' Agregue cualquier inicialización después de la llamada a InitializeComponent().

        End Sub

    Private Sub Timer1_Tick(sender As Object, e As EventArgs) Handles Timer1.Tick
        imgTick.Visible = False
        Timer1.Enabled = False
    End Sub





End Class