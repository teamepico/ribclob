﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmBajaFaltaNoDocente
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.lblEstasSeguro = New System.Windows.Forms.Label()
        Me.lblAdvertencia = New System.Windows.Forms.Label()
        Me.btnCancelar = New System.Windows.Forms.Button()
        Me.btnEliminar = New System.Windows.Forms.Button()
        Me.lblCausal = New System.Windows.Forms.Label()
        Me.lblFecha = New System.Windows.Forms.Label()
        Me.SuspendLayout()
        '
        'lblEstasSeguro
        '
        Me.lblEstasSeguro.AutoSize = True
        Me.lblEstasSeguro.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEstasSeguro.Location = New System.Drawing.Point(40, 239)
        Me.lblEstasSeguro.Name = "lblEstasSeguro"
        Me.lblEstasSeguro.Size = New System.Drawing.Size(104, 18)
        Me.lblEstasSeguro.TabIndex = 33
        Me.lblEstasSeguro.Text = "¿Esta seguro?"
        '
        'lblAdvertencia
        '
        Me.lblAdvertencia.AutoSize = True
        Me.lblAdvertencia.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAdvertencia.Location = New System.Drawing.Point(12, 18)
        Me.lblAdvertencia.Name = "lblAdvertencia"
        Me.lblAdvertencia.Size = New System.Drawing.Size(294, 18)
        Me.lblAdvertencia.TabIndex = 31
        Me.lblAdvertencia.Text = "Atencion, esta por eliminar la siguiente falta:"
        '
        'btnCancelar
        '
        Me.btnCancelar.Location = New System.Drawing.Point(110, 275)
        Me.btnCancelar.Name = "btnCancelar"
        Me.btnCancelar.Size = New System.Drawing.Size(124, 33)
        Me.btnCancelar.TabIndex = 30
        Me.btnCancelar.Text = "Cancelar"
        Me.btnCancelar.UseVisualStyleBackColor = True
        '
        'btnEliminar
        '
        Me.btnEliminar.Location = New System.Drawing.Point(239, 275)
        Me.btnEliminar.Name = "btnEliminar"
        Me.btnEliminar.Size = New System.Drawing.Size(124, 33)
        Me.btnEliminar.TabIndex = 29
        Me.btnEliminar.Text = "Confirmar"
        Me.btnEliminar.UseVisualStyleBackColor = True
        '
        'lblCausal
        '
        Me.lblCausal.AutoSize = True
        Me.lblCausal.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCausal.Location = New System.Drawing.Point(39, 94)
        Me.lblCausal.Name = "lblCausal"
        Me.lblCausal.Size = New System.Drawing.Size(58, 20)
        Me.lblCausal.TabIndex = 35
        Me.lblCausal.Text = "Causal"
        '
        'lblFecha
        '
        Me.lblFecha.AutoSize = True
        Me.lblFecha.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblFecha.Location = New System.Drawing.Point(39, 56)
        Me.lblFecha.Name = "lblFecha"
        Me.lblFecha.Size = New System.Drawing.Size(54, 20)
        Me.lblFecha.TabIndex = 34
        Me.lblFecha.Text = "Fecha"
        '
        'frmBajaFaltaNoDocente
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(375, 320)
        Me.Controls.Add(Me.lblCausal)
        Me.Controls.Add(Me.lblFecha)
        Me.Controls.Add(Me.lblEstasSeguro)
        Me.Controls.Add(Me.lblAdvertencia)
        Me.Controls.Add(Me.btnCancelar)
        Me.Controls.Add(Me.btnEliminar)
        Me.MaximizeBox = False
        Me.MaximumSize = New System.Drawing.Size(391, 359)
        Me.MinimizeBox = False
        Me.MinimumSize = New System.Drawing.Size(391, 359)
        Me.Name = "frmBajaFaltaNoDocente"
        Me.ShowIcon = False
        Me.ShowInTaskbar = False
        Me.Text = "Eliminar falta no docente"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents lblEstasSeguro As System.Windows.Forms.Label
    Friend WithEvents lblAdvertencia As System.Windows.Forms.Label
    Friend WithEvents btnCancelar As System.Windows.Forms.Button
    Friend WithEvents btnEliminar As System.Windows.Forms.Button
    Friend WithEvents lblCausal As System.Windows.Forms.Label
    Friend WithEvents lblFecha As System.Windows.Forms.Label
End Class
