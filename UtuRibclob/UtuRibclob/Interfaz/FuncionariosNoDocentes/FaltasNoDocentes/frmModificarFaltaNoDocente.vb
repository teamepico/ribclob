﻿Public Class frmModificarFaltaNoDocente
    Dim falta As New clsFaltaNoDocente
    Dim excepcion As New clsExcepcion

    Private Sub frmModificarFaltaNoDocente_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        imgTick.Visible = False
        lvlDocente.Text = falta.funcionario.nombre & "  " & falta.funcionario.apellido
        lvlFecha.Text = falta.fecha
        lvlHorainicio.Text = falta.horaInicio
        lvlHorafinal.Text = falta.horaFinal

    End Sub


    Public Sub New(faltaNoDocente As clsFaltaNoDocente)
        falta = faltaNoDocente
        ' Llamada necesaria para el diseñador.
        InitializeComponent()

        ' Agregue cualquier inicialización después de la llamada a InitializeComponent().

    End Sub

    Private Sub btnAceptar_Click(sender As Object, e As EventArgs) Handles btnAceptar.Click
        Dim unaC As New clsControladora
        Dim unaFalta As New clsFaltaNoDocente(vbEmpty, lvlFecha.Text, lvlHorainicio.Text, lvlHorafinal.Text, txtCausal.Text, falta.funcionario, excepcion)
        Dim errorsql As String
        errorsql = unaC.modificarFaltasNoDocentes(unaFalta)

        If errorsql = "" Then
            imgTick.Visible = True
            Timer1.Enabled = True
        End If

    End Sub

    Private Sub Timer1_Tick(sender As Object, e As EventArgs) Handles Timer1.Tick
        imgTick.Visible = False
        Timer1.Enabled = False
    End Sub

End Class