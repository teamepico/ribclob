﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmModificarFaltaNoDocente
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.lvlHorafinal = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.lvlHorainicio = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.lvlFecha = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.imgTick = New System.Windows.Forms.PictureBox()
        Me.btnAceptar = New System.Windows.Forms.Button()
        Me.txtCausal = New System.Windows.Forms.TextBox()
        Me.lvlDocente = New System.Windows.Forms.Label()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.Timer1 = New System.Windows.Forms.Timer(Me.components)
        CType(Me.imgTick, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'lvlHorafinal
        '
        Me.lvlHorafinal.AutoSize = True
        Me.lvlHorafinal.Location = New System.Drawing.Point(78, 148)
        Me.lvlHorafinal.Name = "lvlHorafinal"
        Me.lvlHorafinal.Size = New System.Drawing.Size(39, 13)
        Me.lvlHorafinal.TabIndex = 102
        Me.lvlHorafinal.Text = "Label4"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(12, 148)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(58, 13)
        Me.Label5.TabIndex = 101
        Me.Label5.Text = "Hora Final:"
        '
        'lvlHorainicio
        '
        Me.lvlHorainicio.AutoSize = True
        Me.lvlHorainicio.Location = New System.Drawing.Point(78, 135)
        Me.lvlHorainicio.Name = "lvlHorainicio"
        Me.lvlHorainicio.Size = New System.Drawing.Size(39, 13)
        Me.lvlHorainicio.TabIndex = 100
        Me.lvlHorainicio.Text = "Label4"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(12, 135)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(60, 13)
        Me.Label1.TabIndex = 99
        Me.Label1.Text = "Hora inicio:"
        '
        'lvlFecha
        '
        Me.lvlFecha.AutoSize = True
        Me.lvlFecha.Location = New System.Drawing.Point(55, 113)
        Me.lvlFecha.Name = "lvlFecha"
        Me.lvlFecha.Size = New System.Drawing.Size(39, 13)
        Me.lvlFecha.TabIndex = 98
        Me.lvlFecha.Text = "Label1"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(9, 113)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(40, 13)
        Me.Label4.TabIndex = 97
        Me.Label4.Text = "Fecha:"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(7, 49)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(42, 13)
        Me.Label7.TabIndex = 96
        Me.Label7.Text = "Causal:"
        '
        'imgTick
        '
        Me.imgTick.Image = Global.UtuRibclob.My.Resources.Resources.tick
        Me.imgTick.Location = New System.Drawing.Point(160, 176)
        Me.imgTick.Name = "imgTick"
        Me.imgTick.Size = New System.Drawing.Size(35, 33)
        Me.imgTick.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.imgTick.TabIndex = 95
        Me.imgTick.TabStop = False
        Me.imgTick.Visible = False
        '
        'btnAceptar
        '
        Me.btnAceptar.Location = New System.Drawing.Point(15, 175)
        Me.btnAceptar.Margin = New System.Windows.Forms.Padding(2, 2, 2, 2)
        Me.btnAceptar.Name = "btnAceptar"
        Me.btnAceptar.Size = New System.Drawing.Size(140, 34)
        Me.btnAceptar.TabIndex = 94
        Me.btnAceptar.Text = "Aceptar"
        Me.btnAceptar.UseVisualStyleBackColor = True
        '
        'txtCausal
        '
        Me.txtCausal.Location = New System.Drawing.Point(51, 49)
        Me.txtCausal.Margin = New System.Windows.Forms.Padding(2, 2, 2, 2)
        Me.txtCausal.Multiline = True
        Me.txtCausal.Name = "txtCausal"
        Me.txtCausal.Size = New System.Drawing.Size(177, 53)
        Me.txtCausal.TabIndex = 93
        '
        'lvlDocente
        '
        Me.lvlDocente.AutoSize = True
        Me.lvlDocente.Location = New System.Drawing.Point(189, 9)
        Me.lvlDocente.Name = "lvlDocente"
        Me.lvlDocente.Size = New System.Drawing.Size(39, 13)
        Me.lvlDocente.TabIndex = 92
        Me.lvlDocente.Text = "Docen"
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label9.Location = New System.Drawing.Point(12, 9)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(130, 13)
        Me.Label9.TabIndex = 91
        Me.Label9.Text = "Modificando Falta de:"
        '
        'Timer1
        '
        '
        'frmModificarFaltaNoDocente
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(247, 235)
        Me.Controls.Add(Me.lvlHorafinal)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.lvlHorainicio)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.lvlFecha)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.Label7)
        Me.Controls.Add(Me.imgTick)
        Me.Controls.Add(Me.btnAceptar)
        Me.Controls.Add(Me.txtCausal)
        Me.Controls.Add(Me.lvlDocente)
        Me.Controls.Add(Me.Label9)
        Me.MaximizeBox = False
        Me.MaximumSize = New System.Drawing.Size(263, 274)
        Me.MinimizeBox = False
        Me.MinimumSize = New System.Drawing.Size(263, 274)
        Me.Name = "frmModificarFaltaNoDocente"
        Me.ShowIcon = False
        Me.ShowInTaskbar = False
        Me.Text = "Modificar Faltas No Docente"
        Me.TopMost = True
        CType(Me.imgTick, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents lvlHorafinal As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents lvlHorainicio As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents lvlFecha As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents imgTick As System.Windows.Forms.PictureBox
    Friend WithEvents btnAceptar As System.Windows.Forms.Button
    Friend WithEvents txtCausal As System.Windows.Forms.TextBox
    Friend WithEvents lvlDocente As System.Windows.Forms.Label
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents Timer1 As System.Windows.Forms.Timer
End Class
