﻿Public Class frmBajaFaltaNoDocente
    Dim falta As New clsFaltaNoDocente
    Dim frmFalta As frmFaltasNoDocentes

    Private fechaActual As Date = DateTime.Now.ToString("yyyy/MM/dd")
    Private fechaFalta As Date

    Public Sub New(unaF As clsFaltaNoDocente, unFrm As frmFaltasNoDocentes)

        ' Llamada necesaria para el diseñador.
        InitializeComponent()

        ' Agregue cualquier inicialización después de la llamada a InitializeComponent().
        falta = unaF
        frmFalta = unFrm
    End Sub

    Private Sub frmBajaFaltaNoDocente_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Dim fechaFalta As Date = Date.ParseExact(falta.fecha, "dd/MM/yyyy", System.Globalization.DateTimeFormatInfo.InvariantInfo)

        If fechaFalta = fechaActual Or fechaActual > fechaFalta Then
            MsgBox("Solo se pueden modificar faltas que sean avisos.")
            Me.Close()
        End If

        lblCausal.Text = falta.descripcion
        lblFecha.Text = falta.fecha
    End Sub

    Private Sub checkFecha()

    End Sub

    Private Sub btnCancelar_Click(sender As Object, e As EventArgs) Handles btnCancelar.Click
        Me.Close()
    End Sub

    Private Sub btnEliminar_Click(sender As Object, e As EventArgs) Handles btnEliminar.Click
        Dim resultadoSQL
        Dim unaC As New clsControladora
        resultadoSQL = unaC.bajaFaltaNoDocente(falta)

        If Not resultadoSQL = "" Then
            MsgBox(resultadoSQL)
        ElseIf resultadoSQL = "" Then
            frmFalta.Activate()
            Me.Close()
        End If
    End Sub
End Class