﻿Public Class frmModificarFuncionarioNoDocente
    Dim datosFuncionario As New clsFuncionarioNoDocente
    Dim datosUsuario As New clsUser
    Private frmFuncionarioNoDocente As frmFuncionariosNoDocentes
    Public Sub New(unFuncionario As clsFuncionarioNoDocente, elUsuario As clsUser, unFrm As frmFuncionariosNoDocentes)

        ' Llamada necesaria para el diseñador.
        InitializeComponent()

        ' Agregue cualquier inicialización después de la llamada a InitializeComponent().
        datosFuncionario = unFuncionario
        datosUsuario = elUsuario
        frmFuncionarioNoDocente = unFrm
    End Sub

    Private Sub frmModificarFuncionarioNoDocente_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        txtCI.Text = datosFuncionario.ci
        txtNombre.Text = datosFuncionario.nombre
        txtApellido.Text = datosFuncionario.apellido
        txtTelefono.Text = datosFuncionario.numero
        txtDireccion.Text = datosFuncionario.direccion
        txtCorreo.Text = datosFuncionario.correo
    End Sub

    Private Sub btnAgregar_Click(sender As Object, e As EventArgs) Handles btnAgregar.Click
        Dim unFun As New clsFuncionarioNoDocente(txtCI.Text.ToString, txtNombre.Text, txtApellido.Text, txtCorreo.Text, txtTelefono.Text, txtDireccion.Text)
        Dim unC As New clsControladora
        Dim errorSQL As String
        errorSQL = unC.modificarFuncionarioNoDocente(unFun)

        If errorSQL = "" Then
            Me.Close()
            frmFuncionarioNoDocente.Activate()
        Else
            MsgBox(errorSQL & "Ocurrio un error en la modificacion")
        End If

    End Sub



    Private Sub txtCI_MaskInputRejected(sender As Object, e As MaskInputRejectedEventArgs) Handles txtCI.MaskInputRejected

    End Sub
End Class