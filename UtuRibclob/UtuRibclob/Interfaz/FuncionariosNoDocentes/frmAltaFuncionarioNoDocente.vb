﻿Public Class frmAltaFuncionarioNoDocente
    Private frmFuncionarioNoDocente As frmFuncionariosNoDocentes
    Dim errorSQL As String

    Private Sub frmAltaFuncionarioNoDocente_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        imgTick.Visible = False
    End Sub
    Public Sub New(frmf As frmFuncionariosNoDocentes)
        frmFuncionarioNoDocente = frmf
        ' Llamada necesaria para el diseñador.
        InitializeComponent()

        ' Agregue cualquier inicialización después de la llamada a InitializeComponent().

    End Sub
    Private Sub limpiar()
        txtCI.Clear()
        txtApellido.Clear()
        txtNombre.Clear()
        txtCorreo.Clear()
        txtDireccion.Clear()
        txtTelefono.Clear()
    End Sub

    Private Sub btnAgregar_Click(sender As Object, e As EventArgs) Handles btnAgregar.Click

            Dim unFun As New clsFuncionarioNoDocente(txtCI.Text.ToString, txtNombre.Text, txtApellido.Text, txtCorreo.Text, txtTelefono.Text, txtDireccion.Text)
            Dim unaC As New clsControladora

            errorSQL = unaC.altaFuncionarioNoDocente(unFun)

        If errorSQL = "" Then
            frmFuncionarioNoDocente.Activate()
            Me.Activate()
            imgTick.Visible = True
            Timer1.Enabled = True
            limpiar()
        ElseIf errorSQL = "Ya existe un Funcionario No Docente con ese CI" Then
            MsgBox("Ya existe un Funcionario No Docente con ese CI")
        Else
            MsgBox("Ha ocurrido un error, intentelo mas tarde")
            MsgBox(errorSQL)
        End If

    End Sub

    Public Function verificarPersona(ByVal cCi As String) As Boolean
        Dim unaC As New clsControladora
        Return unaC.verificarPersona(cCi)
    End Function

    Private Sub Timer1_Tick(sender As Object, e As EventArgs) Handles Timer1.Tick
        imgTick.Visible = False
        Timer1.Enabled = False
    End Sub
End Class