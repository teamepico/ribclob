﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmFuncionariosNoDocentes
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmFuncionariosNoDocentes))
        Me.ContextMenuStrip1 = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.ModificarToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.EliminarToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ListarFaltasToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ListarStrikesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ListarHorariosToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.btnSalir = New System.Windows.Forms.Button()
        Me.lsvFuncionarios = New System.Windows.Forms.ListView()
        Me.Nombre = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.Apellido = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.CI = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.Numero = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.Correo = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.Dirección = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.btnAgregar = New System.Windows.Forms.Button()
        Me.btnModificar = New System.Windows.Forms.Button()
        Me.btnBorrar = New System.Windows.Forms.Button()
        Me.btnHorarios = New System.Windows.Forms.Button()
        Me.txtBuscar = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.btnRegistros = New System.Windows.Forms.Button()
        Me.btnFaltas = New System.Windows.Forms.Button()
        Me.ContextMenuStrip1.SuspendLayout()
        Me.SuspendLayout()
        '
        'ContextMenuStrip1
        '
        Me.ContextMenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ModificarToolStripMenuItem, Me.EliminarToolStripMenuItem, Me.ListarFaltasToolStripMenuItem, Me.ListarStrikesToolStripMenuItem, Me.ListarHorariosToolStripMenuItem})
        Me.ContextMenuStrip1.Name = "ContextMenuStrip1"
        Me.ContextMenuStrip1.Size = New System.Drawing.Size(149, 114)
        '
        'ModificarToolStripMenuItem
        '
        Me.ModificarToolStripMenuItem.Name = "ModificarToolStripMenuItem"
        Me.ModificarToolStripMenuItem.Size = New System.Drawing.Size(148, 22)
        Me.ModificarToolStripMenuItem.Text = "Modificar"
        '
        'EliminarToolStripMenuItem
        '
        Me.EliminarToolStripMenuItem.Name = "EliminarToolStripMenuItem"
        Me.EliminarToolStripMenuItem.Size = New System.Drawing.Size(148, 22)
        Me.EliminarToolStripMenuItem.Text = "Eliminar"
        '
        'ListarFaltasToolStripMenuItem
        '
        Me.ListarFaltasToolStripMenuItem.Name = "ListarFaltasToolStripMenuItem"
        Me.ListarFaltasToolStripMenuItem.Size = New System.Drawing.Size(148, 22)
        Me.ListarFaltasToolStripMenuItem.Text = "Listar faltas"
        '
        'ListarStrikesToolStripMenuItem
        '
        Me.ListarStrikesToolStripMenuItem.Name = "ListarStrikesToolStripMenuItem"
        Me.ListarStrikesToolStripMenuItem.Size = New System.Drawing.Size(148, 22)
        Me.ListarStrikesToolStripMenuItem.Text = "Listar strikes"
        '
        'ListarHorariosToolStripMenuItem
        '
        Me.ListarHorariosToolStripMenuItem.Name = "ListarHorariosToolStripMenuItem"
        Me.ListarHorariosToolStripMenuItem.Size = New System.Drawing.Size(148, 22)
        Me.ListarHorariosToolStripMenuItem.Text = "Listar horarios"
        '
        'btnSalir
        '
        Me.btnSalir.Location = New System.Drawing.Point(640, 504)
        Me.btnSalir.Name = "btnSalir"
        Me.btnSalir.Size = New System.Drawing.Size(117, 30)
        Me.btnSalir.TabIndex = 1
        Me.btnSalir.Text = "Salir"
        Me.btnSalir.UseVisualStyleBackColor = True
        '
        'lsvFuncionarios
        '
        Me.lsvFuncionarios.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.Nombre, Me.Apellido, Me.CI, Me.Numero, Me.Correo, Me.Dirección})
        Me.lsvFuncionarios.GridLines = True
        Me.lsvFuncionarios.Location = New System.Drawing.Point(12, 34)
        Me.lsvFuncionarios.Name = "lsvFuncionarios"
        Me.lsvFuncionarios.Size = New System.Drawing.Size(745, 435)
        Me.lsvFuncionarios.TabIndex = 6
        Me.lsvFuncionarios.UseCompatibleStateImageBehavior = False
        Me.lsvFuncionarios.View = System.Windows.Forms.View.Details
        '
        'Nombre
        '
        Me.Nombre.Text = "Nombre"
        Me.Nombre.Width = 96
        '
        'Apellido
        '
        Me.Apellido.Text = "Apellido"
        Me.Apellido.Width = 97
        '
        'CI
        '
        Me.CI.Text = "CI"
        Me.CI.Width = 111
        '
        'Numero
        '
        Me.Numero.Text = "Número"
        Me.Numero.Width = 109
        '
        'Correo
        '
        Me.Correo.Text = "Correo"
        Me.Correo.Width = 129
        '
        'Dirección
        '
        Me.Dirección.Text = "Dirección"
        Me.Dirección.Width = 136
        '
        'btnAgregar
        '
        Me.btnAgregar.Location = New System.Drawing.Point(12, 475)
        Me.btnAgregar.Name = "btnAgregar"
        Me.btnAgregar.Size = New System.Drawing.Size(117, 30)
        Me.btnAgregar.TabIndex = 7
        Me.btnAgregar.Text = "Agregar"
        Me.btnAgregar.UseVisualStyleBackColor = True
        '
        'btnModificar
        '
        Me.btnModificar.Location = New System.Drawing.Point(135, 475)
        Me.btnModificar.Name = "btnModificar"
        Me.btnModificar.Size = New System.Drawing.Size(122, 30)
        Me.btnModificar.TabIndex = 8
        Me.btnModificar.Text = "Modificar"
        Me.btnModificar.UseVisualStyleBackColor = True
        '
        'btnBorrar
        '
        Me.btnBorrar.Location = New System.Drawing.Point(263, 475)
        Me.btnBorrar.Name = "btnBorrar"
        Me.btnBorrar.Size = New System.Drawing.Size(117, 30)
        Me.btnBorrar.TabIndex = 11
        Me.btnBorrar.Text = "Eliminar"
        Me.btnBorrar.UseVisualStyleBackColor = True
        '
        'btnHorarios
        '
        Me.btnHorarios.ContextMenuStrip = Me.ContextMenuStrip1
        Me.btnHorarios.Location = New System.Drawing.Point(12, 511)
        Me.btnHorarios.Name = "btnHorarios"
        Me.btnHorarios.Size = New System.Drawing.Size(117, 30)
        Me.btnHorarios.TabIndex = 10
        Me.btnHorarios.Text = "Horarios"
        Me.btnHorarios.UseVisualStyleBackColor = True
        '
        'txtBuscar
        '
        Me.txtBuscar.Location = New System.Drawing.Point(155, 6)
        Me.txtBuscar.Name = "txtBuscar"
        Me.txtBuscar.Size = New System.Drawing.Size(167, 20)
        Me.txtBuscar.TabIndex = 13
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(29, 13)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(120, 13)
        Me.Label1.TabIndex = 12
        Me.Label1.Text = "Buscar por apellido:"
        '
        'btnRegistros
        '
        Me.btnRegistros.ContextMenuStrip = Me.ContextMenuStrip1
        Me.btnRegistros.Location = New System.Drawing.Point(135, 511)
        Me.btnRegistros.Name = "btnRegistros"
        Me.btnRegistros.Size = New System.Drawing.Size(122, 30)
        Me.btnRegistros.TabIndex = 14
        Me.btnRegistros.Text = "Registros"
        Me.btnRegistros.UseVisualStyleBackColor = True
        '
        'btnFaltas
        '
        Me.btnFaltas.ContextMenuStrip = Me.ContextMenuStrip1
        Me.btnFaltas.Location = New System.Drawing.Point(263, 511)
        Me.btnFaltas.Name = "btnFaltas"
        Me.btnFaltas.Size = New System.Drawing.Size(117, 30)
        Me.btnFaltas.TabIndex = 15
        Me.btnFaltas.Text = "Faltas"
        Me.btnFaltas.UseVisualStyleBackColor = True
        '
        'frmFuncionariosNoDocentes
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(769, 552)
        Me.Controls.Add(Me.btnFaltas)
        Me.Controls.Add(Me.btnRegistros)
        Me.Controls.Add(Me.txtBuscar)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.btnBorrar)
        Me.Controls.Add(Me.btnHorarios)
        Me.Controls.Add(Me.btnModificar)
        Me.Controls.Add(Me.btnAgregar)
        Me.Controls.Add(Me.lsvFuncionarios)
        Me.Controls.Add(Me.btnSalir)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MinimumSize = New System.Drawing.Size(785, 591)
        Me.Name = "frmFuncionariosNoDocentes"
        Me.Text = "Funcionarios No Docentes"
        Me.ContextMenuStrip1.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents btnSalir As System.Windows.Forms.Button
    Friend WithEvents ContextMenuStrip1 As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents ModificarToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents EliminarToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ListarFaltasToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ListarStrikesToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ListarHorariosToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents lsvFuncionarios As System.Windows.Forms.ListView
    Friend WithEvents Nombre As System.Windows.Forms.ColumnHeader
    Friend WithEvents Apellido As System.Windows.Forms.ColumnHeader
    Friend WithEvents CI As System.Windows.Forms.ColumnHeader
    Friend WithEvents Numero As System.Windows.Forms.ColumnHeader
    Friend WithEvents Correo As System.Windows.Forms.ColumnHeader
    Friend WithEvents Dirección As System.Windows.Forms.ColumnHeader
    Friend WithEvents btnAgregar As System.Windows.Forms.Button
    Friend WithEvents btnModificar As System.Windows.Forms.Button
    Friend WithEvents btnBorrar As System.Windows.Forms.Button
    Friend WithEvents btnHorarios As System.Windows.Forms.Button
    Friend WithEvents txtBuscar As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents btnRegistros As System.Windows.Forms.Button
    Friend WithEvents btnFaltas As System.Windows.Forms.Button
End Class
