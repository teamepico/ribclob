﻿Public Class frmBajaNoDocente
    Private frmFuncionariosNoDocentes As frmFuncionariosNoDocentes
    Private bajaNoDocente As New clsFuncionarioNoDocente
    Private datosUsuario As New clsUser

    Private Sub frmBajaDocente_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        lblUsuario.Text = bajaNoDocente.nombre & " " & bajaNoDocente.apellido
        lblCI.Text = bajaNoDocente.ci
    End Sub

    Public Sub New(bajaNoD As clsFuncionarioNoDocente, unU As clsUser, unFrm As frmFuncionariosNoDocentes)
        frmFuncionariosNoDocentes = unFrm
        bajaNoDocente = bajaNoD
        datosUsuario = unU
        InitializeComponent()
    End Sub


    Private Sub btnEliminar_Click(sender As Object, e As EventArgs) Handles btnEliminar.Click
        Dim resultadoSQL As String
        Dim unaC As New clsControladora
        resultadoSQL = unaC.bajaNoDocente(bajaNoDocente.ci)

        If Not resultadoSQL = Nothing Then
            MsgBox(resultadoSQL)
        ElseIf resultadoSQL = Nothing Then
            frmFuncionariosNoDocentes.Activate()
            Me.Close()
        End If
    End Sub

    Private Sub btnCancelar_Click(sender As Object, e As EventArgs) Handles btnCancelar.Click
        Me.Close()
    End Sub
End Class