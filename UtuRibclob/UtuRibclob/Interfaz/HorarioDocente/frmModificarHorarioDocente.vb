﻿Public Class frmModificarHorariosDocentes
    Dim seleccionado As clsHorarioDocente
    Dim unUser As New clsUser
    Private frmHorarioDocente As frmHorarioDocente

    Public Sub New(lsv As clsHorarioDocente, user As clsUser, unFrm As frmHorarioDocente)
        ' TODO: Complete member initialization 
        seleccionado = lsv
        unUser = user
        frmHorarioDocente = unFrm

        InitializeComponent()
    End Sub

    Private Sub frmAltaHorariosDocentes_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        'txtCI.Text = seleccionado.idHorarioDocente
        txtEntrada.Text = seleccionado.entrada
        txtSalida.Text = seleccionado.salida
        'txtDia.Text = seleccionado.diaSemanal
        'txtSalon.Text = seleccionado.salon
    End Sub

    Private Sub limpiar()
        'txtCI.Clear()
        txtEntrada.Clear()
        txtSalida.Clear()
        'txtDia.Clear()
        'txtCI.Clear()
        'txtSalon.Clear()
    End Sub

    Private Sub btnAgregar_Click(sender As Object, e As EventArgs) Handles btnAgregar.Click
        Dim unHorario As New clsHorarioDocente(vbEmpty, txtEntrada.Text, txtSalida2.Text)
        Dim nuevoI As String = txtEntrada2.Text
        Dim unaC As New clsControladora
        unaC.modificarHorarioDocente(unHorario, nuevoI)
        frmHorarioDocente.Activate()
        limpiar()
    End Sub

End Class