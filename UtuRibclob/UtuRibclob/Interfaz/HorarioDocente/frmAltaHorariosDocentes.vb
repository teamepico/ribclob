﻿Public Class frmAltaHorariosDocentes
    Private frmHorariosDocentes As frmHorarioDocente

    Dim horaEntrada As Integer
    Dim horaSalida As Integer
    Dim minutoEntrada As Integer
    Dim minutoSalida As Integer

    Dim horaEsCorrecta As Boolean = False

    Public Sub New(unFrm As frmHorarioDocente)

        ' Llamada necesaria para el diseñador.
        InitializeComponent()

        ' Agregue cualquier inicialización después de la llamada a InitializeComponent().
        frmHorariosDocentes = unFrm
    End Sub

    Private Sub limpiar()
        txtEntrada.Clear()
        txtSalida.Clear()
    End Sub

    Private Sub btnAgregar_Click(sender As Object, e As EventArgs) Handles btnAgregar.Click
        Dim unaC As New clsControladora

        If horaSalida > horaEntrada Then
            horaEsCorrecta = True
        ElseIf horaEntrada = horaSalida And minutoSalida > minutoEntrada Then
            horaEsCorrecta = True
        End If

        Dim horarioEntrada As String = txtEntrada.Text & ":00"
        Dim horarioSalida As String = txtSalida.Text & ":00"

        If horaEsCorrecta = True Then
            Dim unHorario As New clsHorarioDocente(vbEmpty, horarioEntrada, horarioSalida)
            Dim errorSql As String

            errorSql = unaC.altaHorarioDocente(unHorario)
            limpiar()

            If errorSql = "" Then
                imgTick.Visible = True
                Timer1.Enabled = True
                frmHorariosDocentes.Activate()
                Me.Activate()
            Else
                MsgBox("Error al insertar el horario")
            End If
        Else
            MsgBox("El horario de salida no puede ser menor a el de entrada.")
        End If
    End Sub

    Private Sub frmAltaHorariosDocentes_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        imgTick.Visible = False
        Timer1.Enabled = False
    End Sub

    Private Sub txtEntrada_Validating(sender As Object, e As System.ComponentModel.CancelEventArgs) Handles txtEntrada.Validating
        Dim value As String = txtEntrada.Text.Replace(":", "").Trim
        If value = String.Empty Then Return

        Dim hora As Int32
        If Int32.TryParse(value.Substring(0, 2), hora) = False Then
            e.Cancel = True
            Return
        Else
            If hora < 0 OrElse hora > 23 Then
                MsgBox("Ese horario es incorrecto, ingrese el horario en formato de 24hs.")
                e.Cancel = True
                Return
            End If
        End If

        Dim minuto As Int32
        If Int32.TryParse(value.Substring(2, 2), minuto) = False Then
            e.Cancel = True
            Return
        Else
            If minuto < 0 OrElse minuto > 59 Then
                e.Cancel = True
                MsgBox("Ese horario es incorrecto, ingrese el horario en formato de 24hs.")
                Return
            End If
        End If

        horaEntrada = hora
        minutoEntrada = minuto
    End Sub

    Private Sub txtSalida_Validating(sender As Object, e As System.ComponentModel.CancelEventArgs) Handles txtSalida.Validating
        Dim value As String = txtSalida.Text.Replace(":", "").Trim
        Dim horarioSalida As String = ""

        If value = String.Empty Then Return

        Dim hora As Int32
        If Int32.TryParse(value.Substring(0, 2), hora) = False Then
            e.Cancel = True
            Return
        Else
            If hora < 0 OrElse hora > 23 Then
                e.Cancel = True
                MsgBox("Ese horario es incorrecto, ingrese el horario en formato de 24hs.")
                Return
            End If
        End If

        Dim minuto As Int32
        If Int32.TryParse(value.Substring(2, 2), minuto) = False Then
            e.Cancel = True
            Return
        Else
            If minuto < 0 OrElse minuto > 59 Then
                e.Cancel = True
                MsgBox("Ese horario es incorrecto, ingrese el horario en formato de 24hs.")
                Return
            End If
        End If

        horaSalida = hora
        minutoSalida = minuto
    End Sub

    Private Sub imgTick_Click(sender As Object, e As EventArgs) Handles imgTick.Click
       
    End Sub

    Private Sub Timer1_Tick(sender As Object, e As EventArgs) Handles Timer1.Tick
        imgTick.Visible = False
        Timer1.Enabled = False
    End Sub
End Class