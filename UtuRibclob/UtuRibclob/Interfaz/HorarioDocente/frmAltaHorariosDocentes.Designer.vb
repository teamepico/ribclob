﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmAltaHorariosDocentes
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.btnAgregar = New System.Windows.Forms.Button()
        Me.lblTelefono = New System.Windows.Forms.Label()
        Me.lblApellido = New System.Windows.Forms.Label()
        Me.txtEntrada = New System.Windows.Forms.MaskedTextBox()
        Me.txtSalida = New System.Windows.Forms.MaskedTextBox()
        Me.imgTick = New System.Windows.Forms.PictureBox()
        Me.Timer1 = New System.Windows.Forms.Timer(Me.components)
        CType(Me.imgTick, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'btnAgregar
        '
        Me.btnAgregar.Location = New System.Drawing.Point(124, 171)
        Me.btnAgregar.Margin = New System.Windows.Forms.Padding(4)
        Me.btnAgregar.Name = "btnAgregar"
        Me.btnAgregar.Size = New System.Drawing.Size(165, 41)
        Me.btnAgregar.TabIndex = 23
        Me.btnAgregar.Text = "Confirmar"
        Me.btnAgregar.UseVisualStyleBackColor = True
        '
        'lblTelefono
        '
        Me.lblTelefono.AutoSize = True
        Me.lblTelefono.Location = New System.Drawing.Point(59, 82)
        Me.lblTelefono.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lblTelefono.Name = "lblTelefono"
        Me.lblTelefono.Size = New System.Drawing.Size(47, 17)
        Me.lblTelefono.TabIndex = 20
        Me.lblTelefono.Text = "Salida"
        '
        'lblApellido
        '
        Me.lblApellido.AutoSize = True
        Me.lblApellido.Location = New System.Drawing.Point(59, 50)
        Me.lblApellido.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lblApellido.Name = "lblApellido"
        Me.lblApellido.Size = New System.Drawing.Size(58, 17)
        Me.lblApellido.TabIndex = 17
        Me.lblApellido.Text = "Entrada"
        '
        'txtEntrada
        '
        Me.txtEntrada.Location = New System.Drawing.Point(239, 42)
        Me.txtEntrada.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.txtEntrada.Mask = "00:00"
        Me.txtEntrada.Name = "txtEntrada"
        Me.txtEntrada.Size = New System.Drawing.Size(145, 22)
        Me.txtEntrada.TabIndex = 40
        '
        'txtSalida
        '
        Me.txtSalida.Location = New System.Drawing.Point(239, 79)
        Me.txtSalida.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.txtSalida.Mask = "00:00"
        Me.txtSalida.Name = "txtSalida"
        Me.txtSalida.Size = New System.Drawing.Size(145, 22)
        Me.txtSalida.TabIndex = 41
        '
        'imgTick
        '
        Me.imgTick.Image = Global.UtuRibclob.My.Resources.Resources.tick
        Me.imgTick.Location = New System.Drawing.Point(297, 171)
        Me.imgTick.Margin = New System.Windows.Forms.Padding(4)
        Me.imgTick.Name = "imgTick"
        Me.imgTick.Size = New System.Drawing.Size(47, 41)
        Me.imgTick.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.imgTick.TabIndex = 42
        Me.imgTick.TabStop = False
        Me.imgTick.Visible = False
        '
        'Timer1
        '
        Me.Timer1.Interval = 1000
        '
        'frmAltaHorariosDocentes
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(429, 217)
        Me.Controls.Add(Me.imgTick)
        Me.Controls.Add(Me.txtSalida)
        Me.Controls.Add(Me.txtEntrada)
        Me.Controls.Add(Me.btnAgregar)
        Me.Controls.Add(Me.lblTelefono)
        Me.Controls.Add(Me.lblApellido)
        Me.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.MaximizeBox = False
        Me.MaximumSize = New System.Drawing.Size(447, 264)
        Me.MinimizeBox = False
        Me.MinimumSize = New System.Drawing.Size(447, 264)
        Me.Name = "frmAltaHorariosDocentes"
        Me.ShowIcon = False
        Me.ShowInTaskbar = False
        Me.Text = "Agregar horario escolar"
        Me.TopMost = True
        CType(Me.imgTick, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents btnAgregar As System.Windows.Forms.Button
    Friend WithEvents lblTelefono As System.Windows.Forms.Label
    Friend WithEvents lblApellido As System.Windows.Forms.Label
    Friend WithEvents txtEntrada As System.Windows.Forms.MaskedTextBox
    Friend WithEvents txtSalida As System.Windows.Forms.MaskedTextBox
    Friend WithEvents imgTick As System.Windows.Forms.PictureBox
    Friend WithEvents Timer1 As System.Windows.Forms.Timer
End Class
