﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmModificarHorariosDocentes
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.btnAgregar = New System.Windows.Forms.Button()
        Me.lblSalida = New System.Windows.Forms.Label()
        Me.lblentrada = New System.Windows.Forms.Label()
        Me.txtEntrada = New System.Windows.Forms.MaskedTextBox()
        Me.txtSalida = New System.Windows.Forms.MaskedTextBox()
        Me.txtSalida2 = New System.Windows.Forms.MaskedTextBox()
        Me.txtEntrada2 = New System.Windows.Forms.MaskedTextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.SuspendLayout()
        '
        'btnAgregar
        '
        Me.btnAgregar.Location = New System.Drawing.Point(141, 340)
        Me.btnAgregar.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.btnAgregar.Name = "btnAgregar"
        Me.btnAgregar.Size = New System.Drawing.Size(165, 41)
        Me.btnAgregar.TabIndex = 23
        Me.btnAgregar.Text = "Confirmar"
        Me.btnAgregar.UseVisualStyleBackColor = True
        '
        'lblSalida
        '
        Me.lblSalida.AutoSize = True
        Me.lblSalida.Location = New System.Drawing.Point(56, 113)
        Me.lblSalida.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lblSalida.Name = "lblSalida"
        Me.lblSalida.Size = New System.Drawing.Size(47, 17)
        Me.lblSalida.TabIndex = 20
        Me.lblSalida.Text = "Salida"
        '
        'lblentrada
        '
        Me.lblentrada.AutoSize = True
        Me.lblentrada.Location = New System.Drawing.Point(56, 81)
        Me.lblentrada.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lblentrada.Name = "lblentrada"
        Me.lblentrada.Size = New System.Drawing.Size(58, 17)
        Me.lblentrada.TabIndex = 17
        Me.lblentrada.Text = "Entrada"
        '
        'txtEntrada
        '
        Me.txtEntrada.Enabled = False
        Me.txtEntrada.Location = New System.Drawing.Point(305, 79)
        Me.txtEntrada.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.txtEntrada.Mask = "00:00:00"
        Me.txtEntrada.Name = "txtEntrada"
        Me.txtEntrada.Size = New System.Drawing.Size(55, 22)
        Me.txtEntrada.TabIndex = 27
        Me.txtEntrada.Text = "  0000"
        '
        'txtSalida
        '
        Me.txtSalida.Enabled = False
        Me.txtSalida.Location = New System.Drawing.Point(305, 108)
        Me.txtSalida.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.txtSalida.Mask = "00:00:00"
        Me.txtSalida.Name = "txtSalida"
        Me.txtSalida.Size = New System.Drawing.Size(55, 22)
        Me.txtSalida.TabIndex = 28
        Me.txtSalida.Text = "  0000"
        '
        'txtSalida2
        '
        Me.txtSalida2.Location = New System.Drawing.Point(305, 182)
        Me.txtSalida2.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.txtSalida2.Mask = "00:00:00"
        Me.txtSalida2.Name = "txtSalida2"
        Me.txtSalida2.Size = New System.Drawing.Size(55, 22)
        Me.txtSalida2.TabIndex = 32
        Me.txtSalida2.Text = "  0000"
        '
        'txtEntrada2
        '
        Me.txtEntrada2.Location = New System.Drawing.Point(305, 153)
        Me.txtEntrada2.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.txtEntrada2.Mask = "00:00:00"
        Me.txtEntrada2.Name = "txtEntrada2"
        Me.txtEntrada2.Size = New System.Drawing.Size(55, 22)
        Me.txtEntrada2.TabIndex = 31
        Me.txtEntrada2.Text = "  0000"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(56, 187)
        Me.Label1.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(47, 17)
        Me.Label1.TabIndex = 30
        Me.Label1.Text = "Salida"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(56, 155)
        Me.Label2.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(58, 17)
        Me.Label2.TabIndex = 29
        Me.Label2.Text = "Entrada"
        '
        'frmModificarHorariosDocentes
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(437, 399)
        Me.Controls.Add(Me.txtSalida2)
        Me.Controls.Add(Me.txtEntrada2)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.txtSalida)
        Me.Controls.Add(Me.txtEntrada)
        Me.Controls.Add(Me.btnAgregar)
        Me.Controls.Add(Me.lblSalida)
        Me.Controls.Add(Me.lblentrada)
        Me.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.MaximizeBox = False
        Me.MaximumSize = New System.Drawing.Size(455, 446)
        Me.MinimizeBox = False
        Me.MinimumSize = New System.Drawing.Size(455, 446)
        Me.Name = "frmModificarHorariosDocentes"
        Me.ShowIcon = False
        Me.ShowInTaskbar = False
        Me.Text = "Modificar horario escolar"
        Me.TopMost = True
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents btnAgregar As System.Windows.Forms.Button
    Friend WithEvents lblSalida As System.Windows.Forms.Label
    Friend WithEvents lblentrada As System.Windows.Forms.Label
    Friend WithEvents txtEntrada As System.Windows.Forms.MaskedTextBox
    Friend WithEvents txtSalida As System.Windows.Forms.MaskedTextBox
    Friend WithEvents txtSalida2 As System.Windows.Forms.MaskedTextBox
    Friend WithEvents txtEntrada2 As System.Windows.Forms.MaskedTextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
End Class
