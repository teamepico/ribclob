﻿Public Class frmBajaHorarioDocente
    Dim bajaHorario As New clsHorarioDocente
    Dim datosUsuario As New clsUser
    Dim datosFuncionario As New clsFuncionarioDocente
    Private frmHorarioDocente As frmHorarioDocente

    Private Function obtenerDatos(ByVal unFuncionario As String) As clsFuncionarioDocente
        Dim unaC As New clsControladora
        Return unaC.obtenerDatosFuncionarioDocente(unFuncionario)
    End Function

    Private Sub frmAltaInterno_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        'datosFuncionario = obtenerDatos(bajaHorario.ci)
        lblEntrada.Text = bajaHorario.entrada
        lblSalida.Text = bajaHorario.salida

    End Sub

    Public Sub New(bajaH As clsHorarioDocente, unU As clsUser, unFrm As frmHorarioDocente)
        bajaHorario = bajaH
        datosUsuario = unU
        frmHorarioDocente = unFrm
        InitializeComponent()
    End Sub

    Private Sub btnEliminar_Click_1(sender As Object, e As EventArgs) Handles btnEliminar.Click
        Dim resultadoSQL As String
        Dim unaC As New clsControladora
        resultadoSQL = unaC.bajaHorarioDocente(bajaHorario.entrada)

        If Not resultadoSQL = Nothing Then
            MsgBox(resultadoSQL)
        ElseIf resultadoSQL = Nothing Then
            frmHorarioDocente.Activate()
            Me.Close()
        End If

        Me.Close()
    End Sub
End Class