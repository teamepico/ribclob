﻿Imports MySql.Data
Imports MySql.Data.MySqlClient

Public Class clspRegistroFuncionarios
    Inherits clsPersistencia

    Public Function crearRegistro(datos As MySqlDataReader) As clsRegistroFuncionarios
        Dim unF As New clsRegistroFuncionarios

        unF.fecha = datos.Item("DATE_FORMAT(fechaCuadernoRegistros, '%d/%m/%Y %H:%i')").ToString
        unF.id = datos.Item("idCuadernoRegistros").ToString
        unF.residencia = datos.Item("residenciaRegistrosFuncionarios").ToString


        Return unF
    End Function

    Public Function listarRegistro(ci As String, inicial As String, final As String) As List(Of clsRegistroFuncionarios)
        Dim consulta As String
        consulta = "SELECT idCuadernoRegistros, residenciaRegistrosFuncionarios, DATE_FORMAT(fechaCuadernoRegistros, '%d/%m/%Y %H:%i') FROM `registrosfuncionarios` WHERE (Funcionario_Persona_ciPersona  = '" & ci & "') AND (fechaCuadernoRegistros BETWEEN STR_TO_DATE('" & inicial & " 00:00:00', '%e/%c/%Y %H:%i:%s') AND STR_TO_DATE('" & final & " 23:59:59', '%e/%c/%Y %H:%i:%s'))"

        Dim colRegistro As New List(Of clsRegistroFuncionarios)
        Dim datos As MySqlDataReader

        datos = ejecutarYdevolver(consulta)

        While datos.Read
            Dim unF As New clsRegistroFuncionarios
            unF = crearRegistro(datos)
            colRegistro.Add(unF)
        End While

        Return colRegistro
    End Function

    Public Function altaRegistro(ci As String, fecha As String) As String
        Dim consulta As String
        Dim errorsql As String
        Dim datos As MySqlDataReader
        Dim entrada As Boolean = True
        Dim residencia As String = ""

        ' Chequea si es entrada o salida
        consulta = "SELECT idCuadernoRegistros FROM registrosfuncionarios WHERE Funcionario_Persona_ciPersona  = '" & ci & "' AND fechaCuadernoRegistros > CURDATE();"
        datos = ejecutarYdevolver(consulta)
        While datos.Read
            If entrada = True Then
                entrada = False
            ElseIf entrada = False Then
                entrada = True
            End If
        End While

        If entrada = True Then
            residencia = "0"
        ElseIf entrada = False Then
            residencia = "1"
        End If


        consulta = "INSERT INTO registrosfuncionarios VALUES ('', STR_TO_DATE('" & fecha & "', '%Y/%c/%e %H:%i:%s'), '" & ci & "', '" & residencia & "');"

        errorsql = ejecutarSQL(consulta)

        Return errorsql
    End Function
End Class
