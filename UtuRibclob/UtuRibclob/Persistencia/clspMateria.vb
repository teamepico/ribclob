Imports MySql.Data
Imports MySql.Data.MySqlClient

Public Class clspMateria
    Inherits clsPersistencia

    Public Sub altaMateria(unMat As clsMateria)
        Dim consulta As String
        consulta = "INSERT INTO Materias VALUES ('" & unMat.NombreMateria & "','" & unMat.DescripcionMateria & "');"
        ejecutarSQL(consulta)
    End Sub

    Public Function crearMateria(ByVal datos As MySqlDataReader) As clsMateria
        Dim unMat As New clsMateria

        unMat.NombreMateria = datos.Item("nombreMateria").ToString
        unMat.DescripcionMateria = datos.Item("descripcionMateria").ToString

        Return unMat
    End Function

    Public Function listarMaterias() As List(Of clsMateria)
        Dim consulta As String
        consulta = "SELECT * FROM materias"
        Dim colMaterias As New List(Of clsMateria)
        Dim datos As MySqlDataReader

        datos = ejecutarYdevolver(consulta)

        While datos.Read
            Dim unaMateria As clsMateria
            unaMateria = crearMateria(datos)
            colMaterias.Add(unaMateria)
        End While
        Return colMaterias
    End Function


    Public Function altaMateria(nombre As String, descripcion As String) As String
        Dim consulta As String
        consulta = "INSERT INTO materias VALUES ('" & nombre & "','" & descripcion & "');"

        Dim errorSQL As String
        errorSQL = ejecutarSQL(consulta)

        Return errorSQL
    End Function

    Public Function bajaMateria(nombre As String) As String
        Dim consulta As String
        consulta = "DELETE FROM materias WHERE nombreMateria = '" & nombre & "';"

        Dim errorSQL As String
        errorSQL = ejecutarSQL(consulta)

        Return errorSQL
    End Function

    

    Public Function modificarMateria(unMat As clsMateria) As String
        Dim consulta = "UPDATE Materias SET descripcionMateria = '" & unMat.DescripcionMateria & "' WHERE nombreMateria = '" & unMat.NombreMateria & "';"

        Dim resultadoSQL As String
        resultadoSQL = ejecutarSQL(consulta)

        Return resultadoSQL
    End Function

    Public Function buscarMateria(ByVal filtro As String) As List(Of clsMateria)
        Dim consulta = "SELECT nombreMateria, descripcionMateria FROM materias WHERE nombreMateria LIKE '" & filtro & "%';"
        Dim colMaterias As New List(Of clsMateria)
        Dim datos As MySqlDataReader

        datos = ejecutarYdevolver(consulta)

        While datos.Read
            Dim unaM As New clsMateria
            unaM = crearMateria(datos)
            colMaterias.Add(unaM)
        End While

        Return colMaterias
    End Function

End Class
