﻿Imports MySql.Data
Imports MySql.Data.MySqlClient

Public Class clspRegistroInternos
    Inherits clsPersistencia

    Public Function crearRegistro(datos As MySqlDataReader) As clsRegistroInternos
        Dim unI As New clsRegistroInternos

        unI.fecha = datos.Item("DATE_FORMAT(fechaCudernoDeInterno, '%d/%m/%Y %H:%i')").ToString
        unI.id = datos.Item("idCuadernoDeInternos").ToString
        unI.residencia = datos.Item("residenciaCuadernoDeInternos").ToString

        Return unI
    End Function

    Public Function listarRegistro(ci As String, inicial As String, final As String) As List(Of clsRegistroInternos)
        Dim consulta As String
        consulta = "SELECT idCuadernoDeInternos, residenciaCuadernoDeInternos, DATE_FORMAT(fechaCudernoDeInterno, '%d/%m/%Y %H:%i') FROM `registrointernos` WHERE (Internos_Persona_ciPersona = '" & ci & "') AND (fechaCudernoDeInterno BETWEEN STR_TO_DATE('" & inicial & " 00:00:00', '%e/%c/%Y %H:%i:%s') AND STR_TO_DATE('" & final & " 23:59:59', '%e/%c/%Y %H:%i:%s'))"

        Dim colRegistro As New List(Of clsRegistroInternos)
        Dim datos As MySqlDataReader

        datos = ejecutarYdevolver(consulta)

        While datos.Read
            Dim unI As New clsRegistroInternos()
            unI = crearRegistro(datos)
            colRegistro.Add(unI)
        End While

        Return colRegistro
    End Function

    Public Function altaRegistro(ci As String, fecha As String) As String
        Dim consulta As String
        Dim errorsql As String
        Dim datos As MySqlDataReader
        Dim entrada As Boolean = True
        Dim residencia As String = 0


        ' Chequea si es entrada o salida
        consulta = "SELECT idCuadernoDeInternos FROM registrointernos WHERE Internos_Persona_ciPersona = '" & ci & "' AND fechaCudernoDeInterno > CURDATE();"
        datos = ejecutarYdevolver(consulta)
        While datos.Read
            If entrada = True Then
                entrada = False
            ElseIf entrada = False Then
                entrada = True
            End If
        End While

        If entrada = True Then
            residencia = "0"
        ElseIf entrada = False Then
            residencia = "1"
        End If

        consulta = "INSERT INTO registrointernos VALUES ('', '" & residencia & "', STR_TO_DATE('" & fecha & "', '%Y/%c/%e %H:%i:%s'), '" & ci & "')"

        errorsql = ejecutarSQL(consulta)

        Return errorsql
    End Function
End Class
