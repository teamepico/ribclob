﻿Imports MySql.Data
Imports MySql.Data.MySqlClient

Public Class clspSalon
    Inherits clsPersistencia

    Public Function altaSalon(unSal As clsSalon) As String
        Dim consulta As String
        consulta = "INSERT INTO salones VALUES ('" & unSal.ubicacionSalon & "','" & unSal.nombreSalon & "');"
        Return ejecutarSQL(consulta)
    End Function

    Public Function crearSalon(ByVal datos As MySqlDataReader) As clsSalon
        Dim unSalon As New clsSalon

        unSalon.ubicacionSalon = datos.Item("ubicacionSalon").ToString
        unSalon.nombreSalon = datos.Item("nombreSalon").ToString

        Return unSalon
    End Function

    Public Function listarSalones() As List(Of clsSalon)
        Dim consulta As String
        consulta = "SELECT ubicacionSalon, nombreSalon FROM salones"

        Dim colSalones As New List(Of clsSalon)
        Dim datos As MySqlDataReader
        datos = ejecutarYdevolver(consulta)

        While datos.Read
            Dim unSalon As New clsSalon
            unSalon = crearSalon(datos)
            colSalones.Add(unSalon)
        End While

        Return colSalones
    End Function

    Public Function bajaSalones(unS As clsSalon) As String
        Dim consulta = "DELETE FROM salones WHERE nombreSalon = '" & unS.nombreSalon & "';"
        Dim resultadoSQL As String

        resultadoSQL = ejecutarSQL(consulta)
        Return resultadoSQL
    End Function

    Public Function modificarSalon(unS As clsSalon) As String
        Dim consulta = "UPDATE salones SET ubicacionSalon = '" & unS.ubicacionSalon & "' WHERE nombreSalon = '" & unS.nombreSalon & "';"

        Dim resultadoSQL As String
        resultadoSQL = ejecutarSQL(consulta)

        Return resultadoSQL
    End Function

    Public Function buscarSalon(filtro As String) As List(Of clsSalon)
        Dim consulta = "SELECT ubicacionSalon, nombreSalon FROM salones WHERE nombreSalon LIKE '" & filtro & "%';"
        Dim colSalones As New List(Of clsSalon)
        Dim datos As MySqlDataReader

        datos = ejecutarYdevolver(consulta)

        While datos.Read
            Dim unSalon As New clsSalon
            unSalon = crearSalon(datos)
            colSalones.Add(unSalon)
        End While

        Return colSalones
    End Function
End Class
