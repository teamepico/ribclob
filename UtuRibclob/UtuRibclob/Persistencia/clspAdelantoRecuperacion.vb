﻿Imports MySql.Data
Imports MySql.Data.MySqlClient

Public Class clspAdelantoRecuperacion
    Inherits clsPersistencia

    Public Function listarAdelantos(inicio As String, final As String) As List(Of clsAdelantoRecuperacion)
        Dim consulta As String
        Dim colAdelantos As New List(Of clsAdelantoRecuperacion)
        Dim datos As MySqlDataReader

        Dim inicioAsDate As Date = Date.ParseExact(inicio, "dd/MM/yyyy", System.Globalization.DateTimeFormatInfo.InvariantInfo)
        inicio = inicioAsDate.ToString("yyyy-MM-dd")

        Dim finalAsDate As Date = Date.ParseExact(final, "dd/MM/yyyy", System.Globalization.DateTimeFormatInfo.InvariantInfo)
        final = finalAsDate.ToString("yyyy-MM-dd")

        consulta = "SELECT personas.nombrePersona, personas.apellidoPersona, horariosdocente.entradaHorarioDocente, horariosdocente.salidaHorarioDocente, adelantosrecuperaciones.`idAdelantos-Recuperaciones`, adelantosrecuperaciones.`horaEntradaAdelantos-Recuperaciones`, adelantosrecuperaciones.`horaSalidaAdelantos-Recuperaciones`, DATE_FORMAT(adelantosrecuperaciones.`fechaAdelantos-Recuperaciones`, '%d/%m/%Y'), adelantosrecuperaciones.`ciDocenteHasHorarioDocente`, adelantosrecuperaciones.`diaDocenteHasHorarioDocente` FROM adelantosrecuperaciones INNER JOIN docentes_has_horariosdocente ON adelantosrecuperaciones.idDocenteHasHorariosDocente = HorariosDocente_idHorariosDocente AND ciDocenteHasHorarioDocente = Docentes_Funcionarios_Persona_ciPersona AND diaDocenteHasHorarioDocente = diasemanalHorarioDocente INNER JOIN personas ON adelantosrecuperaciones.ciDocenteHasHorarioDocente = personas.ciPersona INNER JOIN horariosdocente ON idDocenteHasHorariosDocente = idHorariosDocente WHERE adelantosrecuperaciones.`fechaAdelantos-Recuperaciones` > '" & inicio & "' AND adelantosrecuperaciones.`fechaAdelantos-Recuperaciones` < '" & final & "';"
        datos = ejecutarYdevolver(consulta)

        While datos.Read()
            Dim adelantos As New clsAdelantoRecuperacion
            adelantos = crearAdelantos(datos)
            colAdelantos.Add(adelantos)
        End While

        Return colAdelantos
    End Function

    Public Function bajaAdelantos(adelanto As clsAdelantoRecuperacion) As String
        Dim consulta As String
        Dim errorSQL As String

        consulta = "DELETE FROM adelantosrecuperaciones WHERE `idAdelantos-Recuperaciones` = '" & adelanto.id & "';"
        errorSQL = ejecutarSQL(consulta)

        Return errorSQL
    End Function

    Public Function altaAdelanto(adelanto As clsAdelantoRecuperacion) As String
        Dim consulta As String
        Dim errorSQL As String
        Dim datos As MySqlDataReader
        Dim id As String = "xD"

        consulta = "SELECT idHorariosDocente FROM horariosdocente WHERE entradaHorarioDocente = '" & adelanto.horario.horario.entrada & "' AND salidaHorarioDocente = '" & adelanto.horario.horario.salida & "';"
        datos = ejecutarYdevolver(consulta)

        If datos.Read Then
            adelanto.horario.horario.IdHorarioDocente = datos.Item("idHorariosDocente").ToString
        End If

        Dim fecha As Date = Date.ParseExact(adelanto.fecha, "dd/MM/yyyy", System.Globalization.DateTimeFormatInfo.InvariantInfo)
        adelanto.fecha = fecha.ToString("yyyy-MM-dd")

        consulta = "INSERT INTO adelantosrecuperaciones VALUES('" & adelanto.id & "', '" & adelanto.horaInicio & "', '" & adelanto.horaFinal & "', '" & adelanto.fecha & "', '" & adelanto.horario.horario.IdHorarioDocente & "', '" & adelanto.docente.ci & "', '" & adelanto.horario.diaHorario & "')"
        errorSQL = ejecutarSQL(consulta)

        Return errorSQL
    End Function

    Public Function modificarAdelantos(unAd As clsAdelantoRecuperacion) As String
        Dim consulta As String
        Dim errorSQL As String

        Dim fecha As Date = Date.ParseExact(unAd.fecha, "dd/MM/yyyy", System.Globalization.DateTimeFormatInfo.InvariantInfo)
        unAd.fecha = fecha.ToString("yyyy-MM-dd")

        consulta = "UPDATE adelantosrecuperaciones SET `horaEntradaAdelantos-Recuperaciones` = '" & unAd.horaInicio & "', `horaSalidaAdelantos-Recuperaciones` = '" & unAd.horaFinal & "', `fechaAdelantos-Recuperaciones` = '" & unAd.fecha & "' WHERE `idAdelantos-Recuperaciones` = '" & unAd.id & "';"
        errorSQL = ejecutarSQL(consulta)

        Return errorSQL
    End Function

    Public Function crearAdelantos(datos As MySqlDataReader) As clsAdelantoRecuperacion
        Dim unAdelanto As New clsAdelantoRecuperacion

        unAdelanto.id = datos.Item("idAdelantos-Recuperaciones").ToString
        unAdelanto.docente.nombre = datos.Item("nombrePersona").ToString
        unAdelanto.docente.apellido = datos.Item("apellidoPersona").ToString
        unAdelanto.horario.horario.entrada = datos.Item("entradaHorarioDocente").ToString
        unAdelanto.horario.horario.salida = datos.Item("salidaHorarioDocente").ToString
        unAdelanto.id = datos.Item("idAdelantos-Recuperaciones").ToString
        unAdelanto.horaInicio = datos.Item("horaEntradaAdelantos-Recuperaciones").ToString
        unAdelanto.fecha = datos.Item("DATE_FORMAT(adelantosrecuperaciones.`fechaAdelantos-Recuperaciones`, '%d/%m/%Y')").ToString
        unAdelanto.horaFinal = datos.Item("horaSalidaAdelantos-Recuperaciones").ToString
        unAdelanto.docente.ci = datos.Item("ciDocenteHasHorarioDocente").ToString
        unAdelanto.horario.diaHorario = datos.Item("diaDocenteHasHorarioDocente").ToString

        Return unAdelanto
    End Function

    Public Function obtenerDatosAdelanto(unAd As clsAdelantoRecuperacion) As clsAdelantoRecuperacion
        Dim consulta As String
        Dim datos As MySqlDataReader
        Dim adelanto As New clsAdelantoRecuperacion

        consulta = "SELECT personas.nombrePersona, personas.apellidoPersona, horariosdocente.entradaHorarioDocente, horariosdocente.salidaHorarioDocente, adelantosrecuperaciones.`idAdelantos-Recuperaciones`, adelantosrecuperaciones.`horaEntradaAdelantos-Recuperaciones`, adelantosrecuperaciones.`horaSalidaAdelantos-Recuperaciones`, DATE_FORMAT(adelantosrecuperaciones.`fechaAdelantos-Recuperaciones`, '%d/%m/%Y'), adelantosrecuperaciones.`ciDocenteHasHorarioDocente`, adelantosrecuperaciones.`diaDocenteHasHorarioDocente` FROM adelantosrecuperaciones INNER JOIN docentes_has_horariosdocente ON adelantosrecuperaciones.idDocenteHasHorariosDocente = HorariosDocente_idHorariosDocente AND ciDocenteHasHorarioDocente = Docentes_Funcionarios_Persona_ciPersona AND diaDocenteHasHorarioDocente = diasemanalHorarioDocente INNER JOIN personas ON adelantosrecuperaciones.ciDocenteHasHorarioDocente = personas.ciPersona INNER JOIN horariosdocente ON idDocenteHasHorariosDocente = idHorariosDocente WHERE personas.ciPersona = '" & unAd.docente.ci & "' AND  adelantosrecuperaciones.`fechaAdelantos-Recuperaciones` = STR_TO_DATE('" & unAd.fecha & "', '%d/%m/%Y');"
        datos = ejecutarYdevolver(consulta)

        If datos.Read Then
            adelanto = crearAdelantos(datos)
        End If

        Return adelanto
    End Function
End Class
