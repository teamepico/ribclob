﻿Imports MySql.Data
Imports MySql.Data.MySqlClient

Public Class clspFaltaDocente
    Inherits clsPersistencia

    Public Function altaFaltasDocentes(unaFalta As clsFaltaDocente) As String

        Dim consulta As String
        consulta = "INSERT INTO faltasdocentes VALUES('' , '" & unaFalta.fecha & "' , '" & unaFalta.descripcion & "' , '" & unaFalta.docente.ci & "' , '" & unaFalta.horarinicio & "', '" & unaFalta.horafinal & "', NULL);"
        Dim errorsql As String
        errorsql = ejecutarSQL(consulta)

        Return errorsql

    End Function
    Public Function modificarFaltasDocentes(unafalta As clsFaltaDocente) As String
        Dim consulta As String
        Dim datos As String
        Dim consultaID As String
        Dim datosID As MySqlDataReader
        Dim idFal As String = "xD"

        Dim fecha As Date = Date.ParseExact(unafalta.fecha, "dd/MM/yyyy", System.Globalization.DateTimeFormatInfo.InvariantInfo)
        Dim fechaAsString As String = fecha.ToString("yyyy-MM-dd")

        consultaID = "SELECT idFaltaDocente FROM faltasdocentes WHERE fechaFaltaDocente = '" & fechaAsString & "' AND horaInicioFaltasDocentes = '" & unafalta.horarinicio & "' AND horaFinalFaltasDocentes = '" & unafalta.horafinal & "' AND Docentes_Funcionarios_Persona_ciPersona = '" & unafalta.docente.ci & "' "
        datosID = ejecutarYdevolver(consultaID)

        While datosID.Read
            idFal = datosID.Item("idFaltaDocente").ToString
        End While

        consulta = "UPDATE  faltasdocentes SET descripcionFaltaDocente = '" & unafalta.descripcion & "' WHERE idFaltaDocente = " & idFal & ""
        datos = ejecutarSQL(consulta)
        Return datos
    End Function

    Public Function bajaFalta(unaFalta As clsFaltaDocente) As String
        Dim consulta As String
        Dim errorSQL As String

        Dim consultaID As String
        Dim datosID As MySqlDataReader
        Dim id As String
        id = vbEmpty

        Dim fecha As Date = Date.ParseExact(unaFalta.fecha, "dd/MM/yyyy", System.Globalization.DateTimeFormatInfo.InvariantInfo)
        Dim fechaAsString As String = fecha.ToString("yyyy-MM-dd")

        consultaID = "SELECT idFaltaDocente FROM faltasdocentes WHERE fechaFaltaDocente = '" & fechaAsString & "' AND horaInicioFaltasDocentes = '" & unaFalta.horarinicio & "' AND horaFinalFaltasDocentes = '" & unaFalta.horafinal & "' AND descripcionFaltaDocente = '" & unaFalta.descripcion & "' AND Funcionarios_Persona_ciPersona = '" & unaFalta.docente.ci & "';"
        datosID = ejecutarYdevolver(consultaID)
        While datosID.Read()
            id = datosID.Item("idFaltaDocente").ToString
        End While

        consulta = "DELETE FROM faltasdocentes WHERE idFaltaDocente = '" & id & "';"
        errorSQL = ejecutarSQL(consulta)

        Return errorSQL
    End Function

    Public Function listarFaltas(ByVal ci As String, ByVal fechaInicial As String, ByVal fechaFinal As String) As List(Of clsFaltaDocente)
        Dim consulta As String
        Dim datos As MySqlDataReader
        Dim colFaltas As New List(Of clsFaltaDocente)

        consulta = "SELECT idFaltaDocente, DATE_FORMAT(faltasdocentes.fechaFaltaDocente, '%d/%m/%Y'), Excepciones_idExcepciones, descripcionFaltaDocente, Docentes_Funcionarios_Persona_ciPersona, horaInicioFaltasDocentes, horaFinalFaltasDocentes FROM faltasdocentes WHERE Docentes_Funcionarios_Persona_ciPersona = '" & ci & "' AND fechaFaltaDocente >  STR_TO_DATE('" & fechaInicial & " 00:00:00', '%e/%c/%Y %H:%i:%s') AND fechaFaltaDocente < STR_TO_DATE('" & fechaFinal & " 23:59:59', '%e/%c/%Y %H:%i:%s');"

        datos = ejecutarYdevolver(consulta)

        While datos.Read
            Dim unaFalta As New clsFaltaDocente
            unaFalta = crearFalta(datos)
            colFaltas.Add(unaFalta)
        End While

        Return colFaltas
    End Function

    Public Function listarFaltas(ByVal fechaInicial As String, ByVal fechaFinal As String) As List(Of clsFaltaDocente)
        Dim consulta As String
        Dim datos As MySqlDataReader
        Dim colFaltas As New List(Of clsFaltaDocente)

        consulta = "SELECT faltasdocentes.idFaltaDocente, excepciones.tipoExcepciones, personas.nombrePersona, personas.apellidoPersona, faltasdocentes.descripcionFaltaDocente, faltasdocentes.horaInicioFaltasDocentes, faltasdocentes.horaFinalFaltasDocentes, faltasdocentes.Docentes_Funcionarios_Persona_ciPersona, DATE_FORMAT(faltasdocentes.fechaFaltaDocente, '%d/%m/%Y') FROM faltasdocentes INNER JOIN personas ON faltasdocentes.Docentes_Funcionarios_Persona_ciPersona = personas.ciPersona LEFT JOIN excepciones ON faltasdocentes.Excepciones_idExcepciones = excepciones.idExcepciones WHERE faltasdocentes.Docentes_Funcionarios_Persona_ciPersona = personas.ciPersona AND fechaFaltaDocente >  STR_TO_DATE('" & fechaInicial & " 00:00:00', '%e/%c/%Y %H:%i:%s') AND fechaFaltaDocente < STR_TO_DATE('" & fechaFinal & " 23:59:59', '%e/%c/%Y %H:%i:%s');"


        datos = ejecutarYdevolver(consulta)

        While datos.Read
            Dim unaFalta As New clsFaltaDocente
            unaFalta = crearFaltaReporte(datos)
            colFaltas.Add(unaFalta)
        End While

        Return colFaltas
    End Function

    Public Function registroDiarioDocente(ByVal unaFecha As String) As String

        ' ======================================================================================================================================================
        ' Esta funcion basicamente esta creada para registrar la falta de los funcionarios docentes de acuerdo al horario que tienen establecido en el programa.
        '
        ' PD de Lucius:
        ' -> Capaz esta todo mal y hay que hacerlo de vuelta. No se, yo hice mi mejor intento.
        ' -> No la he probado porque faltan cambiar cosas en la BD
        ' -> No creo que funcione igual xd
        ' -> Todavia falta pila de cosas por hacer
        ' -> Primero se fija en los horarios y despues comprueba que hubo registro
        ' -> Par favar, cualquier cosa que agreguen todo bien sabelo ñeri, pero traten de mantenerlo ordenado y documentado porque esta es por lejos la funcion 
        ' mas heivy que tenemos
        ' -> Cualquier cosa que no entiendan me agitan en Discord 
        '
        ' To-Do:
        ' (*) Que tenga en cuenta la entrada y salida
        ' (*) Que tenga en cuenta excepciones
        ' (*) Que tenga en cuenta que se haya firmado salida en registro
        ' (*) Imposible que compare fechas si las dos son String
        ' (*) Que inserte la falta
        ' (*) Que inserte un strike
        ' (*) Que tenga una consideracion de 5 minutos
        ' (*) Que funcione ahre igual si
        ' (*) Hacer un formulario lindo donde se pueda acceder a esta funcion (frmReporte)
        ' (*) Que se pueda ingresar un dia para que vuelva a sacar las faltas en vez de que tome el dia presente estaria lindo
        ' (*) Que tenga en cuenta adelantos/recuperaciones
        '
        ' ======================================================================================================================================================

        ' Pasamos la fecha que nos proporciono el usuario de String a Date
        Dim fechaUsuario As Date = Date.ParseExact(unaFecha, "yyyy-MM-dd HH:mm:ss", System.Globalization.DateTimeFormatInfo.InvariantInfo)
        Dim fechaUsuarioString As String = fechaUsuario.ToString("yyyy-MM-dd")
        Dim fechaUsuarioStringInicio As String = fechaUsuarioString & " 00:00:00"
        Dim fechaUsuarioStringFinal As String = fechaUsuarioString & " 23:59:59"

        Dim devolverError As String = ""
        Dim excepcion As New clsExcepcion ' Donde vamos a guardar la excepcion
        Dim consulta As String ' Guardamos las consultas en solo una variable para ahorrar memoria
        Dim datos As MySqlDataReader ' Donde guardamos los datos traidos de MySQL
        Dim colHorarios As New List(Of clsDocentes_horariosdocente) ' Donde guardamos todos los horarios
        Dim excepcionIsReal As Boolean = False ' Hay una excepcion en este dia?
        Dim excepcionSeAplica As Boolean = False ' La excepcion se aplica a este horario?
        Dim fechaFalta As Date
        Dim aplicaExcepcion As String = "NULL" ' En caso de que haya una excepcion, aqui se guarda la ID o se deja NULL en el caso de que no aplique una.
        Dim errorSQL As String = "" ' En caso de que haya un error en SQL aqui se guardara
        Dim diaActual As Integer = fechaUsuario.DayOfWeek ' La fecha actual de la semana (Lunes = '1', Martes = '2', etc)

        ' Para que sea mas facil debuggear se manda todo a la consola
        Console.WriteLine("-> Empezando sistema de Falta Docente.")
        Console.WriteLine("-> Fecha enviada: " & unaFecha)

        'Consulta por excepciones
        consulta = "SELECT idExcepciones, tipoExcepciones, DATE_FORMAT(comienzoExcpeciones, '%Y-%m-%d %H:%i:%s'), DATE_FORMAT(finalExcepciones, '%Y-%m-%d %H:%i:%s') FROM excepciones WHERE '" & fechaUsuarioStringInicio & "' > comienzoExcpeciones AND '" & fechaUsuarioStringFinal & "' < finalExcepciones;"
        datos = ejecutarYdevolver(consulta)

        While datos.Read
            excepcion.idExcepcion = datos.Item("idExcepciones").ToString
            excepcion.comienzoExcepcion = datos.Item("DATE_FORMAT(comienzoExcpeciones, '%Y-%m-%d %H:%i:%s')").ToString
            excepcion.finalExcepcion = datos.Item("DATE_FORMAT(finalExcepciones, '%Y-%m-%d %H:%i:%s')").ToString
        End While

        ' Se fija si hay una excepcion en este dia
        If Not excepcion.comienzoExcepcion = "" Then
            excepcionIsReal = True
            Console.WriteLine("-> Existe una excepcion este dia")
        End If

        ' Obtenemos los horarios
        consulta = "SELECT horariosdocente.entradaHorarioDocente,  horariosdocente.salidaHorarioDocente, Docentes_has_HorariosDocente.Docentes_Funcionarios_Persona_ciPersona, Docentes_has_HorariosDocente.diasemanalHorarioDocente, Docentes_has_HorariosDocente.HorariosDocente_idHorariosDocente FROM Docentes_has_HorariosDocente INNER JOIN horariosdocente WHERE idHorariosDocente = HorariosDocente_idHorariosDocente"
        datos = ejecutarYdevolver(consulta)
        While datos.Read
            Dim unHor As New clsDocentes_horariosdocente
            unHor = crearHorario(datos)
            colHorarios.Add(unHor)
        End While

        ' ---------------------------------
        ' Empezamos a recorrer los horarios
        ' ---------------------------------
        For Each unH In colHorarios
            ' Dim horas As String = "HH:mm:ss"
            Dim colRegistroEntrada As New List(Of clsRegistroFuncionarios) ' Donde guardamos todos los registros de ENTRADA
            Dim colRegistroSalida As New List(Of clsRegistroFuncionarios) ' Donde guardamos todos los registros de SALIDA
            Dim colAdelantos As New List(Of clsAdelantoRecuperacion) ' Donde guardamos todas las recuperaciones
            Dim llevaFalta As Boolean = True 'Asume que lleva falta hasta que existan pruebas de lo contrario
            Dim recuperaOtroDia As Boolean = False ' Asume que el horario no tiene asociada un adelanto o recuperacion
            Dim noVino As Boolean = False ' Si no existen registros
            Dim aplicaDia As Boolean = False ' Si el horario es en este dia
            Dim diaHorario As Integer ' El dia del horario en Integer
            Dim aplicaAdelanto As Boolean = False
            Dim adelantoEsHoy As Boolean = False

            Console.WriteLine("")
            Console.WriteLine("-------------------------------")
            Dim consola As String = "Recorriendo horario " & unH.horario.entrada & "/" & unH.horario.salida & " de " & unH.docente.ci
            Console.WriteLine(consola)
            Console.WriteLine("-------------------------------")
            Console.WriteLine("")

            Select Case unH.diaHorario
                Case "Lunes"
                    diaHorario = 1
                Case "Martes"
                    diaHorario = 2
                Case "Miercoles"
                    diaHorario = 3
                Case "Jueves"
                    diaHorario = 4
                Case "Viernes"
                    diaHorario = 5
                Case "Sabado"
                    diaHorario = 6
            End Select

            ' Obtenemos los adelantos/recuperaciones
            consulta = "SELECT horariosdocente.entradaHorarioDocente, horariosdocente.salidaHorarioDocente, adelantosrecuperaciones.`idAdelantos-Recuperaciones`, adelantosrecuperaciones.`horaEntradaAdelantos-Recuperaciones`, adelantosrecuperaciones.`horaSalidaAdelantos-Recuperaciones`, DATE_FORMAT(adelantosrecuperaciones.`fechaAdelantos-Recuperaciones`, '%Y-%m-%d'), adelantosrecuperaciones.`ciDocenteHasHorarioDocente`, adelantosrecuperaciones.`diaDocenteHasHorarioDocente` FROM adelantosrecuperaciones INNER JOIN docentes_has_horariosdocente ON adelantosrecuperaciones.idDocenteHasHorariosDocente = HorariosDocente_idHorariosDocente AND ciDocenteHasHorarioDocente = Docentes_Funcionarios_Persona_ciPersona AND diaDocenteHasHorarioDocente = diasemanalHorarioDocente INNER JOIN horariosdocente ON idDocenteHasHorariosDocente = idHorariosDocente WHERE adelantosrecuperaciones.`fechaAdelantos-Recuperaciones` = '" & fechaUsuarioString & "' AND idDocenteHasHorariosDocente = '" & unH.horario.IdHorarioDocente & "' AND Docentes_Funcionarios_Persona_ciPersona = '" & unH.docente.ci & "';"
            datos = ejecutarYdevolver(consulta)
            While datos.Read
                Dim unAd As New clsAdelantoRecuperacion
                unAd = crearAdelantos(datos)
                colAdelantos.Add(unAd)
            End While

            ' Se fija si existe algun adelanto o recuperacion que corresponda a este hora
            For Each unAd In colAdelantos
                Console.WriteLine("-> Recorriendo adelantos/recuperaciones id=" & unAd.id)

                Dim fechaAdelanto As Date = Date.ParseExact(unAd.fecha, "yyyy-MM-dd", System.Globalization.DateTimeFormatInfo.InvariantInfo)
                Dim fechaAdelantoString As String = fechaAdelanto.ToString("yyyy-MM-dd")
                Dim diaAdelanto As Integer = 0

                diaAdelanto = fechaAdelanto.DayOfWeek

                ' Si esto se cumple, entonces el horario al que se buscaba adelantar es el que se esta recorriendo ahora mismo
                If unAd.horario.horario.entrada = unH.horario.entrada And unAd.horario.horario.salida = unH.horario.salida Then
                    Console.WriteLine("!!! Hoy aviso que no viene !!!")
                    aplicaAdelanto = True
                End If

                ' Si esto se cumpple, entonces hoy se recuperaba/adelantaba el horario
                If fechaAdelantoString = fechaUsuarioString Then
                    Console.WriteLine("!!! Hoy se recupera/adelanta !!!")
                    adelantoEsHoy = True
                End If

                Console.WriteLine("aplicaAdelanto = " & aplicaAdelanto)

                ' Si aplica el adelanto/recuperacion hoy, entonces cambia el horario del docente por el de la recuperacion
                If adelantoEsHoy = True Then
                    unH.horario.entrada = unAd.horario.horario.entrada
                    unH.horario.salida = unAd.horario.horario.salida
                    diaHorario = diaAdelanto
                End If
            Next

            ' Pasamos las fechas de String a Date para que VB pueda operar con ellas
            ' Horario
            Dim horarioVBentrada As String = fechaUsuarioString & " " & unH.horario.entrada
            Dim horarioVBsalida As String = fechaUsuarioString & " " & unH.horario.salida
            Dim horarioEntrada As Date = Date.ParseExact(horarioVBentrada, "yyyy-MM-dd HH:mm:ss", System.Globalization.DateTimeFormatInfo.InvariantInfo)
            Dim horarioSalida As Date = Date.ParseExact(horarioVBsalida, "yyyy-MM-dd HH:mm:sss", System.Globalization.DateTimeFormatInfo.InvariantInfo)

            ' Chequea que el dia del horario corresponda al dia en el que estamos
            If diaActual = diaHorario Then
                aplicaDia = True
                Console.WriteLine("El dia es correcto")
            End If

            'Excepcion
            If excepcionIsReal = True Then
                Dim excepcionComienzo As Date = Date.ParseExact(excepcion.comienzoExcepcion, "yyyy-MM-dd HH:mm:ss", System.Globalization.DateTimeFormatInfo.InvariantInfo)
                Dim excepcionSalida As Date = Date.ParseExact(excepcion.finalExcepcion, "yyyy-MM-dd HH:mm:ss", System.Globalization.DateTimeFormatInfo.InvariantInfo)

                ' Verificamos que la excepcion haya occurido durante el lapso del horario
                If excepcionIsReal = True Then
                    If excepcionComienzo > horarioEntrada And excepcionSalida < horarioEntrada Then
                        excepcionSeAplica = True
                        Console.WriteLine("-> Se aplica excepcion a este horario.")
                    End If
                End If
            End If

            If aplicaAdelanto = True Then

                llevaFalta = False
                Console.WriteLine("-> Se va a adelantar o recuperar este horario, por lo tanto no lleva falta.")

            Else
                ' Primero chequea que el horario sea en este dia
                If aplicaDia = True Then
                    '
                    If excepcionSeAplica = True Then
                        ' Si la excepcion se aplica, ni se molesta en verificar si el docente vino
                        Console.WriteLine("-> Entra a excepcion se aplica, por lo tanto no hace nada")
                        aplicaExcepcion = "'" & excepcion.idExcepcion & "'"
                    Else
                        Console.WriteLine("---! No hay excepcion !---")
                        'Si la excepcion no se aplica o nunca hubo excepcion en primer lugar, el programa sigue verificando normalmente

                        Dim registrosInicio As String = fechaUsuarioString & " 00:00:00"
                        Dim registrosFinal As String = fechaUsuarioString & " 23:59:59"

                        ' Obtenemos fecha de entradas de entradas
                        consulta = "SELECT DATE_FORMAT(registrosfuncionarios.fechaCuadernoRegistros, '%d/%m/%Y %H:%i:%s'), registrosfuncionarios.residenciaRegistrosFuncionarios, docentes.Funcionarios_Persona_ciPersona FROM registrosfuncionarios INNER JOIN docentes WHERE docentes.Funcionarios_Persona_ciPersona = registrosfuncionarios.Funcionario_Persona_ciPersona AND registrosfuncionarios.fechaCuadernoRegistros > '" & registrosInicio & "' AND registrosfuncionarios.fechaCuadernoRegistros < '" & registrosFinal & "' AND registrosfuncionarios.Funcionario_Persona_ciPersona = '" & unH.docente.ci & "' AND registrosfuncionarios.residenciaRegistrosFuncionarios = '0';"
                        datos = ejecutarYdevolver(consulta)
                        While datos.Read
                            Dim unR As New clsRegistroFuncionarios
                            unR = crearRegistro(datos)
                            colRegistroEntrada.Add(unR)
                        End While

                        ' Obtenemos fecha de entradas de salidas
                        consulta = "SELECT DATE_FORMAT(registrosfuncionarios.fechaCuadernoRegistros, '%d/%m/%Y %H:%i:%s'), registrosfuncionarios.residenciaRegistrosFuncionarios, docentes.Funcionarios_Persona_ciPersona FROM registrosfuncionarios INNER JOIN docentes WHERE docentes.Funcionarios_Persona_ciPersona = registrosfuncionarios.Funcionario_Persona_ciPersona AND registrosfuncionarios.fechaCuadernoRegistros > '" & registrosInicio & "' AND registrosfuncionarios.fechaCuadernoRegistros < '" & registrosFinal & "' AND registrosfuncionarios.Funcionario_Persona_ciPersona = '" & unH.docente.ci & "' AND registrosfuncionarios.residenciaRegistrosFuncionarios = '1';"
                        datos = ejecutarYdevolver(consulta)
                        While datos.Read
                            Dim unR As New clsRegistroFuncionarios
                            unR = crearRegistro(datos)
                            colRegistroSalida.Add(unR)
                        End While

                        ' Obtenemos fecha de entradas
                        Dim registros As New List(Of clsRegistroFuncionarios)
                        consulta = "SELECT DATE_FORMAT(registrosfuncionarios.fechaCuadernoRegistros, '%d/%m/%Y %H:%i:%s'), registrosfuncionarios.residenciaRegistrosFuncionarios FROM registrosfuncionarios WHERE registrosfuncionarios.fechaCuadernoRegistros > '" & registrosInicio & "' AND registrosfuncionarios.fechaCuadernoRegistros < '" & registrosFinal & "' AND registrosfuncionarios.Funcionario_Persona_ciPersona = '" & unH.docente.ci & "';"
                        datos = ejecutarYdevolver(consulta)
                        While datos.Read
                            Dim unR As New clsRegistroFuncionarios
                            unR = crearRegistro(datos)
                            registros.Add(unR)
                        End While

                        If registros.Count = "0" Then
                            Console.WriteLine("!!! Ni siquiera vino, lol !!!")
                            noVino = True
                        Else
                            Dim ultimoRegistro As clsRegistroFuncionarios = registros.Last

                            ' Chequea que exista una ultima salida
                            If ultimoRegistro.residencia = "0" Then
                                Console.WriteLine("-> Lleva sancion :( ")

                                consulta = "INSERT INTO sanciones VALUES('', 'No firmó salida', '" & unH.docente.ci & "')"
                                ejecutarSQL(consulta)
                            End If
                        End If

                        ' Si no existen registros ni siquiera se gasta
                        If noVino = False Then

                            ' ----------------------------------
                            ' Empezamos a recorrer los registros
                            ' ----------------------------------
                            For Each unR In colRegistroEntrada

                                Console.WriteLine("-------------------------------")
                                consola = "Recorriendo registros de entrada: " & unR.fecha
                                Console.WriteLine(consola)
                                Console.WriteLine("-------------------------------")

                                Dim registroFechaEntrada As Date = Date.ParseExact(unR.fecha, "dd/MM/yyyy HH:mm:ss", System.Globalization.DateTimeFormatInfo.InvariantInfo)

                                noVino = False ' Si hay un registro significa que vino

                                fechaFalta = registroFechaEntrada ' Le manda la fecha del registro para que sepa la falta

                                ' Si la entrada del horario esta alrededor de 5 minutos del registro
                                horarioEntrada.AddMinutes(-5)

                                If registroFechaEntrada < horarioEntrada Then
                                    Console.WriteLine("-> La hora de entrada esta bien")
                                    Console.WriteLine("-> Recorriendo registros para ver salida")
                                    ' Esta todo bien ;)
                                    ' Recorre los registros de nuevo para encontrar uno que se aproxime a la salida del horario

                                    For Each otroR In colRegistroSalida
                                        ' Pasa de String a Date
                                        Dim registroFechaSalida = Date.ParseExact(otroR.fecha, "dd/MM/yyyy HH:mm:ss", System.Globalization.DateTimeFormatInfo.InvariantInfo)


                                        Console.WriteLine("Horario salida: " & horarioSalida)
                                        Console.WriteLine("Registro salida: " & registroFechaSalida)

                                        horarioSalida.AddMinutes(-5)

                                        If registroFechaSalida > horarioSalida Then
                                            ' Bien ahi guachin, la estas encarando toda
                                            ' No lleva falta
                                            Console.WriteLine("-> Bien ahi, la encaraste toda")
                                            llevaFalta = False
                                        End If
                                    Next
                                    ' Termina de recorrer los registros de salida
                                Else
                                    ' No existe registros, lleva falta igual
                                End If

                            Next
                            ' Termina de recorrer los registros

                        End If
                        ' Termina de verificar
                    End If

                ElseIf aplicaDia = False Then
                    ' Si el horario no es en este dia, no le pasa falta
                    llevaFalta = False
                End If
            End If

            If llevaFalta = True Then
                Console.WriteLine("-> Lleva falta :(")

                Dim faltaToString As String = fechaFalta.ToString("yyyy-MM-dd hh:mm:ss")

                consulta = "SELECT idFaltaDocente FROM faltasdocentes WHERE fechaFaltaDocente = '" & faltaToString & "' AND horaInicioFaltasDocentes = '" & unH.horario.entrada & "' AND horaFinalFaltasDocentes = '" & unH.horario.salida & "' AND Docentes_Funcionarios_Persona_ciPersona = '" & unH.docente.ci & "';" 'xD
                datos = ejecutarYdevolver(consulta)
                Dim existeFalta As Boolean = False

                If datos.Read Then
                    existeFalta = True
                End If

                Dim tipoFalta As String = ""
                If noVino = False Then
                    tipoFalta = "Falta"
                ElseIf noVino = True Then
                    tipoFalta = "Llegada tarde"
                End If

                If aplicaAdelanto = True Then
                    tipoFalta = "Adelanto o recuperacion"
                End If

                If existeFalta = False Then
                    consulta = "INSERT INTO faltasdocentes VALUES('0', '" & faltaToString & "', '" & tipoFalta & "', '" & unH.docente.ci & "', '" & unH.horario.entrada & "', '" & unH.horario.salida & "', " & aplicaExcepcion & ")"
                    errorSQL = ejecutarSQL(consulta)

                    If Not errorSQL = "" Then
                        Console.WriteLine("################## ERROR SQL ##################")
                        Console.WriteLine("Error en cuestion: " & errorSQL)
                        Console.WriteLine("SQL enviado: " & consulta)
                        Console.WriteLine("###############################################")
                    End If
                End If

            End If
            ' Termina de verificar

            Console.WriteLine("")
            Console.WriteLine("!!!!! Termino el horario !!!!!! Lleva falta: " & llevaFalta)
            Console.WriteLine("")

        Next
        ' Termina de recorrer los horarios

        Return devolverError

    End Function

    Public Function crearFaltaReporte(datos As MySqlDataReader) As clsFaltaDocente
        Dim unaFalta As New clsFaltaDocente

        unaFalta.id = datos.Item("idFaltaDocente").ToString
        unaFalta.descripcion = datos.Item("descripcionFaltaDocente").ToString
        unaFalta.docente.nombre = datos.Item("nombrePersona")
        unaFalta.docente.apellido = datos.Item("apellidoPersona")
        unaFalta.fecha = datos.Item("DATE_FORMAT(faltasdocentes.fechaFaltaDocente, '%d/%m/%Y')").ToString
        unaFalta.horarinicio = datos.Item("horaInicioFaltasDocentes").ToString
        unaFalta.horafinal = datos.Item("horaFinalFaltasDocentes").ToString
        unaFalta.docente.ci = datos.Item("Docentes_Funcionarios_Persona_ciPersona")

        If IsDBNull(datos.Item("tipoExcepciones")) Then

        Else
            unaFalta.excepcion.tipoExcepcion = datos.Item("tipoExcepciones")
        End If

        Return unaFalta
    End Function

    Public Function crearFalta(datos As MySqlDataReader) As clsFaltaDocente
        Dim unaFalta As New clsFaltaDocente

        unaFalta.id = datos.Item("idFaltaDocente").ToString
        unaFalta.descripcion = datos.Item("descripcionFaltaDocente").ToString
        unaFalta.fecha = datos.Item("DATE_FORMAT(faltasdocentes.fechaFaltaDocente, '%d/%m/%Y')").ToString
        unaFalta.horarinicio = datos.Item("horaInicioFaltasDocentes").ToString
        unaFalta.horafinal = datos.Item("horaFinalFaltasDocentes").ToString
        unaFalta.docente.ci = datos.Item("Docentes_Funcionarios_Persona_ciPersona")

        Return unaFalta
    End Function

    Public Function crearRegistro(datos As MySqlDataReader) As clsRegistroFuncionarios
        Dim unF As New clsRegistroFuncionarios

        unF.fecha = datos.Item("DATE_FORMAT(registrosfuncionarios.fechaCuadernoRegistros, '%d/%m/%Y %H:%i:%s')").ToString
        unF.residencia = datos.Item("residenciaRegistrosFuncionarios").ToString
        'unF.excepcion = datos.Item("Excepciones_idExcepciones ").ToString

        Return unF
    End Function

    Public Function crearAdelantos(datos As MySqlDataReader) As clsAdelantoRecuperacion
        Dim unAdelanto As New clsAdelantoRecuperacion

        unAdelanto.id = datos.Item("idAdelantos-Recuperaciones").ToString
        unAdelanto.horario.horario.entrada = datos.Item("entradaHorarioDocente").ToString
        unAdelanto.horario.horario.salida = datos.Item("salidaHorarioDocente").ToString
        unAdelanto.id = datos.Item("idAdelantos-Recuperaciones").ToString
        unAdelanto.horaInicio = datos.Item("horaEntradaAdelantos-Recuperaciones").ToString
        unAdelanto.fecha = datos.Item("DATE_FORMAT(adelantosrecuperaciones.`fechaAdelantos-Recuperaciones`, '%Y-%m-%d')").ToString
        unAdelanto.horaFinal = datos.Item("horaSalidaAdelantos-Recuperaciones").ToString
        unAdelanto.docente.ci = datos.Item("ciDocenteHasHorarioDocente").ToString
        unAdelanto.dia = datos.Item("diaDocenteHasHorarioDocente").ToString

        Return unAdelanto
    End Function

    Public Function crearHorario(ByVal datos As MySqlDataReader) As clsDocentes_horariosdocente
        Dim unHo As New clsDocentes_horariosdocente

        unHo.docente.ci = datos.Item("Docentes_Funcionarios_Persona_ciPersona").ToString
        unHo.horario.entrada = datos.Item("entradaHorarioDocente").ToString
        unHo.horario.salida = datos.Item("salidaHorarioDocente").ToString
        unHo.diaHorario = datos.Item("diasemanalHorarioDocente").ToString
        unHo.horario.IdHorarioDocente = datos.Item("HorariosDocente_idHorariosDocente").ToString

        Return unHo
    End Function

    Public Function crearSancion(datos As MySqlDataReader) As clsSancion
        Dim unaSancion As New clsSancion

        unaSancion.descripcion = datos.Item("descripcionCuadernoDeSanciones ")

        Return unaSancion
    End Function
End Class
