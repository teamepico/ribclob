﻿Imports MySql.Data
Imports MySql.Data.MySqlClient
Public Class clspHorarioNoDocente
    Inherits clsPersistencia
    Public Function listarHorarioNoDocente(ci As String) As List(Of clsHorarioNoDocente)
        Dim consulta As String
        consulta = "SELECT * FROM horariosnodocente WHERE NoDocente_Funcionarios_Persona_ciPersona ='" & ci & "';"

        Dim datos As MySqlDataReader
        datos = ejecutarYdevolver(consulta)

        Dim colHorarios As New List(Of clsHorarioNoDocente)

        While datos.Read
            Dim unHorario As New clsHorarioNoDocente
            unHorario = crearHorarios(datos)
            colHorarios.Add(unHorario)

        End While

        Return colHorarios
    End Function

    Public Function crearHorarios(datos As MySqlDataReader) As clsHorarioNoDocente
        Dim unHo As New clsHorarioNoDocente

        unHo.idHorarioNoDocente = datos.Item("idHorarioNoDocente").ToString
        unHo.entradaHorarioNoDocente = datos.Item("entradaHorarioNoDocente").ToString
        unHo.salidaHorarioNoDocente = datos.Item("salidaHorarioNoDocente").ToString
        unHo.diaSemanalHorarioNoDocente = datos.Item("diasemanalHorarioNoDocente").ToString
        unHo.funcionario.ci = datos.Item("NoDocente_Funcionarios_Persona_ciPersona").ToString

        Return unHo
    End Function


    Public Function altaHorarioNoDocente(unH As clsHorarioNoDocente) As String
        Dim consulta As String
        consulta = "INSERT INTO horariosnodocente VALUES(0,'" & unH.entradaHorarioNoDocente & "' , '" & unH.salidaHorarioNoDocente & "','" & unH.diaSemanalHorarioNoDocente & "','" & unH.funcionario.ci & "');"
        Dim errorSql As String
        errorSql = ejecutarSQL(consulta)
        Return errorSql

    End Function

    Public Function bajaHorarioNoDocente(unH As clsHorarioNoDocente) As String
        Dim consulta As String
        Dim errorsql As String

        consulta = "DELETE FROM horariosnodocente WHERE entradaHorarioNoDocente = '" & unH.entradaHorarioNoDocente & "' AND salidaHorarioNoDocente = '" & unH.salidaHorarioNoDocente & "' AND diasemanalHorarioNoDocente = '" & unH.diaSemanalHorarioNoDocente & "';"
        errorsql = ejecutarSQL(consulta)

        Return errorsql
    End Function

    Public Function modificarHorarioNoDocente(unHorarioNuevo As clsHorarioNoDocente, unHorarioViejo As clsHorarioNoDocente) As String
        Dim consulta As String
        Dim errorSQL As String

        consulta = "UPDATE horariosnodocente SET entradaHorarioNoDocente = '" & unHorarioNuevo.entradaHorarioNoDocente & "', salidaHorarioNoDocente = '" & unHorarioNuevo.salidaHorarioNoDocente & "', diasemanalHorarioNoDocente = '" & unHorarioNuevo.diaSemanalHorarioNoDocente & "' WHERE entradaHorarioNoDocente = '" & unHorarioViejo.entradaHorarioNoDocente & "' AND salidaHorarioNoDocente = '" & unHorarioViejo.salidaHorarioNoDocente & "' AND diasemanalHorarioNoDocente = '" & unHorarioViejo.diaSemanalHorarioNoDocente & "' AND NoDocente_Funcionarios_Persona_ciPersona = '" & unHorarioNuevo.funcionario.ci & "';"
        errorSQL = ejecutarSQL(consulta)

        Return errorSQL
    End Function

End Class
