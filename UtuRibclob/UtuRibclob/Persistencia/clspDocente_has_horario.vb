﻿Imports MySql.Data
Imports MySql.Data.MySqlClient

Public Class clspDocente_has_horario
    Inherits clsPersistencia
    Public Function listarHorario(ci As String) As List(Of clsDocentes_horariosdocente)
        Dim consulta As String
        consulta = "SELECT docentes_has_horariosdocente.Docentes_Funcionarios_Persona_ciPersona, docentes_has_horariosdocente.Salones_nombreSalon, docentes_has_horariosdocente.Grupos_nombreGrupos, docentes_has_horariosdocente.Materias_nombreMateria, docentes_has_horariosdocente.diasemanalHorarioDocente, docentes_has_horariosdocente.HorariosDocente_idHorariosDocente, horariosdocente.idHorariosDocente, horariosdocente.entradaHorarioDocente, horariosdocente.salidaHorarioDocente FROM docentes_has_horariosdocente INNER JOIN horariosdocente WHERE horariosdocente.idHorariosDocente = docentes_has_horariosdocente.HorariosDocente_idHorariosDocente AND Docentes_Funcionarios_Persona_ciPersona = '" & ci & "';"

        Dim datos As MySqlDataReader
        datos = ejecutarYdevolver(consulta)

        Dim colHorarios As New List(Of clsDocentes_horariosdocente)

        While datos.Read
            Dim unHorario As New clsDocentes_horariosdocente
            unHorario = crearHorario(datos)
            colHorarios.Add(unHorario)
        End While

        Return colHorarios
    End Function

    Public Function crearHorario(ByVal datos As MySqlDataReader) As clsDocentes_horariosdocente
        Dim unHo As New clsDocentes_horariosdocente
        unHo.docente.ci = datos.Item("Docentes_Funcionarios_Persona_ciPersona").ToString
        unHo.horario.entrada = datos.Item("entradaHorarioDocente").ToString
        unHo.salon.nombreSalon = datos.Item("Salones_nombreSalon").ToString
        unHo.grupo.NombreGrupo = datos.Item("Grupos_nombreGrupos").ToString
        unHo.materia.NombreMateria = datos.Item("Materias_nombreMateria").ToString
        unHo.horario.salida = datos.Item("salidaHorarioDocente").ToString
        unHo.diaHorario = datos.Item("diasemanalHorarioDocente").ToString

        Return unHo
    End Function

    Public Function altaHorario(unHorario As clsDocentes_horariosdocente) As String
        Dim consulta As String
        consulta = "INSERT INTO docentes_has_horariosdocente VALUES('" & unHorario.docente.ci & "', '" & unHorario.salon.nombreSalon & "', '" & unHorario.grupo.NombreGrupo & "', '" & unHorario.materia.NombreMateria & "', '" & unHorario.diaHorario & "', '" & unHorario.horario.IdHorarioDocente & "');"

        Dim errorSQL As String
        errorSQL = ejecutarSQL(consulta)

        Return errorSQL
    End Function

    Public Function bajaHorario(horario As clsDocentes_horariosdocente) As String
        Dim consulta As String
        Dim datos As MySqlDataReader
        Dim horarioId As String = "0"

        consulta = "SELECT idHorariosDocente FROM `horariosdocente` WHERE entradaHorarioDocente = '" & horario.horario.entrada & "' AND salidaHorarioDocente = '" & horario.horario.salida & "';"
        datos = ejecutarYdevolver(consulta)

        While datos.Read()
            horarioId = datos.Item("idHorariosDocente").ToString
        End While

        consulta = "DELETE FROM docentes_has_horariosdocente WHERE Docentes_Funcionarios_Persona_ciPersona = '" & horario.docente.ci & "' AND HorariosDocente_idHorariosDocente = '" & horarioId & "' AND diasemanalHorarioDocente = '" & horario.diaHorario & "';"

        Dim errorSQL As String
        errorSQL = ejecutarSQL(consulta)

        Return errorSQL
    End Function

    Public Function modificarHorario(unHorNuevo As clsDocentes_horariosdocente, unHorViejo As clsDocentes_horariosdocente) As String
        Dim consulta As String
        Dim datos As MySqlDataReader

        consulta = "SELECT idHorariosDocente FROM `horariosdocente` WHERE entradaHorarioDocente = '" & unHorNuevo.horario.entrada & "' AND salidaHorarioDocente = '" & unHorNuevo.horario.salida & "';"
        datos = ejecutarYdevolver(consulta)

        While datos.Read()
            unHorNuevo.horario.IdHorarioDocente = datos.Item("idHorariosDocente").ToString
        End While

        consulta = "SELECT idHorariosDocente FROM `horariosdocente` WHERE entradaHorarioDocente = '" & unHorViejo.horario.entrada & "' AND salidaHorarioDocente = '" & unHorViejo.horario.salida & "';"
        datos = ejecutarYdevolver(consulta)

        While datos.Read()
            unHorViejo.horario.IdHorarioDocente = datos.Item("idHorariosDocente").ToString
        End While

        consulta = "UPDATE docentes_has_horariosdocente SET Docentes_Funcionarios_Persona_ciPersona = '" & unHorViejo.docente.ci & "', HorariosDocente_idHorariosDocente = '" & unHorNuevo.horario.IdHorarioDocente & "', Salones_nombreSalon = '" & unHorNuevo.salon.nombreSalon & "', Grupos_nombreGrupos = '" & unHorNuevo.grupo.NombreGrupo & "', Materias_nombreMateria = '" & unHorNuevo.materia.NombreMateria & "', diasemanalHorarioDocente = '" & unHorNuevo.diaHorario & "' WHERE Docentes_Funcionarios_Persona_ciPersona = '" & unHorViejo.docente.ci & "' AND diasemanalHorarioDocente = '" & unHorViejo.diaHorario & "' AND HorariosDocente_idHorariosDocente = '" & unHorViejo.horario.IdHorarioDocente & "' "

        Dim errorSQL As String
        errorSQL = ejecutarSQL(consulta)

        Return errorSQL
    End Function




End Class
