﻿Imports MySql.Data
Imports MySql.Data.MySqlClient

Public Class clspSancion
    Inherits clsPersistencia

    Public Function crearSancion(datos As MySqlDataReader) As clsSancion
        Dim unaSancion As New clsSancion

        unaSancion.descripcion = datos.Item("descripcionCuadernoDeSanciones").ToString

        Return unaSancion
    End Function

    Public Function listarSanciones(ci As String) As List(Of clsSancion)
        Dim consulta As String
        Dim datos As MySqlDataReader
        Dim colSanciones As New List(Of clsSancion)

        consulta = "SELECT descripcionCuadernoDeSanciones FROM sanciones WHERE Funcionario_Persona_ciPersona = '" & ci & "';"
        datos = ejecutarYdevolver(consulta)

        While datos.Read
            Dim unaS As New clsSancion
            unaS = crearSancion(datos)
            colSanciones.Add(unaS)
        End While

        Return colSanciones
    End Function
End Class
