﻿Imports MySql.Data
Imports MySql.Data.MySqlClient

Public Class clspFuncionarioNoDocente
    Inherits clsPersistencia

    Public Function crearFuncionario(ByVal datos As MySqlDataReader) As clsFuncionarioNoDocente
        Dim unFunc As New clsFuncionarioNoDocente

        unFunc.ci = datos.Item("ciPersona").ToString
        unFunc.nombre = datos.Item("nombrePersona").ToString
        unFunc.apellido = datos.Item("apellidoPersona").ToString
        unFunc.numero = datos.Item("numeroPersona").ToString
        unFunc.direccion = datos.Item("direccionPersona").ToString
        unFunc.correo = datos.Item("correoPersona").ToString

        Return unFunc
    End Function

    Public Function listarFuncionariosNoDocentes() As List(Of clsFuncionarioNoDocente)
        Dim consulta As String
        consulta = "SELECT personas.nombrePersona, personas.apellidoPersona, personas.ciPersona, personas.correoPersona, personas.numeroPersona, personas.direccionPersona FROM personas INNER JOIN nodocentes ON personas.ciPersona = nodocentes.Funcionarios_Persona_ciPersona"
        Dim datos As MySqlDataReader
        datos = ejecutarYdevolver(consulta)
        Dim colFun As New List(Of clsFuncionarioNoDocente)

        While datos.Read
            Dim unFun As New clsFuncionarioNoDocente
            unFun = crearFuncionario(datos)
            colFun.Add(unFun)
        End While

        Return colFun
    End Function

    Public Function altaFuncionario(ByVal unFun As clsFuncionarioNoDocente) As String
        Dim errorSQL As String

        Dim consultaPersonas As String
        consultaPersonas = "INSERT INTO personas VALUES ('" & unFun.ci & "', '" & unFun.nombre & "', '" & unFun.apellido & "', '" & unFun.numero & "', '" & unFun.correo & "', '" & unFun.direccion & "')"

        Dim consultaFuncionarios As String
        consultaFuncionarios = "INSERT INTO funcionarios VALUES ('" & unFun.ci & "')"

        Dim consultaNoDocentes As String
        consultaNoDocentes = "INSERT INTO nodocentes VALUES ('" & unFun.ci & "')"


        Dim existe As MySqlDataReader
        Dim existeFunci As String
        existeFunci = "SELECT nodocentes.Funcionarios_Persona_ciPersona FROM nodocentes WHERE Funcionarios_Persona_ciPersona = '" & unFun.ci & "';"
        existe = ejecutarYdevolver(existeFunci)

        If existe.Read Then
            errorSQL = "Ya existe un Funcionario No Docente con ese CI"
        Else
            errorSQL = ejecutarSQL(consultaPersonas)

            If errorSQL = "" Then
                ejecutarSQL(consultaFuncionarios)

                If errorSQL = "" Then
                    ejecutarSQL(consultaNoDocentes)
                End If
            End If
        End If
        Return errorSQL
    End Function

    Public Function modificarDoocente(ByVal unFun As clsFuncionarioNoDocente) As String
        Dim consultaPersonas = "UPDATE personas SET nombrePersona = '" & unFun.nombre & "', apellidoPersona = '" & unFun.apellido & "', numeroPersona = '" & unFun.numero & "', correoPersona = '" & unFun.correo & "', direccionPersona = '" & unFun.direccion & "' WHERE ciPersona = '" & unFun.ci & "';"
        Dim errorSQL As String
        errorSQL = ejecutarSQL(consultaPersonas)
        Return errorSQL
    End Function

    Public Function bajaNoDocente(ByVal ci As String) As String
        Dim consultaNoDocente As String
        consultaNoDocente = "DELETE FROM nodocentes WHERE Funcionarios_Persona_ciPersona = '" & ci & "';"

        Dim consultaFuncionario As String
        consultaFuncionario = "DELETE FROM funcionarios WHERE Persona_ciPersona = '" & ci & "';"

        Dim consultaPersona As String
        consultaPersona = "DELETE FROM personas WHERE ciPersona = '" & ci & "';"

        Dim consultaHorario As String
        consultaHorario = "DELETE FROM horariosnodocente WHERE NoDocente_Funcionarios_Persona_ciPersona ='" & ci & "';"

        Dim errorSQL As String

        'Primero elimina el horario asociado con el no docente para que no existan conflictos
        errorSQL = ejecutarSQL(consultaHorario)

        If errorSQL = "" Then

            ' Luego elimina el funcionario en "nodocentes"
            errorSQL = ejecutarSQL(consultaNoDocente)

            If errorSQL = "" Then

                ' Luego elimina el funcionario de "funcionarios"
                errorSQL = ejecutarSQL(consultaFuncionario)

                If errorSQL = "" Then

                    ' Luego elimina el funcionario de "personas"
                    errorSQL = ejecutarSQL(consultaPersona)
                End If
            End If
        End If

        Return errorSQL
    End Function

    Public Function buscarFuncionariosNoDocentes(ByVal filtro As String) As List(Of clsFuncionarioNoDocente)
        Dim consulta As String
        consulta = "SELECT personas.nombrePersona, personas.apellidoPersona, personas.ciPersona, personas.correoPersona, personas.numeroPersona, personas.direccionPersona FROM personas INNER JOIN nodocentes ON personas.ciPersona = nodocentes.Funcionarios_Persona_ciPersona WHERE apellidoPersona LIKE '" & filtro & "%';"
        Dim datos As MySqlDataReader
        datos = ejecutarYdevolver(consulta)
        Dim colFun As New List(Of clsFuncionarioNoDocente)

        While datos.Read
            Dim unFun As New clsFuncionarioNoDocente
            unFun = crearFuncionario(datos)
            colFun.Add(unFun)
        End While

        Return colFun
    End Function

    Public Function checkFuncionarioNoDocente(ByVal ci As String) As clsFuncionarioNoDocente
        Dim consulta As String
        Dim datos As MySqlDataReader
        Dim unF As New clsFuncionarioNoDocente

        consulta = "SELECT personas.nombrePersona, personas.apellidoPersona, personas.ciPersona, personas.correoPersona, personas.numeroPersona, personas.direccionPersona FROM personas INNER JOIN nodocentes ON personas.ciPersona = nodocentes.Funcionarios_Persona_ciPersona WHERE ciPersona = '" & ci & "';"
        datos = ejecutarYdevolver(consulta)

        While datos.Read
            unF = crearFuncionario(datos)
        End While

        Return unF
    End Function
End Class
