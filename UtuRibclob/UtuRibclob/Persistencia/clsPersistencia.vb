﻿Imports MySql.Data
Imports MySql.Data.MySqlClient

Public Class clsPersistencia
    Private con As MySqlConnection
    Private Function conectar() As MySqlConnection
        If Not con Is Nothing Then con.Close()
        Dim miConexion As String
        miConexion = "server=localhost; uid=root ; password= ; database=ribclob;"
        Try
            con = New MySqlConnection(miConexion)
            con.Open()
        Catch ex As MySqlException
            MessageBox.Show("Error al conectarse con el servidor " & ex.Message)
        End Try
        Return con
    End Function

    Private Sub desconectar()
        con.Close()
    End Sub

    Protected Function ejecutarSQL(ByVal consultaSQL As String) As String
        Dim comando As New MySqlCommand()
        Dim errorSQL As String
        comando.CommandText = consultaSQL
        comando.CommandType = CommandType.Text
        comando.Connection = conectar()
        Try
            comando.ExecuteNonQuery()
            errorSQL = ""
        Catch ex As MySqlException
            errorSQL = ex.Message
        End Try
        desconectar()
        Return errorSQL
    End Function

    Protected Function ejecutarYdevolver(ByVal sql As String) As MySqlDataReader
        Dim comando As New MySqlCommand

        Dim declarar As New MySqlCommand
        declarar.CommandText = "SELECT 1"
        declarar.CommandType = CommandType.Text
        declarar.Connection = conectar()

        Dim resultados As MySqlDataReader

        resultados = declarar.ExecuteReader()

        comando.CommandText = sql
        comando.CommandType = CommandType.Text
        comando.Connection = conectar()
        Try
            resultados = comando.ExecuteReader()
        Catch ex As MySqlException
            MessageBox.Show("Error al ejecutar las sentencias sql" & ex.Message)
        End Try
        Return resultados
        desconectar()
    End Function
End Class

