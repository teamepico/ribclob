﻿Imports MySql.Data
Imports MySql.Data.MySqlClient

Public Class clspFuncionarioDocente
    Inherits clsPersistencia

    Public Function crearFuncionario(ByVal datos As MySqlDataReader) As clsFuncionarioDocente
        Dim unFunc As New clsFuncionarioDocente

        unFunc.ci = datos.Item("ciPersona").ToString
        unFunc.nombre = datos.Item("nombrePersona").ToString
        unFunc.apellido = datos.Item("apellidoPersona").ToString
        unFunc.numero = datos.Item("numeroPersona").ToString
        unFunc.direccion = datos.Item("direccionPersona").ToString
        unFunc.correo = datos.Item("correoPersona").ToString

        Return unFunc
    End Function

    Public Function altaDocente(ByVal unDoc As clsFuncionarioDocente) As String
        Dim errorsql As String
        Dim consultaPersonas As String
        Dim fuci As String = unDoc.ci

        Dim existe As MySqlDataReader
        Dim existeFunci As String
        existeFunci = "SELECT docentes.Funcionarios_Persona_ciPersona FROM docentes WHERE Funcionarios_Persona_ciPersona = '" & fuci & "';"
        existe = ejecutarYdevolver(existeFunci)

        If existe.Read Then
            errorsql = "Ya existe un Funcionario Docente con ese CI"
        Else

            consultaPersonas = "INSERT INTO personas VALUES('" & unDoc.ci & "' , '" & unDoc.nombre & "','" & unDoc.apellido & "','" & unDoc.numero & "','" & unDoc.correo & "','" & unDoc.direccion & "');"
            errorsql = ejecutarSQL(consultaPersonas)

            Dim consultaFuncionarios As String
            consultaFuncionarios = "INSERT INTO funcionarios VALUES('" & fuci & "');"
            errorsql += ejecutarSQL(consultaFuncionarios)


            Dim consultaDocentes As String
            consultaDocentes = "INSERT INTO docentes VALUES('" & fuci & "');"
            errorsql += ejecutarSQL(consultaDocentes)

        End If

        Return errorsql
    End Function

    Public Function listarFuncionariosDocentes() As List(Of clsFuncionarioDocente)
        Dim consulta As String
        consulta = "SELECT personas.nombrePersona, personas.apellidoPersona, personas.ciPersona, personas.correoPersona, personas.numeroPersona, personas.direccionPersona FROM personas INNER JOIN docentes ON personas.ciPersona = docentes.Funcionarios_Persona_ciPersona"
        Dim datos As MySqlDataReader
        datos = ejecutarYdevolver(consulta)
        Dim colFun As New List(Of clsFuncionarioDocente)

        While datos.Read
            Dim unFun As New clsFuncionarioDocente
            unFun = crearFuncionario(datos)
            colFun.Add(unFun)
        End While

        Return colFun
    End Function

    Public Function obtenerDatos(ByVal ci As String) As clsFuncionarioDocente
        Dim consulta As String
        consulta = "SELECT nombrePersona, apellidoPersona, ciPersona, correoPersona, numeroPersona, direccionPersona FROM personas WHERE ciPersona = " & ci & ";"

        Dim datos As MySqlDataReader
        datos = ejecutarYdevolver(consulta)
        Dim unFun As New clsFuncionarioDocente

        While datos.Read
            unFun = crearFuncionario(datos)
        End While

        Return unFun
    End Function

    Public Function modificarFuncionarioDocente(unFun As clsFuncionarioDocente) As String
        Dim consulta = "UPDATE personas SET nombrePersona = '" & unFun.nombre & "', apellidoPersona = '" & unFun.apellido & "', correoPersona = '" & unFun.correo & "', numeroPersona = '" & unFun.numero & "', direccionPersona = '" & unFun.direccion & "' WHERE ciPersona = '" & unFun.ci & "';"
        Dim errorSQL As String

        errorSQL = ejecutarSQL(consulta)
        Return errorSQL
    End Function

    Public Function bajaDocente(ByVal ci As String) As String
        Dim consultaDocente As String
        consultaDocente = "DELETE FROM docentes WHERE Funcionarios_Persona_ciPersona = '" & ci & "';"

        Dim consultaFuncionario As String
        consultaFuncionario = "DELETE FROM funcionarios WHERE Persona_ciPersona = '" & ci & "';"


        Dim consultaPersona As String
        consultaPersona = "DELETE FROM personas WHERE ciPersona = '" & ci & "';"

        Dim consultaHorario As String
        consultaHorario = "DELETE FROM docentes_has_horariosdocente WHERE Docentes_Funcionarios_Persona_ciPersona ='" & ci & "';"

        Dim errorSQL As String
        errorSQL = ejecutarSQL(consultaHorario)


        If errorSQL = "" Then
            errorSQL = ejecutarSQL(consultaDocente)

        End If

        If errorSQL = "" Then
            errorSQL = ejecutarSQL(consultaFuncionario)
        End If

        If errorSQL = "" Then
            errorSQL = ejecutarSQL(consultaPersona)
        End If


        Return errorSQL
    End Function

    Public Function buscarFuncionarioDocente(ByVal filtro As String) As List(Of clsFuncionarioDocente)
        Dim consulta As String
        Dim datos As MySqlDataReader
        Dim colFun As New List(Of clsFuncionarioDocente)

        consulta = "SELECT personas.nombrePersona, personas.apellidoPersona, personas.ciPersona, personas.correoPersona, personas.numeroPersona, personas.direccionPersona FROM personas INNER JOIN docentes ON personas.ciPersona = docentes.Funcionarios_Persona_ciPersona WHERE apellidoPersona LIKE '" & filtro & "%'"

        datos = ejecutarYdevolver(consulta)

        While datos.Read
            Dim unFun As New clsFuncionarioDocente
            unFun = crearFuncionario(datos)
            colFun.Add(unFun)
        End While

        Return colFun
    End Function

    Public Function checkFuncionarioDocente(ByVal ci As String) As clsFuncionarioDocente
        Dim consulta As String
        Dim datos As MySqlDataReader
        Dim unF As New clsFuncionarioDocente

        consulta = "SELECT personas.nombrePersona, personas.apellidoPersona, personas.ciPersona, personas.correoPersona, personas.numeroPersona, personas.direccionPersona FROM personas INNER JOIN docentes ON personas.ciPersona = docentes.Funcionarios_Persona_ciPersona  WHERE ciPersona = '" & ci & "';"
        datos = ejecutarYdevolver(consulta)

        While datos.Read
            unF = crearFuncionario(datos)
        End While

        Return unF
    End Function
End Class
