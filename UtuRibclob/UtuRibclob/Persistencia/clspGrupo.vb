﻿Imports MySql.Data
Imports MySql.Data.MySqlClient

Public Class clspGrupo
    Inherits clsPersistencia

    Public Sub altaGrupos(unGru As clsGrupo)
        Dim consulta As String
        consulta = " INSERT INTO grupos VALUES ('" & unGru.NombreGrupo & "', '" & unGru.DescripcionGrupo & "');"
        ejecutarSQL(consulta)
    End Sub

    Public Function bajaGrupos(unGru As clsGrupo) As String
        Dim consulta = "DELETE FROM grupos WHERE nombreGrupos = '" & unGru.NombreGrupo & "';"
        Dim resultadoSQL As String

        resultadoSQL = ejecutarSQL(consulta)
        Return resultadoSQL
    End Function

    Public Function modificarGrupos(unGru As clsGrupo) As String
        Dim consulta = "UPDATE grupos SET descripcionGrupos = '" & unGru.DescripcionGrupo & "' WHERE nombreGrupos = '" & unGru.NombreGrupo & "';"

        Dim resultadoSQL As String

        resultadoSQL = ejecutarSQL(consulta)
        Return resultadoSQL
    End Function

    Public Function listarGupos() As List(Of clsGrupo)
        Dim consulta As String
        consulta = "SELECT nombreGrupos, descripcionGrupos FROM grupos"

        Dim colGrupo As New List(Of clsGrupo)
        Dim datos As MySqlDataReader
        datos = ejecutarYdevolver(consulta)

        While datos.Read
            Dim unGrupo As New clsGrupo
            unGrupo = crearGrupos(datos)
            colGrupo.Add(unGrupo)
        End While

        Return colGrupo
    End Function

    Public Function crearGrupos(ByVal datos As MySqlDataReader) As clsGrupo
        Dim unGrupo As New clsGrupo

        unGrupo.NombreGrupo = datos.Item("nombreGrupos").ToString
        unGrupo.DescripcionGrupo = datos.Item("descripcionGrupos").ToString

        Return unGrupo
    End Function

    Public Function buscarGrupo(ByVal filtro As String) As List(Of clsGrupo)
        Dim consulta As String
        Dim datos As MySqlDataReader
        Dim colGrupos As New List(Of clsGrupo)

        consulta = "SELECT nombreGrupos, descripcionGrupos FROM grupos WHERE nombreGrupos LIKE '" & filtro & "';"

        datos = ejecutarYdevolver(consulta)

        While datos.Read
            Dim unG As New clsGrupo
            unG = crearGrupos(datos)
            colGrupos.Add(unG)
        End While

        Return colGrupos
    End Function

End Class

