﻿Imports MySql.Data
Imports MySql.Data.MySqlClient

Public Class clspFaltaNoDocente
    Inherits clsPersistencia

    Public Function registroDiarioNoDocente(ByVal unaFecha As String) As String

        ' ======================================================================================================================================================
        ' Esta funcion basicamente esta creada para registrar la falta de los funcionarios NO docentes de acuerdo al horario que tienen establecido en el programa.
        '
        ' PD de Lucius:
        ' -> Esta basada en clspFaltaDocente pero adaptada para su correcto funcionamiento
        ' -> Todavia no funciona ni la he probado porque no la he terminado xD
        '
        '
        ' To-Do:
        ' (*) Que tenga en cuenta la entrada y salida
        ' (*) Que tenga en cuenta excepciones
        ' (*) Que tenga en cuenta que se haya firmado salida en registro
        ' (*) Imposible que compare fechas si las dos son String
        ' (*) Que inserte la falta
        ' (*) Que tenga una consideracion de 5 minutos
        ' () Que funcione ahre igual si
        ' (*) Hacer un formulario lindo donde se pueda acceder a esta funcion (frmReporte)
        ' (*) Que se pueda ingresar un dia para que vuelva a sacar las faltas en vez de que tome el dia presente estaria lindo
        '
        ' /\_/\
        '( -.o )
        ' > ^ <
        '
        ' ======================================================================================================================================================

        ' Pasamos la fecha que nos proporciono el usuario de String a Date
        Dim fechaUsuario As Date = Date.ParseExact(unaFecha, "yyyy-MM-dd HH:mm:ss", System.Globalization.DateTimeFormatInfo.InvariantInfo)
        Dim fechaUsuarioString As String = fechaUsuario.ToString("yyyy-MM-dd")
        Dim fechaUsuarioStringInicio As String = fechaUsuarioString & " 00:00:00"
        Dim fechaUsuarioStringFinal As String = fechaUsuarioString & " 23:59:59"

        Console.WriteLine("-> Iniciando sistema de registro de faltas a No Docentes")
        Console.WriteLine("-> La fecha introducida por el usuario es:" & unaFecha)

        Dim devolverError As String = ""
        Dim colHorarios As New List(Of clsHorarioNoDocente)
        Dim consulta As String = ""
        Dim datos As MySqlDataReader
        Dim excepcion As New clsExcepcion
        Dim excepcionIsReal As Boolean = False
        Dim fechaFalta As Date
        Dim excepcionSeAplica As Boolean = False
        Dim aplicaExcepcion As String = "NULL"
        Dim diaActual As Integer = fechaUsuario.DayOfWeek ' La fecha actual de la semana (Lunes = '1', Martes = '2', etc)


        ' Rescatamos horarios de no docentes
        consulta = "SELECT horariosnodocente.idHorarioNoDocente, horariosnodocente.entradaHorarioNoDocente, horariosnodocente.salidaHorarioNoDocente, horariosnodocente.diasemanalHorarioNoDocente, horariosnodocente.NoDocente_Funcionarios_Persona_ciPersona FROM horariosnodocente INNER JOIN nodocentes WHERE nodocentes.Funcionarios_Persona_ciPersona = horariosnodocente.NoDocente_Funcionarios_Persona_ciPersona;"
        datos = ejecutarYdevolver(consulta)
        While datos.Read
            Dim unH As New clsHorarioNoDocente
            unH = crearHorarios(datos)
            colHorarios.Add(unH)
        End While

        'Consulta por excepciones
        consulta = "SELECT idExcepciones, tipoExcepciones, DATE_FORMAT(comienzoExcpeciones, '%Y-%m-%d %H:%i:%s'), DATE_FORMAT(finalExcepciones, '%Y-%m-%d %H:%i:%s') FROM excepciones WHERE '" & fechaUsuarioStringInicio & "' > comienzoExcpeciones AND '" & fechaUsuarioStringFinal & "' < finalExcepciones;"

        datos = ejecutarYdevolver(consulta)
        While datos.Read
            excepcion.idExcepcion = datos.Item("idExcepciones").ToString
            excepcion.comienzoExcepcion = datos.Item("DATE_FORMAT(comienzoExcpeciones, '%Y-%m-%d %H:%i:%s')").ToString
            excepcion.finalExcepcion = datos.Item("DATE_FORMAT(finalExcepciones, '%Y-%m-%d %H:%i:%s')").ToStrin
        End While

        ' Se fija si hay una excepcion en este dia
        If Not excepcion.comienzoExcepcion = "" Then
            excepcionIsReal = True
            Console.WriteLine("-> Existe una excepcion este dia")
        End If

        For Each unH In colHorarios

            Console.WriteLine("")
            Console.WriteLine("--------------------------------------------------------------")
            Console.WriteLine("Recorriendo horario " & unH.entradaHorarioNoDocente & "/" & unH.salidaHorarioNoDocente & " de " & unH.funcionario.ci)
            Console.WriteLine("--------------------------------------------------------------")
            Console.WriteLine("")

            Dim colRegistroEntrada As New List(Of clsRegistroFuncionarios)
            Dim colRegistroSalida As New List(Of clsRegistroFuncionarios)
            Dim llevaFalta As Boolean = True 'Asume que lleva falta hasta que existan pruebas de lo contrario
            Dim noVino As Boolean = True ' Si no existen registros
            Dim aplicaDia As Boolean = False ' Si el horario es en este dia
            Dim diaHorario As Integer ' El dia del horario en Integer

            Select Case unH.diaSemanalHorarioNoDocente
                Case "Lunes"
                    diaHorario = 1
                Case "Martes"
                    diaHorario = 2
                Case "Miercoles"
                    diaHorario = 3
                Case "Jueves"
                    diaHorario = 4
                Case "Viernes"
                    diaHorario = 5
                Case "Sabado"
                    diaHorario = 6
            End Select

            Console.WriteLine("-> Dia: " & diaHorario)

            Dim horarioVBentrada As String = fechaUsuarioString & " " & unH.entradaHorarioNoDocente
            Dim horarioVBsalida As String = fechaUsuarioString & " " & unH.salidaHorarioNoDocente
            Dim horarioEntrada As Date = Date.ParseExact(horarioVBentrada, "yyyy-MM-dd HH:mm:ss", System.Globalization.DateTimeFormatInfo.InvariantInfo)
            Dim horarioSalida As Date = Date.ParseExact(horarioVBsalida, "yyyy-MM-dd HH:mm:sss", System.Globalization.DateTimeFormatInfo.InvariantInfo)

            'Excepcion
            If excepcionIsReal = True Then
                Dim excepcionComienzo As Date = Date.ParseExact(excepcion.comienzoExcepcion, "dd/MM/yyyy HH:mm:ss", System.Globalization.DateTimeFormatInfo.InvariantInfo)
                Dim excepcionSalida As Date = Date.ParseExact(excepcion.finalExcepcion, "dd/MM/yyyy HH:mm:ss", System.Globalization.DateTimeFormatInfo.InvariantInfo)

                ' Verificamos que la excepcion haya occurido durante el lapso del horario

                If excepcionComienzo > horarioEntrada And excepcionSalida < horarioEntrada Then
                    excepcionSeAplica = True
                    Console.WriteLine("-> Se aplica excepcion a este horario.")
                    aplicaExcepcion = "'" & excepcion.idExcepcion & "'"
                End If

            End If

            ' Chequea que el dia del horario corresponda al dia en el que estamos
            If diaActual = diaHorario Then
                aplicaDia = True
                Console.WriteLine("El dia es correcto")
            End If

            ' Para que sea mas facil debuggear se manda todo a la consola
            Console.WriteLine("-> Empezando sistema de Falta No Docente.")
            Console.WriteLine("-> Fecha enviada: " & unaFecha)

            If aplicaDia = True Then
                If excepcionSeAplica = True Then
                    ' Si la excepcion se aplica, ni se molesta en verificar si el docente vino
                    Console.WriteLine("-> Entra a excepcion se aplica, por lo tanto no hace nada")
                    aplicaExcepcion = "'" & excepcion.idExcepcion & "'"
                Else
                    Console.WriteLine("-> No se aplica una excepcion")
                    ' Rescatamos los registros de entrada

                    consulta = "SELECT DATE_FORMAT(fechaCuadernoRegistros, '%d/%m/%Y %H:%i'), residenciaRegistrosFuncionarios FROM registrosfuncionarios WHERE Funcionario_Persona_ciPersona = '" & unH.funcionario.ci & "' AND residenciaRegistrosFuncionarios = '0' AND fechaCuadernoRegistros > '" & unaFecha & "' < fechaCuadernoRegistros;"
                    datos = ejecutarYdevolver(consulta)

                    While datos.Read
                        Dim unR As New clsRegistroFuncionarios
                        unR = crearRegistro(datos)
                        colRegistroEntrada.Add(unR)
                    End While

                    For Each unR In colRegistroEntrada

                        Dim registroFechaEntrada As Date = Date.ParseExact(unR.fecha, "dd/MM/yyyy HH:mm", System.Globalization.DateTimeFormatInfo.InvariantInfo)
                        fechaFalta = registroFechaEntrada

                        If registroFechaEntrada < horarioEntrada Then

                            ' Rescatamos los registros de entrada

                            Console.WriteLine("-> Hay un registro para la entrada del horario")
                            consulta = "SELECT DATE_FORMAT(fechaCuadernoRegistros, '%d/%m/%Y %H:%i'), residenciaRegistrosFuncionarios FROM registrosfuncionarios WHERE Funcionario_Persona_ciPersona = '" & unH.funcionario.ci & "' AND residenciaRegistrosFuncionarios = '1' AND fechaCuadernoRegistros > '" & unaFecha & "' < fechaCuadernoRegistros;"
                            datos = ejecutarYdevolver(consulta)

                            While datos.Read
                                Dim otroR As New clsRegistroFuncionarios
                                otroR = crearRegistro(datos)
                                colRegistroSalida.Add(otroR)
                            End While

                            For Each otroR In colRegistroSalida

                                Dim registroFechaSalida As Date = Date.ParseExact(otroR.fecha, "dd/MM/yyyy HH:mm", System.Globalization.DateTimeFormatInfo.InvariantInfo)

                                If registroFechaSalida > horarioSalida Then
                                    llevaFalta = False
                                    Console.WriteLine("-> No lleva falta :)")
                                End If
                            Next

                        ElseIf Not registroFechaEntrada < horarioEntrada Then
                            llevaFalta = True
                        End If

                    Next

                    If llevaFalta = True Then
                        Console.WriteLine("-> Lleva falta :(")

                        Dim faltaToString As String = fechaFalta.ToString("yyyy-MM-dd hh:mm:ss")

                        consulta = "SELECT idFaltasNoDocentes FROM faltasnodocentes WHERE fechaFaltasNoDocentes = '" & faltaToString & "';"
                        datos = ejecutarYdevolver(consulta)
                        Dim existeFalta As Boolean = False

                        If datos.Read Then
                            existeFalta = True
                        End If

                        Dim tipoFalta As String = ""
                        If noVino = False Then
                            tipoFalta = "Falta"
                        ElseIf noVino = True Then
                            tipoFalta = "Llegada tarde"
                        End If

                        If existeFalta = False Then
                            consulta = "INSERT INTO faltasnodocentes VALUES('', '" & faltaToString & "', '" & unH.entradaHorarioNoDocente & "', '" & unH.salidaHorarioNoDocente & "', '" & tipoFalta & "', '" & unH.funcionario.ci & "', " & aplicaExcepcion & ")"
                            devolverError = ejecutarSQL(consulta)

                            If Not devolverError = "" Then
                                Console.WriteLine("################## ERROR SQL ##################")
                                Console.WriteLine("Error en cuestion: " & devolverError)
                                Console.WriteLine("SQL enviado: " & consulta)
                                Console.WriteLine("###############################################")
                            End If
                        End If

                    End If
                End If
            End If
        Next

        Return devolverError
    End Function

    Public Function crearHorarios(datos As MySqlDataReader) As clsHorarioNoDocente
        Dim unHo As New clsHorarioNoDocente

        unHo.idHorarioNoDocente = datos.Item("idHorarioNoDocente").ToString
        unHo.entradaHorarioNoDocente = datos.Item("entradaHorarioNoDocente").ToString
        unHo.salidaHorarioNoDocente = datos.Item("salidaHorarioNoDocente").ToString
        unHo.diaSemanalHorarioNoDocente = datos.Item("diasemanalHorarioNoDocente").ToString
        unHo.funcionario.ci = datos.Item("NoDocente_Funcionarios_Persona_ciPersona").ToString

        Return unHo
    End Function

    Public Function crearFalta(datos As MySqlDataReader) As clsFaltaNoDocente
        Dim unaFalta As New clsFaltaNoDocente

        unaFalta.id = datos.Item("idFaltasNoDocentes").ToString
        unaFalta.fecha = datos.Item("DATE_FORMAT(faltasnodocentes.fechaFaltasNoDocentes, '%d/%m/%Y')").ToString
        unaFalta.horaInicio = datos.Item("horaInicioFaltasNoDocentes").ToString
        unaFalta.horaFinal = datos.Item("horaFinalFaltasNoDocentes").ToString
        unaFalta.descripcion = datos.Item("descripcionFaltasNoDocentes").ToString
        unaFalta.funcionario.ci = datos.Item("NoDocentes_Funcionarios_Persona_ciPersona").ToString
        If IsDBNull(datos.Item("Excepciones_idExcepciones")) Then

        Else
            unaFalta.excepcion = datos.Item("Excepciones_idExcepciones")
        End If


        Return unaFalta
    End Function

    Public Function crearFaltaRegistro(datos As MySqlDataReader) As clsFaltaNoDocente
        Dim unaFalta As New clsFaltaNoDocente

        unaFalta.id = datos.Item("idFaltasNoDocentes").ToString
        unaFalta.fecha = datos.Item("DATE_FORMAT(faltasnodocentes.fechaFaltasNoDocentes, '%d/%m/%Y')").ToString
        unaFalta.horaInicio = datos.Item("horaInicioFaltasNoDocentes").ToString
        unaFalta.horaFinal = datos.Item("horaFinalFaltasNoDocentes").ToString
        unaFalta.descripcion = datos.Item("descripcionFaltasNoDocentes").ToString
        unaFalta.funcionario.ci = datos.Item("NoDocentes_Funcionarios_Persona_ciPersona").ToString
        unaFalta.funcionario.nombre = datos.Item("nombrePersona").ToString
        unaFalta.funcionario.apellido = datos.Item("apellidoPersona").ToString

        If IsDBNull(datos.Item("Excepciones_idExcepciones")) Then

        Else
            unaFalta.excepcion = datos.Item("Excepciones_idExcepciones")
        End If


        Return unaFalta
    End Function

    Public Function crearRegistro(datos As MySqlDataReader) As clsRegistroFuncionarios
        Dim unF As New clsRegistroFuncionarios

        unF.fecha = datos.Item("DATE_FORMAT(fechaCuadernoRegistros, '%d/%m/%Y %H:%i')").ToString
        unF.residencia = datos.Item("residenciaRegistrosFuncionarios").ToString
        Return unF
    End Function

    Public Function listarFaltas(ByVal ci As String, ByVal fechaInicial As String, ByVal fechaFinal As String) As List(Of clsFaltaNoDocente)
        Dim consulta As String
        Dim datos As MySqlDataReader
        Dim colFaltas As New List(Of clsFaltaNoDocente)

        consulta = "SELECT faltasnodocentes.idFaltasNoDocentes, DATE_FORMAT(faltasnodocentes.fechaFaltasNoDocentes, '%d/%m/%Y'), faltasnodocentes.horaInicioFaltasNoDocentes, faltasnodocentes.horaFinalFaltasNoDocentes, faltasnodocentes.descripcionFaltasNoDocentes, faltasnodocentes.NoDocentes_Funcionarios_Persona_ciPersona, faltasnodocentes.Excepciones_idExcepciones FROM faltasnodocentes WHERE NoDocentes_Funcionarios_Persona_ciPersona = '" & ci & "' AND fechaFaltasNoDocentes >  STR_TO_DATE('" & fechaInicial & " 00:00:00', '%e/%c/%Y %H:%i:%s') AND fechaFaltasNoDocentes < STR_TO_DATE('" & fechaFinal & " 23:59:59', '%e/%c/%Y %H:%i:%s');"

        datos = ejecutarYdevolver(consulta)

        While datos.Read
            Dim unaFalta As New clsFaltaNoDocente
            unaFalta = crearFalta(datos)
            colFaltas.Add(unaFalta)
        End While

        Return colFaltas
    End Function

    Public Function listarFaltas(ByVal fechaInicial As String, ByVal fechaFinal As String) As List(Of clsFaltaNoDocente)
        Dim consulta As String
        Dim datos As MySqlDataReader
        Dim colFaltas As New List(Of clsFaltaNoDocente)

        consulta = "SELECT personas.nombrePersona, personas.apellidoPersona, excepciones.tipoExcepciones, faltasnodocentes.idFaltasNoDocentes, DATE_FORMAT(faltasnodocentes.fechaFaltasNoDocentes, '%d/%m/%Y'), faltasnodocentes.horaInicioFaltasNoDocentes, faltasnodocentes.horaFinalFaltasNoDocentes, faltasnodocentes.descripcionFaltasNoDocentes, faltasnodocentes.NoDocentes_Funcionarios_Persona_ciPersona, faltasnodocentes.Excepciones_idExcepciones FROM faltasnodocentes INNER JOIN personas ON personas.ciPersona = faltasnodocentes.NoDocentes_Funcionarios_Persona_ciPersona LEFT JOIN excepciones ON faltasnodocentes.Excepciones_idExcepciones = excepciones.idExcepciones WHERE fechaFaltasNoDocentes >  STR_TO_DATE('" & fechaInicial & " 00:00:00', '%e/%c/%Y %H:%i:%s') AND fechaFaltasNoDocentes < STR_TO_DATE('" & fechaFinal & " 23:59:59', '%e/%c/%Y %H:%i:%s');"

        datos = ejecutarYdevolver(consulta)

        While datos.Read
            Dim unaFalta As New clsFaltaNoDocente
            unaFalta = crearFaltaRegistro(datos)
            colFaltas.Add(unaFalta)
        End While

        Return colFaltas
    End Function

    Public Function altaFaltasNoDocentes(unafalta As clsFaltaNoDocente) As String
        Dim consulta As String
        Dim datos As String
        consulta = "INSERT INTO faltasnodocentes VALUES('', '" & unafalta.fecha & "', '" & unafalta.horaInicio & "', '" & unafalta.horaFinal & "', '" & unafalta.descripcion & "', '" & unafalta.funcionario.ci & "' , NULL )"
        datos = ejecutarSQL(consulta)
        Return datos
    End Function

    Public Function modificarFaltasNoDocentes(unafalta As clsFaltaNoDocente) As String
        Dim consulta As String
        Dim datos As String
        Dim consultaID As String
        Dim datosID As MySqlDataReader
        Dim idExe As String = "xD"

        Dim fecha As Date = Date.ParseExact(unafalta.fecha, "dd/MM/yyyy", System.Globalization.DateTimeFormatInfo.InvariantInfo)
        Dim fechaAsString As String = fecha.ToString("yyyy-MM-dd")

        consultaID = "SELECT idFaltasNoDocentes FROM faltasnodocentes WHERE fechaFaltasNoDocentes = '" & fechaAsString & "' AND horaInicioFaltasNoDocentes = '" & unafalta.horaInicio & "' AND horaFinalFaltasNoDocentes = '" & unafalta.horaFinal & "' AND NoDocentes_Funcionarios_Persona_ciPersona = '" & unafalta.funcionario.ci & "' "
        datosID = ejecutarYdevolver(consultaID)

        While datosID.Read
            idExe = datosID.Item("idFaltasNoDocentes").ToString
        End While

        consulta = "UPDATE  faltasnodocentes SET descripcionFaltasNoDocentes = " & unafalta.descripcion & " WHERE idFaltasNoDocentes = " & idExe & ""
        datos = ejecutarSQL(consulta)
        Return datos
    End Function

    Public Function bajaFaltasNoDocentes(unaFalta As clsFaltaNoDocente) As String
        Dim consulta As String
        Dim errorSQL As String

        Dim fecha As Date = Date.ParseExact(unaFalta.fecha, "dd/MM/yyyy", System.Globalization.DateTimeFormatInfo.InvariantInfo)
        unaFalta.fecha = fecha.ToString("yyyy-MM-dd")

        consulta = "DELETE FROM faltasnodocentes WHERE fechaFaltasNoDocentes = '" & unaFalta.fecha & "' AND descripcionFaltasNoDocentes = '" & unaFalta.descripcion & "' AND NoDocentes_Funcionarios_Persona_ciPersona = '" & unaFalta.funcionario.ci & "';"
        errorSQL = ejecutarSQL(consulta)

        Return errorSQL
    End Function
End Class
