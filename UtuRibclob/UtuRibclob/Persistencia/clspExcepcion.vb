﻿Imports MySql.Data
Imports MySql.Data.MySqlClient

Public Class clspExcepcion
    Inherits clsPersistencia

    Public Function altaExcepcion(datos As clsExcepcion) As String
        Dim consulta As String
        Dim errorsql As String

        consulta = "INSERT INTO excepciones VALUES ('" & vbEmpty & "', '" & datos.tipoExcepcion & "', (STR_TO_DATE('" & datos.comienzoExcepcion & "', '%Y-%m-%d %H:%i:%s')), (STR_TO_DATE('" & datos.finalExcepcion & "', '%Y-%m-%d %H:%i:%s')));"
        errorsql = ejecutarSQL(consulta)

        Return errorsql
    End Function

    Public Function bajaExcepciones(datos As clsExcepcion) As String
        Dim consulta As String
        Dim errorsql As String

        consulta = "DELETE FROM excepciones WHERE tipoExcepciones = '" & datos.tipoExcepcion & "' AND comienzoExcpeciones = STR_TO_DATE('" & datos.comienzoExcepcion & "', '%d/%m/%Y %H:%i:%s') AND finalExcepciones = STR_TO_DATE('" & datos.finalExcepcion & "', '%d/%m/%Y %H:%i:%s');"

        errorsql = ejecutarSQL(consulta)

        Return errorsql
    End Function

    Public Function listarExcepciones() As List(Of clsExcepcion)
        Dim consulta As String
        Dim colExpeciones As New List(Of clsExcepcion)
        Dim datos As MySqlDataReader

        consulta = "SELECT idExcepciones, tipoExcepciones, DATE_FORMAT(comienzoExcpeciones, '%d/%m/%Y %H:%i:%s'), DATE_FORMAT(finalExcepciones, '%d/%m/%Y %H:%i:%s') FROM excepciones;"

        datos = ejecutarYdevolver(consulta)

        While datos.Read
            Dim unaEx As New clsExcepcion

            unaEx = crearExcepciones(datos)

            colExpeciones.Add(unaEx)
        End While

        Return colExpeciones
    End Function

    Public Function crearExcepciones(datos As MySqlDataReader) As clsExcepcion
        Dim unaE As New clsExcepcion

        unaE.comienzoExcepcion = datos.Item("DATE_FORMAT(comienzoExcpeciones, '%d/%m/%Y %H:%i:%s')").ToString
        unaE.finalExcepcion = datos.Item("DATE_FORMAT(finalExcepciones, '%d/%m/%Y %H:%i:%s')").ToString
        unaE.tipoExcepcion = datos.Item("tipoExcepciones").ToString
        unaE.idExcepcion = datos.Item("idExcepciones").ToString

        Return unaE
    End Function

    Public Function modificarExcepciones(exNueva As clsExcepcion, exVieja As clsExcepcion) As String
        Dim consulta As String
        Dim errorsql As String
        Dim consultaID As String
        Dim datosID As MySqlDataReader
        Dim idExe As String = "xD"

        consultaID = "SELECT idExcepciones FROM excepciones WHERE tipoExcepciones = '" & exVieja.tipoExcepcion & "' AND comienzoExcpeciones = STR_TO_DATE('" & exVieja.comienzoExcepcion & "', '%d/%m/%Y %H:%i:%s') AND finalExcepciones = STR_TO_DATE('" & exVieja.finalExcepcion & "', '%d/%m/%Y %H:%i:%s');"
        datosID = ejecutarYdevolver(consultaID)

        While datosID.Read
            idExe = datosID.Item("idExcepciones").ToString
        End While

        consulta = "UPDATE excepciones SET tipoExcepciones = '" & exNueva.tipoExcepcion & "', comienzoExcpeciones = STR_TO_DATE('" & exNueva.comienzoExcepcion & "', '%Y-%m-%d %H:%i:%s'), finalExcepciones = STR_TO_DATE('" & exNueva.finalExcepcion & "', '%Y-%m-%d %H:%i:%s') WHERE idExcepciones = '" & idExe & "';"

        errorsql = ejecutarSQL(consulta)

        Return errorsql
    End Function

    Public Function buscarExcepciones(ByVal filtro As String) As List(Of clsExcepcion)
        Dim consulta As String
        Dim colExpeciones As New List(Of clsExcepcion)
        Dim datos As MySqlDataReader

        consulta = "SELECT idExcepciones, tipoExcepciones, DATE_FORMAT(comienzoExcpeciones, '%d/%m/%Y %H:%i:%s'), DATE_FORMAT(finalExcepciones, '%d/%m/%Y %H:%i:%s') FROM excepciones WHERE tipoExcepciones LIKE '" & filtro & "%';"

        datos = ejecutarYdevolver(consulta)

        While datos.Read
            Dim unaEx As New clsExcepcion

            unaEx = crearExcepciones(datos)

            colExpeciones.Add(unaEx)
        End While

        Return colExpeciones
    End Function
End Class
