﻿Imports MySql.Data
Imports MySql.Data.MySqlClient

Public Class clspHorarioDocente
    Inherits clsPersistencia

    Public Function altaHorarioDocente(ByVal unHorarioD As clsHorarioDocente) As String
        Dim consulta As String
        Dim errorSql As String

        consulta = "INSERT INTO horariosdocente VALUES('' , STR_TO_DATE('" & unHorarioD.entrada & "', '%H:%i:%s') , STR_TO_DATE('" & unHorarioD.salida & "', '%H:%i:%s'));"
        errorSql = ejecutarSQL(consulta)

        Return errorSql
    End Function

    Public Function listarHorarioDocente() As List(Of clsHorarioDocente)
        Dim consulta As String
        consulta = "SELECT idHorariosDocente, entradaHorarioDocente, salidaHorarioDocente FROM horariosdocente;"

        Dim datos As MySqlDataReader
        datos = ejecutarYdevolver(consulta)

        Dim colHorarios As New List(Of clsHorarioDocente)

        While datos.Read
            Dim unHorario As New clsHorarioDocente
            unHorario = crearHorario(datos)
            colHorarios.Add(unHorario)
        End While

        Return colHorarios
    End Function

    Public Function crearHorario(ByVal datos As MySqlDataReader) As clsHorarioDocente
        Dim unHo As New clsHorarioDocente

        unHo.IdHorarioDocente = datos.Item("idHorariosDocente").ToString
        unHo.entrada = datos.Item("entradaHorarioDocente").ToString
        unHo.salida = datos.Item("salidaHorarioDocente").ToString
        Return unHo
    End Function

    Public Function bajaHorario(ByVal unHorario As String) As String
        Dim consulta As String
        consulta = "DELETE FROM horariosdocente WHERE entradaHorarioDocente = '" & unHorario & "';"

        Dim resultadoSQL As String
        resultadoSQL = ejecutarSQL(consulta)

        Return resultadoSQL
    End Function

    Public Function modificarHorario(unHor As clsHorarioDocente, ByVal nuevoI As String) As String
        Dim consulta = "UPDATE horariosdocente SET idHorarioDocente = "" , entradaHorarioDocente = '" & nuevoI & "', salidaHorarioDocente = '" & unHor.salida & "' WHERE idHorarioDocente =" & unHor.IdHorarioDocente & ";"
        Dim errorSQL As String

        errorSQL = ejecutarSQL(consulta)
        Return errorSQL
    End Function







End Class
