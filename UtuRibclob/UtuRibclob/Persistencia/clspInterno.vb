﻿Imports MySql.Data
Imports MySql.Data.MySqlClient

Public Class clspInterno
    Inherits clsPersistencia

    Public Function crearInterno(ByVal datos As MySqlDataReader) As clsInterno
        Dim unI As New clsInterno

        unI.ci = datos.Item("ciPersona").ToString
        unI.nombre = datos.Item("nombrePersona").ToString
        unI.apellido = datos.Item("apellidoPersona").ToString
        unI.correo = datos.Item("correoPersona").ToString
        unI.numero = datos.Item("numeroPersona").ToString
        unI.direccion = datos.Item("direccionPersona").ToString
        unI.tutor = datos.Item("tutorInternos").ToString
        unI.numeroTutor = datos.Item("numeroTutorInternos").ToString
        unI.curso = datos.Item("cursoInternos").ToString

        Return unI
    End Function

    Public Function listarInternos() As List(Of clsInterno)
        Dim consultaInternos As String
        consultaInternos = "SELECT internos.tutorInternos, internos.numeroTutorInternos, internos.cursoInternos, personas.nombrePersona, personas.apellidoPersona, personas.ciPersona, personas.correoPersona, personas.numeroPersona, personas.direccionPersona FROM personas INNER JOIN internos ON personas.ciPersona = internos.ciInterno_Persona_ciPersona"

        Dim datos As MySqlDataReader
        datos = ejecutarYdevolver(consultaInternos)

        Dim colInternos As New List(Of clsInterno)

        While (datos.Read())
            Dim unI As New clsInterno
            unI = crearInterno(datos)
            colInternos.Add(unI)
        End While

        Return colInternos
    End Function

    Public Function altaInterno(ByVal unI As clsInterno) As String
        Dim errorSQL As String

        Dim consultaPersonas As String
        consultaPersonas = "INSERT INTO personas VALUES ('" & unI.ci & "', '" & unI.nombre & "', '" & unI.apellido & "', '" & unI.numero & "', '" & unI.correo & "', '" & unI.direccion & "')"

        Dim consultaInternos As String
        consultaInternos = "INSERT INTO internos VALUES ('" & unI.ci & "','" & unI.tutor & "','" & unI.numeroTutor & "', '" & unI.curso & "')"

        errorSQL = ejecutarSQL(consultaPersonas)

        If errorSQL = "" Then
            ejecutarSQL(consultaInternos)
        End If

        Return errorSQL
    End Function

    Public Function modificarInterno(ByVal unI As clsInterno) As String
        Dim consultaPersona As String
        consultaPersona = "UPDATE personas SET nombrePersona = '" & unI.nombre & "', apellidoPersona = '" & unI.apellido & "', numeroPersona = '" & unI.numero & "', correoPersona = '" & unI.correo & "', direccionPersona = '" & unI.direccion & "' WHERE ciPersona = '" & unI.ci & "';"

        Dim consultaInternos As String
        consultaInternos = "UPDATE internos SET tutorInternos = '" & unI.tutor & "', numeroTutorInternos = '" & unI.numeroTutor & "', cursoInternos = '" & unI.curso & "' WHERE ciInterno_Persona_ciPersona = '" & unI.ci & "';"

        Dim resultadoSQL As String
        resultadoSQL = ejecutarSQL(consultaPersona)

        If resultadoSQL = "" Then
            resultadoSQL = ejecutarSQL(consultaInternos)
        End If

        Return resultadoSQL
    End Function

    Public Function obtenerDatosInterno(ByVal ci As String) As clsInterno
        Dim consultaSQL As String
        consultaSQL = "SELECT personas.nombrePersona, personas.apellidoPersona, personas.correoPersona, personas.numeroPersona, personas.direccionPersona, personas.ciPersona, internos.tutorInternos, internos.numeroTutorInternos, internos.cursoInternos FROM personas INNER JOIN internos ON personas.ciPersona = internos.ciInterno_Persona_ciPersona WHERE internos.ciInterno_Persona_ciPersona = '" & ci & "';"

        Dim datos As MySqlDataReader
        Dim interno As New clsInterno
        datos = ejecutarYdevolver(consultaSQL)

        While (datos.Read())
            interno = crearInterno(datos)
        End While

        Return interno
    End Function

    Public Function bajaInterno(ByVal ci As String) As String
        Dim consultaInterno As String
        consultaInterno = "DELETE FROM internos WHERE ciInterno_Persona_ciPersona = '" & ci & "';"

        Dim consultaPersona As String
        consultaPersona = "DELETE FROM personas WHERE ciPersona = '" & ci & "';"

        Dim errorSQL As String
        errorSQL = ejecutarSQL(consultaInterno)

        If errorSQL = "" Then
            errorSQL = ejecutarSQL(consultaPersona)
        End If

        Return errorSQL
    End Function

    Public Function buscarInterno(ByVal filtro As String) As List(Of clsInterno)
        Dim consulta As String
        Dim datos As MySqlDataReader
        Dim colInterno As New List(Of clsInterno)

        consulta = "SELECT internos.tutorInternos, internos.numeroTutorInternos, internos.cursoInternos, personas.nombrePersona, personas.apellidoPersona, personas.ciPersona, personas.correoPersona, personas.numeroPersona, personas.direccionPersona FROM personas INNER JOIN internos ON personas.ciPersona = internos.ciInterno_Persona_ciPersona WHERE apellidoPersona LIKE '" & filtro & "%';"

        datos = ejecutarYdevolver(consulta)

        While datos.Read
            Dim unInterno As New clsInterno
            unInterno = crearInterno(datos)
            colInterno.Add(unInterno)
        End While

        Return colInterno
    End Function

    Public Function checkInternos(ByVal ci As String) As clsInterno
        Dim consulta As String
        Dim datos As MySqlDataReader
        Dim unI As New clsInterno

        consulta = "SELECT internos.tutorInternos, internos.numeroTutorInternos, internos.cursoInternos, personas.nombrePersona, personas.apellidoPersona, personas.ciPersona, personas.correoPersona, personas.numeroPersona, personas.direccionPersona FROM personas INNER JOIN internos ON personas.ciPersona = internos.ciInterno_Persona_ciPersona WHERE ciPersona = '" & ci & "';"
        datos = ejecutarYdevolver(consulta)

        While datos.Read
            unI = crearInterno(datos)
        End While

        Return unI
    End Function
End Class
