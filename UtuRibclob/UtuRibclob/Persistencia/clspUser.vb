﻿Imports MySql.Data
Imports MySql.Data.MySqlClient

Public Class clspUser
    Inherits clsPersistencia

    Public Function crearUsuario(ByVal datos As MySqlDataReader) As clsUser
        Dim unU As New clsUser

        While datos.Read
            unU.tipo = datos.Item("tipoUsuario").ToString
            unU.nombre = datos.Item("nombrePersona").ToString
            unU.apellido = datos.Item("apellidoPersona").ToString
            unU.ci = datos.Item("ciPersona").ToString
            unU.correo = datos.Item("correoPersona").ToString
            unU.numero = datos.Item("numeroPersona").ToString
            unU.direccion = datos.Item("direccionPersona").ToString
            'unU.contrasena = datos.Item("contrasenaUsuario").ToString
        End While

        Return unU
    End Function

    Public Function crearUsuarioListar(ByVal datos As MySqlDataReader) As clsUser
        Dim unU As New clsUser

        unU.tipo = datos.Item("tipoUsuario").ToString
        unU.nombre = datos.Item("nombrePersona").ToString
        unU.apellido = datos.Item("apellidoPersona").ToString
        unU.ci = datos.Item("ciPersona").ToString
        unU.correo = datos.Item("correoPersona").ToString
        unU.numero = datos.Item("numeroPersona").ToString
        unU.direccion = datos.Item("direccionPersona").ToString
        'unU.contrasena = datos.Item("contrasenaUsuario").ToString

        Return unU
    End Function

    Public Function login(ByVal user As String, ByVal pass As String) As clsUser
        Dim consulta As String
        consulta = "SELECT usuarios.tipoUsuario, personas.nombrePersona, personas.apellidoPersona, personas.ciPersona, personas.correoPersona, personas.numeroPersona, personas.direccionPersona FROM personas INNER JOIN usuarios ON personas.ciPersona = usuarios.Funcionarios_Persona_ciPersona WHERE usuarios.Funcionarios_Persona_ciPersona = '" & user & "' AND usuarios.contrasenaUsuario = '" & pass & "';"

        Dim datos As MySqlDataReader
        datos = ejecutarYdevolver(consulta)

        Dim unUsuario As New clsUser
        unUsuario = crearUsuario(datos)
        Return unUsuario
    End Function

    Public Function listarUsuarios() As List(Of clsUser)
        Dim sql As String
        sql = "SELECT usuarios.Funcionarios_Persona_ciPersona , usuarios.tipoUsuario, personas.nombrePersona, personas.apellidoPersona, personas.ciPersona, personas.correoPersona, personas.numeroPersona, personas.direccionPersona FROM personas INNER JOIN usuarios ON personas.ciPersona = usuarios.Funcionarios_Persona_ciPersona"
        Dim datos As MySqlDataReader
        datos = ejecutarYdevolver(sql)

        Dim colUsuarios As New List(Of clsUser)

        While datos.Read
            Dim unU As New clsUser
            unU = crearUsuarioListar(datos)
            colUsuarios.Add(unU)
        End While

        Return colUsuarios
    End Function

    Public Function altaUsuario(ByVal unU As clsUser) As String
        Dim consulta As String
        Dim persona As String
        Dim datos As MySqlDataReader

        persona = "SELECT nombrePersona FROM personas WHERE ciPersona = '" & unU.ci & "';"
        datos = ejecutarYdevolver(persona)

        Dim resultado As String
        resultado = ""

        If datos.Read() Then
            Dim tienedatos As Boolean
            tienedatos = datos.HasRows
            If datos.HasRows Then
                consulta = "INSERT INTO usuarios VALUES ('" & unU.contrasena & "', '" & unU.tipo & "', '" & unU.ci & "')"
                ejecutarSQL(consulta)
            Else
                resultado = "No existe CI"
            End If

        End If

        Return resultado
    End Function

    Public Function modificarUsuario(unU As clsUser) As String
        Dim consulta As String
        Dim resultado As String

        consulta = "UPDATE usuarios SET contrasenaUsuario = '" & unU.contrasena & "', tipoUsuario = '" & unU.tipo & "' WHERE Funcionarios_Persona_ciPersona = '" & unU.ci & "';"
        resultado = ejecutarSQL(consulta)

        Return resultado
    End Function

    Public Function bajaUsuario(ci As String) As String
        Dim consulta As String
        Dim resultado As String

        consulta = "DELETE FROM usuarios WHERE Funcionarios_Persona_ciPersona = '" & ci & "';"
        resultado = ejecutarSQL(consulta)

        Return resultado
    End Function

    Public Function buscarUsuarios(ByVal filtro As String) As List(Of clsUser)
        Dim sql As String
        sql = "SELECT usuarios.Funcionarios_Persona_ciPersona, usuarios.tipoUsuario, personas.nombrePersona, personas.apellidoPersona, personas.ciPersona, personas.correoPersona, personas.numeroPersona, personas.direccionPersona FROM personas INNER JOIN usuarios ON personas.ciPersona = usuarios.Funcionarios_Persona_ciPersona WHERE apellidoPersona LIKE '" & filtro & "%';"
        Dim datos As MySqlDataReader
        datos = ejecutarYdevolver(sql)

        Dim colUsuarios As New List(Of clsUser)

        While datos.Read
            Dim unU As New clsUser
            unU = crearUsuarioListar(datos)
            colUsuarios.Add(unU)
        End While

        Return colUsuarios
    End Function
End Class
