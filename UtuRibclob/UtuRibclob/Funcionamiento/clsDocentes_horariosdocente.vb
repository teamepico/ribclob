﻿Public Class clsDocentes_horariosdocente
    Private mDiasemanalHorarioDocente As String
    Private mHorario As New clsHorarioDocente
    Private mDocente As New clsFuncionarioDocente
    Private mSalon As New clsSalon
    Private mGrupo As New clsGrupo
    Private mMateria As New clsMateria

    Property docente() As clsFuncionarioDocente
        Get
            Return mDocente
        End Get
        Set(ByVal value As clsFuncionarioDocente)
            mDocente = value
        End Set
    End Property

    Property salon() As clsSalon
        Get
            Return mSalon
        End Get
        Set(ByVal value As clsSalon)
            mSalon = value
        End Set
    End Property

    Property grupo() As clsGrupo
        Get
            Return mGrupo
        End Get
        Set(ByVal value As clsGrupo)
            mGrupo = value
        End Set
    End Property

    Property materia() As clsMateria
        Get
            Return mMateria
        End Get
        Set(ByVal value As clsMateria)
            mMateria = value
        End Set
    End Property

    Property diaHorario() As String
        Get
            Return mDiasemanalHorarioDocente
        End Get
        Set(ByVal value As String)
            mDiasemanalHorarioDocente = value
        End Set
    End Property

    Property horario() As clsHorarioDocente
        Get
            Return mHorario
        End Get
        Set(ByVal value As clsHorarioDocente)
            mHorario = value
        End Set
    End Property

    Public Sub New()

    End Sub

    Public Sub New(sal As clsSalon, mat As clsMateria, hor As clsHorarioDocente, doc As clsFuncionarioDocente, gru As clsGrupo, diaHor As String)
        horario = hor
        diaHorario = diaHor
        grupo = gru
        materia = mat
        salon = sal
        docente = doc
    End Sub

    Public Function listarHorario(ci As String) As List(Of clsDocentes_horariosdocente)
        Dim unHorario As New clspDocente_has_horario
        Return unHorario.listarHorario(ci)
    End Function

    Public Function altaHorario(unHor As clsDocentes_horariosdocente) As String
        Dim unHorario As New clspDocente_has_horario
        Return unHorario.altaHorario(unHor)
    End Function

    Public Function bajaHorario(horario As clsDocentes_horariosdocente) As String
        Dim unHorario As New clspDocente_has_horario
        Return unHorario.bajaHorario(horario)
    End Function

    Public Function modificarHorario(unHorNuevo As clsDocentes_horariosdocente, unHorViejo As clsDocentes_horariosdocente) As String
        Dim unHorario As New clspDocente_has_horario
        Return unHorario.modificarHorario(unHorNuevo, unHorViejo)
    End Function

End Class
