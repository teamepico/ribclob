﻿Public Class clsSalon

    Private mNombreSalon As String
    Private mUbicacionSalon As String

    Property nombreSalon() As String
        Get
            Return mNombreSalon
        End Get
        Set(ByVal value As String)
            mNombreSalon = value
        End Set
    End Property

    Property ubicacionSalon() As String
        Get
            Return mUbicacionSalon
        End Get
        Set(ByVal value As String)
            mUbicacionSalon = value
        End Set
    End Property

    Public Sub New()

    End Sub

    Public Sub New(ByVal nom As String, ByVal ubicacion As String)
        nombreSalon = nom
        ubicacionSalon = ubicacion
    End Sub

    Public Function listarSalones() As List(Of clsSalon)
        Dim unSal As New clspSalon
        Return unSal.listarSalones()
    End Function

    Public Function altaSalon(unSal As clsSalon) As String
        Dim unSalon As New clspSalon
        Return unSalon.altaSalon(unSal)
    End Function

    Public Function bajaSalones(unS As clsSalon) As String
        Dim unSalon As New clspSalon
        Return unSalon.bajaSalones(unS)
    End Function

    Public Function modificarSalones(unS As clsSalon) As String
        Dim unSalon As New clspSalon
        Return unSalon.modificarSalon(unS)
    End Function

    Public Function buscarSalon(ByVal filtro As String) As List(Of clsSalon)
        Dim unSalon As New clspSalon
        Return unSalon.buscarSalon(filtro)
    End Function
End Class
