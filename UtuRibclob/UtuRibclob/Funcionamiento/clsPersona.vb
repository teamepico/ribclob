﻿Public Class clsPersona
    Private mCiPersona As String
    Private mNombrePersona As String
    Private mApellidoPersona As String
    Private mCorreoPersona As String
    Private mNumeroPersona As String
    Private mDireccionPersona As String

    'Propertys
    Property ci() As String
        Get
            Return mCiPersona
        End Get
        Set(ByVal value As String)
            mCiPersona = value
        End Set
    End Property

    Property nombre() As String
        Get
            Return mNombrePersona
        End Get
        Set(ByVal value As String)
            mNombrePersona = value
        End Set
    End Property

    Property apellido() As String
        Get
            Return mApellidoPersona
        End Get
        Set(ByVal value As String)
            mApellidoPersona = value
        End Set
    End Property

    Property correo() As String
        Get
            Return mCorreoPersona
        End Get
        Set(ByVal value As String)
            mCorreoPersona = value
        End Set
    End Property

    Property numero() As String
        Get
            Return mNumeroPersona
        End Get
        Set(ByVal value As String)
            mNumeroPersona = value
        End Set
    End Property


    Property direccion() As String
        Get
            Return mDireccionPersona
        End Get
        Set(ByVal value As String)
            mDireccionPersona = value
        End Set
    End Property

    'Constructor parametrizado
    Public Sub New(ByVal c As String, ByVal nom As String, ByVal ape As String, ByVal cor As String, ByVal num As String, ByVal dir As String)
        ci = c
        nombre = nom
        apellido = ape
        correo = cor
        numero = num
        direccion = num
        direccion = dir
    End Sub

    Public Sub New()

    End Sub


    'VerificarPersonas
    Public Function verificarPersona(ByVal cCi As String) As Boolean
        Dim unapPersona As New clspPersona
        Return unapPersona.verificarPersona(cCi)

    End Function



End Class
