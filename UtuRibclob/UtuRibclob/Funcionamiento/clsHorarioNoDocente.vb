﻿Public Class clsHorarioNoDocente
    Private mIdHorarioNoDocente As Integer
    Private mEntradaHorarioNoDocente As String
    Private mSalidaHorarioNoDocente As String
    Private mDiaSemanalHorarioNoDocente As String
    Private mFuncionario As New clsFuncionarioNoDocente

    Property idHorarioNoDocente() As Integer
        Get
            Return mIdHorarioNoDocente
        End Get
        Set(ByVal value As Integer)
            mIdHorarioNoDocente = value
        End Set
    End Property

    Property entradaHorarioNoDocente() As String
        Get
            Return mEntradaHorarioNoDocente
        End Get
        Set(ByVal value As String)
            mEntradaHorarioNoDocente = value
        End Set
    End Property

    Property salidaHorarioNoDocente() As String
        Get
            Return mSalidaHorarioNoDocente
        End Get
        Set(ByVal value As String)
            mSalidaHorarioNoDocente = value
        End Set
    End Property

    Property diaSemanalHorarioNoDocente() As String
        Get
            Return mDiaSemanalHorarioNoDocente
        End Get
        Set(ByVal value As String)
            mDiaSemanalHorarioNoDocente = value
        End Set
    End Property

    Property funcionario() As clsFuncionarioNoDocente
        Get
            Return mFuncionario
        End Get
        Set(ByVal value As clsFuncionarioNoDocente)
            mFuncionario = value
        End Set
    End Property

    Public Sub New()



    End Sub


    Public Sub New(ByVal id As Integer, ByVal entrada As String, ByVal salida As String, ByVal dia As String, f As clsFuncionarioNoDocente)
        idHorarioNoDocente = id
        entradaHorarioNoDocente = entrada
        salidaHorarioNoDocente = salida
        diaSemanalHorarioNoDocente = dia
        funcionario = f
    End Sub



    Public Function listarHorarioNoDocente(ci As String) As List(Of clsHorarioNoDocente)
        Dim unpHorario As New clspHorarioNoDocente
        Return unpHorario.listarHorarioNoDocente(ci)

    End Function

    Public Function altaHorarioNoDocente(unH As clsHorarioNoDocente) As String
        Dim unpHorario As New clspHorarioNoDocente
        Return unpHorario.altaHorarioNoDocente(unH)
    End Function

    Public Function bajaHorarioNoDocente(unH As clsHorarioNoDocente) As String
        Dim unpH As New clspHorarioNoDocente
        Return unpH.bajaHorarioNoDocente(unH)
    End Function

    Public Function modificarHorarioNoDocente(unHorarioNuevo As clsHorarioNoDocente, unHorarioViejo As clsHorarioNoDocente) As String
        Dim unpH As New clspHorarioNoDocente
        Return unpH.modificarHorarioNoDocente(unHorarioNuevo, unHorarioViejo)
    End Function
End Class
