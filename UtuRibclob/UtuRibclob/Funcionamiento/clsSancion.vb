﻿Public Class clsSancion
    Private mIdSancion As Integer
    Private mDescripcionSancion As String

    Property id() As String
        Get
            Return mIdSancion
        End Get
        Set(ByVal value As String)
            mIdSancion = value
        End Set
    End Property

    Property descripcion() As String
        Get
            Return mDescripcionSancion
        End Get
        Set(ByVal value As String)
            mDescripcionSancion = value
        End Set
    End Property



    Public Sub New(ByVal i As Integer, Des As String)
        id = i
        descripcion = Des

    End Sub

    Public Sub New()

    End Sub

    Public Function listarSanciones(ByVal ci As String) As List(Of clsSancion)
        Dim unaS As New clspSancion
        Return unaS.listarSanciones(ci)
    End Function

End Class
