﻿Public Class clsFuncionarioDocente
    Inherits clsFuncionario

    Public Sub New(ByVal c As String, ByVal nom As String, ByVal ape As String, ByVal cor As String, ByVal num As String, ByVal dir As String)
        ci = c
        nombre = nom
        apellido = ape
        correo = cor
        numero = num
        direccion = dir
    End Sub

    Public Sub New()

    End Sub

    'alta,baja,modificacion

    Public Function altaDocente(unDoc As clsFuncionarioDocente) As String
        Dim unPdoc As New clspFuncionarioDocente
        Return unPdoc.altaDocente(unDoc)
    End Function

    Public Function listarFuncionariosDocentes() As List(Of clsFuncionarioDocente)
        Dim unFun As New clspFuncionarioDocente
        Return unFun.listarFuncionariosDocentes()
    End Function


    Public Function obtenerDatos(ByVal ci As String) As clsFuncionarioDocente
        Dim unFun As New clspFuncionarioDocente
        Return unFun.obtenerDatos(ci)
    End Function

    Public Function bajaDocente(ByVal ci As String) As String
        Dim unpDocente As New clspFuncionarioDocente
        Return unpDocente.bajaDocente(ci)
    End Function


    Public Function modificarFuncionarioDocente(unFun As clsFuncionarioDocente)
        Dim unDoc As New clspFuncionarioDocente
        Return unDoc.modificarFuncionarioDocente(unFun)
    End Function

    Public Function buscarFuncionarioDocente(ByVal filtro As String) As List(Of clsFuncionarioDocente)
        Dim unFun As New clspFuncionarioDocente
        Return unFun.buscarFuncionarioDocente(filtro)
    End Function

    Public Function checkFuncionarioDocente(ByVal ci As String) As clsFuncionarioDocente
        Dim unFun As New clspFuncionarioDocente
        Return unFun.checkFuncionarioDocente(ci)
    End Function
End Class
