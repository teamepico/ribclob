﻿Public Class clsRegistroInternos
    Private mId As String
    Private mResidencia As String
    Private mFecha As String

    Property id() As String
        Set(value As String)
            mId = value
        End Set
        Get
            Return mId
        End Get
    End Property

    Property residencia() As String
        Set(value As String)
            mResidencia = value
        End Set
        Get
            Return mResidencia
        End Get
    End Property

    Property fecha() As String
        Set(value As String)
            mFecha = value
        End Set
        Get
            Return mFecha
        End Get
    End Property

    Public Sub New(ByVal i As String, ByVal re As String, ByVal fe As String)
        id = i
        residencia = re
        fecha = fe
    End Sub

    Public Sub New()

    End Sub

    Public Function listarRegistro(ci As String, inicial As String, final As String) As List(Of clsRegistroInternos)
        Dim unRegistro As New clspRegistroInternos
        Return unRegistro.listarRegistro(ci, inicial, final)
    End Function

    Public Function altaRegistro(ByVal ci As String, fecha As String) As String
        Dim unRegistro As New clspRegistroInternos
        Return unRegistro.altaRegistro(ci, fecha)
    End Function
End Class
