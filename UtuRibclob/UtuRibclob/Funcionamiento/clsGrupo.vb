﻿Public Class clsGrupo
    Private mNombreGrupo As String
    Private mDescripcionGrupo As String

    Property NombreGrupo() As String
        Get
            Return mNombreGrupo
        End Get
        Set(ByVal value As String)
            mNombreGrupo = value
        End Set
    End Property
    Property DescripcionGrupo() As String
        Get
            Return mDescripcionGrupo
        End Get
        Set(ByVal value As String)
            mDescripcionGrupo = value
        End Set
    End Property
    Public Sub New(ByVal Nombre As String, ByVal Descripcion As String)
        NombreGrupo = Nombre
        DescripcionGrupo = Descripcion
    End Sub

    Public Sub New()



    End Sub
    Public Function listarGrupos() As List(Of clsGrupo)
        Dim unGru As New clspGrupo
        Return unGru.listarGupos()
    End Function

    Public Sub altaGrupos(unGru As clsGrupo)
        Dim unGrupo As New clspGrupo
        unGrupo.altaGrupos(unGru)
    End Sub

    Public Function bajaGrupos(unGru As clsGrupo) As String
        Dim unGrupo As New clspGrupo
        Return unGrupo.bajaGrupos(unGru)
    End Function

    Public Function modificarGrupos(unGru As clsGrupo) As String
        Dim unGrupo As New clspGrupo
        Return unGrupo.modificarGrupos(unGru)
    End Function

    Public Function buscarGrupos(ByVal filtro As String) As List(Of clsGrupo)
        Dim unGrupo As New clspGrupo
        Return unGrupo.buscarGrupo(filtro)
    End Function
End Class

