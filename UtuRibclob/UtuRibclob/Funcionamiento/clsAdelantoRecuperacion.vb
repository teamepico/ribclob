﻿Public Class clsAdelantoRecuperacion
    Private mIdAdelantoRecuperacion As Integer
    Private mFechaAdelantoRecuperacion As String
    Private mHoraInicioAdelantoRecuperacion As String
    Private mHoraFinalAdelantoRecuperacion As String
    Private mDiaAdelantoRecuperacion As String
    Private mHorario As New clsDocentes_horariosdocente
    Private mDocente As New clsFuncionarioDocente

    Property id() As Integer
        Get
            Return mIdAdelantoRecuperacion
        End Get
        Set(ByVal value As Integer)
            mIdAdelantoRecuperacion = value
        End Set
    End Property

    Property fecha() As String
        Get
            Return mFechaAdelantoRecuperacion
        End Get
        Set(ByVal value As String)
            mFechaAdelantoRecuperacion = value
        End Set
    End Property

    Property horaInicio() As String
        Get
            Return mHoraInicioAdelantoRecuperacion
        End Get
        Set(ByVal value As String)
            mHoraInicioAdelantoRecuperacion = value
        End Set
    End Property
    Property horaFinal() As String
        Get
            Return mHoraFinalAdelantoRecuperacion
        End Get
        Set(ByVal value As String)
            mHoraFinalAdelantoRecuperacion = value
        End Set
    End Property

    Property dia() As String
        Get
            Return mDiaAdelantoRecuperacion
        End Get
        Set(ByVal value As String)
            mDiaAdelantoRecuperacion = value
        End Set
    End Property

    Property docente As clsFuncionarioDocente
        Get
            Return mDocente
        End Get
        Set(value As clsFuncionarioDocente)
            mDocente = value
        End Set
    End Property

    Property horario As clsDocentes_horariosdocente
        Get
            Return mHorario
        End Get
        Set(value As clsDocentes_horariosdocente)
            mHorario = value
        End Set
    End Property

    Public Sub New()


    End Sub

    Public Function listarAdelantoRecuperacion(inicio As String, final As String) As List(Of clsAdelantoRecuperacion)
        Dim unAd As New clspAdelantoRecuperacion
        Return unAd.listarAdelantos(inicio, final)
    End Function

    Public Function altaAdelanto(unA As clsAdelantoRecuperacion) As String
        Dim unAd As New clspAdelantoRecuperacion
        Return unAd.altaAdelanto(unA)
    End Function

    Public Function bajaAdelanto(unA As clsAdelantoRecuperacion) As String
        Dim unAd As New clspAdelantoRecuperacion
        Return unAd.bajaAdelantos(unA)
    End Function

    Public Function obtenerDatosAdelanto(unAd As clsAdelantoRecuperacion) As clsAdelantoRecuperacion
        Dim unAdelanto As New clspAdelantoRecuperacion
        Return unAdelanto.obtenerDatosAdelanto(unAd)
    End Function

    Public Function modificarAdelanto(unA As clsAdelantoRecuperacion) As String
        Dim adelanto As New clspAdelantoRecuperacion
        Return adelanto.modificarAdelantos(unA)
    End Function

    Public Sub New(ByVal i As Integer, ByVal f As String, ByVal hi As String, ByVal hf As String, ByVal d As String, doc As clsFuncionarioDocente, h As clsDocentes_horariosdocente)
        id = i
        fecha = f
        horaInicio = hi
        horaFinal = hf
        dia = d
        horario = h
        docente = doc
    End Sub
End Class
