﻿Public Class clsUser
    Inherits clsPersona
    Private mTipo As String
    Private mContrasena As String

    Public Sub New()

    End Sub

    Property tipo() As String
        Get
            Return mTipo
        End Get
        Set(ByVal value As String)
            mTipo = value
        End Set
    End Property

    Property contrasena() As String
        Get
            Return mContrasena
        End Get
        Set(ByVal value As String)
            mContrasena = value
        End Set
    End Property

    Public Sub New(ByVal c As String, ByVal nom As String, ByVal ape As String, ByVal cor As String, ByVal num As String, ByVal dir As String, ByVal tip As String, ByVal contra As String)
        ci = c
        nombre = nom
        apellido = ape
        correo = cor
        numero = num
        direccion = num
        direccion = dir
        tipo = tip
        contrasena = contra

    End Sub

    Public Function login(ByVal user As String, ByVal pass As String) As clsUser
        Dim unUsuario As New clspUser
        Return unUsuario.login(user, pass)
    End Function

    Public Function listarUsuarios() As List(Of clsUser)
        Dim unU As New clspUser
        Return unU.listarUsuarios()
    End Function

    Public Function bajaUsuario(ByVal ci As String)
        Dim unUsuario As New clspUser
        Return unUsuario.bajaUsuario(ci)
    End Function

    Public Function altaUser(ByVal unU As clsUser) As String
        Dim unUsuario As New clspUser
        Return unUsuario.altaUsuario(unU)
    End Function

    Public Function buscarUsuario(ByVal filtro As String) As List(Of clsUser)
        Dim unU As New clspUser
        Return unU.buscarUsuarios(filtro)
    End Function
End Class
