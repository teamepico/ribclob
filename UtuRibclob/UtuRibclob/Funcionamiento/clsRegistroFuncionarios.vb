﻿Public Class clsRegistroFuncionarios
    Private mId As String
    Private mResidencia As String
    Private mFecha As String
    Private mExcepcion As clsExcepcion

    Property id() As String
        Set(value As String)
            mId = value
        End Set
        Get
            Return mId
        End Get
    End Property

    Property residencia() As String
        Set(value As String)
            mResidencia = value
        End Set
        Get
            Return mResidencia
        End Get
    End Property

    Property fecha() As String
        Set(value As String)
            mFecha = value
        End Set
        Get
            Return mFecha
        End Get
    End Property

    Property excepcion() As clsExcepcion
        Get
            Return mExcepcion
        End Get
        Set(value As clsExcepcion)
            mExcepcion = value
        End Set
    End Property

    Public Sub New(ByVal i As String, ByVal re As String, ByVal fe As String, ByVal ex As clsExcepcion)
        id = i
        residencia = re
        fecha = fe
        excepcion = ex
    End Sub

    Public Sub New()

    End Sub

    Public Function altaRegistro(ByVal ci As String, ByVal fecha As String) As String
        Dim unR As New clspRegistroFuncionarios()
        Return unR.altaRegistro(ci, fecha)
    End Function

    Public Function listarRegistro(ByVal ci As String, inicial As String, final As String) As List(Of clsRegistroFuncionarios)
        Dim unR As New clspRegistroFuncionarios
        Return unR.listarRegistro(ci, inicial, final)
    End Function
End Class
