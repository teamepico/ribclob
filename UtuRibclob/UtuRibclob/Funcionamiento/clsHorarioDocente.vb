﻿Public Class clsHorarioDocente
    Private mIdHorarioDocente As Integer
    Private mEntradaHorarioDocente As String
    Private mSalidaHorarioDocente As String

    Property IdHorarioDocente As Integer
        Get
            Return mIdHorarioDocente
        End Get
        Set(value As Integer)
            mIdHorarioDocente = value
        End Set
    End Property

    Property entrada() As String
        Get
            Return mEntradaHorarioDocente
        End Get
        Set(ByVal value As String)
            mEntradaHorarioDocente = value
        End Set
    End Property

    Property salida() As String
        Get
            Return mSalidaHorarioDocente
        End Get
        Set(ByVal value As String)
            mSalidaHorarioDocente = value
        End Set
    End Property


    Public Sub New()



    End Sub


    Public Sub New(ByVal idHorario As Integer, ByVal entradaHorario As String, ByVal salidaHorario As String)
        IdHorarioDocente = idHorario
        entrada = entradaHorario
        salida = salidaHorario

    End Sub


    'altaHorarioDocente
    Public Function bajaHorarioDocente(ByVal unHorario As String) As String
        Dim unH As New clspHorarioDocente
        Return unH.bajaHorario(unHorario)
    End Function

    Public Function altaHorarioDocente(unHorarioD As clsHorarioDocente) As String
        Dim unPhorarioDoc As New clspHorarioDocente
        Return unPhorarioDoc.altaHorarioDocente(unHorarioD)
    End Function

    Public Function listarHorarioDocente() As List(Of clsHorarioDocente)
        Dim unHorario As New clspHorarioDocente
        Return unHorario.listarHorarioDocente()
    End Function

    Public Function modificarHorario(unHor As clsHorarioDocente, ByVal nuevoI As String)
        Dim unHorario As New clspHorarioDocente
        Return unHorario.modificarHorario(unHor, nuevoI)
    End Function

End Class
