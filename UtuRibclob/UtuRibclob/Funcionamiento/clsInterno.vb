﻿Public Class clsInterno
    Inherits clsPersona

    Private mTutor As String
    Private mNumeroTutor As String
    Private mCursoInterno As String


    Property tutor() As String
        Get
            Return mTutor
        End Get
        Set(ByVal value As String)
            mTutor = value
        End Set
    End Property
    Property numeroTutor() As String
        Get
            Return mNumeroTutor
        End Get
        Set(ByVal value As String)
            mNumeroTutor = value
        End Set
    End Property
    Property curso() As String
        Get
            Return mCursoInterno
        End Get
        Set(ByVal value As String)
            mCursoInterno = value
        End Set
    End Property


    Public Sub New(ByVal c As String, ByVal nom As String, ByVal ape As String, ByVal cor As String, ByVal num As String, ByVal dir As String, ByVal tut As String, ByVal numerotut As String, ByVal cursoint As String)
        ci = c
        nombre = nom
        apellido = ape
        correo = cor
        numero = num
        direccion = num
        direccion = dir
        tutor = tut
        numeroTutor = numerotut
        curso = cursoint
    End Sub

    Public Sub New()

    End Sub

    Public Function listarInternos() As List(Of clsInterno)
        Dim unI As New clspInterno
        Return unI.listarInternos()
    End Function

    Public Function altaInterno(ByVal unI As clsInterno) As String
        Dim unInterno As New clspInterno
        Return unInterno.altaInterno(unI)
    End Function

    Public Function modificarInterno(ByVal unI As clsInterno) As String
        Dim unInterno As New clspInterno
        Return unInterno.modificarInterno(unI)
    End Function

    Public Function obtenerDatosInterno(ByVal ci As String) As clsInterno
        Dim unInterno As New clspInterno
        Return unInterno.obtenerDatosInterno(ci)
    End Function

    Public Function bajaInterno(ByVal ci As String) As String
        Dim unInterno As New clspInterno
        Return unInterno.bajaInterno(ci)
    End Function

    Public Function buscarInterno(ByVal filtro As String) As List(Of clsInterno)
        Dim unInterno As New clspInterno
        Return unInterno.buscarInterno(filtro)
    End Function

    Public Function checkInternos(ByVal ci As String) As clsInterno
        Dim unInterno As New clspInterno
        Return unInterno.checkInternos(ci)
    End Function
End Class
