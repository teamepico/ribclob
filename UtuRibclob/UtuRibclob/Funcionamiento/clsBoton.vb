﻿Public Class clsBoton
    Inherits Button

    Private mPresionado As Boolean
    Private mEntrada As String
    Private mSalida As String
    Private mIdHorario
    Private mDiaSemanal As String

    Property presionado As Boolean
        Get
            Return mPresionado
        End Get
        Set(value As Boolean)
            mPresionado = value
        End Set
    End Property

    Property entrada As String
        Get
            Return mEntrada
        End Get
        Set(value As String)
            mEntrada = value
        End Set
    End Property


    Property salida As String
        Get
            Return mSalida
        End Get
        Set(value As String)
            mSalida = value
        End Set
    End Property

    Property idHorario As String
        Get
            Return mIdHorario
        End Get
        Set(value As String)
            mIdHorario = value
        End Set
    End Property

    Property diaSemanal As String
        Get
            Return mDiaSemanal
        End Get
        Set(value As String)
            mDiaSemanal = value
        End Set
    End Property
End Class
