﻿Public Class clsMateria
    Private mNombreMateria As String
    Private mDescripcionMateria As String

    Property NombreMateria() As String
        Get
            Return mNombreMateria
        End Get
        Set(ByVal value As String)
            mNombreMateria = value
        End Set
    End Property

    Property DescripcionMateria() As String
        Get
            Return mDescripcionMateria
        End Get
        Set(ByVal value As String)
            mDescripcionMateria = value
        End Set
    End Property

    Public Sub New()

    End Sub

    Public Sub New(ByVal nombmat As String, ByVal desmat As String)
        NombreMateria = nombmat
        DescripcionMateria = desmat
    End Sub
    Public Sub altaMateria(unMat As clsMateria)
        Dim unM As New clspMateria
        unM.altaMateria(unMat)
    End Sub
    Public Function modificarMateria(unM As clsMateria) As String
        Dim unMat As New clspMateria
        Return unMat.modificarMateria(unM)
    End Function

    Public Function listarMaterias() As List(Of clsMateria)
        Dim unMat As New clspMateria
        Return unMat.listarMaterias()
    End Function

    Public Function bajaMateria(nombre As String)
        Dim unM As New clspMateria
        Return unM.bajaMateria(nombre)
    End Function

    Public Function buscarMateria(filtro As String) As List(Of clsMateria)
        Dim unaM As New clspMateria
        Return unaM.buscarMateria(filtro)
    End Function
End Class
