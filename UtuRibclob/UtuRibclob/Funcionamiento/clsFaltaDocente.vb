﻿Public Class clsFaltaDocente
    Private mIdFaltaDocente As String
    Private mFechaFaltaDocente As String
    Private mDescripcionFaltaDocente As String
    Private mHoraInicio As String
    Private mHoraFinal As String
    Private mExcepcion As New clsExcepcion
    Private mDocente As New clsFuncionarioDocente

    Property id As Integer
        Get
            Return mIdFaltaDocente
        End Get
        Set(value As Integer)
            mIdFaltaDocente = value
        End Set
    End Property

    Property fecha As String
        Get
            Return mFechaFaltaDocente
        End Get
        Set(value As String)
            mFechaFaltaDocente = value
        End Set
    End Property

    Property descripcion As String
        Get
            Return mDescripcionFaltaDocente
        End Get
        Set(value As String)
            mDescripcionFaltaDocente = value
        End Set
    End Property

    Property horarinicio As String
        Get
            Return mHoraInicio
        End Get
        Set(value As String)
            mHoraInicio = value
        End Set
    End Property

    Property horafinal As String
        Get
            Return mHoraFinal
        End Get
        Set(value As String)
            mHoraFinal = value
        End Set
    End Property

    Property docente() As clsFuncionarioDocente
        Get
            Return mDocente
        End Get
        Set(ByVal value As clsFuncionarioDocente)
            mDocente = value
        End Set
    End Property

    Property excepcion() As clsExcepcion
        Get
            Return mExcepcion
        End Get
        Set(value As clsExcepcion)
            mExcepcion = value
        End Set
    End Property

    Public Sub New(nId As Integer, nFechas As String, nDesc As String, nHorainicio As String, nHorafinal As String, doc As clsFuncionarioDocente, ex As clsExcepcion)
        id = nId
        fecha = nFechas
        descripcion = nDesc
        horarinicio = nHorainicio
        horafinal = nHorafinal
        docente = doc
        excepcion = ex
    End Sub

    Public Sub New()

    End Sub

    Public Function registroDiarioDocente(ByVal unaFecha As String) As String
        Dim unaF As New clspFaltaDocente
        Return unaF.registroDiarioDocente(unaFecha)
    End Function

    Public Function listarFaltas(ByVal ci As String, ByVal fechaInicial As String, ByVal fechaFinal As String) As List(Of clsFaltaDocente)
        Dim unaF As New clspFaltaDocente
        Return unaF.listarFaltas(ci, fechaInicial, fechaFinal)
    End Function

    Public Function listarFaltas(ByVal fechaInicial As String, ByVal fechaFinal As String) As List(Of clsFaltaDocente)
        Dim unaF As New clspFaltaDocente
        Return unaF.listarFaltas(fechaInicial, fechaFinal)
    End Function

    Public Function altaFaltasDocentes(unafalta As clsFaltaDocente) As String
        Dim unafal As New clspFaltaDocente
        Return unafal.altaFaltasDocentes(unafalta)
    End Function

    Public Function bajaFaltaDocente(unafalta As clsFaltaDocente)
        Dim unafal As New clspFaltaDocente
        Return unafal.bajaFalta(unafalta)
    End Function
    Public Function modificarFaltasDocentes(unafalta As clsFaltaDocente) As String
        Dim unafal As New clspFaltaDocente
        Return unafal.modificarFaltasDocentes(unafalta)
    End Function
End Class
