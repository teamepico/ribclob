﻿Public Class clsFaltaNoDocente
    Private mId As String
    Private mFecha As String
    Private mHoraInicio As String
    Private mHoraFinal As String
    Private mDescripcion As String
    Private mFuncionario As New clsFuncionarioNoDocente
    Private mExcepcion As New clsExcepcion

    Property id As String
        Get
            Return mId
        End Get
        Set(value As String)
            mId = value
        End Set
    End Property

    Property fecha As String
        Get
            Return mFecha
        End Get
        Set(value As String)
            mFecha = value
        End Set
    End Property

    Property horaInicio As String
        Get
            Return mHoraInicio
        End Get
        Set(value As String)
            mHoraInicio = value
        End Set
    End Property

    Property horaFinal As String
        Get
            Return mHoraFinal
        End Get
        Set(value As String)
            mHoraFinal = value
        End Set
    End Property

    Property descripcion As String
        Get
            Return mDescripcion
        End Get
        Set(value As String)
            mDescripcion = value
        End Set
    End Property

    Property funcionario As clsFuncionarioNoDocente
        Get
            Return mFuncionario
        End Get
        Set(value As clsFuncionarioNoDocente)
            mFuncionario = value
        End Set
    End Property

    Property excepcion As clsExcepcion
        Get
            Return mExcepcion
        End Get
        Set(value As clsExcepcion)
            mExcepcion = value
        End Set
    End Property

    Public Sub New(i As String, fe As String, hI As String, hF As String, des As String, c As clsFuncionarioNoDocente, e As clsExcepcion)
        id = i
        fecha = fe
        horaInicio = hI
        horaFinal = hF
        descripcion = des
        funcionario = c
        excepcion = e
    End Sub

    Public Sub New()

    End Sub

    Public Function bajaFaltaNoDocente(unaFalta As clsFaltaNoDocente) As String
        Dim unaF As New clspFaltaNoDocente
        Return unaF.bajaFaltasNoDocentes(unaFalta)
    End Function

    Public Function registroDiarioFaltasNoDocentes(ByVal unaFecha As String) As String
        Dim unaF As New clspFaltaNoDocente
        Return unaF.registroDiarioNoDocente(unaFecha)
    End Function

    Public Function listarFaltas(ByVal ci As String, ByVal fechaInicial As String, ByVal fechaFinal As String) As List(Of clsFaltaNoDocente)
        Dim unaF As New clspFaltaNoDocente
        Return unaF.listarFaltas(ci, fechaInicial, fechaFinal)
    End Function

    Public Function listarFaltas(ByVal fechaInicial As String, ByVal fechaFinal As String) As List(Of clsFaltaNoDocente)
        Dim unaF As New clspFaltaNoDocente
        Return unaF.listarFaltas(fechaInicial, fechaFinal)
    End Function

    Public Function altaFaltasNoDocentes(unafalta As clsFaltaNoDocente) As String
        Dim unafal As New clspFaltaNoDocente
        Return unafal.altaFaltasNoDocentes(unafalta)
    End Function

    Public Function modificarFaltasNoDocentes(unafalta As clsFaltaNoDocente) As String
        Dim unafal As New clspFaltaNoDocente
        Return unafal.modificarFaltasNoDocentes(unafalta)
    End Function
End Class
