﻿Public Class clsExcepcion

    Private mIdExcepcion As String
    Private mTipoExcepcion As String
    Private mComienzoExcepcion As String
    Private mFinalExcepcion As String

    Property idExcepcion() As String
        Get
            Return mIdExcepcion
        End Get
        Set(ByVal value As String)
            mIdExcepcion = value
        End Set
    End Property

    Property tipoExcepcion() As String
        Get
            Return mTipoExcepcion
        End Get
        Set(ByVal value As String)
            mTipoExcepcion = value
        End Set
    End Property

    Property comienzoExcepcion() As String
        Get
            Return mComienzoExcepcion
        End Get
        Set(ByVal value As String)
            mComienzoExcepcion = value
        End Set
    End Property

    Property finalExcepcion() As String
        Get
            Return mFinalExcepcion
        End Get
        Set(ByVal value As String)
            mFinalExcepcion = value
        End Set
    End Property

    Public Sub New()



    End Sub



    Public Sub New(ByVal id As Integer, ByVal tipo As String, ByVal comienzo As String, ByVal final As String)
        idExcepcion = id
        tipoExcepcion = tipo
        comienzoExcepcion = comienzo
        finalExcepcion = final


    End Sub

    Public Function listarExcepciones() As List(Of clsExcepcion)
        Dim unaE As New clspExcepcion
        Return unaE.listarExcepciones
    End Function

    Public Function altaExcepciones(datos As clsExcepcion) As String
        Dim unaE As New clspExcepcion
        Return unaE.altaExcepcion(datos)
    End Function

    Public Function modificarExcepcion(exNueva As clsExcepcion, exVieja As clsExcepcion) As String
        Dim unaE As New clspExcepcion
        Return unaE.modificarExcepciones(exNueva, exVieja)
    End Function

    Public Function bajaExcepcion(datos As clsExcepcion) As String
        Dim unaE As New clspExcepcion
        Return unaE.bajaExcepciones(datos)
    End Function

    Public Function buscarExcepcion(ByVal filtro As String) As List(Of clsExcepcion)
        Dim unaE As New clspExcepcion
        Return unaE.buscarExcepciones(filtro)
    End Function
End Class
