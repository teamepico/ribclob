﻿Public Class clsControladora
    ' Funcion login

    Public Function login(ByVal user As String, ByVal pass As String) As clsUser
        Dim unUsuario As New clsUser
        Return unUsuario.login(user, pass)
    End Function

    ' Funciones de listado

    Public Function listarAdelantoRecuperacion(ByVal inicio As String, ByVal final As String) As List(Of clsAdelantoRecuperacion)
        Dim unAd As New clsAdelantoRecuperacion
        Return unAd.listarAdelantoRecuperacion(inicio, final)
    End Function

    Public Function listarFaltasNoDocentes(ByVal ci As String, ByVal fechaInicial As String, ByVal fechaFinal As String) As List(Of clsFaltaNoDocente)
        Dim unaF As New clsFaltaNoDocente
        Return unaF.listarFaltas(ci, fechaInicial, fechaFinal)
    End Function

    Public Function listarFaltasNoDocentes(ByVal fechaInicial As String, ByVal fechaFinal As String) As List(Of clsFaltaNoDocente)
        Dim unaF As New clsFaltaNoDocente
        Return unaF.listarFaltas(fechaInicial, fechaFinal)
    End Function

    Public Function listarFaltasDocentes(ByVal fechaInicial As String, ByVal fechaFinal As String) As List(Of clsFaltaDocente)
        Dim unaF As New clsFaltaDocente
        Return unaF.listarFaltas(fechaInicial, fechaFinal)
    End Function

    Public Function listarFaltasDocentes(ByVal ci As String, ByVal fechaInicial As String, ByVal fechaFinal As String) As List(Of clsFaltaDocente)
        Dim unaF As New clsFaltaDocente
        Return unaF.listarFaltas(ci, fechaInicial, fechaFinal)
    End Function

    Public Function listarSanciones(ByVal ci As String) As List(Of clsSancion)
        Dim unaS As New clsSancion
        Return unaS.listarSanciones(ci)
    End Function

    Public Function listarRegistroFuncionarios(ci As String, inicio As String, final As String) As List(Of clsRegistroFuncionarios)
        Dim unR As New clsRegistroFuncionarios
        Return unR.listarRegistro(ci, inicio, final)
    End Function

    Public Function listarExcepciones() As List(Of clsExcepcion)
        Dim unaE As New clsExcepcion
        Return unaE.listarExcepciones()
    End Function

    Public Function listarUsuarios() As List(Of clsUser)
        Dim unU As New clsUser
        Return unU.listarUsuarios()
    End Function

    Public Function listarInternos() As List(Of clsInterno)
        Dim unI As New clsInterno
        Return unI.listarInternos()
    End Function

    Public Function listarFuncionarioNoDocentes() As List(Of clsFuncionarioNoDocente)
        Dim unFun As New clsFuncionarioNoDocente
        Return unFun.listarFuncionariosNoDocentes()
    End Function

    Public Function listarFuncionarioDocentes() As List(Of clsFuncionarioDocente)
        Dim unFun As New clsFuncionarioDocente
        Return unFun.listarFuncionariosDocentes()
    End Function

    Public Function listarHorarioDocente() As List(Of clsHorarioDocente)
        Dim unHorario As New clsHorarioDocente
        Return unHorario.listarHorarioDocente()
    End Function

    Public Function listarSalones() As List(Of clsSalon)
        Dim unSal As New clsSalon
        Return unSal.listarSalones()
    End Function

    Public Function listarRegistroInternos(ci As String, inicial As String, final As String) As List(Of clsRegistroInternos)
        Dim unRegistro As New clsRegistroInternos
        Return unRegistro.listarRegistro(ci, inicial, final)
    End Function

    Public Function listarHorarioNoDocente(ci As String) As List(Of clsHorarioNoDocente)
        Dim unHorario As New clsHorarioNoDocente
        Return unHorario.listarHorarioNoDocente(ci)
    End Function

    Public Function listarHorarioHasDocente(ci As String) As List(Of clsDocentes_horariosdocente)
        Dim unHorario As New clsDocentes_horariosdocente
        Return unHorario.listarHorario(ci)
    End Function

    Public Function listarGrupos() As List(Of clsGrupo)
        Dim unGru As New clsGrupo
        Return unGru.listarGrupos()
    End Function


    Public Function listarMaterias() As List(Of clsMateria)
        Dim unMat As New clsMateria
        Return unMat.listarMaterias()
    End Function


    ' Funciones de alta

    Public Function altaRegistroFuncionarios(ByVal ci As String, ByVal fecha As String) As String
        Dim unR As New clsRegistroFuncionarios
        Return unR.altaRegistro(ci, fecha)
    End Function

    Public Function altaRegistroInternos(ByVal ci As String, ByVal fecha As String) As String
        Dim unR As New clsRegistroInternos
        Return unR.altaRegistro(ci, fecha)
    End Function

    Public Function altaExcepciones(datos As clsExcepcion) As String
        Dim unaE As New clspExcepcion
        Return unaE.altaExcepcion(datos)
    End Function

    Public Function altaUsuario(ByVal unU As clsUser) As String
        Dim unUsuario As New clsUser
        Return unUsuario.altaUser(unU)
    End Function

    Public Function altaAdelanto(ByVal unA As clsAdelantoRecuperacion) As String
        Dim unAd As New clsAdelantoRecuperacion
        Return unAd.altaAdelanto(unA)
    End Function

    Public Function altaFuncionarioNoDocente(ByVal unFun As clsFuncionarioNoDocente) As String
        Dim unFuncionario As New clsFuncionarioNoDocente
        Return unFuncionario.altaFuncionarioNoDocente(unFun)
    End Function

    Public Function altaFuncionarioDocente(ByVal unFun As clsFuncionarioDocente) As String
        Dim unFuncionario As New clsFuncionarioDocente
        Return unFuncionario.altaDocente(unFun)
    End Function

    Public Function altaHorarioDocente(unHorarioD As clsHorarioDocente) As String
        Dim unHorarioDocente As New clsHorarioDocente
        Return unHorarioDocente.altaHorarioDocente(unHorarioD)
    End Function

    Public Function altaInterno(ByVal unI As clsInterno) As String
        Dim unInterno As New clsInterno
        Return unInterno.altaInterno(unI)
    End Function

    Public Sub altaDocente(unDoc As clsFuncionarioDocente)
        Dim unDocente As New clsFuncionarioDocente
        unDocente.altaDocente(unDoc)
    End Sub

    Public Function altaSalon(unSal As clsSalon) As String
        Dim unSalon As New clsSalon
        Return unSalon.altaSalon(unSal)
    End Function

    Public Function altaDocentesHorarioDocentes(unHor As clsDocentes_horariosdocente)
        Dim unHorario As New clsDocentes_horariosdocente
        Return unHorario.altaHorario(unHor)
    End Function

    Public Function altaHorarioNoDocente(unH As clsHorarioNoDocente) As String
        Dim unHorario As New clsHorarioNoDocente
        Return unHorario.altaHorarioNoDocente(unH)
    End Function


    Public Sub altaGrupo(unGru As clsGrupo)
        Dim unGrupo As New clsGrupo
        unGrupo.altaGrupos(unGru)
    End Sub

    Public Sub altaMateria(unMat As clsMateria)
        Dim unM As New clspMateria
        unM.altaMateria(unMat)
    End Sub

    Public Function altaDocenteHasHorario(unHorario As clsDocentes_horariosdocente) As String
        Dim unDocenteHasHorario As New clsDocentes_horariosdocente
        Return unDocenteHasHorario.altaHorario(unHorario)
    End Function

    Public Function altaFaltasDocentes(unafalta As clsFaltaDocente) As String
        Dim unafal As New clsFaltaDocente
        Return unafal.altaFaltasDocentes(unafalta)
    End Function

    Public Function altaFaltasNoDocentes(unafalta As clsFaltaNoDocente) As String
        Dim unafal As New clsFaltaNoDocente
        Return unafal.altaFaltasNoDocentes(unafalta)
    End Function
    ' Funciones de modificar

    Public Function modificarAdelanto(unA As clsAdelantoRecuperacion) As String
        Dim adelanto As New clsAdelantoRecuperacion
        Return adelanto.modificarAdelanto(unA)
    End Function
    Public Function modificarFaltasNoDocentes(unafalta As clsFaltaNoDocente) As String
        Dim unafal As New clsFaltaNoDocente
        Return unafal.modificarFaltasNoDocentes(unafalta)
    End Function
    Public Function modificarFaltasDocentes(unafalta As clsFaltaDocente) As String
        Dim unafal As New clsFaltaDocente
        Return unafal.modificarFaltasDocentes(unafalta)
    End Function

    Public Function modificarHorarioNoDocente(unHorarioNuevo As clsHorarioNoDocente, unHorarioViejo As clsHorarioNoDocente) As String
        Dim unH As New clsHorarioNoDocente
        Return unH.modificarHorarioNoDocente(unHorarioNuevo, unHorarioViejo)
    End Function

    Public Function modificarExcepciones(exNueva As clsExcepcion, exVieja As clsExcepcion) As String
        Dim unaE As New clsExcepcion
        Return unaE.modificarExcepcion(exNueva, exVieja)
    End Function

    Public Sub modificarHorarioDocente(unU As clsHorarioDocente, ByVal nuevoI As String)
        Dim unHor As New clsHorarioDocente
        unHor.modificarHorario(unHor, nuevoI)
    End Sub

    Public Function modificarDocenteHorarioDocente(unHNuevo As clsDocentes_horariosdocente, unHViejo As clsDocentes_horariosdocente) As String
        Dim unHor As New clsDocentes_horariosdocente
        Return unHor.modificarHorario(unHNuevo, unHViejo)
    End Function

    Public Sub modificarUsuario(unU As clsUser)
        Dim unUsuario As New clspUser
        unUsuario.modificarUsuario(unU)
    End Sub

    Public Function modificarInterno(unI As clsInterno) As String
        Dim unInterno As New clsInterno
        Return unInterno.modificarInterno(unI)
    End Function

    Public Function modificarSalon(unS As clsSalon) As String
        Dim unSalon As New clsSalon
        Return unSalon.modificarSalones(unS)
    End Function

    Public Function modificarFuncionarioDocente(unFun As clsFuncionarioDocente) As String
        Dim unFuncionario As New clsFuncionarioDocente
        Return unFuncionario.modificarFuncionarioDocente(unFun)
    End Function

    Public Function modificarFuncionarioNoDocente(unFun As clsFuncionarioNoDocente) As String
        Dim unFuncionario As New clsFuncionarioNoDocente
        Return unFuncionario.modificarNoDocente(unFun)
    End Function

    Public Function modificarGrupo(unGru As clsGrupo) As String
        Dim unGrupo As New clsGrupo
        Return unGrupo.modificarGrupos(unGru)
    End Function

    Public Function modificarMateria(unM As clsMateria) As String
        Dim unMat As New clsMateria
        Return unMat.modificarMateria(unM)
    End Function

    ' Funciones de baja

    Public Function bajaAdelantos(unA As clsAdelantoRecuperacion) As String
        Dim unAd As New clsAdelantoRecuperacion
        Return unAd.bajaAdelanto(unA)
    End Function

    Public Function bajaHorarioNoDocente(ByVal unH As clsHorarioNoDocente) As String
        Dim unHorario As New clsHorarioNoDocente
        Return unHorario.bajaHorarioNoDocente(unH)
    End Function

    Public Function bajaHorarioDocente(ByVal unHorario As String) As String
        Dim unH As New clsHorarioDocente
        Return unH.bajaHorarioDocente(unHorario)
    End Function

    Public Function bajaUsuario(ci As String) As String
        Dim unUsuario As New clsUser
        Return unUsuario.bajaUsuario(ci)
    End Function

    Public Function bajaInterno(ci As String) As String
        Dim unInterno As New clsInterno
        Return unInterno.bajaInterno(ci)
    End Function

    Public Function bajaSalones(unSalon As clsSalon) As String
        Dim unSalonxd As New clsSalon
        Return unSalonxd.bajaSalones(unSalon)
    End Function

    Public Function bajaDocente(ci As String) As String
        Dim unDocente As New clsFuncionarioDocente
        Return unDocente.bajaDocente(ci)
    End Function

    Public Function bajaNoDocente(ci As String) As String
        Dim unNoDocente As New clsFuncionarioNoDocente
        Return unNoDocente.bajaNoDocente(ci)
    End Function

    Public Function bajaGrupo(unGru As clsGrupo) As String
        Dim unGrupo As New clsGrupo
        Return unGrupo.bajaGrupos(unGru)
    End Function

    Public Function bajaDocenteHorarioDocente(horario As clsDocentes_horariosdocente) As String
        Dim unHor As New clsDocentes_horariosdocente
        Return unHor.bajaHorario(horario)
    End Function

    Public Function bajaMateria(nombre As String) As String
        Dim unaMateria As New clsMateria
        Return unaMateria.bajaMateria(nombre)
    End Function

    Public Function bajaExcepcion(datos As clsExcepcion) As String
        Dim unaE As New clsExcepcion
        Return unaE.bajaExcepcion(datos)
    End Function

    Public Function bajaFaltaDocente(datos As clsFaltaDocente) As String
        Dim unaFal As New clsFaltaDocente
        Return unaFal.bajaFaltaDocente(datos)
    End Function

    Public Function bajaFaltaNoDocente(falta As clsFaltaNoDocente) As String
        Dim unaFal As New clsFaltaNoDocente
        Return unaFal.bajaFaltaNoDocente(falta)
    End Function




    'Verificaciones

    Public Function verificarPersona(ByVal cCi As String) As Boolean
        Dim unaP As New clsPersona
        Return unaP.verificarPersona(cCi)

    End Function

    ' Obtener datos completos

    ' Hay algunas tablas que no tienen toda la informacion, entonces estas funciones obtienen la informacion completa desde la BD

    Public Function obtenerDatosInterno(ByVal ci As String) As clsInterno
        Dim unInterno As New clsInterno
        Return unInterno.obtenerDatosInterno(ci)
    End Function

    Public Function obtenerDatosFuncionarioDocente(ByVal ci As String) As clsFuncionarioDocente
        Dim unFuncionario As New clsFuncionarioDocente
        Return unFuncionario.obtenerDatos(ci)
    End Function

    Public Function obtenerDatosAdelanto(ByVal unAd As clsAdelantoRecuperacion) As clsAdelantoRecuperacion
        Dim unAdelanto As New clsAdelantoRecuperacion
        Return unAdelanto.obtenerDatosAdelanto(unAd)
    End Function

    ' Funciones de busqueda

    Public Function buscarInternos(ByVal filtro As String) As List(Of clsInterno)
        Dim unI As New clsInterno
        Return unI.buscarInterno(filtro)
    End Function

    Public Function buscarSalones(ByVal filtro As String) As List(Of clsSalon)
        Dim unS As New clsSalon
        Return unS.buscarSalon(filtro)
    End Function

    Public Function buscarMaterias(ByVal filtro As String) As List(Of clsMateria)
        Dim unaM As New clsMateria
        Return unaM.buscarMateria(filtro)
    End Function

    Public Function buscarGrupos(ByVal filtro As String) As List(Of clsGrupo)
        Dim unG As New clsGrupo
        Return unG.buscarGrupos(filtro)
    End Function

    Public Function buscarFuncionariosDocentes(ByVal filtro As String) As List(Of clsFuncionarioDocente)
        Dim unFun As New clsFuncionarioDocente
        Return unFun.buscarFuncionarioDocente(filtro)
    End Function

    Public Function buscarFuncionariosNoDocentes(ByVal filtro As String) As List(Of clsFuncionarioNoDocente)
        Dim unFun As New clspFuncionarioNoDocente
        Return unFun.buscarFuncionariosNoDocentes(filtro)
    End Function

    Public Function buscarUsuarios(ByVal filtro As String) As List(Of clsUser)
        Dim unU As New clsUser
        Return unU.buscarUsuario(filtro)
    End Function

    Public Function buscarExcepciones(ByVal filtro As String) As List(Of clsExcepcion)
        Dim unaE As New clsExcepcion
        Return unaE.buscarExcepcion(filtro)
    End Function

    ' Funciones de chequeo

    Public Function checkInternos(ByVal ci As String) As clsInterno
        Dim unI As New clsInterno
        Return unI.checkInternos(ci)
    End Function

    Public Function checkFuncionarios(ByVal ci As String) As clsFuncionarioNoDocente
        Dim unF As New clsFuncionarioNoDocente
        Return unF.checkFuncionarioNoDocente(ci)
    End Function

    Public Function checkDocentes(ByVal ci As String) As clsFuncionarioDocente
        Dim unF As New clsFuncionarioDocente
        Return unF.checkFuncionarioDocente(ci)
    End Function

    ' Funciones de reporte

    Public Function registroDiarioDocente(ByVal unaFecha As String) As String
        Dim unaF As New clsFaltaDocente
        Return unaF.registroDiarioDocente(unaFecha)
    End Function

    Public Function registroDiarioNoDocente(ByVal unaFecha As String) As String
        Dim unaF As New clsFaltaNoDocente
        Return unaF.registroDiarioFaltasNoDocentes(unaFecha)
    End Function
End Class
